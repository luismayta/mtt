<?php


class Mtt_Controller_Plugin_CurrencySelector
        extends Zend_Controller_Plugin_Abstract
    {


    public function preDispatch( Zend_Controller_Request_Abstract $request )
        {
        parent::preDispatch( $request );

        $mtt = new Zend_Session_Namespace( 'MTT' );

        $viewRenderer =
                Zend_Controller_Action_HelperBroker::getStaticHelper(
                        'viewRenderer'
        );
        if ( null === $viewRenderer->view )
            {
            $viewRenderer->initView();
            }
        $view = $viewRenderer->view;
        
        if(!isset($mtt->currency->id))
            {
                $mtt->currency->id = 1;
            }
        if(!isset($mtt->currency->code))
            {
                $mtt->currency->code = "CH";
            }
        if(!isset($mtt->currency->codeGoogle))
            {
                $mtt->currency->codeGoogle = "CHF";
            }
          
        $view->assign(
                    'formCurrency' , new Mtt_Form_Currency(
                        array(
                            'data'=>array(
                                'moneda_id' => $mtt->currency->id
                                )
                                )
                        )
            ) ;
        $view->assign( 'currency' , $mtt->currency );

        }


    }