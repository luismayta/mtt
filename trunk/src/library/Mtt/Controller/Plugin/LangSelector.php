<?php


class Mtt_Controller_Plugin_LangSelector
        extends Zend_Controller_Plugin_Abstract
    {


    public function preDispatch( Zend_Controller_Request_Abstract $request )
        {
        parent::preDispatch( $request );

        $mtt = new Zend_Session_Namespace( 'MTT' );

        if ( !isset( $mtt->config->lang ) )
            {
            $zl = new Zend_Locale();
            $mtt->config->lang = $zl->getLanguage();
            }

        if ( $mtt->config->lang !== 'en' && $mtt->config->lang !== 'de' &&
                $mtt->config->lang !== 'es' && $mtt->config->lang !== 'pl' )
            {
            $mtt->config->lang = 'en';
            }

        if ( !isset( $mtt->config->idlang ) )
            {
            $_idioma = new Mtt_Models_Bussines_Idioma();

            $mtt->config->idlang = $_idioma->getByPrefijo(
                            $mtt->config->lang
                    )->id;
            }


        $translate = new Zend_Translate(
                        Zend_Translate::AN_GETTEXT ,
                        APPLICATION_PATH . '/configs/locale/' ,
                        $mtt->config->lang ,
                        array( 'scan' => Zend_Translate::LOCALE_FILENAME ) ,
                        $mtt->config->lang );

        Zend_Registry::set( 'Zend_Translate' , $translate );

        /* translate para Zend_Validate */
        $translator = new Zend_Translate(
                        Zend_Translate::AN_ARRAY ,
                        APPLICATION_PATH . '/configs/resources/languages/' ,
                        $mtt->config->lang ,
                        array( 'scan' => Zend_Translate::LOCALE_DIRECTORY )
        );

        Zend_Validate_Abstract::setDefaultTranslator( $translator );


        /*         * variables para la vista* */

        $viewRenderer =
                Zend_Controller_Action_HelperBroker::getStaticHelper(
                        'viewRenderer'
        );
        if ( null === $viewRenderer->view )
            {
            $viewRenderer->initView();
            }
        $view = $viewRenderer->view;

        $view->assign( 'sessionMtt' , $mtt );

        /*         * variables para la vista* */




        /* translate para Zend_Validate */


        /* formulario idioma */
        /* fixme formulario idiomapais */

//        $data = array( );
//        if ( isset( $mtt->config->lang ) )
//            {
//            if ( isset( $mtt->config->idlang ) )
//                {
//                $data['idioma'] = $mtt->config->idlang;
//                }
//            if ( isset( $mtt->config->idpais ) )
//                {
//                $data['pais'] = $mtt->config->idpais;
//                }
//            }



        /**/
        /* formulario idioma */
        }


    }