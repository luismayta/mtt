<?php


/**
 * This service class is used for image operations within the application. The 
 * main function is to generate square avatars from an abritary sized image.
 * 
 * Use the images.ini file to configure settings
 */
class Mtt_Service_Image
    {

    protected $_configuration = NULL;
    protected $_configurationFilePath = '/configs/images.ini';
    protected $_targetFilename;
    protected $_targetHeight;
    protected $_targetWidth;
    protected $_targetQuality;
    protected $_imageTargetFilepath;
    protected $_tempFilepath;
    protected $_element;


    public function get_configuration()
        {
        return $this->_configuration;


        }


    public function set_configuration( $_configuration )
        {
        $this->_configuration = $_configuration;


        }


    public function get_configurationFilePath()
        {
        return $this->_configurationFilePath;


        }


    public function set_configurationFilePath( $_configurationFilePath )
        {
        $this->_configurationFilePath = $_configurationFilePath;


        }


    public function get_targetFilename()
        {
        return $this->_targetFilename;


        }


    public function set_targetFilename( $_targetFilename )
        {
        $this->_targetFilename = $_targetFilename;


        }


    public function get_targetHeight()
        {
        return $this->_targetHeight;


        }


    public function set_targetHeight( $_targetHeight )
        {
        $this->_targetHeight = $_targetHeight;


        }


    public function get_targetWidth()
        {
        return $this->_targetWidth;


        }


    public function set_targetWidth( $_targetWidth )
        {
        $this->_targetWidth = $_targetWidth;


        }


    public function get_targetQuality()
        {
        return $this->_targetQuality;


        }


    public function set_targetQuality( $_targetQuality )
        {
        $this->_targetQuality = $_targetQuality;


        }


    public function get_imageTargetFilepath()
        {
        return $this->_imageTargetFilepath;


        }


    public function set_imageTargetFilepath( $_imageTargetFilepath )
        {
        $this->_imageTargetFilepath = $_imageTargetFilepath;


        }


    public function get_tempFilepath()
        {
        return $this->_tempFilepath;


        }


    public function set_tempFilepath( $_tempFilepath )
        {
        $this->_tempFilepath = $_tempFilepath;


        }


    public function get_element()
        {
        return $this->_element;


        }


    public function set_element( $_element )
        {
        $this->_element = $_element;


        }


    public function __construct( array $config )
        {
        $this->_element = $config['element']
        ;


        }


    /**
     * Initiates the process of generating an avatar
     *
     * @param String $originalFilename
     * @param String $targetFilename 
     */
    public function processImageAvata( $originalFilename , $targetFilename )
        {
        $this->_setupConfiguration( 'avatar' );
        $this->_targetFilename = $targetFilename;
        $this->_originalFilename = $originalFilename;

        $this->_resizeImage();


        }


    /**
     * Initiates the process of generating an avatar
     *
     * @param String $originalFilename
     * @param String $targetFilename 
     */
    public function processImageProduct( $originalFilename , $targetFilename )
        {
        $this->_setupConfiguration( 'product' );
        $this->_targetFilename = $targetFilename;
        $this->_originalFilename = $originalFilename;
        $this->_resizeImage();


        }


    /**
     * Initiates the process of generating an avatar
     *
     * @param String $originalFilename
     * @param String $targetFilename 
     */
    public function processImageEquipo( $originalFilename , $targetFilename )
        {
        $this->_setupConfiguration( 'equipo' );
        $this->_targetFilename = $targetFilename;
        $this->_originalFilename = $originalFilename;

        $this->_resizeImage();


        }


    /**
     * Initiates the process of generating an avatar
     *
     * @param String $originalFilename
     * @param String $targetFilename 
     */
    public function processImageThumb( $originalFilename , $targetFilename )
        {
        $this->_setupConfiguration( 'thumb' );
        $this->_targetFilename = $targetFilename;
        $this->_originalFilename = $originalFilename;

        $this->_resizeImage();


        }


    /**
     * Setup the target size and quality of the final image.
     *
     * @param String $type : The type of image been processed (e.g. avatar or logo). The settings should be 
     * 						 available in images.ini
     */
    protected function _setupConfiguration( $type )
        {
        $this->_configuration = new Zend_Config_Ini(
                        APPLICATION_PATH . $this->_configurationFilePath ,
                        APPLICATION_ENV
        );

        $element = $this->_element;

        $this->_targetHeight = $this->_configuration->$element->$type->height;
        $this->_targetWidth = $this->_configuration->$element->$type->width;
        $this->_targetQuality = $this->_configuration->$element->$type->quality;
        $this->_imageTargetFilepath = $this->_configuration->$element->$type->path;
        $this->_tempFilepath = $this->_configuration->path->uploadTemp;


        }


    /**
     * Resizes the image
     */
    protected function _resizeImage()
        {
        if ( !isset( $this->_configuration ) )
            {
            throw new Zend_Exception( 'image configuration has not been setup' );
            }

        switch ( $this->getExtension( $this->_originalFilename ) )
            {
            case '.jpg':
                $image = imagecreatefromjpeg(
                        $this->_originalFilename
                );

                break;
            case '.png':

                $image = imagecreatefrompng(
                        $this->_originalFilename
                );

                break;
            case '.gif':

                $image = imagecreatefromgif(
                        $this->_originalFilename
                );

                break;

            default:
                break;
            }


        $imageWidth = imagesx( $image );
        $imageHeight = imagesy( $image );

        if ( $imageHeight > $imageWidth )
            {
            $cropHeight = $imageWidth;
            $cropWidth = $imageWidth;
            $cropX = 0;
            $cropY = ($imageHeight - $cropHeight) / 2;
            }

        if ( $imageHeight < $imageWidth )
            {
            $cropHeight = $imageHeight;
            $cropWidth = $imageHeight;
            $cropX = ($imageWidth - $cropWidth) / 2;
            $cropY = 0;
            }

        if ( $imageHeight == $imageWidth )
            {
            $cropHeight = $imageHeight;
            $cropWidth = $imageWidth;
            $cropX = 0;
            $cropY = 0;
            }

        $imagePlaceHolder = imagecreatetruecolor( $this->_targetWidth ,
                                                  $this->_targetHeight );
        
        imagecopyresampled( $imagePlaceHolder , $image , 0 , 0 ,
                            ( int ) $cropX , ( int ) $cropY ,
                            $this->_targetWidth , $this->_targetHeight ,
                            $cropWidth , $cropHeight );

        switch ( $this->getExtension( $this->_originalFilename ) )
            {
            case '.jpg':
                imagejpeg( $imagePlaceHolder ,
                   $this->_imageTargetFilepath . $this->_targetFilename ,
                   $this->_targetQuality );

                break;
            case '.png':

                 imagepng( $imagePlaceHolder ,
                   $this->_imageTargetFilepath . $this->_targetFilename ,
                   $this->_targetQuality );

                break;
            case '.gif':

                imagegif( $imagePlaceHolder ,
                   $this->_imageTargetFilepath . $this->_targetFilename ,
                   $this->_targetQuality );

                break;

            default:
                break;
            }
        


        }


    /**
     *
     * @param String $filename 
     */
    public function deleteImage( $filename )
        {
        //@TODO unlink files to be deleted here


        }


    public function getExtension( $filename )
        {
        $x = explode( '.' , $filename );
        return '.' . end( $x );


        }


    }