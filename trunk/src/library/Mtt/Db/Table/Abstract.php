<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


abstract class Mtt_Db_Table_Abstract
        extends Zend_Db_Table_Abstract
    {

    const ACTIVE = 1;
    const DESACTIVATE = 0;


    protected $sessionMtt;
    //protected $objtranslate;


    public function __construct( $config = array( ) )
        {
        $this->sessionMtt = new Zend_Session_Namespace( 'MTT' );
        //$this->objtranslate = new Mtt_Models_Bussines_Traducciones();
        parent::__construct( $config );
        }


    public function getFindId( $id )
        {
        return $this->fetchRow(
                        $this->_primary . ' = ' . $id
        );
        }


    }