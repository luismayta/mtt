<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Mtt_Form_Idioma
        extends Mtt_Formy
    {

    protected $nombre;
    protected $prefijo;
    protected $submit;
    protected $_id;


    public function __construct( $options = null , $id = null )
        {
        if ( !is_null( $id ) )
            {
            $this->_id = $id;
            }
        parent::__construct( $options );
        }


    private function _setValidator()
        {

        if ( is_null( $this->_id ) )
            {
            $this->nombre
                    ->addValidator( new Zend_Validate_Db_NoRecordExists(
                                    array(
                                        'table' => 'idiomas' ,
                                        'field' => 'nombre'
                                    )
                    ) )
            ;
            }
        else
            {
            $this->nombre
                    ->addValidator( new Zend_Validate_Db_NoRecordExists(
                                    'idiomas' , 'nombre' , array(
                                'field' => 'id' , 'value' => $this->_id
                                    )
                    ) )
            ;
            }
        }


    public function init()
        {
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' , 'frmIdioma' )
        ;

        $this->nombre = new Zend_Form_Element_Text( 'nombre' );
        $this->nombre->setLabel(
                        $this->_translate->translate( 'idioma' )
                )
                ->setRequired();

        $this->_setValidator();

        $this->addElement( $this->nombre );


        $this->prefijo = new Zend_Form_Element_Text( 'prefijo' );
        $this->prefijo->setLabel(
                        $this->_translate->translate( 'prefijo' )
                )
                ->setRequired()
        ;

        $this->addElement( $this->prefijo );




        $this->submit = new Zend_Form_Element_Button( 'submit' );
        $this->submit->setLabel(
                $this->_translate->translate( 'guardar' ) );
        $this->submit->setAttrib( 'type' , 'submit' )

        ;
        $this->addElement( $this->submit );
        }


    }

