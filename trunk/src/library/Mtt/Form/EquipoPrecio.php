<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_EquipoPrecio
        extends Mtt_Form
    {

        
    protected $id;
    protected $precioventa;
    protected $submit;
   

    public function init()
        {
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' , 'frmEquipoPrecio' )
        ;
        
        
        
        // Elemento: NroCuotas
        $this->precioventa = new Zend_Form_Element_Text( 'precioventa' );
        $this->precioventa->setLabel( 
                $this->_translate->translate( 'precio de venta' ) 
        );
        $this->precioventa->setAttrib( 'maxlength' , '50' );
        $this->precioventa->setRequired( true );
        $this->precioventa->addValidator( new Zend_Validate_Between(
                        array(
                            'min' => 1 , 'max' => 9999999999
                        )
        ) );
      
       
        $this->submit = new Zend_Form_Element_Button( 'submit' );
        $this->submit->setLabel(
                        $this->_translate->translate('save')
                )
                ->setAttrib(
                        'class' , 'button'
                )
                ->setAttrib( 'type' , 'submit' )
        ;

        //$this->addElement( $submit );
        
        
        $this->addElements(
                array(
                    $this->precioventa ,
                    $this->submit
                )
        );
        }


    }

