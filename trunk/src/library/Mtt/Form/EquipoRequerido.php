<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_EquipoRequerido
        extends Mtt_Form
    {

    protected $_id;
    protected $nombre;
    protected $modelo;
    protected $categoria;
    protected $estado;
    protected $fechaFabricacion;
    protected $fabricante;
    protected $comentario;
    protected $submit;
    protected $captcha;
    protected $_categoria;
    protected $_estadoEquipo;


    public function __construct( $options = null )
        {

        $this->_categoria = new Mtt_Models_Bussines_Categoria();
        $this->_estadoEquipo = new Mtt_Models_Bussines_EstadoEquipo();

        $this->nombre = new Zend_Form_Element_Text( 'nombre' );
        $this->modelo = new Zend_Form_Element_Text( 'modelo' );
        $this->categoria = new Zend_Form_Element_Select( 'categoria_id' );
        $this->estado = new Zend_Form_Element_Select( 'estado_id' );

        /*$this->fechaFabricacion = new ZendX_JQuery_Form_Element_DatePicker(
                        'fechafabricacion' ,
                        array(
                            'jQueryParams' => array(
                                'defaultDate' => date( 'Y-m-d' ) ,
                                'minDate' => '1980-01-01' ,
                                'maxDate' => date( 'Y-m-d' ) ,
                                'showButtonPanel' => 'true'
                            )
                        )
        );*/
        $this->fechaFabricacion = new Zend_Form_Element_Text( 'fechafabricacion' );
        $this->fabricante = new Zend_Form_Element_Text( 'fabricante' );
        $this->comentario = new Zend_Form_Element_Textarea( 'comentario' );
        $this->submit = new Zend_Form_Element_Button( 'submit' );

        parent::__construct( $options );
        }


    public function setId( $id=null )
        {
        $this->_id = $id;
        }


    private function _addCaptcha()
        {
//        $this->captcha = new Zend_Form_Element_Captcha( 'captcha' ,
//                        array(
//                            'captcha' => 'ReCaptcha' ,
//                            'captchaOptions' => array(
//                                'captcha' => 'ReCaptcha'
//                                , 'service' => $this->_recaptcha
//                            ) ,
//                            'ignore' => true
//                        )
//        );

        $this->captcha = new Zend_Form_Element_Captcha(
                        'captcha' ,
                        array(
                            'captcha' => $this->_captcha
                        )
        );

        $this->captcha->setLabel(
                $this->_translate->translate(
                        'escribe los caracteres que ves en la imagen siguiente.'
                )
        );
        }


    /**
     *
     */
    protected function _setGroup()
        {
        $this->addDisplayGroup( array( 'nombre' , 'modelo' ) ,
                'groupidiomas' , array( "legend" => "Paises" ) );
        }


    private function _addContentElement()
        {
        $this->addElement( $this->nombre );
        $this->addElement( $this->modelo );
        $this->addElement( $this->categoria );
        $this->addElement( $this->estado );
        $this->addElement( $this->fechaFabricacion );
        $this->addElement( $this->fabricante );
        $this->addElement( $this->comentario );
        $this->addElement( $this->submit );
        }


    private function _setLabel()
        {
        $this->nombre->setLabel( $this->_translate->translate( 'nombre' ) );
        $this->categoria->setLabel( $this->_translate->translate( 'categoria' ) );
        $this->fabricante->setLabel( $this->_translate->translate( 'fabricante' ) );
        $this->estado->setLabel( $this->_translate->translate( 'estado' ) );
        $this->fechaFabricacion->setLabel( $this->_translate->translate( 'fecha de fabricacion' ) );
        $this->modelo->setLabel( $this->_translate->translate(
                        'modelo'
                )
        );
        $this->comentario->setLabel( $this->_translate->translate( 'comentario' ) );
        //$this->submit->setLabel( $this->_translate->translate('nombre') ) ;
        }


    private function _setRequired()
        {

        }


    private function _addValidator()
        {

//		if ( is_null( $this->_id ) )
//		{
//			$this->nombre->addValidator( new Zend_Validate_Db_NoRecordExists(
//							array(
//								'table' => 'equiporequerido' ,
//								'field' => 'nombre' ,
//							)
//					)
//			) ;
//		}
//		else
//		{
//			$this->nombre
//					->addValidator( new Zend_Validate_Db_NoRecordExists(
//									'equiporequerido' , 'nombre' , array(
//								'field' => 'id' , 'value' => $this->_id
//									)
//							) )
//			;
//		}
        }


    private function _setAttrib()
        {

        //$this->fechaFabricacion->setJQueryParam( 'dateFormat' , 'yy-mm-dd' );

        $this->comentario->setAttrib( 'COLS' , '40' )
                ->setAttrib( 'ROWS' , '4' )
        ;

        $this->submit->setAttrib( 'value' ,
                        $this->_translate->translate( 'enviar' )
                )
                ->setAttrib( 'class' , 'button' )
                ->setAttrib( 'type' , 'submit' )
        ;
        }


    private function _addValuesSelect()
        {
        $dataCategoria = $this->_categoria->getComboValues();
        $this->categoria->addMultiOption( -1 ,
                $this->_translate->translate(
                        'categoria'
                ) );
        $this->categoria->addMultiOptions( $dataCategoria );
        $this->categoria->addValidator(
                new Zend_Validate_InArray(
                        array_keys( $dataCategoria )
                )
        );


        $dataEstado = $this->_estadoEquipo->getComboValues();
        $this->estado->addMultiOption( -1 ,
                $this->_translate->translate(
                        'estado'
                ) );
        $this->estado->addMultiOptions( $dataEstado );
        $this->estado->addValidator(
                new Zend_Validate_InArray(
                        array_keys( $dataEstado )
                )
        );
        }


    public function init()
        {
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' , 'frmEquipoRequerido' )
                ->setAttrib( 'class' , 'zend_form' );

        $this->_setLabel();
        $this->_addValuesSelect();
        $this->_setAttrib();
        $this->_setRequired();
        $this->_addValidator();
        $this->_addContentElement();
        //$this->_setGroup();
        }


    }

