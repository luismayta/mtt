<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_Pais extends Mtt_Formy
{

    protected $nombre ;
    protected $code ;
    protected $submit ;
    protected $_id ;


    public function setId( $id=null )
    {
        $this->_id = $id ;


    }


    public function __construct( $options = null )
    {

        parent::__construct( $options ) ;


    }


    private function _setValidator()
    {

        if ( is_null( $this->_id ) )
        {
            $this->nombre->addValidator(
                    new Zend_Validate_Db_NoRecordExists(
                            array(
                                'table' => 'paises' ,
                                'field' => 'nombre' ,
                            )
                    )
            ) ;
        }
        else
        {
            $this->nombre
                    ->addValidator( new Zend_Validate_Db_NoRecordExists(
                                    'paises' , 'nombre' , array(
                                'field' => 'id' , 'value' => $this->_id
                                    )
                            ) )
            ;
        }


    }


    public function init()
    {
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' , 'frmPais' )
        ;

        //$decorator = new Mtt_Form_Decorator_SimpleInput();

        $this->nombre = new Zend_Form_Element_Text( 'nombre' ) ;
        $this->nombre->setLabel(
                $this->_translate->translate( 'nombre' ) . '*:'
        ) ;
        $this->nombre->setRequired() ;
        $this->_setValidator() ;

        $this->nombre->addValidator(
                new Zend_Validate_StringLength(
                        array(
                            'min' => 2 ,
                            'max' => 50
                        )
                )
        ) ;
        $this->nombre->addValidator( new Zend_Validate_Alnum( true ) ) ;


        $this->code = new Zend_Form_Element_Text( 'code' ) ;
        $this->code->setLabel(
                $this->_translate->translate( 'code' )
                . ':'
        ) ;

        $this->submit = new Zend_Form_Element_Button( 'submit' ) ;
        $this->submit->setLabel(
                        $this->_translate->translate( 'save' )
                )
                ->setAttrib(
                        'class' , 'button'
                )
                ->setAttrib( 'type' , 'submit' ) ;

        //$this->addElement( $this->submit );

        $this->addElements(
                array(
                    $this->nombre ,
                    $this->code ,
                    $this->submit
                ) ) ;


    }


}

