<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_ContactarAdmin
        extends Mtt_Form
{


    public function init()
    {
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' ,
                             'frmContactarAdmin' )
                ->setAttrib( 'class' ,
                             'zend_form' )
        ;


        $e = new Zend_Form_Element_Text( 'nombre' ) ;
        $e->setRequired() ;
        $e->setLabel(
                $this->_translate->translate( 'name' ) . ':'
        ) ;
        $e->addValidator( new Zend_Validate_StringLength(
                        array(
                            'min' => 1
                            ,
                            'max' => 50 )
                )
        ) ;
        $this->addElement( $e ) ;


        /* email */
        $email = new Zend_Form_Element_Text( 'email' ) ;
        $email->setRequired() ;

        $email->setLabel(
                $this->_translate->translate( 'email' ) . ':'
        ) ;
        $email->addValidator( new Zend_Validate_EmailAddress() ) ;
        $this->addElement( $email ) ;
        /* end email */

        $asunto = new Zend_Form_Element_Text( 'asunto' ) ;
        $asunto->setRequired() ;
        //$e->setDecorators( array( $decorator ) );
        $asunto->setLabel(
                $this->_translate->translate( 'asunto' ) . ':'
        ) ;
        $asunto->addValidator( new Zend_Validate_StringLength(
                        array(
                            'min' => 1
                            ,
                            'max' => 255 )
                )
        ) ;
        $this->addElement( $asunto ) ;


        $e = new Zend_Form_Element_Textarea( 'comentario' ) ;
        $e->setRequired() ;
        $e->setLabel(
                        $this->_translate->translate( 'comentario' )
                        . ':'
                )->setAttrib( 'cols' ,
                              '40' )
                ->setAttrib( 'rows' ,
                             '10' )
        ;
        $this->addElement( $e ) ;

        $submit = new Zend_Form_Element_Button( 'submit' ) ;
        $submit->setLabel(
                        $this->_translate->translate( 'enviar' )
                )
                ->setAttrib(
                        'class' ,
                        'button'
                )
                ->setAttrib( 'type' ,
                             'submit' ) ;

        $this->addElement( $submit ) ;


    }


}

