<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_Contactar
        extends Mtt_Form
{

    protected $nombre ;
    protected $direccion ;
    protected $email ;
    protected $codPostal ;
    protected $ciudad ;
    protected $paises_id ;
    protected $telefono ;
    protected $comentario ;
    protected $submit ;
    protected $captcha ;


    public function __construct()
    {
        $this->nombre = new Zend_Form_Element_Text( 'nombre' ) ;
        $this->direccion = new Zend_Form_Element_Text( 'direccion' ) ;
        $this->email = new Zend_Form_Element_Text( 'email' ) ;
        $this->codPostal = new Zend_Form_Element_Text( 'codpostal' ) ;
        $this->ciudad = new Zend_Form_Element_Text( 'ciudad' ) ;
        $this->paises_id = new Zend_Form_Element_Select( 'paises_id' ) ;
        $this->telefono = new Zend_Form_Element_Text( 'telefono' ) ;
        $this->comentario = new Zend_Form_Element_Textarea( 'comentario' ) ;
        $this->submit = new Zend_Form_Element_Button( 'submit' ) ;

        parent::__construct() ;


    }


    private function _addCaptcha()
    {

//        $this->captcha = new Zend_Form_Element_Captcha(
//                        'captcha' ,
//                        array(
//                            'captcha' => 'ReCaptcha' ,
//                            'captchaOptions' => array(
//                                'captcha' => 'ReCaptcha' ,
//                                'service' => $this->_recaptcha
//                            )
//                        )
//        );

        $this->captcha = new Zend_Form_Element_Captcha(
                        'captcha' ,
                        array(
                            'captcha' => $this->_captcha
                        )
                ) ;

        $this->captcha->setLabel(
                $this->_translate->translate(
                        'escribe los caracteres que ves en la imagen siguiente.'
                )
        ) ;


    }


    private function _addContentElement()
    {
        $this->addElement( $this->nombre ) ;
        $this->addElement( $this->direccion ) ;
        $this->addElement( $this->email ) ;
        $this->addElement( $this->codPostal ) ;
        $this->addElement( $this->ciudad ) ;
        $this->addElement( $this->paises_id ) ;
        $this->addElement( $this->telefono ) ;
        $this->addElement( $this->comentario ) ;

        $this->addElement(
                $this->captcha
        ) ;

        $this->addElement( $this->submit ) ;


    }


    public function init()
    {
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' ,
                             'frmContactar' )
                ->setAttrib( 'class' ,
                             'zend_form' )
        ;


        $this->nombre->setRequired() ;
        $this->nombre->setLabel(
                        $this->_translate->translate( 'name' ) . ':'
                )
                ->setAttrib( 'class' ,
                             'select-extens' ) ;
        $this->nombre->addValidator( new Zend_Validate_StringLength(
                        array(
                            'min' => 1
                            ,
                            'max' => 25 )
                )
        ) ;



        $this->direccion->setRequired() ;
        $this->direccion->setLabel(
                        $this->_translate->translate( 'address' ) . ':'
                )
                ->setAttrib( 'class' ,
                             'select-extens' ) ;
        $this->direccion->addValidator(
                new Zend_Validate_StringLength(
                        array(
                            'min' => 1 , 'max' => 25 )
                )
        ) ;

        /* email */

        $this->email->setRequired() ;

        $this->email->setLabel(
                $this->_translate->translate( 'email' ) . ':'
        )->setAttrib( 'class' ,
                      'select-extens' ) ;
        $this->email->addValidator( new Zend_Validate_EmailAddress() ) ;

        /* end email */


        $this->codPostal->setRequired() ;
        //$e->setDecorators( array( $decorator ) );
        $this->codPostal->setLabel(
                $this->_translate->translate( 'codigo postal' ) . ':'
        )->setAttrib( 'class' ,
                      'select-extens' ) ;




        $this->ciudad->setRequired() ;
        //$e->setDecorators( array( $decorator ) );
        $this->ciudad->setLabel(
                $this->_translate->translate( 'ciudad' ) . ':'
        )->setAttrib( 'class' ,
                      'select-extens' ) ;





        $this->paises_id->setLabel(
                $this->_translate->translate( 'pais' ) . ':'
        ) ;
        $this->paises_id->setRequired() ;
        $this->paises_id->setAttrib( 'class' ,
                                     'select-extens' ) ;
        $_pais = new Mtt_Models_Bussines_Paises() ;
        $values = $_pais->getComboValues() ;
        $this->paises_id->addMultiOption( -1 ,
                                          $this->_translate->translate( 'pais' ) . ':'
        ) ;
        $this->paises_id->addMultiOptions( $values ) ;

        $this->paises_id->addValidator( new Zend_Validate_InArray( array_keys( $values ) ) ) ;




        $this->telefono->setRequired() ;
        //$e->setDecorators( array( $decorator ) );
        $this->telefono->setLabel(
                $this->_translate->translate( 'telefono' ) . ':'
        )->setAttrib( 'class' ,
                      'select-extens' ) ;
        $this->telefono->addValidators( array(
            array(
                'validator' => 'Regex' ,
                'breakChainOnFailure' => true ,
                'options' => array(
                    'pattern' => '/^[+]?[-\d() .]*$/i' ,
                    'messages' => array(
                        Zend_Validate_Regex::NOT_MATCH =>
                        $this->_translate->translate(
                                'ingrese un numero telefonico correcto'
                        )
                    )
                )
            ) ,
            array(
                'validator' => 'StringLength' ,
                'breakChainOnFailure' => true ,
                'options' => array(
                    'min' => 1
                )
            )
        ) ) ;



        $this->comentario->setRequired()
                ->setLabel(
                        $this->_translate->translate( 'comentario' ) . ':'
                )->setAttrib( 'rows' ,
                              10 )
                ->setAttrib( 'cols' ,
                             50 )
                ->setAttrib( 'class' ,
                             'select-extens' ) ;




        $this->submit->setAttrib( 'value' ,
                                  ucwords(
                                $this->_translate->translate( 'enviar' )
                        )
                )
                ->setAttrib( 'class' ,
                             'button' )
                ->setAttrib( 'type' ,
                             'submit' )
                ->setLabel(
                        $this->_translate->translate( 'enviar' )
                )
        ;

        $this->_addCaptcha() ;
        $this->_addContentElement() ;


    }


}

