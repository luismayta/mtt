<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_Cms
        extends Mtt_Form
{

    protected $nombre ;
    protected $body ;
    protected $idiomas_id ;
    protected $paises_id ;
    protected $submit ;


    public function __construct( $options = null )
    {
        $this->nombre = new Zend_Form_Element_Text( 'nombre' ) ;
        $this->paises_id = new Zend_Form_Element_Select( 'paises_id' ) ;
        $this->idiomas_id = new Zend_Form_Element_Select( 'idiomas_id' ) ;
        $this->body = new Zend_Form_Element_Textarea( 'body' ) ;
        $this->submit = new Zend_Form_Element_Button( 'submit' ) ;

        parent::__construct( $options ) ;


    }


    public function _setGroup()
    {
        $this->addDisplayGroup( array( 'nombre' , 'idiomas_id' ) ,
                                'groupidiomas' ,
                                array( "legend" => "Paises" ) ) ;
        $this->addDisplayGroup( array( 'paises_id' ) ,
                                'groups' ,
                                array( "legend" => "Paises" ) ) ;
        $this->addDisplayGroup( array( 'submit' ) ,
                                'botones' ,
                                array( "legend" => "Acciones" ) ) ;


    }


    public function init()
    {
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' ,
                             'frmCms' )
                ->setAttrib( 'class' ,
                             'zend_form' ) ;

        //Nombre

        $this->nombre->setRequired() ;
        //$e->setDecorators( array( $decorator ) );
        $this->nombre->setLabel(
                $this->_translate->translate(
                        'nombre'
                )
        ) ;
        $this->nombre->addValidator(
                new Zend_Validate_StringLength(
                        array(
                            'min' => 5
                            ,
                            'max' => 25 )
                )
        ) ;
        $this->addElement(
                $this->nombre
        ) ;


        /* Pais */

        $this->paises_id->setLabel(
                $this->_translate->translate(
                        'pais'
                )
        ) ;
        $_pais = new Mtt_Models_Bussines_Paises() ;
        $values = $_pais->getComboValues() ;
        $this->paises_id->addMultiOption( -1 ,
                                          $this->_translate->translate(
                        'paises'
                )
        ) ;
        $this->paises_id->addMultiOptions( $values ) ;
        $this->addElement( $this->paises_id ) ;
        $this->paises_id->addValidator(
                new Zend_Validate_InArray(
                        array_keys( $values
                        )
                )
        ) ;


        /* idioma */

        $this->idiomas_id->setLabel(
                $this->_translate->translate(
                        'idioma'
                )
        ) ;
        $_idioma = new Mtt_Models_Bussines_Idioma() ;
        $values = $_idioma->getComboValues() ;
        $this->idiomas_id->addMultiOption( -1 ,
                                           $this->_translate->translate(
                        'idiomas'
                )
        ) ;
        $this->idiomas_id->addMultiOptions( $values ) ;
        $this->addElement( $this->idiomas_id ) ;
        $this->idiomas_id->addValidator(
                new Zend_Validate_InArray(
                        array_keys(
                                $values
                        )
                )
        ) ;


        $this->body->setRequired() ;
        $this->body->setLabel(
                $this->_translate->translate(
                        'cuerpo'
                ) ) ;
        $this->body->setAttrib( 'COLS' ,
                                '40' ) ;
        $this->body->setAttrib( 'ROWS' ,
                                '4' ) ;

        $this->addElement( $this->body ) ;

        $this->submit->setLabel(
                        $this->_translate->translate( 'enviar' )
                )
                ->setAttrib( 'class' ,
                             'button' )
                ->setAttrib( 'type' ,
                             'submit' )
        ;
        $this->addElement( $this->submit ) ;

        $this->_setGroup() ;


    }


}

