<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_ContentCountry
        extends Mtt_Form
{

    protected $_page ;
    protected $submit ;
    protected $_data ;


    public function addMemberForms()
    {

        $pages = array(
            array( 'id' => '1' , 'nombre' => 'welcome' ) ,
            array( 'id' => '4' , 'nombre' => 'about us' )
        ) ;

        foreach ( $pages as $page )
        {

            $this->_data['page_id'] = $page['id'] ;
            $this->_data['titulo'] = $page['nombre'] ;

            $key = "member_{$page['id']}" ;
            $subform = new Mtt_Form_SubForm_ContentCountry( $this->_data ) ;
            $this->addSubForm( $subform ,
                               $key ) ;
        }


    }


    public function __construct( $data )
    {

        if ( !is_null( $data ) )
        {
            $this->_data = $data ;
        }


        $this->submit = new Zend_Form_Element_Button( 'submit' ) ;
        parent::__construct() ;


    }


    public function init()
    {
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' ,
                             'frmCms' )
                ->setAttrib( 'class' ,
                             'zend_form' ) ;

        $this->submit->setLabel(
                        $this->_translate->translate( 'enviar' )
                )
                ->setAttrib( 'class' ,
                             'button' )
                ->setAttrib( 'type' ,
                             'submit' )
        ;
        $this->addElement( $this->submit ) ;
        $this->addMemberForms() ;


    }


}

