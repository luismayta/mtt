<?php

class Mtt_Form_Example
        extends Mtt_Form
    {
    
    protected $nombre;
    protected $title;
    protected $thumbnail;
    protected $descripcion;
    protected $submit;
    
 


    public function init()
        {
        

        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' , 'frmCategoria' )
                ->setAttrib( 'enctype' , 'multipart/form-data' )
        ;
        

        $this->nombre = new Mtt_Form_Tabular_Element_Text( 'nombre' );
        $this->nombre->setLabel( 
                 $this->_translate->translate( 'nombre' ) . '*:' 
        );
        $this->nombre->setRequired();
        $this->nombre->addValidator(
                new Zend_Validate_StringLength(
                        array( 'min' => 2 ,
                            'max' => 50 ) )
        );
        //$nombre->addValidator( new Zend_Validate_Alnum( true ) );
        $this->nombre->addValidator(
                new Zend_Validate_Db_NoRecordExists(
                        array(
                            'table' => 'categoria' ,
                            'field' => 'nombre'
                            
                        )
                )
        );

        $this->title = new Mtt_Form_Tabular_Element_Text( 'title' );

        $this->title->setLabel(
                 $this->_translate->translate( 'title' ) . ':'
        );

        $this->descripcion = new Mtt_Form_Tabular_Element_Tinymce(
                        'descripcion'
        );
        $this->descripcion->setLabel( 
                $this->_translate->translate( 'descripcion' ) . '*:' 
        );

        
        
        //Submit
        $this->submit = new Mtt_Form_Tabular_Element_Button( 'submit' );
        $this->submit->setAttrib( 'value' ,
                             $this->_translate->translate( 'registrar' ) 
        )
                ->setAttrib( 'class' , 'button' )
                ->setAttrib( 'type' , 'submit' )
        ;
      
        $this->addElements(array(
            $this->nombre,
            $this->title,
            $this->thumbnail,
            $this->descripcion,
            $this->submit
            )
        );
        }

}
