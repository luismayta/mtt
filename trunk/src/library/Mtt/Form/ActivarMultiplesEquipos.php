<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_ActivarMultiplesEquipos
        extends Mtt_Form
{

    protected $id ;
    protected $precioVenta ;
    protected $submit ;


    public function init()
    {

        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' ,
                             'frmActivarMultiplesEquipos' )
                ->setAttrib( 'enctype' ,
                             'multipart/form-data' )
        ;
        $this->addElementPrefixPath(
                'Mtt_Form_Decorator' ,
                'Mtt/Form/Decorator/' ,
                'decorator'
        ) ;

        $this->id = new Zend_Form_Element_Checkbox( 'id' ) ;
        $this->addElement( $this->id ) ;

        $this->precioVenta = new Zend_Form_Element_Text( 'precioventa' ) ;
        $this->precioVenta->setLabel(
                $this->_translate->translate( 'precio de venta' )
        ) ;

        $this->precioVenta->setRequired( true ) ;
        $this->precioVenta->addValidator(
                new Zend_Validate_Between( array(
                    'min' => 0.1 ,
                    'max' => 9999999999
                        )
                )
        ) ;
        $v = new Zend_Validate_Float( new Zend_Locale( 'US' ) ) ;
        $this->precioVenta->addValidator( $v ) ;
        $this->precioVenta->setAttrib( 'maxlength' ,
                                       '10' ) ;

        $this->addElement( $this->precioVenta ) ;

        /* $equipos = new Zend_Form_Element_MultiCheckbox( 'equipos' );
          $_equipos = new Mtt_Models_Bussines_Equipo();

          foreach ( $_equipos->listEquipUnresolved() as $equipo )
          {
          $equipos->addMultiOption( $equipo->id ,
          $equipo->equipo );
          }

          $this->addElement( $equipos );
         */

        //Submit
        $this->submit = new Zend_Form_Element_Button( 'submit' ) ;
        $this->submit->setAttrib(
                        'value' ,
                        $this->_translate->translate( 'Habilitar'
                        )
                )
                ->setAttrib( 'class' ,
                             'button' )
                ->setAttrib( 'type' ,
                             'submit' )
        ;
        $this->addElement( $this->submit ) ;


    }


}
