<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_Currency
		extends Mtt_Form
{

	protected $moneda ;
	protected $_moneda ;
	protected $_data ;


	public function __construct( $options = null )
	{

		$this->moneda = new Zend_Form_Element_Select( 'moneda' ) ;
		$this->_moneda = new Mtt_Models_Bussines_Moneda() ;

		parent::__construct( $options ) ;


	}


	private function _addContentElement()
	{

		$this->addElement( $this->moneda ) ;


	}


	public function setData( $data=null )
	{
		$this->_data = $data ;


	}


	private function _setDataValue()
	{
		if ( isset( $this->_data['moneda_id'] ) )
		{
			$this->moneda->setValue( $this->_data['moneda_id'] ) ;
		}


	}


	public function init()
	{
		$this
				->setMethod( 'post' )
				->setAction( '/default/moneda/index' )
				->setAttrib( 'id' , 'frmMoneda' )
		;


		$valuesCombo = $this->_moneda->getComboValuesCurrency() ;

		$this->moneda->
				addMultiOption( -1 , $this->_translate->translate( 'moneda' )
				)
				->addMultiOptions(
						$valuesCombo
				)
				->addValidator(
						new Zend_Validate_InArray(
								array_keys(
										$valuesCombo
								)
						)
				)
				->setAttrib( 'style' , 'width:150px' ) ;
		$this->_setDataValue() ;
		$this->_addContentElement() ;


	}


}

