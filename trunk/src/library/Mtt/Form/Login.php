<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_Login
        extends Mtt_Form
    {

    protected $login;
    protected $clave;
    protected $remember;
    protected $submit;
    protected $captcha;


    public function __construct( $options = null )
        {

        $this->login = new Zend_Form_Element_Text( 'login' );
        $this->clave = new Zend_Form_Element_Password( 'clave' );
        $this->remember = new Zend_Form_Element_Checkbox( 'remember' );
        $this->submit = new Zend_Form_Element_Button( 'submit' );
        parent::__construct( $options );
        }


    private function _addCaptcha()
        {

//        $this->captcha = new Zend_Form_Element_Captcha(
//                        'captcha' ,
//                        array(
//                            'captcha' => 'ReCaptcha' ,
//                            'captchaOptions' => array(
//                                'captcha' => 'ReCaptcha' ,
//                                'service' => $this->_recaptcha
//                            )
//                        )
//        );
        
        $this->captcha = new Zend_Form_Element_Captcha(
                        'captcha' ,
                        array(
                            'captcha' => $this->_captcha
                        )
        );
        $this->captcha->setLabel(
                $this->_translate->translate(
                        'escribe los caracteres que ves en la imagen siguiente.'
                )
        );
        }


    private function _addContentElement()
        {
        $this->addElement( $this->login );
        $this->addElement( $this->clave );
        $this->addElement( $this->remember );
        $this->addElement( $this->captcha );
        $this->addElement( $this->submit );
        }


    public function init()
        {
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' , 'frmLogin' )
                ->setAttrib( 'class' , 'zend_form' );


        $this->login->setLabel(
                $this->_translate->translate(
                        'username'
                )
        );
        $this->login->setRequired();
        $this->login->addValidator(
                new Zend_Validate_StringLength(
                        array( 'min' => 3 , 'max' => 20 )
                )
        );
       

        $this->clave->setRequired();
        $this->clave->setLabel(
                $this->_translate->translate(
                        'password'
                )
        );
        //
        
        

        $this->remember->setLabel(
                $this->_translate->translate( 'Remember me' )
        );

        $this->submit->setLabel(
                        $this->_translate->translate( 'login' )
                )
                ->setAttrib(
                        'class' , 'button'
                )
                ->setAttrib( 'type' , 'submit' )
        ;

        $this->_addCaptcha();
        $this->_addContentElement();
        }


    }
