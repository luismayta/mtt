<?php


class Mtt_Form_Equipo
        extends Mtt_Form
    {

    protected $nombre;
    protected $sinonimo;
    protected $descuento;
    protected $precioVenta;
    protected $precioCompra;
    protected $categoria;
    protected $estadoEquipo;
    protected $fabricantes;
    protected $publicacionEquipo;
    protected $tag;
    protected $descripcion;
    protected $moneda;
    protected $monedaventa;
    protected $pais;
    protected $calidad;
    protected $cantidad;
    protected $modelo;
    protected $fechaFabricacion;
    protected $documento;
    protected $sourceDocumento;
    protected $pesoEstimado;
    protected $size;
    protected $ancho;
    protected $alto;
    protected $sizeCaja;
    protected $nombreOferente;
    protected $articuloId;
    protected $ciudadOferente;
    protected $codigoPostalOferente;
    protected $addCart;
    protected $_categoria;
    protected $_estadoEquipo;
    protected $_publicacionEquipo;
    protected $_fabricantes;
    protected $_moneda;
    protected $_pais;
    protected $valueCategoria;
    protected $valueEstadoEquipo;
    protected $valuePublicacionEquipo;
    protected $valueFabricantes;
    protected $valueMoneda;
    protected $valuePais;
    protected $conf;


    public function __construct()
        {
        $this->sinonimo = new Zend_Form_Element_Text( 'sinonimo' );
        $this->descuento = new Zend_Form_Element_Text( 'descuento' );
        $this->categoria = new Zend_Form_Element_Select( 'categoria_id' );
        $this->nombreOferente = new Zend_Form_Element_Text( 'nombreOferente' );
        $this->articuloId = new Zend_Form_Element_Text( 'articulo_id' );
        $this->ciudadOferente = new Zend_Form_Element_Text( 'ciudadOferente' );
        $this->codigoPostalOferente = new Zend_Form_Element_Text( 'codigoPostalOferente' );
        $this->nombre = new Zend_Form_Element_Text( 'nombre' );
        $this->precioVenta = new Zend_Form_Element_Text( 'precioventa' );
        $this->precioCompra = new Zend_Form_Element_Text( 'preciocompra' );
        $this->estadoEquipo = new Zend_Form_Element_Select( 'estadoequipo_id' );
        $this->publicacionEquipo = new Zend_Form_Element_Select( 'publicacionEquipo_id' );
        $this->fabricantes = new Zend_Form_Element_Select( 'fabricantes_id' );
        $this->descripcion = new Zend_Form_Element_Textarea( 'descripcion' );
        $this->documento = new Zend_Form_Element_Text( 'documento' );
        $this->size = new Zend_Form_Element_Text( 'size' );
        $this->moneda = new Zend_Form_Element_Select( 'moneda_id' );
        $this->pais = new Zend_Form_Element_Select( 'paises_id' );
        $this->fechaFabricacion = new Zend_Form_Element_Select( 'fechafabricacion' );
        $this->sourceDocumento = new Zend_Form_Element_File( 'sourceDocumento' );
        $this->monedaventa = new Zend_Form_Element_Select( 'monedaventa_id' );
        $this->calidad = new Zend_Form_Element_Text( 'calidad' );
        $this->cantidad = new Zend_Form_Element_Text( 'cantidad' );
        $this->modelo = new Zend_Form_Element_Text( 'modelo' );
        $this->ancho = new Zend_Form_Element_Text( 'ancho' );
        $this->alto = new Zend_Form_Element_Text( 'alto' );
        $this->sizeCaja = new Zend_Form_Element_Text( 'sizeCaja' );
        $this->pesoEstimado = new Zend_Form_Element_Text( 'pesoEstimado' );
        $this->addCart = new Zend_Form_Element_Checkbox( 'addCart' );
        $this->submit = new Zend_Form_Element_Button( 'submit' );


        $this->_publicacionEquipo = new Mtt_Models_Bussines_PublicacionEquipo;
        $this->_categoria = new Mtt_Models_Bussines_Categoria();
        $this->_estadoEquipo = new Mtt_Models_Bussines_EstadoEquipo();
        $this->_fabricantes = new Mtt_Models_Bussines_Fabricante();
        $this->_moneda = new Mtt_Models_Bussines_Moneda();
        $this->_pais = new Mtt_Models_Bussines_Paises();

        $this->conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/config.ini' , APPLICATION_ENV
                )
        ;

        parent::__construct();


        }


    private function _setValue()
        {
        $this->sourceDocumento->setValue( '' );

        $this->valueCategoria = $this->_categoria->getComboValues();
        $this->categoria->addMultiOption(
                -1 ,
                '--- ' . $this->_translate->translate( 'categoria' ) . '---'
        );
        $this->categoria->addMultiOptions( $this->valueCategoria );


        $this->valueEstadoEquipo = $this->_estadoEquipo->getComboValues();
        $this->estadoEquipo->addMultiOption( -1 ,
                                             '--- ' .
                $this->_translate->translate( 'estado equipo' ) . ' ---' );
        $this->estadoEquipo->addMultiOptions( $this->valueEstadoEquipo );

        $this->valuePublicacionEquipo = $this->_publicacionEquipo->getComboValues();
        $this->publicacionEquipo->addMultiOption(
                -1 ,
                '--- ' . $this->_translate->translate( 'publicacion equipo' ) .
                ' ---'
        );
        $this->publicacionEquipo->addMultiOptions( $this->valuePublicacionEquipo );


        /* fecha de fabricacion */
        $this->fechaFabricacion->addMultiOption( NULL , '' );
        $_years = range( 1980 , 2012 );
        $years = array( );
        foreach ( $_years as $year )
            {
            $years[$year] = $year;
            }
        $this->fechaFabricacion->addMultiOptions( $years );
        /* fecha de fabricacion */

        $this->valueFabricantes = $this->_fabricantes->getComboValues();
        $this->fabricantes->addMultiOption(
                -1 ,
                '--- ' . $this->_translate->translate( 'fabricantes' ) . ' ---'
        );
        $this->fabricantes->addMultiOptions( $this->valueFabricantes );

        $this->valueMoneda = $this->_moneda->getComboValues();


        $this->moneda->addMultiOption( -1 ,
                                       '--- ' . $this->_translate->translate( 'monedas' ) . '  ---'
        );
        $this->moneda->addMultiOptions( $this->valueMoneda );


        $this->monedaventa->addMultiOption( -1 ,
                                            '--- ' . $this->_translate->translate( 'selling currency' ) . '  ---'
        );
        $this->monedaventa->addMultiOptions( $this->valueMoneda );


        $this->valuePais = $this->_pais->getComboValues();
        $this->pais->addMultiOption( -1 ,
                                     '--- ' . $this->_translate->translate( 'pais' ) . ' ---' );
        $this->pais->addMultiOptions( $this->valuePais );


        }


    private function _setRequireds()
        {
        $this->categoria->setRequired();
        $this->nombre->setRequired();
        $this->precioCompra->setRequired();
        $this->estadoEquipo->setRequired();
        $this->publicacionEquipo->setRequired();
        $this->fabricantes->setRequired();
        $this->descripcion->setRequired();
        $this->moneda->setRequired();
        $this->pais->setRequired();
        $this->calidad->setRequired();
        $this->cantidad->setRequired();
        $this->modelo->setRequired();


        }


    private function _setAttribs()
        {
        $this->nombreOferente->setAttrib( 'maxlength' , '200' );
        $this->descripcion->setAttrib( 'maxlength' , '80' );
        $this->descripcion->setAttrib( 'rows' , 10 )
                ->setAttrib( 'cols' , 50 )
        ;
        $this->documento->setAttrib( 'maxlength' , '200' );
        $this->calidad->setAttrib( 'maxlength' , '50' );
        $this->cantidad->setAttrib( 'maxlength' , '7' );
        $this->modelo->setAttrib( 'maxlength' , '50' );
        $this->size->setAttrib( 'maxlength' , '7' );
        $this->ancho->setAttrib( 'maxlength' , '7' );
        $this->alto->setAttrib( 'maxlength' , '7' );
        $this->sizeCaja->setAttrib( 'maxlength' , '7' );
        $this->pesoEstimado->setAttrib( 'maxlength' , '7' );

        $this->submit->setAttrib(
                        'class' , 'button'
                )
                ->setAttrib( 'type' , 'submit' );


        }


    private function _setOptions()
        {
        $this->sourceDocumento->setDestination(
                APPLICATION_PUBLIC . $this->conf->documento->path
        );


        }


    private function _setLabels()
        {
        $this->categoria->setLabel(
                $this->_translate->translate( 'categoria' )
        );
        $this->nombreOferente->setLabel(
                $this->_translate->translate( 'nombre del oferente' )
        );
        $this->articuloId->setLabel(
                $this->_translate->translate( 'articulo id' )
        );
        $this->ciudadOferente->setLabel(
                $this->_translate->translate( 'ciudad del oferente' )
        );
        $this->codigoPostalOferente->setLabel(
                $this->_translate->translate( 'codigo postal del oferente' )
        );
        $this->fabricantes->setLabel(
                $this->_translate->translate( 'fabricantes' )
        );

        $this->nombre->setLabel(
                $this->_translate->translate( 'nombre' )
        );
        $this->sinonimo->setLabel(
                $this->_translate->translate( 'sinonimo' )
        );
        $this->pais->setLabel(
                $this->_translate->translate( 'pais' )
        );
        $this->monedaventa->setLabel(
                $this->_translate->translate( 'selling currency' )
        );
        $this->precioVenta->setLabel(
                $this->_translate->translate( 'precio de venta' )
        );
        $this->descuento->setLabel(
                $this->_translate->translate( 'descuento' )
        );
        $this->moneda->setLabel(
                $this->_translate->translate( 'moneda' )
        );

        $this->precioCompra->setLabel(
                $this->_translate->translate( 'precio de compra' )
        );
        $this->calidad->setLabel(
                $this->_translate->translate( 'calidad' )
        );
        $this->modelo->setLabel( $this->_translate->translate( 'modelo' )
        );
        $this->cantidad->setLabel(
                $this->_translate->translate( 'cantidad ' )
        );
        $this->fechaFabricacion->setLabel(
                $this->_translate->translate( 'fecha de fabricacion' )
        );
        $this->descripcion->setLabel(
                $this->_translate->translate( 'descripcion' )
        );
        $this->documento->setLabel(
                $this->_translate->translate( 'documento' )
        );
        $this->sourceDocumento->setLabel(
                $this->_translate->translate( 'seleccionar archivo' ) . ':'
        );
        $this->pesoEstimado->setLabel(
                $this->_translate->translate( 'peso estimado ' )
        );
        $this->size->setLabel(
                $this->_translate->translate( 'tamaño ' )
        );
        $this->ancho->setLabel(
                $this->_translate->translate( 'ancho' )
        );
        $this->alto->setLabel(
                $this->_translate->translate( 'alto' )
        );
        $this->sizeCaja->setLabel(
                $this->_translate->translate( 'tamaño de la caja' )
        );
        $this->publicacionEquipo->setLabel(
                $this->_translate->translate( 'publicacion de equipo' )
        );
        $this->estadoEquipo->setLabel(
                $this->_translate->translate( 'estado del equipo' )
        );

        $this->addCart->setLabel(
                $this->_translate->translate(
                        'agregar al carro'
                )
        );
        $this->submit->setLabel(
                $this->_translate->translate( 'registrar' )
        );


        }


    private function _setValidators()
        {
        $v = new Zend_Validate_StringLength(
                        array( 'min' => 5 , 'max' => 50 )
        );
        $v->setMessage(
                $this->_translate->translate( "El nombre del Equipo debe
                    tener debe tener al menos
                    %min% characters. ' % value % ' no cumple ese requisito" ) ,
                                              Zend_Validate_StringLength::TOO_SHORT
        );
        $this->nombre->addValidator( $v );



        $v = new Zend_Validate_StringLength(
                        array( 'min' => 1 , 'max' => 45 )
        );
        $v->setMessage(
                $this->_translate->translate(
                        "debe tener al menos
            %min% characters. '%value%' no cumple ese requisito" ) ,
                        Zend_Validate_StringLength::TOO_SHORT
        );
        $this->precioCompra->addValidator( $v );


        $this->categoria->addValidator(
                new Zend_Validate_InArray( array_keys( $this->valueCategoria ) )
        );


        $this->estadoEquipo->addValidator(
                new Zend_Validate_InArray( array_keys( $this->valueEstadoEquipo ) )
        );


        $this->publicacionEquipo->addValidator(
                new Zend_Validate_InArray( array_keys( $this->valuePublicacionEquipo ) )
        );


        $this->fabricantes->addValidator(
                new Zend_Validate_InArray( array_keys( $this->valueFabricantes ) )
        );



        $this->sourceDocumento->addValidator( 'Count' , false , 1 );
        $this->sourceDocumento->addValidator( 'Size' , false ,
                                              $this->conf->documento->upload->maxsize )
                ->setValueDisabled( true );
        $this->sourceDocumento->addValidator(
                'Extension' , false , $this->conf->documento->extension
        );



        $this->moneda->addValidator(
                new Zend_Validate_InArray( array_keys( $this->valueMoneda ) )
        );



        $this->monedaventa->addValidator(
                new Zend_Validate_InArray( array_keys( $this->valueMoneda ) )
        );




        $this->pais->addValidator(
                new Zend_Validate_InArray( array_keys( $this->valuePais ) )
        );



        $v = new Zend_Validate_StringLength(
                        array( 'min' => 2 , 'max' => 50 )
        );
        $v->setMessage(
                $this->_translate->translate(
                        "La Calidad del producto debe tener debe tener al menos
            %min% characters. '%value%' no cumple ese requisito" ) ,
                        Zend_Validate_StringLength::TOO_SHORT
        );
        $this->calidad->addValidator( $v );

        $this->cantidad->addValidator( new Zend_Validate_Between( array( 'min' => 1 , 'max' => 9999 ) ) );


        }


    protected function _addContentElement()
        {

        $this->addElement( $this->categoria );
        $this->addElement( $this->nombreOferente );
        $this->addElement( $this->articuloId );
        $this->addElement( $this->ciudadOferente );
        $this->addElement( $this->codigoPostalOferente );
        $this->addElement( $this->fabricantes );
        $this->addElement( $this->nombre );
        $this->addElement( $this->sinonimo );
        $this->addElement( $this->pais );
        $this->addElement( $this->monedaventa );
        $this->addElement( $this->precioVenta );
        $this->addElement( $this->descuento );
        $this->addElement( $this->moneda );
        $this->addElement( $this->precioCompra );
        $this->addElement( $this->calidad );
        $this->addElement( $this->modelo );
        $this->addElement( $this->cantidad );
        $this->addElement( $this->fechaFabricacion );
        $this->addElement( $this->descripcion );
        $this->addElement( $this->documento );
        $this->addElement( $this->sourceDocumento );
        $this->addElement( $this->pesoEstimado );
        $this->addElement( $this->size );
        $this->addElement( $this->ancho );
        $this->addElement( $this->alto );
        $this->addElement( $this->sizeCaja );
        $this->addElement( $this->publicacionEquipo );
        $this->addElement( $this->estadoEquipo );
        $this->addElement( $this->addCart );
        $this->addElement( $this->submit );


        }


    public function init()
        {
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' , 'frmEquipo' )
                ->setAttrib( 'class' , 'zend_form' )
                ->setAttrib( 'enctype' , 'multipart/form-data' );
        ;

        $this->_setLabels();
        $this->_setAttribs();
        $this->_setRequireds();
        $this->_setOptions();
        $this->_setValue();
        $this->_setValidators();
        $this->_addContentElement();

        parent::init();


        }


    }

