<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_Registrar
        extends Mtt_Form
    {

    protected $tratamiento;
    protected $nombre;
    protected $apellido;
    protected $email;
    protected $login;
    protected $clave;
    protected $clave2;
    protected $direccion;
    protected $codpostal;
    protected $ciudad;
    protected $paises_id;
    protected $institucion;
    protected $submit;
    protected $_id;
    protected $captcha;


    private function _addCaptcha()
        {

//        $this->captcha = new Zend_Form_Element_Captcha(
//                        'captcha' ,
//                        array(
//                            'captcha' => 'ReCaptcha' ,
//                            'captchaOptions' => array(
//                                'captcha' => 'ReCaptcha' ,
//                                'service' => $this->_recaptcha
//                            )
//                        )
//        );

        $this->captcha = new Zend_Form_Element_Captcha(
                        'captcha' ,
                        array(
                            'captcha' => $this->_captcha
                        )
        );
        $this->captcha->setLabel(
                $this->_translate->translate(
                        'escribe los caracteres que ves en la imagen siguiente.'
                )
        );
        }


    public function setId( $id=null )
        {
        $this->_id = $id;
        }


    public function __construct( $options = null )
        {

        parent::__construct( $options );
        }


    private function _setValidator()
        {

        if ( is_null( $this->_id ) )
            {
            $this->email->addValidator(
                    new Zend_Validate_Db_NoRecordExists(
                            array(
                                'table' => 'usuario' ,
                                'field' => 'email' )
                    )
            );

            $this->login->addValidator( new Zend_Validate_Db_NoRecordExists( array(
                        'table' => 'usuario' ,
                        'field' => 'login' ,
                            ) ) );
            }
        else
            {
            $this->email
                    ->addValidator( new Zend_Validate_Db_NoRecordExists(
                                    'usuario' , 'email' , array(
                                'field' => 'id' , 'value' => $this->_id
                                    )
                    ) )
            ;
            $this->login
                    ->addValidator( new Zend_Validate_Db_NoRecordExists(
                                    'usuario' , 'login' , array(
                                'field' => 'id' , 'value' => $this->_id
                                    )
                    ) )
            ;
            }
        }


    private function _addElements()
        {
        $this->addElement( $this->tratamiento );
        $this->addElement( $this->nombre );
        $this->addElement( $this->apellido );
        $this->addElement( $this->email );
        $this->addElement( $this->login );
        $this->addElement( $this->clave );
        $this->addElement( $this->clave2 );
        $this->addElement( $this->direccion );
        $this->addElement( $this->codpostal );
        $this->addElement( $this->ciudad );
        $this->addElement( $this->paises_id );
        $this->addElement( $this->institucion );
        $this->addElement( $this->captcha );
        $this->addElement( $this->submit );
        }


    public function init()
        {
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' , 'frmRegistrar' )
                ->setAttrib( 'class' , 'zend_form' )
        ;

        $this->tratamiento = new Zend_Form_Element_Select( 'tratamiento' );
        $this->tratamiento->addMultiOption( 1 
                , $this->_translate->translate('Sr') 
        );
        $this->tratamiento->addMultiOption( 0 
                , $this->_translate->translate('Sra/Srta') 
        );
        $this->tratamiento->setLabel(
                ' .'
        );
        $decorator = new Mtt_Form_Decorator_SimpleInput();
        $this->nombre = new Zend_Form_Element_Text( 'nombre' );
        $this->nombre->setRequired();
        $this->nombre->setLabel(
                $this->_translate->translate( 'nombre' )
                . ':'
        );
        /*$this->nombre->addValidator(
                new Zend_Validate_StringLength(
                        array(
                            'min' => 1
                            ,
                            'max' => 200 )
                )
        );*/


        $this->apellido = new Zend_Form_Element_Text( 'apellido' );
        $this->apellido->setRequired();
        $this->apellido->setLabel(
                $this->_translate->translate( 'apellido' )
                . ':' );
        $this->apellido->addValidator(
                new Zend_Validate_StringLength(
                        array(
                            'min' => 1 , 'max' => 25 )
                )
        );

        /* email */
        $this->email = new Zend_Form_Element_Text( 'email' );

        $this->email->setRequired();
        $this->email->setLabel( 'Email:' );
        $this->email->addValidator(
                new Zend_Validate_EmailAddress()
        );

        /* end email */
        $this->login = new Zend_Form_Element_Text( 'login' );
        $this->login->setRequired();
        $this->login->setLabel( 'Login:' );

        $this->login->addValidator( new Zend_Validate_Alnum() );

        $this->login->addValidator( new Zend_Validate_StringLength(
                        array( 'min' => 3 , 'max' => 25 ) ) );


        $this->clave = new Zend_Form_Element_Password( 'clave' );
        $this->clave->setRequired();
        $this->clave->setLabel( $this->_translate->translate( 'password' ) . ':' );


        $this->clave2 = new Zend_Form_Element_Password( 'clave_2' );
        $this->clave2->setRequired();
        $this->clave2->setLabel( $this->_translate->translate( 'confirmacion password' ) . ':' );
        $this->clave2->addValidator( new Mtt_Validate_PasswordConfirmation() );



        $this->direccion = new Zend_Form_Element_Text( 'direccion' );
        $this->direccion->setRequired();
        $this->direccion->setLabel( $this->_translate->translate( 'direccion' ) . ':' );



        $this->codpostal = new Zend_Form_Element_Text( 'codpostal' );
        $this->codpostal->setRequired();
        //$e->setDecorators( array( $decorator ) );
        $this->codpostal->setLabel( $this->_translate->translate( 'Codigo postal' ) . ':' );



        $this->ciudad = new Zend_Form_Element_Text( 'ciudad' );
        $this->ciudad->setRequired();
        //$e->setDecorators( array( $decorator ) );
        $this->ciudad->setLabel( $this->_translate->translate( 'ciudad' ) . ':' );



        $this->paises_id = new Zend_Form_Element_Select( 'paises_id' );
        $this->paises_id->setRequired();
        $this->paises_id->setLabel( $this->_translate->translate( 'pais' ) );
        $_pais = new Mtt_Models_Bussines_Paises();
        $values = $_pais->getComboValues();
        $this->paises_id->addMultiOption( -1 
                , '---' . $this->_translate->translate('paises'). '---' );
        $this->paises_id->addMultiOptions( $values )
                ->setAttrib( 'width' , '211' );

        $this->paises_id->addValidator( new Zend_Validate_InArray( array_keys( $values ) ) );



        $this->institucion = new Zend_Form_Element_Text( 'institucion' );
        $this->institucion->setRequired();
        //$e->setDecorators( array( $decorator ) );
        $this->institucion->setLabel( $this->_translate->translate( 'institucion' ) . ':' );


        $this->submit = new Zend_Form_Element_Button( 'submit' );
        $this->submit->setLabel(
                        $this->_translate->translate( 'save' )
                )
                ->setAttrib(
                        'class' , 'button'
                )
                ->setAttrib( 'type' , 'submit' )
        ;
        
        


        $this->_addCaptcha();
        $this->_setValidator();
        $this->_addElements();
        }


    }

