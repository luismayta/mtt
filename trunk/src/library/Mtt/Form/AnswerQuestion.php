<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_AnswerQuestion
        extends Mtt_Form_Table
{

    protected $id ;
    protected $respuesta ;
    protected $submit ;


    public function init()
    {
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' ,
                             'frmAnswerQuestion' )
                ->setAction( '/admin/pregunta/answerquestion' )
        ;

        $this->id = new Mtt_Form_Tabular_Element_Hidden( 'id' ) ;

        $this->respuesta = new Mtt_Form_Tabular_Element_Textarea( 'respuesta' ) ;
        $this->respuesta->setRequired() ;
        $this->respuesta->setLabel(
                $this->_translate->translate(
                        'respuesta'
                ) ) ;
        $this->respuesta->setAttrib( 'COLS' ,
                                     '40' ) ;
        $this->respuesta->setAttrib( 'ROWS' ,
                                     '4' ) ;
        $this->submit = new Mtt_Form_Tabular_Element_Button( 'submit' ) ;
        $this->submit->setLabel(
                        $this->_translate->translate( 'respuesta' )
                )
                ->setAttrib(
                        'class' ,
                        'button'
                )
                ->setAttrib( 'type' ,
                             'submit' )
        ;

        $this->addElements(
                array(
                    $this->id ,
                    $this->respuesta ,
                    $this->submit
                )
        ) ;


    }


}

