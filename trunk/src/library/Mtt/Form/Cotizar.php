<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_Cotizar
        extends Mtt_Form
{

    protected $nombre ;
    protected $organizacion ;
    protected $direccion ;
    protected $codpostal ;
    protected $ciudad ;
    protected $paises ;
    protected $email ;
    protected $asunto ;
    protected $mensaje ;
    protected $precioCheck ;
    protected $precio ;
    protected $moneda_id ;
    protected $toemail ;
    protected $submit ;
    protected $paises_id ;
    protected $captcha ;
    protected $_paises ;
    protected $_monedas ;


    public function __construct( $options = null )
    {

        $this->_paises = new Mtt_Models_Bussines_Paises() ;
        $this->_monedas = new Mtt_Models_Bussines_Moneda() ;

        $this->nombre = new Zend_Form_Element_Text( 'nombre' ) ;
        $this->organizacion = new Zend_Form_Element_Text( 'organizacion' ) ;
        $this->direccion = new Zend_Form_Element_Text( 'direccion' ) ;
        $this->codpostal = new Zend_Form_Element_Text( 'codpostal' ) ;
        $this->ciudad = new Zend_Form_Element_Text( 'ciudad' ) ;
        $this->paises_id = new Zend_Form_Element_Select( 'paises_id' ) ;
        $this->email = new Zend_Form_Element_Text( 'email' ) ;
        $this->asunto = new Zend_Form_Element_Text( 'asunto' ) ;
        $this->mensaje = new Zend_Form_Element_Textarea( 'mensaje' ) ;
        $this->toemail = new Zend_Form_Element_Checkbox( 'toemail' ) ;

        $this->precioCheck = new Zend_Form_Element_Checkbox( 'precioCheck' ) ;
        $this->precio = new Zend_Form_Element_Text( 'precio' ) ;
        $this->moneda_id = new Zend_Form_Element_Select( 'moneda_id' ) ;



        $this->submit = new Zend_Form_Element_Button( 'submit' ) ;
        parent::__construct( $options ) ;


    }


    private function _addCaptcha()
    {
//        $this->captcha = new Zend_Form_Element_Captcha( 'captcha' ,
//                        array(
//                            'captcha' => 'ReCaptcha' ,
//                            'captchaOptions' => array(
//                                'captcha' => 'ReCaptcha'
//                                , 'service' => $this->_recaptcha
//                            ) ,
//                            'ignore' => true
//                        )
//        );

        $this->captcha = new Zend_Form_Element_Captcha(
                        'captcha' ,
                        array(
                            'captcha' => $this->_captcha
                        )
                ) ;

        $this->captcha->setLabel(
                $this->_translate->translate(
                        'escribe los caracteres que ves en la imagen siguiente.'
                )
        ) ;


    }


    private function _addContentElement()
    {
        $this->addElement( $this->nombre ) ;
        $this->addElement( $this->organizacion ) ;
        $this->addElement( $this->direccion ) ;
        $this->addElement( $this->codpostal ) ;
        $this->addElement( $this->ciudad ) ;
        $this->addElement( $this->paises_id ) ;
        $this->addElement( $this->email ) ;
        $this->addElement( $this->asunto ) ;
        $this->addElement( $this->precioCheck ) ;
        $this->addElement( $this->precio ) ;
        $this->addElement( $this->moneda_id ) ;
        $this->addElement( $this->mensaje ) ;
        $this->addElement( $this->toemail ) ;
        $this->addElement( $this->captcha ) ;
        $this->addElement( $this->submit ) ;


    }


    private function _setGroup()
    {
        /* $this->addDisplayGroup( array( 'precio' , 'moneda_id' ) ,
          'groupprecio' , array( "legend" => "precio" ) ); */


    }


    private function _addLabel()
    {
        $this->nombre->setLabel(
                $this->_translate->translate(
                        'nombre'
                )
        ) ;

        $this->organizacion->setLabel(
                $this->_translate->translate(
                        'organizacion'
                )
        ) ;

        $this->direccion->setLabel(
                $this->_translate->translate(
                        'direccion'
                )
        ) ;
        $this->codpostal->setLabel(
                $this->_translate->translate(
                        'codigo postal'
                )
        ) ;
        $this->paises_id->setLabel(
                $this->_translate->translate(
                        'pais'
                )
        ) ;
        $this->ciudad->setLabel(
                $this->_translate->translate(
                        'ciudad'
                )
        ) ;
        $this->email->setLabel(
                $this->_translate->translate(
                        'email'
                )
        ) ;
        $this->asunto->setLabel(
                $this->_translate->translate(
                        'asunto'
                )
        ) ;
        $this->mensaje->setLabel(
                $this->_translate->translate(
                        'mensaje'
                ) ) ;

        $this->precio->setLabel(
                $this->_translate->translate(
                        'precio'
                )
        ) ;

        $this->precioCheck->setLabel(
                $this->_translate->translate(
                        'precio ofertado por el usuario'
                )
        ) ;

        $this->moneda_id->setLabel(
                $this->_translate->translate(
                        'escoger moneda'
                )
        ) ;


    }


    private function _setAttrib()
    {
        $this->mensaje->setAttrib( 'COLS' ,
                                   '40' ) ;
        $this->mensaje->setAttrib( 'ROWS' ,
                                   '4' ) ;

        $this->toemail->setLabel( $this->_translate->translate(
                                'Envie una copia a su correo' ) )
                ->setAttrib( 'id' ,
                             'toemail' ) ;

        $this->submit->setAttrib( 'value' ,
                                  ucwords(
                                $this->_translate->translate( 'enviar' )
                        )
                )
                ->setAttrib( 'class' ,
                             'button' )
                ->setAttrib( 'type' ,
                             'submit' )
        ;


    }


    private function _addValidator()
    {
        $this->nombre->addValidator( new Zend_Validate_StringLength(
                        array(
                            'min' => 1
                            ,
                            'max' => 150 )
                )
        ) ;
        $this->organizacion->addValidator( new Zend_Validate_StringLength(
                        array(
                            'min' => 1 , 'max' => 150 )
                )
        ) ;
        $this->direccion->addValidator( new Zend_Validate_StringLength(
                        array(
                            'min' => 1 , 'max' => 150 )
                )
        ) ;
        $this->email->addValidator( new Zend_Validate_EmailAddress() ) ;


    }


    private function _addRequired()
    {
        $this->nombre->setRequired() ;
        $this->direccion->setRequired() ;
        $this->codpostal->setRequired() ;
        $this->paises_id->setRequired() ;
        $this->ciudad->setRequired() ;
        $this->email->setRequired() ;
        $this->organizacion->setRequired() ;
        $this->asunto->setRequired() ;
        $this->mensaje->setRequired() ;


    }


    public function init()
    {
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' ,
                             'frmCotizar' )
                ->setAttrib( 'class' ,
                             'zend_form' ) ;



        $values = $this->_paises->getComboValues() ;
        $this->paises_id->addMultiOption( -1 ,
                                          $this->_translate->translate(
                        'paises'
                )
        ) ;
        $this->paises_id->addMultiOptions( $values ) ;

        $this->paises_id->addValidator(
                new Zend_Validate_InArray( array_keys( $values ) )
        ) ;


        $values = $this->_monedas->getComboValues() ;
        $this->moneda_id->addMultiOption( -1 ,
                                          $this->_translate->translate(
                        'moneda'
                )
        ) ;
        $this->moneda_id->addMultiOptions( $values ) ;



        $this->_addLabel() ;
        $this->_setAttrib() ;
        $this->_addCaptcha() ;
        $this->_addValidator() ;
        $this->_addRequired() ;
        $this->_addContentElement() ;
        $this->_setGroup() ;


    }


}

