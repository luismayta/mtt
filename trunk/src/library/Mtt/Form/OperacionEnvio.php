<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_OperacionEnvio
        extends Mtt_Form
    {

    protected $descripcion;
    protected $submit;
    
    public function init()
        {
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' , 'frmOperacionEnvio' )
        //->setAction( '/categoria/index' )
        ;
       
        $this->descripcion = new Zend_Form_Element_Text( 'descripcion' );
        $this->descripcion->setLabel(
                $this->_translate->translate(
                        'estado del envio'
                )
        );
        $this->descripcion->setRequired();
        $this->descripcion->addValidator(
                new Zend_Validate_StringLength(
                        array( 'min' => 5 , 'max' => 120 )
                )
        );
        
        $this->submit = new Zend_Form_Element_Button( 'submit' );
        $this->submit->setAttrib( 'value' ,
                            $this->_translate->translate( 'agregar' ) 
        )
                ->setAttrib( 'class' , 'button' )
                ->setAttrib( 'type' , 'submit' )
        ;

        $this->addElements( array(
                $this->descripcion ,
                $this->submit
                )
        );
        }


    }
