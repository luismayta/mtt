<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_Test
        extends Mtt_Form
    {

    protected $nombre;
    protected $captcha;
    protected $submit;


    public function __construct()
        {
        $this->nombre = new Zend_Form_Element_Text( 'nombre' );
        $this->captcha = new Zend_Form_Element_Captcha(
                        'captcha' ,
                        array(
                            'captcha' => 'ReCaptcha' ,
                            'captchaOptions' => array( 'captcha' => 'ReCaptcha' ,
                                'service' => $this->_recaptcha ) ,
                            'ignore' => true
                        )
        );
        $this->submit = new Zend_Form_Element_Button( 'submit' );
        parent::__construct();
        }


    private function _addContentElement()
        {
        $this->addElement( $this->nombre );
        $this->addElement( $this->captcha );
        $this->addElement( $this->submit );
        }


    private function _assignAtrib()
        {

        //$this->captcha->setRequired();
        //$this->captcha->setLabel( 'captcha' );
        //$this->captcha->setService( $this->_recaptcha )
        ;
        //$this->captcha->addValidator();
//
//        $this->captcha = new Zend_Form_Element_Captcha(
//                        'captcha' ,
//                        array(
//                            'label' => 'Type the characters you see in the picture below.' ,
//                            'captcha' => 'ReCaptcha' ,
//                            'captchaOptions' => array(
//                                'captcha' => 'ReCaptcha' ,
//                                'service' => $this->_recaptcha
//                            )
//                        )
//        );


        $this->submit->setAttrib(
                        'value' , $this->_translate->translate( 'registrar' )
                )
                ->setAttrib(
                        'class' , 'button'
                )
                ->setAttrib(
                        'type' , 'submit'
                )
        ;
        }


    public function setValue()
        {
        
        }


    public function init()
        {

        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' , 'frmCategoria' )
        ;

        $this->_assignAtrib();
        $this->_addContentElement();
        }


    }
