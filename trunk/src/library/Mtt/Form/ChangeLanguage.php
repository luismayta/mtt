<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_ChangeLanguage
        extends Mtt_Form
    {

    protected $idioma;
    protected $submit;
    protected $_idioma;


    public function __construct()
        {
        $this->idioma = new Zend_Form_Element_Select( 'idioma_id' );
        $this->submit = new Zend_Form_Element_Button( 'submit' );
        $this->_idioma = new Mtt_Models_Bussines_Idioma();

        parent::__construct();
        }


    public function init()
        {
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' , 'frmChangeLanguage' )
                ->setAttrib( 'class' , 'zend_form' )
                ->setAction( '/admin/idiomas/changeidioma/' )
        ;

        $this->idioma->setLabel(
                        $this->_translate->translate( 'escoger idioma' )
                )
                ->addMultiOption( -1 ,
                                  $this->_translate->translate(
                                'escoger idioma' )
                )
                ->addMultiOptions(
                        $this->_idioma->getComboValues()
                )
                ->addValidator(
                        new Zend_Validate_InArray(
                                array_keys(
                                        $this->_idioma->getComboValues()
                                )
                        )
        );

        $this->addElement( $this->idioma );


        $this->submit->setLabel(
                $this->_translate->translate( 'siguiente' ) );
        $this->submit->setAttrib( 'type' , 'submit' )

        ;
        $this->addElement( $this->submit );

        parent::init();
        }


    }

