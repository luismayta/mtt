<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_Fabricante
        extends Mtt_Form
{

    protected $nombre ;
    protected $imagen ;
    protected $submit ;
    protected $_conf ;
    protected $_id ;


    public function setId( $id = null )
    {
        $this->_id = $id ;


    }


    public function __construct( $options = null )
    {

        $this->nombre = new Zend_Form_Element_Text( 'nombre' ) ;
        $this->imagen = new Zend_Form_Element_File( 'imagen' ) ;
        $this->submit = new Zend_Form_Element_Button( 'submit' ) ;
        $this->_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/images.ini' , APPLICATION_ENV
                )
        ;
        parent::__construct( $options ) ;


    }


    private function _setValidator()
    {

        if ( is_null( $this->_id ) )
        {
            $this->nombre->addValidator( new Zend_Validate_Db_NoRecordExists(
                            array(
                                'table' => 'fabricantes' ,
                                'field' => 'nombre' ,
                            )
                    )
            ) ;
        }
        else
        {
            $this->nombre
                    ->addValidator( new Zend_Validate_Db_NoRecordExists(
                                    'fabricantes' , 'nombre' , array(
                                'field' => 'id' , 'value' => $this->_id
                                    )
                    ) )
            ;
        }

        $this->nombre->addValidator(
                new Zend_Validate_StringLength(
                        array( 'min' => 2 , 'max' => 150 )
                )
        ) ;

        
        $this->imagen->addValidator(
                'Extension' ,
                false ,
                $this->_conf->image->extension
        ) ;

        $this->imagen->addValidator( 'Count' ,
                                     false ,
                                     1 ) ;

        $this->imagen->addValidator( 'Size' ,
                                     false ,
                                     $this->_conf->image->upload->maxsize )
                ->setValueDisabled( true ) ;


    }


    private function _addLabels()
    {
        $this->nombre->setLabel(
                $this->_translate->translate( 'nombre' )
        ) ;
        $this->imagen->setLabel(
                $this->_translate->translate( 'imagen' )
        ) ;
        $this->submit->setLabel(
                        $this->_translate->translate( 'guardar' )
                )
                ->setAttrib(
                        'class' ,
                        'button'
                )
                ->setAttrib( 'type' ,
                             'submit' )
        ;


    }


    private function _addAtribs()
    {
        $this->nombre->setAttrib( 'maxlength' ,
                                  '50' ) ;

        $this->imagen->setDestination(
                $this->_conf->manufacturer->default->path
        ) ;


    }


    private function _addRequired()
    {
        $this->nombre->setRequired() ;
        $this->imagen->setRequired() ;


    }


    private function _addElements()
    {

        $this->addElement( $this->nombre ) ;
        $this->addElement( $this->imagen ) ;
        $this->addElement( $this->submit ) ;


    }


    public function init()
    {
        parent::init() ;

        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' ,
                             'frmFabricante' )
                ->setAttrib( 'class' ,
                             'zend_form' )
                ->setAttrib( 'enctype' ,
                             'multipart/form-data' ) ;
        $this->_addLabels() ;
        $this->_addRequired() ;
        $this->_addAtribs() ;
        $this->_setValidator() ;
        $this->_addElements() ;


    }


}
