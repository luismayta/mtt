<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Mtt_Form_Translate
        extends Mtt_Form
    {

    protected $_entidad;
    protected $submit;
    protected $_data;


    public function __construct( $data )
        {

        if ( !is_null( $data ) )
            {
            $this->_data = $data;
            }
        $this->_entidad = new Mtt_Models_Bussines_Entidades();

        $this->submit = new Zend_Form_Element_Button( 'submit' );

        parent::__construct();
        }


    public function addMemberForms()
        {

        $entidades = $this->_entidad->listCamposByEntidad(
                $this->_data['entidad']
        );

  

        foreach ( $entidades as $entidad )
            {
            $this->_data['campo_id'] = $entidad->id;
            $this->_data['campo'] = $entidad->nombre;
            $this->_data['entidad_id'] = $this->_data['entidad_id'];
            $this->_data['entidad'] = $this->_data['entidad'];
            
            $key = "member_{$entidad->id}";
            $subform = new Mtt_Form_SubForm_Translate(
                            $this->_data
            );
            $this->addSubForm(
                    $subform , $key
            );
            }
        }


    public function init()
        {
        
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' , 'frmTranslate' )
               // ->setAttrib( 'class' , 'zend_form' )
        ;

        $this->submit->setLabel(
                        $this->_translate->translate( 'enviar' )
                )
                ->setAttrib( 'class' , 'button' )
                ->setAttrib( 'type' , 'submit' )
        ;
        $this->addElement( $this->submit );
        $this->addMemberForms();

        }


    }

