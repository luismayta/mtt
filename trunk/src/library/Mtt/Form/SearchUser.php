<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_SearchUser
		extends Mtt_Form
{

	protected $palabras_busqueda ;
	protected $modelo ;
	protected $fabricante ;
	protected $categoria_id ;
	protected $anio_inicio ;
	protected $anio_fin ;
	protected $precio_inicio ;
	protected $precio_fin ;
	protected $id ;
	protected $submit ;


	public function __construct()
	{
		parent::__construct() ;


	}


	public function init()
	{
		$this
				->setMethod( 'post' )
				->setAttrib( 'id' , 'frmSearch' )
				->setAttrib( 'class' , 'zend_form' )
				->setAction( '/user/busqueda/find' )
		;

		$this->palabras_busqueda = new Zend_Form_Element_Text( 'palabras_busqueda' ) ;

		$this->palabras_busqueda->setLabel(
				$this->_translate->translate(
						'buscar por'
				)
		) ;
		//$this->addElement( $e );

		/* Modelo */
		$this->modelo = new Zend_Form_Element_Text( 'modelo' ) ;

		$this->modelo->setLabel(
				$this->_translate->translate(
						'modelo'
				)
		) ;
		//$this->addElement( $e );


		/* Fabricante */
		$this->fabricante = new Zend_Form_Element_Text( 'fabricante' ) ;

		$this->fabricante->setLabel(
				$this->_translate->translate(
						'fabricante'
				)
		) ;
		//$this->addElement( $e );

		/* Categoria */
		$this->categoria_id = new Zend_Form_Element_Select( 'categoria_id' ) ;
		$this->categoria_id->setLabel(
				$this->_translate->translate(
						'categoria'
				)
		) ;
		$_categoria = new Mtt_Models_Bussines_Categoria() ;
		$values = $_categoria->getComboValues() ;
		$this->categoria_id->addMultiOption( -1 , $this->_translate->translate(
						'todos'
				)
		) ;
		$this->categoria_id->addMultiOptions( $values ) ;
		//$this->addElement( $e );

		/* año desde */
		// Creando array
		$ini_year = 1980 ;
		$year_fin = date( 'Y' ) ;
		for ( $i = $ini_year ;
				$i <= $year_fin ;
				$i++ )
		{
			$anio[$i] = $i ;
		}


		$this->anio_inicio = new Zend_Form_Element_Select( 'anio_inicio' ) ;
		$this->anio_inicio->setLabel(
				$this->_translate->translate(
						'desde el año'
				)
		) ;

		$this->anio_inicio->addMultiOption( -1 , $this->_translate->translate(
						'desde'
				)
		) ;
		$this->anio_inicio->addMultiOptions( $anio ) ;
		//$this->addElement( $e );


		/* hasta */
		$this->anio_fin = new Zend_Form_Element_Select( 'anio_fin' ) ;
		$this->anio_fin->addMultiOption( -1 , $this->_translate->translate(
						'hasta'
				)
		) ;
		$this->anio_fin->addMultiOptions( $anio ) ;
		//$this->addElement( $e );

		/* precio desde */
		//Creando array precios
		$precio = array(
			'0' => 0 ,
			'100' => 100 ,
			'500' => 500 ,
			'1000' => 1000 ,
			'2000' => 2000 ,
			'5000' => 5000 ,
			'10000' => 10000 ,
			'50000' => 50000 ,
			'50000' => 50000 ,
			'100000' => 100000 ,
			'1000000' => 1000000 ,
			'10000000' => 10000000 ,
				) ;
		$this->precio_inicio = new Zend_Form_Element_Select( 'precio_inicio' ) ;
		$this->precio_inicio->setLabel(
				$this->_translate->translate(
						'precio'
				)
		) ;

		$this->precio_inicio->addMultiOption( -1 , $this->_translate->translate(
						'desde'
				)
		) ;
		$this->precio_inicio->addMultiOptions( $precio ) ;
		//$this->addElement( $e );


		/* hasta */
		$this->precio_fin = new Zend_Form_Element_Select( 'precio_fin' ) ;
		$this->precio_fin->addMultiOption( -1 , $this->_translate->translate(
						'hasta'
				)
		) ;
		$this->precio_fin->addMultiOptions( $precio ) ;
		//$this->addElement( $e );

		$this->id = new Zend_Form_Element_Hidden( 'id' ) ;
		//$this->addElement( $id );


		$this->submit = new Zend_Form_Element_Button( 'submit' ) ;
		$this->submit->setLabel(
				$this->_translate->translate( 'buscar' )
		) ;
		$this->submit->setAttrib( 'type' , 'submit' )
				->setAttrib( 'class' , 'button' )
		;
		//$this->addElement( $submit );
		$this->addElements(
				array(
					$this->palabras_busqueda ,
					$this->modelo ,
					$this->fabricante ,
					$this->categoria_id ,
					$this->anio_inicio ,
					$this->anio_fin ,
					$this->precio_inicio ,
					$this->precio_fin ,
					$this->id ,
					$this->submit
				) ) ;


	}


}

