<?php


class Mtt_Form_Captcha
        extends Mtt_Form
{

    protected $nombre ;
    protected $captcha ;
    protected $submit ;


    public function __construct( $options = null )
    {
        $this->nombre = new Zend_Form_Element_Text( 'nombre' ) ;
        $this->submit = new Zend_Form_Element_Submit( 'submit' ) ;
        parent::__construct( $options ) ;


    }


    private function _assignAtributo()
    {
        $this->nombre->setLabel( 'data' ) ;


    }


    private function _addContentElement()
    {
        $this->addElement( $this->nombre ) ;
        $this->addElement( $this->captcha ) ;
        $this->addElement( $this->submit ) ;


    }


    public function init()
    {
        $this->captcha = new Zend_Form_Element_Captcha( 'captcha' ,
                        array(
                            'captcha' => 'ReCaptcha' ,
                            'captchaOptions' => array(
                                'captcha' => 'ReCaptcha'
                                , 'service' => $this->_recaptcha
                            ) ,
                            'ignore' => true
                        )
        ) ;

        $this->_assignAtributo() ;
        $this->_addContentElement() ;


    }


}