<?php


class Mtt_Form_ActivarEquipos
        extends Mtt_Form
{

    protected $submit ;
    protected $_data ;


    public function __construct( $data )
    {
        if ( !is_null( $data ) )
        {
            $this->_data = $data ;
        }

        parent::__construct() ;


    }


    public function init()
    {
        parent::init() ;
        $this->setMethod( Zend_Form::METHOD_POST )
                ->setAttrib( 'id' ,
                             'frmActiveEquipment' )

        ;


        $this->addMemberForms() ;


        $this->submit = new Zend_Form_Element_Button( 'submit' ) ;
        $this->submit->setLabel(
                        $this->_translate->translate( 'activar' )
                )
                ->setAttrib(
                        'class' ,
                        'button'
                )
                ->setAttrib( 'type' ,
                             'submit' ) ;

        $this->addElement( $this->submit ) ;


    }


    public function addMemberForms()
    {

        foreach ( $this->_data as $value )
        {
            $key = "member_" . $value->id ;
            $subform = new Mtt_Form_SubForm_ActivarEquipos(
                            $value
            ) ;
            $this->addSubForm( $subform ,
                               $key )
            ;
        }


    }


}
