<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_Content
        extends Mtt_Form
{

    protected $_idioma ;
    protected $submit ;
    protected $_data ;


    public function addMemberForms()
    {

        $idiomas = $this->_idioma->listar() ;

        foreach ( $idiomas as $idioma )
        {

            $this->_data['idioma_id'] = $idioma->id ;
            $this->_data['nomIdioma'] = $idioma->nombre ;

            $key = "member_{$idioma->id}" ;
            $subform = new Mtt_Form_SubForm_Content( $this->_data ) ;
            $this->addSubForm( $subform ,
                               $key ) ;
        }


    }


    public function __construct( $data )
    {

        if ( !is_null( $data ) )
        {
            $this->_data = $data ;
        }
        $this->_idioma = new Mtt_Models_Bussines_Idioma() ;

        $this->submit = new Zend_Form_Element_Button( 'submit' ) ;
        parent::__construct() ;


    }


    public function init()
    {
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' ,
                             'frmCms' )
                ->setAttrib( 'class' ,
                             'zend_form' ) ;

        //titulo

        $this->submit->setLabel(
                        $this->_translate->translate( 'enviar' )
                )
                ->setAttrib( 'class' ,
                             'button' )
                ->setAttrib( 'type' ,
                             'submit' )
        ;
        $this->addElement( $this->submit ) ;
        $this->addMemberForms() ;


    }


}

