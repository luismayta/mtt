<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_Usuario
        extends Mtt_Form
    {
    protected $tratamiento;
    protected $nombre;
    protected $apellido;
    protected $login;
    protected $email;
    protected $clave;
    protected $clave2;
    protected $direccion;
    protected $codPostal;
    protected $ciudad;
    protected $paises;
    protected $rol;
    protected $institucion;
    protected $telefono;
    protected $fax;
    protected $submit;
    protected $_id;

    public function setId( $id=null )
        {
        $this->_id = $id;
        }


    public function __construct( $options = null )
        {

        parent::__construct( $options );
        }


    private function _setValidator()
        {

        if ( is_null( $this->_id ) )
            {
            $this->email->addValidator( new Zend_Validate_Db_NoRecordExists(
                            array(
                                'table' => 'usuario' ,
                                'field' => 'email' )
                    )
            );

            $this->login->addValidator(
                    new Zend_Validate_Db_NoRecordExists(
                            array(
                                'table' => 'usuario' ,
                                'field' => 'login' ,
                            )
                    )
            );
            }
        else
            {
            $this->email
                    ->addValidator( new Zend_Validate_Db_NoRecordExists(
                                    'usuario' , 'email' , array(
                                'field' => 'id' , 'value' => $this->_id
                                    )
                    ) )
            ;
            $this->login
                    ->addValidator( new Zend_Validate_Db_NoRecordExists(
                                    'usuario' , 'login' , array(
                                'field' => 'id' , 'value' => $this->_id
                                    )
                    ) )
            ;
            }
        }


    private function _addElements()
        {
        $this->addElements(
                array(
                    $this->tratamiento ,
                    $this->nombre ,
                    $this->apellido ,
                    $this->login ,
                    $this->email ,
                    $this->clave ,
                    $this->clave2 ,
                    $this->direccion ,
                    $this->codPostal ,
                    $this->ciudad ,
                    $this->paises ,
                    $this->rol ,
                    $this->institucion ,
                    $this->telefono ,
                    $this->fax ,
                    $this->submit
                )
        );
        }


    public function init()
        {
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' , 'frmRegistrar' )
                ->setAttrib( 'class' , 'zend_form' )
        ;

        $this->tratamiento = new Zend_Form_Element_Select( 'tratamiento' );
        $this->tratamiento->setLabel(
                $this->_translate->translate( 'tratamiento' )
        );
        $this->tratamiento->setRequired();

        $_tratam = new Mtt_Models_Bussines_Usuario();
        $values = $_tratam->getTratamientosUsuario();
        $this->tratamiento->addMultiOptions( $values );
        //$this->addElement( $e );
        $this->tratamiento->addValidator( new Zend_Validate_InArray(
                        array_keys( $values )
                )
        );

        $this->nombre = new Zend_Form_Element_Text( 'nombre' );
        $this->nombre->setRequired();
        //$e->setDecorators( array( $decorator ) );
        $this->nombre->setLabel(
                $this->_translate->translate( 'nombre' )
                . ':'
        );
        $this->nombre->addValidator(
                new Zend_Validate_StringLength(
                        array( 'min' => 1 , 'max' => 25 )
                )
        );


        $this->apellido = new Zend_Form_Element_Text( 'apellido' );
        $this->apellido->setRequired();
        //$e->setDecorators( array( $decorator ) );
        $this->apellido->setLabel(
                $this->_translate->translate( 'apellido' )
                . ':'
        );
        $this->apellido->addValidator(
                new Zend_Validate_StringLength(
                        array(
                            'min' => 1 , 'max' => 25
                        )
                )
        );
        //$this->addElement( $e );

        $this->email = new Zend_Form_Element_Text( 'email' );
        $this->email->setRequired();
        //$e->setDecorators( array( $decorator ) );
        $this->email->setLabel(
                $this->_translate->translate( 'email' )
                . ':'
        );



        $this->email->addValidator(
                new Zend_Validate_EmailAddress()
        );
        //$this->addElement( $e );

        $this->login = new Zend_Form_Element_Text( 'login' );
        $this->login->setRequired();
        //$e->setDecorators( array( $decorator ) );
        $this->login->setLabel(
                $this->_translate->translate( 'login' )
                . ':'
        );
        $this->login->addValidator( new Zend_Validate_Alnum() );

        $this->login->addValidator(
                new Zend_Validate_StringLength(
                        array(
                            'min' => 3 , 'max' => 25
                        )
                )
        );
        //$this->addElement( $e );

        $this->clave = new Zend_Form_Element_Password( 'clave' );
        $this->clave->setRequired();
        $this->clave->setLabel(
                ucwords(
                        $this->_translate->translate( 'password' )
                ) . ':'
        );

        $this->clave2 = new Zend_Form_Element_Password( 'clave_2' );
        $this->clave2->setRequired();
        //$e->setDecorators( array( $decorator ) );
        $this->clave2->setLabel(
                $this->_translate->translate( 'confirmar password' )
                . ':'
        );
        $this->clave2->addValidator(
                new Mtt_Validate_PasswordConfirmation()
        );
        //$this->addElement( $e );


        $this->direccion = new Zend_Form_Element_Text( 'direccion' );
        $this->direccion->setRequired();
        //$e->setDecorators( array( $decorator ) );
        $this->direccion->setLabel(
                $this->_translate->translate( 'direccion' )
                . ':'
        );
        //$this->addElement( $e );


        $this->codPostal = new Zend_Form_Element_Text( 'codpostal' );
        $this->codPostal->setRequired();
        //$e->setDecorators( array( $decorator ) );
        $this->codPostal->setLabel(
                $this->_translate->translate( 'codigo postal' )
                . ':'
        );
        //$this->addElement( $e );


        $this->ciudad = new Zend_Form_Element_Text( 'ciudad' );
        $this->ciudad->setRequired();
        //$e->setDecorators( array( $decorator ) );
        $this->ciudad->setLabel(
                $this->_translate->translate( 'ciudad' )
                . ':'
        );
        //$this->addElement( $e );

        $this->paises = new Zend_Form_Element_Select( 'paises_id' );
        $this->paises->setRequired();
        $this->paises->setLabel(
                $this->_translate->translate( 'Escoger pais' )
        );
        $_pais = new Mtt_Models_Bussines_Paises();
        $values = $_pais->getComboValues();
        $this->paises->addMultiOption( -1 , '--- Paises ---' );
        $this->paises->addMultiOptions( $values );
        $this->paises->addValidator( new Zend_Validate_InArray(
                        array_keys( $values )
                )
        );

        $this->rol = new Zend_Form_Element_Select( 'tipousuario_id' );
        $this->rol->setRequired();
        $this->rol->setLabel(
                $this->_translate->translate( 'tipo de usuario' )
        );
        $_tipoUsuario = new Mtt_Models_Bussines_TipoUsuario();
        $values = $_tipoUsuario->getComboValues();
        $this->rol->addMultiOption( -1 , '--- Rol de Usario ---' );
        $this->rol->addMultiOptions( $values );
        $this->rol->addValidator( new Zend_Validate_InArray(
                        array_keys( $values )
                )
        );



        $this->institucion = new Zend_Form_Element_Text( 'institucion' );
        $this->institucion->setRequired();
        //$e->setDecorators( array( $decorator ) );
        $this->institucion->setLabel(
                $this->_translate->translate( 'institucion' )
                . ':'
        );
        //$this->addElement( $e );

        $this->telefono = new Zend_Form_Element_Text( 'telefono' );
        //$this->telefono->setRequired();
        //$e->setDecorators( array( $decorator ) );
        $this->telefono->setLabel(
                $this->_translate->translate( 'telefono' )
                . ':'
        );

        $this->fax = new Zend_Form_Element_Text( 'fax' );
        //$this->fax->setRequired();
        //$e->setDecorators( array( $decorator ) );
        $this->fax->setLabel(
                $this->_translate->translate( 'fax' )
                . ':'
        );

        $this->submit = new Zend_Form_Element_Button( 'submit' );
        $this->submit->setLabel(
                        $this->_translate->translate( 'save' )
                )
                ->setAttrib(
                        'class' , 'button'
                )
                ->setAttrib( 'type' , 'submit' );

        $this->_setValidator();
        $this->_addElements();
        }


    }

?>
