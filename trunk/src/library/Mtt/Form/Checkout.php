<?php


/**
 * 
 */
class Mtt_Form_Checkout
        extends Mtt_Formy
{

    protected $submit ;
    protected $actualizar ;
    protected $total ;
    protected $_data ;
    private $decoratorUser = array( ) ;


    public function setData( $value = null )
    {
        $this->_data = $value ;


    }


    public function _addDecorators()
    {
        $this->decoratorUser = array(
            'ViewHelper' ,
            'Errors' ,
            array( 'ViewScript' , array(
                    'viewScript' => '/decorators/decoratorUser.phtml' , 'placement' => false
                )
            ) ,
        ) ;


    }


    public function __construct( $options = null )
    {
        parent::__construct( $options ) ;


    }


    public function init()
    {

        $this->setMethod( Zend_Form::METHOD_POST )
                ->setAttrib( 'id' ,
                             'frmCheckout' )
        ;
        $this->addMemberForms() ;

        $this->total = new Zend_Form_Element_Text( 'total' ) ;
        $this->total->setLabel(
                $this->_translate->translate( 'Total' )
        ) ;

        $this->addElement( $this->total ) ;

        $this->submit = new Zend_Form_Element_Button( 'submit' ) ;
        $this->submit->setLabel(
                        $this->_translate->translate( 'actualizar' )
                )
                ->setAttrib(
                        'class' ,
                        'button'
                )
                ->setAttrib( 'type' ,
                             'submit' ) ;

        $this->addElement( $this->submit ) ;


    }


    public function addMemberForms()
    {
        for ( $i = 0 ; $i < count( $this->_data ) ; $i++ )
        {
            if ( !empty( $this->_data[$i] ) )
            {
                $key = "member_" . $i ;
                $subform = new Mtt_Form_SubForm_Checkout(
                                array( 'data' => $this->_data[$i] )
                ) ;
                $this->addSubForm( $subform ,
                                   $key )
                        ->getSubForm( $key )
                        ->clearDecorators()
                        ->addDecorator( 'FormElements' )
                        ->addDecorator( 'HtmlTag' ,
                                        array( "tag" => "tr" ) ) ;
            }
        }


    }


}
