<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_CuotasPagoOperacion
        extends Mtt_Form
    {

    protected $pago;
    protected $monto;
    protected $fechaLimite;
    protected $restante;
    protected $submit;
    
    public function init()
        {
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' , 'frmCuotasPago' )
        //->setAction( '/categoria/index' )
        ;
       
        $this->pago = new Zend_Form_Element_Text( 'pago' );
        $this->pago->setLabel(
                $this->_translate->translate(
                        'pago'
                )
        );
        $this->pago->setRequired();
        $this->pago->addValidator(
               new Zend_Validate_Between( array( 'min' => 0 , 'max' => 999999 ) )
        
        );
        
        $this->monto = new Zend_Form_Element_Text( 'monto' );
        $this->monto->setLabel(
                $this->_translate->translate(
                        'monto'
                )
        );
        $this->monto->setRequired();
        $this->monto->addValidator(
               new Zend_Validate_Between( array( 'min' => 1 , 'max' => 999999 ) )
        
        );
        
        $this->restante = new Zend_Form_Element_Text( 'restante' );
        $this->restante->setLabel(
                $this->_translate->translate(
                        'faltante'
                )
        );
        
        $this->fechaLimite = new ZendX_JQuery_Form_Element_DatePicker(
                        'fechaLimite' ,
                        array(
                            'jQueryParams' => array(
                                'defaultDate' => date( 'Y-m-d' ) ,
                                'minDate' => '1980-01-01' ,
                                'maxDate' => '2020-01-01' ,
                                'showButtonPanel' => 'true'
                            )
                        )
        );
        $this->fechaLimite->setLabel(
                $this->_translate->translate( 'fecha limite de pago' )
        );
        $this->fechaLimite->loadDefaultDecorators();
        $this->fechaLimite->setJQueryParam( 'dateFormat' , 'yy-mm-dd' );
        $this->fechaLimite->setRequired( true );
        
        
        $this->fechapago = new ZendX_JQuery_Form_Element_DatePicker(
                        'fechapago' ,
                        array(
                            'jQueryParams' => array(
                                'defaultDate' => date( 'Y-m-d' ) ,
                                'minDate' => '1980-01-01' ,
                                'maxDate' => '2020-01-01' ,
                                'showButtonPanel' => 'true'
                            )
                        )
        );
        $this->fechapago->setLabel(
                $this->_translate->translate( 'fecha de pago' )
        );
        $this->fechapago->loadDefaultDecorators();
        $this->fechapago->setJQueryParam( 'dateFormat' , 'yy-mm-dd' );
        $this->fechapago->setRequired( true );
        
        $this->submit = new Zend_Form_Element_Button( 'submit' );
        $this->submit->setAttrib( 'value' ,
                            $this->_translate->translate( 'agregar' ) 
        )
                ->setAttrib( 'class' , 'button' )
                ->setAttrib( 'type' , 'submit' )
        ;

        $this->addElements( array(
                $this->pago ,
                $this->monto ,
                $this->restante ,
                $this->fechaLimite,
                $this->fechapago,
                $this->submit
                )
        );
        }


    }
