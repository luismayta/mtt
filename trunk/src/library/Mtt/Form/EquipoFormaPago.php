<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_EquipoFormaPago
        extends Mtt_Form
    {

        
    protected $id;
    protected $formapago_id;
    protected $nrocuotas;
    protected $pago;
    protected $dias;
    protected $totalpago;
    protected $moraxdia;
    protected $submit;
   

    public function init()
        {
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' , 'frmEquipoFormaPago' )
        ;
        
        $this->formapago_id = new Zend_Form_Element_Select( 'formapago_id' );
        $this->formapago_id->setRequired();
        $this->formapago_id->setLabel(
                $this->_translate->translate( 'forma de pago' )
        )->setOrder( 0 );
        $_formapago = new Mtt_Models_Bussines_FormaPago();
        $values = $_formapago->getComboValues();
        $this->formapago_id->addMultiOption( -1 
                , '--- ' . $this->_translate->translate( 'forma de pago' ) . '---'
        );
        $this->formapago_id->addMultiOptions( $values );
        //$this->addElement($this->categoria);
        $this->formapago_id->addValidator(
                new Zend_Validate_InArray( array_keys( $values ) )
        );
        
        // Elemento: Nombre
        $this->nrocuotas = new Zend_Form_Element_Text( 'nrocuotas' );
        $this->nrocuotas->setLabel( 
                $this->_translate->translate( 'numero de cuotas' ) 
        );
        $this->nrocuotas->setAttrib( 'maxlength' , '50' );
        $this->nrocuotas->setRequired( true );
        $this->nrocuotas->addValidator( new Zend_Validate_Between(
                        array(
                            'min' => 1 , 'max' => 9999999999
                        )
        ) );
      
        //$this->addElement( $this->nombre );


        // Elemento: Pago
        $this->pago = new Zend_Form_Element_Text( 'pago' );
        $this->pago->setLabel( 
                $this->_translate->translate( 'pago' )  . ': ' 
        );
        $this->pago->setRequired( true );
        $v = new Zend_Validate_StringLength(
                        array( 'min' => 1 , 'max' => 5 )
        );
        $this->pago->addValidator( new Zend_Validate_Between(
                        array(
                            'min' => 0.1 , 'max' => 9999999999
                        )
        ) );
        
        $this->pago->addValidator( $v );
        
        
         // Elemento: Dias
        $this->dias = new Zend_Form_Element_Text( 'dias' );
        $this->dias->setLabel( 
                 $this->_translate->translate( 'dias' ) . ': ' 
        );
        $this->dias->setRequired( true );
        $v = new Zend_Validate_StringLength(
                        array( 'min' => 1 , 'max' => 5 )
        );
        $this->dias->addValidator( new Zend_Validate_Between(
                        array(
                            'min' => 1 , 'max' => 30
                        )
        ) );
        
        $this->dias->addValidator( $v );
        
        
         // Elemento: MoraxDia
        $this->moraxdia = new Zend_Form_Element_Text( 'moraxdia' );
        $this->moraxdia->setLabel( 
                 $this->_translate->translate( 'mora por dia' ) . ': ' 
        );
        $this->moraxdia->setRequired( true );
        $v = new Zend_Validate_StringLength(
                        array( 'min' => 1 , 'max' => 5 )
        );
        $this->moraxdia->addValidator( new Zend_Validate_Between(
                        array(
                            'min' => 0.1 , 'max' => 10
                        )
        ) );
        
        $this->moraxdia->addValidator( $v );
        
       
        $this->submit = new Zend_Form_Element_Button( 'submit' );
        $this->submit->setLabel(
                        $this->_translate->translate('save')
                )
                ->setAttrib(
                        'class' , 'button'
                )
                ->setAttrib( 'type' , 'submit' )
        ;

        //$this->addElement( $submit );
        
        
        $this->addElements(
                array(
                    $this->formapago_id ,
                    $this->nrocuotas,
                    $this->pago,
                    $this->dias,
                    $this->moraxdia,
                    $this->submit
                )
        );
        }


    }

