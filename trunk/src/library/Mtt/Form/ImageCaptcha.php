<?php


class Mtt_Form_ImageCaptcha
        extends Mtt_Form
    {

    protected $nombre;
    protected $captcha;
    protected $submit;


    public function __construct( $options = null )
        {
        $this->nombre = new Zend_Form_Element_Text( 'nombre' );
        $this->submit = new Zend_Form_Element_Submit( 'submit' );

        parent::__construct( $options );
        }


    private function _assignAtributo()
        {
        $this->nombre->setLabel( 'data' );
        }


    private function _addContentElement()
        {
        $this->addElement( $this->nombre );
        $this->addElement( $this->captcha );
        $this->addElement( $this->submit );
        }


    public function init()
        {
        $this->setAttrib('class', 'zend_form');
        
        $this->captcha = new Zend_Form_Element_Captcha(
                        'captcha' ,
                        array(
                            'captcha' => $this->_captcha
                        )
        );
        $this->captcha->setLabel('agrega el captcha');
//        $this->captcha->setCaptcha( 'Image' );
//        $this->captcha->setLabel( 'agrega el captcha' );
//        $this->captcha->setOptions(
//                array(
//                    'wordLen' => 4 ,
//                    'titmeout' => 300 ,
//                    'font' => APPLICATION_PATH . '/font/IMPACTED.TTF' ,
//                    'imgDir' => APPLICATION_PATH . '/../public/captcha/' ,
//                    'imgUrl' => '/captcha/' ,
//                    'height' => '40'
//                )
//        );
//
//        $captcha = new Zend_Form_Element_Captcha(
//                        'captcha' , // This is the name of the input field  
//                        array( 'label' => 'Write the chars to the field' ,
//                            'captcha' => array( // Here comes the magic...  
//// First the type...  
//                                'captcha' => 'Image' ,
//                                // Length of the word...  
//                                'wordLen' => 4 ,
//                                // Captcha timeout, 5 mins  
//                                'timeout' => 300 ,
//                                // What font to use...  
//                                'font' => APPLICATION_PATH . '/font/IMPACTED.TTF' ,
//                                'imgDir' => APPLICATION_PATH . '/../public/captcha/' ,
//                                'imgUrl' => '/captcha/' ,
//                            ) ) );

        $this->_assignAtributo();
        $this->_addContentElement();
        }


    }