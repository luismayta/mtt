<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_RecoveryPassword
		extends Mtt_Form
{

	protected $email ;
	protected $submit ;
	protected $captcha ;


	public function __construct( $options = null )
	{

		$this->email = new Zend_Form_Element_Text( 'email' ) ;
		$this->submit = new Zend_Form_Element_Button( 'submit' ) ;
		parent::__construct( $options ) ;


	}


	private function _addCaptcha()
	{

		$this->captcha = new Zend_Form_Element_Captcha(
						'captcha' ,
						array(
							'captcha' => $this->_captcha
						)
				) ;
		$this->captcha->setLabel(
				$this->_translate->translate(
						'escribe los caracteres que ves en la imagen siguiente.'
				)
		) ;


	}


	private function _addRequired()
	{
		$this->email->setRequired() ;


	}


	private function _addLabels()
	{
		$this->email->setLabel(
				$this->_translate->translate( 'email'
				)
		) ;
		$this->submit->setLabel( $this->_translate->translate( 'enviar'
		) ) ;


	}


	private function _addValidators()
	{
		$this->email->addValidator(
				new Zend_Validate_EmailAddress()
		) ;


	}


	private function _addAttributes()
	{
		$this->submit->setAttrib(
						'value' ,
						$this->_translate->translate( 'enviar'
						)
				)
				->setAttrib( 'class' , 'button' )
				->setAttrib( 'type' , 'submit' )
		;


	}


	private function _addContent()
	{
		$this->addElement( $this->email ) ;
		$this->addElement( $this->captcha ) ;
		$this->addElement( $this->submit ) ;


	}


	public function init()
	{

		$this
				->setMethod( 'post' )
				->setAttrib( 'id' , 'frmRecoveryPassword' )
				->setAttrib( 'enctype' , 'multipart/form-data' )
				->setAttrib( 'class' , 'zend_form' )
		;

		$this->_addLabels() ;
		$this->_addAttributes() ;
		$this->_addValidators() ;
		$this->_addRequired() ;
		$this->_addCaptcha() ;
		$this->_addContent() ;


	}


}
