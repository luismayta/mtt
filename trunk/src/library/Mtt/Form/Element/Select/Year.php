<?php


class Mtt_Form_Element_Select_Year
        extends Mtt_Form_Element_Select
    {

    protected $_years = array( );


    public function __construct( $spec , $options = null )
        {

        parent::__construct( $spec , $options );


        }


    public function init()
        {
        $this->addMultiOption( '' , '' );
        $years = range( 1980 , 2012 );
        foreach ( $years as $year )
            {
            $this->_years[$year] = $year;
            }
        $this->addMultiOptions( $this->_years );


        }


    }


?>
