<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Form_CotizarUsuario
		extends Mtt_Form_Cotizar
{


	public function __construct( $options = null )
	{


		parent::__construct( $options ) ;


	}


	public function init()
	{
		parent::init() ;
		$this
				->setMethod( 'post' )
				->setAttrib( 'id' , 'frmCotizarUsuario' )
				->setAttrib( 'class' , 'zend_form' ) ;


		$this->removeElement( $this->organizacion->getName() ) ;
		$this->removeElement( $this->ciudad->getName() ) ;
		$this->removeElement( $this->direccion->getName() ) ;
		$this->removeElement( $this->paises_id->getName() ) ;
		$this->removeElement( $this->codpostal->getName() ) ;
		$this->removeElement( $this->captcha->getName() ) ;


	}


}

