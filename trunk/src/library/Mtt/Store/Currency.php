<?php


class Mtt_Store_Currency
    {

    protected $id;
    protected $code;
    protected $_moneda;
    protected $mtt;
    private static $instancia;


    private function __construct()
        {
        $this->mtt = new Zend_Session_Namespace( 'MTT' );
        }


    public static function getInstance()
        {
        if ( !self::$instancia instanceof self )
            {
            self::$instancia = new self;
            }
        return self::$instancia;
        }


    public function setId( $value )
        {
        $this->id = $value;
        $this->mtt->currency->id = $this->id;
        }


    public function getId()
        {
        return $this->id;
        }


    public function setCode( $value )
        {
        $this->code = $value;
        $this->mtt->currency->code = $this->code;
        }


    public function getCode()
        {
        return $this->code;
        }


    public function getCurrency()
        {
        return $this->mtt->currency;
        }


    }

?>
