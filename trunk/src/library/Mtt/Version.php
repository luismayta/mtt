<?php
/**
 * 
 */
final class Mtt_Version 
{
	/**
	 * Get current Mtt version
	 * 
	 * @return string
	 */
	public static function getVersion() 
	{
		return '1.0.0';
	}
}
