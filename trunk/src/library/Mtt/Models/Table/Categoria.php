<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Table_Categoria
        extends Mtt_Db_Table_Abstract
    {

    protected $_name = 'categoria';
    protected $_primary = 'id';
    protected $_entidad_id = 1;

    const ENTIDAD_ID = 1;

//    public function __construct( $config = array( ) )
//        {
//        
//        parent::__construct( $config );
//        }


    public function listarCategoriasEquipos()
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->distinct()
                ->from( $this->_name ,
                        array(
                    'slug' => 'categoria.slug' , 'id' => 'categoria.id' )
                )
                ->joinInner(
                        'equipo' , 'equipo.categoria_id =categoria.id' ,
                        array( 'idequipo' => 'equipo.id' ) )
                ->where( 'categoria.active = ?' , self::ACTIVE )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ );
        }


    public function listar()
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active = ?' , self::ACTIVE )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ );
        }


    public function getCategoryWithEquip()
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name
                )
                ->joinLeft( 'equipo'
                        ,
                            'categoria.id = equipo.categoria_id'
                        , array( 'COUNT(equipo.id)' ) )
                ->where( 'categoria.active = ?' , self::ACTIVE )
                ->where( 'equipo.active = ?' , self::ACTIVE )
                ->where( 'equipo.publicacionEquipo_id = ?'
                        , Mtt_Models_Table_PublicacionEquipo::Activada )
                ->group( 'categoria.nombre' )
                ->order ( 'order')
                ->having( 'COUNT(equipo.id)>0' )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ );
        }


    }
