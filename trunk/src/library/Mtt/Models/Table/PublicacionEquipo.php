<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Table_PublicacionEquipo
        extends Mtt_Db_Table_Abstract
    {

    const Pendiente = 1;
    const Activada = 2;
    const Eliminada = 3;
    const Vendido = 4;
    
    const ENTIDAD_ID = 7;


    protected $_entidad_id = 7;
    protected $_name = 'publicacionEquipo';
    protected $_primary = 'id';


    public function __construct( $config = array( ) )
        {

        parent::__construct( $config );
        }


    }
