<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Catalog_Equipo
        extends Mtt_Models_Bussines_Equipo
{


    public function __construct( $config = array( ) )
    {

        parent::__construct( $config ) ;


    }


    /**
     * Metodo usado para la parte de Paginacion de
     * Equipos
     *
     * @author Luis Alberto Mayta Mamani
     * @method showEquipos
     * @return devuelve todos los items de la tabla
     * productos convertidos en objects
     *
     */
    public function showEquipos()
    {
        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/myConfig.ini' , 'paginator'
        ) ;
        $data = $_conf->toArray() ;
        $object = Zend_Paginator::factory( $this->getProducts() ) ;
        $object->setItemCountPerPage(
                $data['ItemCountPerPage']
        ) ;
        return $object ;


    }


    /**
     * @author Luis Alberto Mayta Mamani <slovacus@gmail.com>
     * @param type Integer
     * @return type Object
     * @method showEquiposOfers
     * @category Bussines
     * @package MTT
     */
    public function showEquiposOfers( $limit = 0 )
    {

        return $this->getProductsOfersAll( $limit ) ;


    }


    /**
     *
     * @param type $category_id
     * @param type $limit
     * @return type objects
     */
    public function showEquiposOfersByCategory( $category_id , $limit = null )
    {
        return $this->getProductsOfersAllByCategory( $category_id ,
                                                     $limit ) ;


    }


    /**
     * para enviar correo de autorizacion
     * @param array $data
     * @param string $subject
     */
    public function sendMail( array $data , $subject )
    {

        $_conf = new Zend_Config_Ini( APPLICATION_PATH . '/configs/mail.ini' ,
                        APPLICATION_ENV ) ;

        Zend_Mail::setDefaultFrom(
                $_conf->email->contacto ,
                $_conf->data->contacto
        ) ;

        $objSmtp = new Zend_Mail_Transport_Smtp(
                        $_conf->config->smtp ,
                        $_conf->config->toArray()
        ) ;
        Zend_Mail::setDefaultReplyTo(
                $_conf->email->contacto ,
                $_conf->data->contacto
        ) ;

        Zend_Mail::setDefaultTransport( $objSmtp ) ;


        $_confIni = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/myConfig.ini' ,
                        APPLICATION_ENV
        ) ;


        $m = new Mtt_Html_Mail_Mailer(
                        $_conf->email->contacto ,
                        $_conf->data->contacto
        ) ;
        $m->setSubject( $subject ) ;

        $m->addTo( $data['email'] ) ;

        $m->setViewParam( 'email' ,
                          $data['email'] )
                //->setViewParam( 'url' , $_confIni->Url . $data['url'] )
                ->setViewParam( 'url' ,
                                $data['url'] )
                ->setViewParam( 'nombre' ,
                                $data['nombre'] )
        ;
        $m->sendHtmlTemplate( "sendtofriend.phtml" ) ;


    }


}
