<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Catalog_OperationEquipo
        extends Mtt_Models_Bussines_OperationEquipo
    {

    

    public function __construct( $config = array( ) )
        {
       
        parent::__construct( $config );
        }



    /**
     *
     * @return type booleano
     */
    public function checkFormaPagoExist()
        {
        if ( count( $this->sessionMtt->operacion->detalles ) )
            {
            for ( $index = 0;
                        $index < count( $this->sessionMtt->operacion->detalles );
                        $index++ )
                {
                if ( !empty( $this->sessionMtt->operacion->detalles[$index] ) )
                    {
                    $detalle = $this->sessionMtt->operacion->detalles[$index];
                    if (
                            $detalle->getEquipo_has_formaPago() == '0'
                    )
                        {
                        return true;
                        }
                    }
                }
            }
        return false;
        }


    public function loadOperacionDetalle()
        {
        $cart;

        if ( count( $this->sessionMtt->operacion->detalles ) )
            {
            $newArray = array( );
            foreach ( $this->sessionMtt->operacion->detalles as $k => $detalle )
                {
                if ( $k != $i )
                    {
                    $nuevo_detalle[] = $detalle;
                    }
                }
            $S->venta->detalles = $nuevo_detalle;

            for ( $index = 0;
                        $index < count( $this->sessionMtt->operacion->detalles );
                        $index++ )
                {
                if ( is_array( $this->sessionMtt->operacion->detalles[$index] ) )
                    {


                    $nuevo_detalle = array( );
                    foreach ( $S->venta->detalles as $k => $detalle )
                        {
                        if ( $k != $i )
                            {
                            $nuevo_detalle[] = $detalle;
                            }
                        }
                    $S->venta->detalles = $nuevo_detalle;

                    $id = $this->sessionMtt->operacion->detalles[$index]->getId();

                    if (
                            $id
                            == $operacionDetalle[$id]['equipo_id']
                    )
                        {

                        if ( !is_null( $operacionDetalle[$id]['id_equipo_check'] ) )
                            {
                            $this->sessionMtt->operacion->detalles[$index] = array( );
                            }
                        }
                    }
                }
            }
        }


    public function deleteItem( $operacionDetalle )
        {
        if ( count( $this->sessionMtt->operacion->detalles ) )
            {
            for ( $index = 0;
                        $index < count( $this->sessionMtt->operacion->detalles );
                        $index++ )
                {
                if ( !empty( $this->sessionMtt->operacion->detalles[$index] ) )
                    {

                    $id = $this->sessionMtt->operacion->detalles[$index]->getId();
                    if (
                            $id
                            == $operacionDetalle[$id]['equipo_id']
                    )
                        {

                        if ( !is_null( $operacionDetalle[$id]['id_equipo_check'] ) )
                            {
                            $this->sessionMtt->operacion->detalles[$index] = array( );
                            }
                        }
                    }
                }
            }
        }


    public function fillDetalle( $operacionDetalle )
        {
        //$this->deleteItem( $operacionDetalle );
        $log = new Zend_Log();
        $writer = new Zend_Log_Writer_Stream(
                        APPLICATION_PATH . '/../logs/log.txt'
        );
        $log->addWriter( $writer );

        if ( count( $this->sessionMtt->operacion->detalles ) )
            {
            for ( $index = 0;
                        $index < count( $this->sessionMtt->operacion->detalles );
                        $index++ )
                {
                $log->log( $index , Zend_Log::INFO );
                if ( !empty( $this->sessionMtt->operacion->detalles[$index] ) )
                    {

                    $id = $this->sessionMtt->operacion->detalles[$index]->getId();
                    if (
                            $id
                            == $operacionDetalle[$id]['equipo_id']
                    )
                        {
                        $log->log( "es cierto" , Zend_Log::INFO );

                        if ( isset( $operacionDetalle[$id]['equipo'] ) )
                            {
                            $this->sessionMtt->operacion->detalles[$index]->setNombre(
                                    $operacionDetalle[$id]['equipo']
                            );
                            }

                        $this->sessionMtt->operacion->detalles[$index]->setEquipo_has_formaPago(
                                $operacionDetalle[$id]['equipo_has_formapago_id']
                        );
                        }
                    }
                }
            }

        $this->getTotal();
        }


    public function getExistDetalle( $operacionDetalle )
        {

        if ( count( $this->sessionMtt->operacion->detalles ) )
            {
            for ( $index = 0;
                        $index < count( $this->sessionMtt->operacion->detalles );
                        $index++ )
                {
                if ( !empty( $this->sessionMtt->operacion->detalles[$index] ) )
                    {
                    if (
                            $this->sessionMtt->operacion->detalles[$index]->getId()
                            == $operacionDetalle->getId()
                    )
                        {
                        return true;
                        }
                    }
                }
            return false;
            }
        return false;
        }


    public function addOperacionDetalle( $operacionDetalle )
        {

        if ( isset( $this->sessionMtt->operacion->detalles ) )
            {
            if ( !$this->getExistDetalle( $operacionDetalle ) )
                {
                $this->sessionMtt->operacion->detalles[] = $operacionDetalle;
                }
            }
        else
            {
            $this->sessionMtt->operacion->detalles[] = $operacionDetalle;
            }

        $this->getTotal();
        }


    public function getOperacionDetalles()
        {

        return $this->sessionMtt->operacion->detalles;

//        return !empty( $this->sessionMtt->operacion->detalles ) ?
//                $this->sessionMtt->operacion->detalles : array( );
        }


    public static function getDetalles()
        {

        return isset( $this->sessionMtt->operacion->detalles ) ?
                $this->sessionMtt->operacion->detalles : array( );
        }


    public function getTotal()
        {
        if ( isset( $this->sessionMtt->operacion->detalles ) &&
                count( $this->sessionMtt->operacion->detalles )
        )
            {
            $total = 0;

            for ( $index = 0;
                        $index < count( $this->sessionMtt->operacion->detalles );
                        $index++ )
                {
                if ( !empty( $this->sessionMtt->operacion->detalles[$index] ) )
                    {
                    $detalle = $this->sessionMtt->operacion->detalles[$index];
                    $total+= $detalle->getPrecio();
                    }
                }
            return $this->sessionMtt->operacion->total = $total;
            //return $this->sessionMtt->operacion->detalles;
            }
        }


    public function clearOperacionDetalles()
        {
        $this->sessionMtt->operacion->detalles = array( );
        }


    public function getDetallesOperacionActual()
        {

        $detallesVenta = $this->getVentaDetalles();
        $datalles_ids = array( );
        foreach ( $detallesVenta as $d )
            {
            $datalles_ids[] = $d['id_producto'];
            }
        $_producto = new Application_Model_Producto();

        if ( count( $datalles_ids ) )
            {
            $detallesProducto = $_producto->getDetalles( $datalles_ids );
            }
        else
            {
            $detallesProducto = array( );
            }

        $detallesProducto_porId = array( );

        foreach ( $detallesProducto as $p )
            {
            $detallesProducto_porId[$p['id_producto']] = $p;
            }
        $detallesProducto = $detallesProducto_porId;

        foreach ( $detallesVenta as $k => $d )
            {
            $detallesVenta[$k] = $detallesVenta[$k] + $detallesProducto[$d['id_producto']];
            }

        return $detallesVenta;
        }


    /* TODO Revisar este cambio */


    public function saveOperacionDetalle( $userId , $operacionId , $data )
        {
        $_equipo = new Mtt_Models_Bussines_Equipo();
        foreach ( $data as $item )
            {
            $detalle = array( );
            $dataUpdate = array( );
            if ( !empty( $item ) )
                {
                $detalle = array(
                    'operacion_id' => $operacionId ,
                    'equipo_id' => $item->getId() ,
                    'precio' => $item->getPrecio() ,
                    'equipo_has_formapago_id' => $item->getEquipo_has_formaPago()
                );
                $this->insert( $detalle );
                $dataUpdate = array(
                    'publicacionEquipo_id' =>
                    Mtt_Models_Bussines_PublicacionEquipo::Vendido
                );
                $_equipo->updateEquipo( $dataUpdate , $item->getId() );
                }
            }
        }


    public function addOperacion( $venta )
        {
        $venta['fechahora'] = date( 'Y-m-d H:i:s' );
        $venta_id = $this->insert( $venta );
        $_venta_detalle = new Application_Model_VentaDetalle();
        $_producto = new Application_Model_Producto();
        //var_dump($this->getVentaDetalles());exit;
        foreach ( $this->getVentaDetalles() as $venta_detalle )
            {
            $producto = $_producto->fetchRow( 'id=' . $venta_detalle['id_producto'] );
            $venta_detalle['id_venta'] = $venta_id;
            $venta_detalle['precio_venta'] = $producto['precio'];
            $_venta_detalle->insert( $venta_detalle );
            }
        $this->clearOperacionDetalles();
        }


    public function borrarDetalle( $i )
        {
        $S = new Zend_Session_Namespace( 'ventas' );
        $nuevo_detalle = array( );
        foreach ( $S->venta->detalles as $k => $detalle )
            {
            if ( $k != $i )
                {
                $nuevo_detalle[] = $detalle;
                }
            }
        $S->venta->detalles = $nuevo_detalle;
        }


    public function verDetalle( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name , array( ) )
                ->join(
                        'venta_detalle' , 'venta.id=venta_detalle.id_venta' ,
                        array(
                    'precio_venta' ,
                    'cantidad' ,
                    'total_venta' => '(precio_venta*cantidad)'
                        )
                )
                ->join(
                        'producto' , 'producto.id = venta_detalle.id_producto' ,
                        array(
                    'producto' => 'nombre' ,
                    'precio_actual' => 'precio'
                        )
                )
                ->joinLeft(
                        'categoria' , 'categoria.id = producto.id_categoria' ,
                        array(
                    'categoria' => 'nombre'
                        )
                )
                ->joinLeft(
                        'fabricante' ,
                        'fabricante.id = producto.id_fabricante' ,
                        array(
                    'fabricante' => 'nombre' ,
                    'ruc'
                        )
                )
                ->where( 'venta.id = ? ' , $id );
        return $db->fetchAll( $query );
        }


    }

