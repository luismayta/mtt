<?php


class Mtt_Models_Bussines_Usuario
        extends Mtt_Models_Table_Usuario
{

    const PASSWPRD_SALT = "asdw452112355" ;


    public function __construct( $config = array( ) )
    {

        parent::__construct( $config ) ;


    }


    public function auth( $login , $pwd )
    {

        $db = Zend_Db_Table_Abstract::getDefaultAdapter() ;
        $authAdapter = new Mtt_Auth_Adapter_DbTable_Mtt( $db ) ;

        $authAdapter->setIdentity( $login ) ;
        $authAdapter->setCredential( $pwd ) ;

        $auth = Zend_Auth::getInstance() ;

        $result = $auth->authenticate( $authAdapter ) ;

        $isValid = $result->isValid() ;

        if ( $isValid )
        {
            $authStorage = $auth->getStorage() ;
            $_user = $this->getByRol( $login ) ;
            $authStorage->write(
                    array(
                        'usuario' => $authAdapter->getResultRowObject(
                                null ,
                                'clave'
                        ) ,
                        'loginAt' => date( 'Y-m-d H:i:s' ) ,
                        'rol' => $_user->rol
                    )
            ) ;
        }


        return $isValid ;


    }


    public function getLastUserRegistred( $limit = 5 , $order = 'desc' )
    {
        $date = new Zend_Date( ) ;
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name ,
                        array(
                    'id' ,
                    'nombre_user' => 'usuario.nombre' ,
                    'apellido' ,
                    'login' ,
                    'fecharegistro'
                        )
                )
                ->joinInner( 'paises' ,
                             'paises.id = usuario.paises_id ' ,
                             array( 'pais' => 'paises.nombre' )
                )
                ->where( 'usuario.active IN (?)' ,
                         self::ACTIVE )
                ->where( 'usuario.tipousuario_id  = ?' ,
                         Mtt_Models_Table_TipoUsuario::USER )
                ->order( 'usuario.fecharegistro ' . $order )
                ->limit( $limit )
                ->query() ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function findLogin( $login )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'login' , 'tipousuario_id' )
                )
                ->where( 'usuario.active = ?' ,
                         self::ACTIVE )
                ->where( 'usuario.login = ?' ,
                         $login )
                ->query() ;
        ;
        return $query->fetchObject() ;


    }


    public function search( $term )
    {
        $db = $this->getAdapter() ;

        $query = $db->select()
                ->from( $this->_name ,
                        'nombre' )
                ->joinInner( 'tipousuario' ,
                             'tipousuario.id = usuario.tipousuario_id' ,
                             array( 'tipousuario.nombre as rol' )
                )
                ->joinInner( 'paises' ,
                             'paises.id = usuario.paises_id' ,
                             array( 'paises.nombre as pais' )
                )
                ->where( "usuario.nombre LIKE '$term%'" )
                ->where( 'usuario.active in (?)' ,
                         self::ACTIVE )
                ->query()
        ;

        $filas = $query->fetchAll( Zend_Db::FETCH_OBJ ) ;

        $values = array( ) ;
        foreach ( $filas as $fila )
        {
            $values[] = $fila->nombre ;
        }
        return $values ;


    }


    public function listArray()
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->joinInner( 'tipousuario' ,
                             'tipousuario.id = usuario.tipousuario_id' ,
                             array( 'tipousuario.nombre as rol' )
                )
                ->joinInner( 'paises' ,
                             'paises.id = usuario.paises_id' ,
                             array( 'paises.nombre as pais' )
                ) ;


        $query->query() ;
        return $db->fetchAll( $query ) ;


    }


    public function findByNombre( $active , $nombre = null )
    {

        $db = $this->getAdapter() ;
        $db->setFetchMode( Zend_Db::FETCH_OBJ ) ;
        $query = $db->select()
                ->from( $this->_name )
                ->joinInner( 'tipousuario' ,
                             'tipousuario.id = usuario.tipousuario_id' ,
                             array( 'tipousuario.nombre as rol' )
                )
                ->joinInner( 'paises' ,
                             'paises.id = usuario.paises_id' ,
                             array( 'paises.nombre as pais' )
                )
                ->where( 'usuario.active in (?)' ,
                         $active ) ;

        if ( isset( $nombre ) )
        {
            $query->where( "usuario.nombre LIKE '$nombre%'" ) ;
        }

        ;
        $query->query() ;
        return $db->fetchAll( $query ) ;


    }


    /**
     *
     * @param type $checkMail
     */
    public function getByValidacionEmail( $checkMail )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name
                )
                ->where( 'active IN (?)' ,
                         self::DESACTIVATE )
                ->where( 'activacion IN (?)' ,
                         $checkMail )
                ->query() ;

        return $query->fetchObject() ;


    }


    public function getPaginator()
    {
        $p = Zend_Paginator::factory( $this->fetchAll() ) ;
        $p->setItemCountPerPage( 3 ) ;
        return $p ;


    }


    public function listarArray()
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name ,
                        array(
                    'id' ,
                    'nombre' ,
                    'apellido' ,
                    'email' ,
                    'login' ,
                    'fecharegistro' ,
                    'direccion' ,
                    'codpostal' ,
                    'ciudad' ,
                    'institucion' ,
                    'active' ,
                    'acreditado'
                ) )
                ->joinInner( 'tipousuario' ,
                             'tipousuario.id = usuario.tipousuario_id' ,
                             array( 'tipousuario.nombre as rol' ) )
                ->joinInner( 'paises' ,
                             'paises.id = usuario.paises_id' ,
                             array( 'paises.nombre as pais' ) )
                ->where( 'usuario.tipousuario_id = ?' ,
                         Mtt_Models_Bussines_TipoUsuario::USER
                )
                //->where( 'usuario.active = ?' , self::ACTIVE )
                ->query()
        ;

        return $query->fetchAll() ;
        //Zend_Db::FETCH_OBJ


    }


    public function listar()
    {

        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name ,
                        array(
                    'id' ,
                    'nombre' ,
                    'apellido' ,
                    'email' ,
                    'login' ,
                    'fecharegistro' ,
                    'direccion' ,
                    'codpostal' ,
                    'ciudad' ,
                    'institucion' ,
                    'active'
                ) )
                ->joinInner( 'tipousuario' ,
                             'tipousuario.id = usuario.tipousuario_id' ,
                             array( 'tipousuario.nombre as rol' ) )
                ->joinInner( 'paises' ,
                             'paises.id = usuario.paises_id' ,
                             array( 'paises.nombre as pais' ) )
                ->where( 'usuario.tipousuario_id = ?' ,
                         Mtt_Models_Bussines_TipoUsuario::USER
                )
                //->where( 'usuario.active = ?' , self::ACTIVE )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function pagListUser( $active = null , $nombre = null )
    {
        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/myConfigAdmin.ini' ,
                        'usuario'
                ) ;
        $data = $_conf->toArray() ;

        $value = ($active == null) ? array( 0 , 1 ) : 1 ;

        $object = Zend_Paginator::factory(
                        $this->findByNombre( $value ,
                                             $nombre )
                ) ;
        $object->setItemCountPerPage(
                $data['ItemCountPerPage']
        ) ;
        return $object ;


    }


    public function listarRegistrados()
    {

        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name ,
                        array(
                    'id' ,
                    'nombre' ,
                    'apellido' ,
                    'email' ,
                    'login' ,
                    'fecharegistro' ,
                    'direccion' ,
                    'codpostal' ,
                    'ciudad' ,
                    'institucion' ,
                    'active'
                ) )
                ->joinInner( 'tipousuario' ,
                             'tipousuario.id = usuario.tipousuario_id' ,
                             array( 'tipousuario.nombre as rol' ) )
                ->joinInner( 'paises' ,
                             'paises.id = usuario.paises_id' ,
                             array( 'paises.nombre as pais' ) )
                ->where( 'usuario.active = ?' ,
                         self::ACTIVE )
                ->where( 'usuario.tipousuario_id = ?' ,
                         Mtt_Models_Bussines_TipoUsuario::REGISTERED
                )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function countRegistrados()
    {

        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name ,
                        array(
                    'cantidad' => 'COUNT(id)'
                        )
                )
                ->where( 'usuario.active = ?' ,
                         self::ACTIVE )
                ->where( 'usuario.tipousuario_id = ?' ,
                         Mtt_Models_Bussines_TipoUsuario::REGISTERED
                )
                ->query()
        ;

        return $query->fetchObject() ;


    }


    public function getTratamientosUsuario()
    {

        $tratamientos = array( '1' => 'Sr' , '2' => 'Sra/Srta' ) ;
        return $tratamientos ;


    }


    public function updateUsuario( array $data , $id )
    {
        $data['fechamodificacion'] = date( 'Y-m-d H:i:s' ) ;
        $this->update( $data ,
                       'id = ' . $id ) ;


    }


    public function saveUsuario( array $data )
    {
        $passwordUser = $data["clave"] ;

        $activacion = Mtt_Auth_Adapter_DbTable_Mtt::generatePassword(
                        $data["login"]
                ) ;

        $valuesDefault = array(
            "clave" => Mtt_Auth_Adapter_DbTable_Mtt::generatePassword(
                    $data["clave"]
            ) ,
            "tipousuario_id" => Mtt_Models_Bussines_TipoUsuario::REGISTERED ,
            "fecharegistro" =>
            Zend_Date::now()->toString(
                    "YYYY-MM-dd hh-mm-ss"
            ) ,
            "ultimavisita" => Zend_Date::now()->toString(
                    "YYYY-MM-dd hh-mm-ss"
            ) ,
            "activacion" => $activacion
                ) ;


        unset( $data["clave_2"] ) ;
        unset( $data["clave"] ) ;
        unset( $data["captcha"] ) ;

        $usuario = array_merge( $valuesDefault ,
                                $data ) ;

        if ( ( $this->insert( $usuario ) ) )
        {

            $arrayNew = array(
                'password' => $passwordUser ,
                'activacion' => $activacion
                    ) ;

            $data = array_merge( $arrayNew ,
                                 $data ) ;

            $this->sendMail( $data ,
                             'Welcome to Medtechtrade' ) ;
        }


    }


    public function deleteUsuario( $id )
    {

        $this->delete( 'id = ?' ,
                       $id ) ;


    }


    public function activarUsuario( $id )
    {

        $this->update( array( "active" => self::ACTIVE ) ,
                       'id = ' . $id ) ;


    }


    public function desactivarUsuario( $id )
    {

        $this->update( array( "active" => self::DESACTIVATE ) ,
                       'id = ' . $id ) ;


    }


    public function acreditarUsuario( $id )
    {

        $this->update( array( "acreditado" => self::ACTIVE ) ,
                       'id = ' . $id ) ;


    }


    public function desacreditarUsuario( $id )
    {

        $this->update( array( "acreditado" => self::DESACTIVATE ) ,
                       'id = ' . $id ) ;


    }


    public function habilitarUsuario( $id )
    {

        $this->update(
                array(
            "tipousuario_id" => Mtt_Models_Table_TipoUsuario::USER ) ,
                'id = ' . $id ) ;


    }


    public function activeUsuario( $validacion )
    {
        $data = $this->getByValidacionEmail( $validacion ) ;
        if ( isset( $data ) && !is_null( $data ) && is_object( $data ) )
        {
            $this->updateUsuario(
                    array( 'active' => self::ACTIVE )
                    ,
                    $data->id
            ) ;
            return $data ;
        }
        return false ;


    }


    public function changePassword( $id , $password )
    {

        $this->update(
                array(
            "clave" => $password ) ,
                'id = ' . $id ) ;


    }


    /**
     * para enviar correo de autorizacion
     * @param array $data
     * @param string $subject
     */
    public function sendMail( array $data , $subject )
    {

        $_conf = new Zend_Config_Ini( APPLICATION_PATH . '/configs/mail.ini' , APPLICATION_ENV ) ;

        Zend_Mail::setDefaultFrom(
                $_conf->email->contacto ,
                $_conf->data->contacto
        ) ;

        $objSmtp = new Zend_Mail_Transport_Smtp(
                        $_conf->config->smtp ,
                        $_conf->config->toArray()
                ) ;
        Zend_Mail::setDefaultReplyTo(
                $_conf->email->contacto ,
                $_conf->data->contacto
        ) ;

        Zend_Mail::setDefaultTransport( $objSmtp ) ;


        $dataUser = $data['nombre'] . '  ' . $data['apellido'] ;


        $m = new Mtt_Html_Mail_Mailer(
                        $_conf->email->contacto ,
                        $_conf->data->contacto
                ) ;
        $m->setSubject( $subject ) ;

        $m->addTo( $data['email'] ) ;

        $m->setViewParam( 'usuario' ,
                          $dataUser )
                ->setViewParam( 'login' ,
                                $data['login'] )
                ->setViewParam( 'clave' ,
                                $data['password'] )
                ->setViewParam( 'enlace' ,
                                $data['activacion'] ) ;
        ;
        $m->sendHtmlTemplate( "index.phtml" ) ;


    }


    /**
     * para enviar correo de autorizacion
     * @param array $data
     * @param string $subject
     */
    public function sendMailToAdmin( array $data , $subject )
    {
        $_conf = new Zend_Config_Ini( APPLICATION_PATH . '/configs/mail.ini' , APPLICATION_ENV ) ;

        Zend_Mail::setDefaultFrom(
                $_conf->email->admin ,
                $_conf->data->admin
        ) ;

        $objSmtp = new Zend_Mail_Transport_Smtp(
                        $_conf->config->smtp ,
                        $_conf->config->toArray()
                ) ;
        Zend_Mail::setDefaultReplyTo(
                $_conf->email->admin ,
                $_conf->data->admin
        ) ;

        Zend_Mail::setDefaultTransport( $objSmtp ) ;
        Zend_Mail::setDefaultFrom(
                $_conf->email->admin ,
                $_conf->data->admin
        ) ;

        Zend_Mail::setDefaultReplyTo(
                $_conf->email->admin ,
                $_conf->data->admin
        ) ;

        $m = new Mtt_Html_Mail_Mailer() ;
        $m->setSubject( $data['asunto'] ) ;

        $m->addTo( $_conf->email->admin ) ;

        $m->setViewParam( 'nombre' ,
                          $data['nombre'] )
                ->setViewParam( 'email' ,
                                $data['email'] )
                ->setViewParam( 'comentario' ,
                                $data['comentario'] )
        ;
        $m->sendHtmlTemplate( "contacttoadmin.phtml" ) ;


    }


    /**
     *
     * @param type $login
     * @return type
     */
    public function getByRol( $login )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name ,
                        array(
                    'id' ,
                    'nombre' ,
                    'apellido' ,
                    'email' ,
                    'login' ,
                    'clave' ,
                    'active'
                        )
                )
                ->joinInner( 'tipousuario' ,
                             'tipousuario.id = usuario.tipousuario_id' ,
                             array(
                    'rol' => 'nombre'
                        )
                )
                ->where( 'usuario.active =?' ,
                         self::ACTIVE )
                ->where( 'usuario.login =?' ,
                         $login )
                ->query()
        ;

        return $query->fetchObject() ;


    }


    /**
     *
     * @param <type> $email
     * @return <type> 
     */
    public function getByEmail( $email )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name ,
                        array(
                    'id' ,
                    'nombre' ,
                    'apellido' ,
                    'email' ,
                    'login' ,
                    'clave' ,
                    'active'
                        )
                )
                ->where( 'usuario.active =?' ,
                         self::ACTIVE )
                ->where( 'usuario.email =?' ,
                         $email )
                ->query()
        ;

        return $query->fetchObject() ;


    }


    public function sendClaveMail( array $data , $subject )
    {

        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/mail.ini' , APPLICATION_ENV
                ) ;

        Zend_Mail::setDefaultFrom(
                $_conf->email->contacto ,
                $_conf->data->contacto
        ) ;

        $objSmtp = new Zend_Mail_Transport_Smtp(
                        $_conf->config->smtp ,
                        $_conf->config->toArray()
                ) ;
        Zend_Mail::setDefaultReplyTo(
                $_conf->email->contacto ,
                $_conf->data->contacto
        ) ;

        Zend_Mail::setDefaultTransport( $objSmtp ) ;


        $dataUser = $data['usuario'] ;


        $m = new Mtt_Html_Mail_Mailer(
                        $_conf->email->contacto ,
                        $_conf->data->contacto
                ) ;
        $m->setSubject( $subject ) ;

        $m->addTo( $data['email'] ) ;

        $m->setViewParam( 'usuario' ,
                          $dataUser )
                ->setViewParam( 'login' ,
                                $data['login'] )
                ->setViewParam( 'clave' ,
                                $data['password'] ) ;
        ;
        $m->sendHtmlTemplate( "mailrecovery.phtml" ) ;


    }


    public function recoveryPassword( $email )
    {

        $objUsuario = $this->getByEmail( $email ) ;

        if ( is_object( $objUsuario ) )
        {
            $claveTexto = Mtt_Tools_Text::generarPassword() ;
            $clave = Mtt_Auth_Adapter_DbTable_Mtt::generatePassword(
                            $claveTexto
                    ) ;

            $this->changePassword( $objUsuario->id ,
                                   $clave ) ;

            $data = array(
                'usuario' => $objUsuario->nombre . ' ' . $objUsuario->apellido ,
                'login' => $objUsuario->login ,
                'email' => $objUsuario->email ,
                'password' => $claveTexto
                    ) ;
            $this->sendClaveMail( $data ,
                                  'recovery password' ) ;
            return true ;
        }
        else
        {
            return FALSE ;
        }


    }


}

