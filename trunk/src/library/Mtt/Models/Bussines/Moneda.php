<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Bussines_Moneda
		extends Mtt_Models_Table_Moneda
{


	public function __construct( $config = array( ) )
	{

		parent::__construct( $config ) ;


	}


	public function getComboValuesCodePais()
	{
		$db = $this->getAdapter() ;
		$query = $db->select()
						->from( $this->_name )
						->where( 'active = ?' , self::ACTIVE )
						->order( 'nombre' )
						->query()
		;
		$filas = $query->fetchAll( Zend_Db::FETCH_ASSOC ) ;

		$values = array( ) ;
		foreach ( $filas as $fila )
		{
			$values[$fila['codepais']] = $fila['nombre'] ;
		}
		return $values ;


	}


	public function getComboValues()
	{
		$db = $this->getAdapter() ;
		$query = $db->select()
						->from( $this->_name )
						->where( 'active = ?' , self::ACTIVE )
						->order( 'nombre' )
						->query()
		;
		$filas = $query->fetchAll( Zend_Db::FETCH_ASSOC ) ;

		$values = array( ) ;
		foreach ( $filas as $fila )
		{
			$values[$fila['id']] = $fila['nombre'] ;
		}
		return $values ;


	}


	public function getComboValuesCurrency()
	{
		$db = $this->getAdapter() ;
		$query = $db->select()
						->from( $this->_name )
						->where( 'id <> ?' , 0 )
						->where( 'nombre <> ?' , '-' )
						->where( 'active = ?' , self::ACTIVE )
						->order( 'nombre' )
						->query()
		;
		$filas = $query->fetchAll( Zend_Db::FETCH_ASSOC ) ;

		$values = array( ) ;
		foreach ( $filas as $fila )
		{
			$values[$fila['id']] = $fila['nombre'] ;
		}
		return $values ;


	}


	public function getFindId( $id )
	{
		$db = $this->getAdapter() ;
		$query = $db->select()
						->from( $this->_name )
						->where( 'id = ?' , $id )
						->where( 'active = ?' , self::ACTIVE )
						->query()
		;
		return $query->fetchObject() ;


	}


	public function listar( $active )
	{
		$db = $this->getAdapter() ;
		$query = $db->select()
						->from( $this->_name )
						->where( 'active in (?)' , $active )
						->query()
		;

		return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


	}


	public function pagListMoneda( $active = null )
	{
		$_conf = new Zend_Config_Ini(
						APPLICATION_PATH . '/configs/myConfigAdmin.ini'
						, 'moneda'
		) ;
		$data = $_conf->toArray() ;

		$value = ($active == null)
				? array( 0 , 1 )
				: 1 ;

		$object = Zend_Paginator::factory( $this->listar( $value ) ) ;
		$object->setItemCountPerPage(
				$data['ItemCountPerPage']
		) ;
		return $object ;


	}


	public function updateMoneda( array $data ,
							   $id )
	{

		$this->update( $data , 'id = ' . $id ) ;


	}


	public function saveMoneda( array $data )
	{

		$this->insert( $data ) ;


	}


	public function deleteMoneda( $id )
	{

		$this->delete( 'id =' . ( int ) $id ) ;


	}


	public function activarMoneda( $id )
	{

		$this->update( array( "active" => self::ACTIVE ) , 'id = ' . $id ) ;


	}


	public function desactivarMoneda( $id )
	{

		$this->update( array( "active" => self::DESACTIVATE ) , 'id = ' . $id ) ;


	}


}
