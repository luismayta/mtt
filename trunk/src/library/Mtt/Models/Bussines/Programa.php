<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Bussines_Programa
        extends Mtt_Models_Table_Programa
{


    public function __construct( $config = array( ) )
    {

        parent::__construct( $config ) ;


    }


    public function listar()
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active = ?' ,
                         '1' )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function savePrograma( array $data )
    {
        $registro = array( ) ;
        for ( $i = 1 ; $i <= Mtt_Models_Table_Alerta::NAlertas ; $i++ )
        {

            $obj = "alerta" . $i ;

            $registro['usuario_id'] = $data['usuario_id'] ;
            $registro['tipo'] = $i ;
            $registro['active'] = $data[$obj] ;
            $registro['fecharegistro'] = date( 'Y-m-d H:i:s' ) ;

            $registro['detalle'] = ($i == 2) ?
                    implode( ',' ,
                             $data['categorias'] ) : null ;
            $this->insert( $registro ) ;
        }


    }


    public function updatePrograma( array $data , array $dataUsuario )
    {
        $registro = array( ) ;
        $alerta = array( ) ;
        foreach ( $dataUsuario as $fila )
        {
            $alerta[$fila->tipo] = $fila ;
        }
        for ( $i = 1 ; $i <= Mtt_Models_Table_Alerta::NAlertas ; $i++ )
        {

            $obj = "alerta" . $i ;
            $registro['active'] = $data[$obj] ;
            $registro['fechamodificacion'] = date( 'Y-m-d H:i:s' ) ;

            if ( isset( $data['categorias'] ) )
            {
                $registro['detalle'] = ($i == 2) ?
                        implode( ',' ,
                                 $data['categorias'] ) : null ;
            }
            $this->update( $registro ,
                           'id = ' . $alerta[$i]->id ) ;
        }


    }


    public function deletePrograma( $id )
    {

        $this->delete( 'id = ?' ,
                       $id ) ;


    }


    public function activarPrograma( $id )
    {

        $this->update( array( "active" => self::ACTIVE ) ,
                       'id = ' . $id ) ;


    }


    public function desactivarPrograma( $id )
    {

        $this->update( array( "active" => self::DESACTIVATE ) ,
                       'id = ' . $id ) ;


    }


}
