<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Bussines_Paises
        extends Mtt_Models_Table_Paises
{


    public function __construct( $config = array( ) )
    {

        parent::__construct( $config ) ;


    }


    public function listIntegrate()
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name ,
                        array( 'id' , 'nombre' , 'idioma' )
                )
                ->where( 'active = ?' ,
                         self::ACTIVE )
                ->where( 'eliminado = ?' ,
                         self::DESACTIVATE )
                ->where( 'integrate =?' ,
                         self::ACTIVE )
                ->order( 'nombre' )
                ->query()
        ;
        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function getComboValues()
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active = ?' ,
                         self::ACTIVE )
                ->where( 'eliminado = ?' ,
                         self::DESACTIVATE )
                ->order( 'nombre' )
                ->query()
        ;
        $filas = $query->fetchAll( Zend_Db::FETCH_ASSOC ) ;

        $values = array( ) ;
        foreach ( $filas as $fila )
        {
            $values[$fila['id']] = $fila['nombre'] ;
        }
        return $values ;


    }


    public function getComboValuesIntegrate()
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active = ?' ,
                         self::ACTIVE )
                ->where( 'eliminado = ?' ,
                         self::DESACTIVATE )
                ->where( 'integrate =?' ,
                         self::ACTIVE )
                ->order( 'nombre' )
                ->query()
        ;
        $filas = $query->fetchAll( Zend_Db::FETCH_ASSOC ) ;

        $values = array( ) ;
        foreach ( $filas as $fila )
        {
            $values[$fila['id']] = $fila['nombre'] ;
        }
        return $values ;


    }


    public function search( $term )
    {
        $db = $this->getAdapter() ;

        $query = $db->select()
                ->from( $this->_name ,
                        'nombre' )
                ->where( "nombre LIKE '$term%'" )
                ->where( 'active in (?)' ,
                         self::ACTIVE )
                ->where( 'eliminado = ?' ,
                         self::DESACTIVATE )
                ->query()
        ;

        $filas = $query->fetchAll( Zend_Db::FETCH_OBJ ) ;

        $values = array( ) ;
        foreach ( $filas as $fila )
        {
            $values[] = $fila->nombre ;
        }
        return $values ;


    }


    public function findByNombre( $active , $nombre = null )
    {

        $db = $this->getAdapter() ;
        $db->setFetchMode( Zend_Db::FETCH_OBJ ) ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active in (?)' ,
                         $active )
                ->where( 'eliminado = ?' ,
                         self::DESACTIVATE ) ;
        //->query();
        if ( isset( $nombre ) )
        {
            $query->where( "nombre LIKE '$nombre%'" ) ;
        }

        $query->order( 'nombre asc ' ) ;
        $query->query() ;
        return $db->fetchAll( $query ) ;


    }


    public function getPaginator()
    {
        $p = Zend_Paginator::factory( $this->fetchAll() ) ;
        $p->setItemCountPerPage( 3 ) ;
        return $p ;


    }


    public function listar( $active = null )
    {
        $value = ($active == null) ? array( 0 , 1 ) : 1 ;
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active IN (?)' ,
                         $value )
                ->where( 'eliminado = (?)' ,
                         self::DESACTIVATE )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function pagListCountries( $active = null , $nombre = null )
    {
        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/myConfigAdmin.ini'
                        , 'pais'
                ) ;
        $data = $_conf->toArray() ;

        $value = ($active == null) ? array( 0 , 1 ) : 1 ;

        $object = Zend_Paginator::factory(
                        $this->findByNombre( $value ,
                                             $nombre )
                ) ;
        $object->setItemCountPerPage(
                $data['ItemCountPerPage']
        ) ;
        return $object ;


    }


    public function getFindId( $id , $active = 1 )
    {

        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'id = ?' ,
                         $id )
                ->where( 'active IN (?)' ,
                         $active )
                ->query()
        ;
        return $query->fetchObject() ;


    }


    public function updatePais( array $data , $id )
    {

        $this->update( $data ,
                       'id = ' . $id ) ;


    }


    public function savePais( array $data )
    {
        $slug = new Mtt_Filter_Slug( array(
                    'field' => 'slug' ,
                    'model' => $this
                        ) ) ;

        $dataNew = array(
            'slug' => $slug->filter( $data['nombre'] )
                ) ;

        $data = array_merge( $dataNew ,
                             $data ) ;

        $this->insert( $data ) ;


    }


    public function deletePais( $id )
    {

        //$this->delete( 'id =' . ( int ) $id );
        $this->update( array(
            "active" => self::DESACTIVATE ,
            "eliminado" => self::ACTIVE )
                ,
                       'id = ' . $id ) ;


    }


    public function activarPais( $id )
    {

        $this->update( array(
            "active" => self::ACTIVE )
                ,
                       'id = ' . $id ) ;


    }


    public function desactivarPais( $id )
    {

        $this->update( array(
            "active" => self::DESACTIVATE )
                ,
                       'id = ' . $id ) ;


    }


}
