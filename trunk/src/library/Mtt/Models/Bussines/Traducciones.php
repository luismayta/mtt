<?php


/**
 * 
 */
class Mtt_Models_Bussines_Traducciones
        extends Mtt_Models_Table_Traducciones
{

//
//    public function __construct( $config = array( ) )
//        {
//
//        parent::__construct( $config );
//        }


    /**
     *
     * @param type $entidad_id
     * @param type $campo_id
     * @param type $idioma_id
     * @param type $value string
     * @return type devuelve el valor de la traduccion
     */
    public function getTranslate(
    $entidad_id , $campo_id , $idioma_id , $value
    )
    {


        $translate = $this->getTraduccionByEntidadCampoIdioma( $entidad_id ,
                                                               $campo_id ,
                                                               $idioma_id ) ;

        if (
                is_object( $translate ) && !empty( $translate->texto )
        )
        {
            $value = $translate->texto ;
        }

        return $value ;


    }


    /**
     * devuelve un objeto
     * 
     * @param type $entidad_id
     * @param type $campo_id
     * @param type $idioma_id
     * @return type object
     */
    public function getTraduccionByEntidadCampoIdioma(
    $entidad_id , $campo_id , $idioma_id
    )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'entidades_id IN (?)' ,
                         $entidad_id )
                ->where( 'campo_id IN (?)' ,
                         $campo_id )
                ->where( 'idiomas_id IN (?)' ,
                         $idioma_id )
                ->where( 'active = ?' ,
                         self::ACTIVE )
                ->query()
        ;
        return $query->fetchObject() ;


    }


    public function getComboValues()
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active = ?' ,
                         self::ACTIVE )
                ->order( 'nombre' )
                ->query()
        ;
        $filas = $query->fetchAll( Zend_Db::FETCH_ASSOC ) ;

        $values = array( ) ;
        foreach ( $filas as $fila )
        {
            $values[$fila['id']] = $fila['nombre'] ;
        }
        return $values ;


    }


    public function findByNombre( $active , $nombre = null )
    {

        $db = $this->getAdapter() ;
        $db->setFetchMode( Zend_Db::FETCH_OBJ ) ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active in (?)' ,
                         $active ) ;

        if ( isset( $nombre ) )
        {
            $query->where( "nombre LIKE '$nombre%'" ) ;
        }

        ;
        $query->query() ;
        return $db->fetchAll( $query ) ;


    }


    public function getTraducciones( $id )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'id IN (?)' ,
                         $id )
                ->where( 'active = ?' ,
                         '1' )
                ->query()
        ;
        return $query->fetchObject() ;


    }


    public function search( $term )
    {
        $db = $this->getAdapter() ;

        $query = $db->select()
                ->from( $this->_name ,
                        'nombre' )
                ->where( "nombre LIKE '$term%'" )
                ->where( 'active in (?)' ,
                         self::ACTIVE )
                ->query()
        ;

        $filas = $query->fetchAll( Zend_Db::FETCH_OBJ ) ;

        $values = array( ) ;
        foreach ( $filas as $fila )
        {
            $values[] = $fila->nombre ;
        }
        return $values ;


    }


    public function updateTraducciones( array $data , $id )
    {

        $this->update( $data ,
                       'id = ' . $id ) ;


    }


    public function saveTraducciones( array $data )
    {
        $this->insert( $data ) ;


    }


    /**
     *
     * @param array $datos 
     */
    public function saveTraduccionesAll( array $datos )
    {
        $idiomas = new Mtt_Models_Bussines_Idioma() ;
        $dataSave = array( ) ;
        foreach ( $datos as $data )
        {
            $dataSave['entidades_id'] = $data['entidades_id'] ;
            $dataSave['campo_id'] = $data['campo_id'] ;
            $dataSave['nombrecampo'] = $data['nombrecampo'] ;
            foreach ( $idiomas->listar() as $idioma )
            {
                $dataSave['idiomas_id'] = $idioma->id ;
                $dataSave['texto'] = $data[$idioma->id] ;

                $this->insert( $dataSave ) ;
            }
        }


    }


    public function deleteTraducciones( $id )
    {

        $this->delete( 'id =' . ( int ) $id ) ;


    }


    public function activarTraducciones( $id )
    {

        $this->update( array(
            "active" => self::ACTIVE )
                ,
                       'id = ' . $id ) ;


    }


    public function desactivarTraducciones( $id )
    {

        $this->update( array(
            "active" => self::DESACTIVATE )
                ,
                       'id = ' . $id ) ;


    }


    public function getCheckExist( array $datos )
    {

        $idiomas = new Mtt_Models_Bussines_Idioma() ;
        $dataSave = array( ) ;
        $dataTraducciones = "" ;

        foreach ( $datos as $data )
        {
            $dataSave['entidades_id'] = $data['entidades_id'] ;
            $dataSave['campo_id'] = $data['campo_id'] ;
            $dataSave['nombrecampo'] = $data['nombrecampo'] ;

            foreach ( $idiomas->listar() as $idioma )
            {

                $dataTraducciones = $this->getTraduccionByEntidadCampoIdioma(
                        $dataSave['entidades_id'] ,
                        $dataSave['campo_id'] ,
                        $idioma->id
                ) ;
                $dataSave['idiomas_id'] = $idioma->id ;
                $dataSave['texto'] = $data[$idioma->id] ;

                if ( is_object( $dataTraducciones ) )
                {
                    $this->updateTraducciones(
                            $dataSave ,
                            $dataTraducciones->id
                    ) ;
                }
                else
                {
                    $this->saveTraducciones( $dataSave ) ;
                }
            }
        }

        return true ;


    }


}
