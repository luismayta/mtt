<?php


/**
 * @author slovacus@gmail.com
 */
class Mtt_Models_Bussines_Entidades
        extends Mtt_Models_Table_Entidades
{


    public function __construct( $config = array( ) )
    {

        parent::__construct( $config ) ;


    }


    public function getComboValues()
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active = ?' ,
                         self::ACTIVE )
                ->order( 'nombre' )
                ->query()
        ;
        $filas = $query->fetchAll( Zend_Db::FETCH_ASSOC ) ;

        $values = array( ) ;
        foreach ( $filas as $fila )
        {
            $values[$fila['id']] = $fila['nombre'] ;
        }
        return $values ;


    }


    public function findByNombre( $active , $nombre = null )
    {

        $db = $this->getAdapter() ;
        $db->setFetchMode( Zend_Db::FETCH_OBJ ) ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active in (?)' ,
                         $active ) ;

        if ( isset( $nombre ) )
        {
            $query->where( "nombre LIKE '$nombre%'" ) ;
        }

        ;
        $query->query() ;
        return $db->fetchAll( $query ) ;


    }


    public function getId( $id )
    {
        $db = $this->getAdapter() ;
        $db->setFetchMode( Zend_Db::FETCH_OBJ ) ;
        $query = $db->select()
                ->from( $this->_name )
                ->where(
                        'active in (?)' ,
                        Mtt_Models_Bussines_Entidades::ACTIVE
                )
                ->where(
                        'id in (?)' ,
                        $id
                )
                ->query()
        ;


        return $query->fetchObject() ;


    }


    public function listar( $active )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active in (?)' ,
                         $active )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function listCamposByEntidad( $entidad )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $entidad ,
                        array( 'id' , 'nombre' ) )
                ->where(
                        'active in (?)' ,
                        Mtt_Models_Bussines_Entidades::ACTIVE
                )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function updateEntidades( array $data , $id )
    {

        $this->update( $data ,
                       'id = ' . $id ) ;


    }


    public function saveEntidades( array $data )
    {

        $this->insert( $data ) ;


    }


    public function deleteEntidades( $id )
    {

        $this->delete( 'id =' . ( int ) $id ) ;


    }


    public function activarEntidades( $id )
    {

        $this->update( array( "active" => self::ACTIVE ) ,
                       'id = ' . $id ) ;


    }


    public function desactivarEntidades( $id )
    {

        $this->update( array( "active" => self::DESACTIVATE ) ,
                       'id = ' . $id ) ;


    }


}
