<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Bussines_AlertaEmail
        extends Mtt_Models_Table_AlertaEmail
{


    public function __construct( $config = array( ) )
    {

        parent::__construct( $config ) ;


    }


    public function getEquipmentsPublication()
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name ,
                        array( 'equipo_id' )
                )
                ->joinInner( 'equipo'
                        ,
                             'alerta_email.equipo_id = equipo.id' ,
                             array(
                    'nombre' , 'precioventa' ,
                    'categoria_id' , 'fabricantes_id' ,
                    'modelo' , 'fechafabricacion'
                        )
                )
                ->joinInner( 'fabricantes'
                        ,
                             'equipo.fabricantes_id = fabricantes.id'
                        ,
                             array(
                    'fabricante' => 'nombre'
                        )
                )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function saveAlertaEmail( array $data )
    {
        $this->insert( $data ) ;


    }


    /**
     * para enviar correo de autorizacion
     * @param array $data
     * @param string $subject
     */
    public function sendMailToAlert( array $data , $subject , array $emails )
    {
        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/mail.ini' ,
                        APPLICATION_ENV
        ) ;


        Zend_Mail::setDefaultFrom(
                $_conf->email->contacto ,
                $_conf->data->contacto
        ) ;

        $objSmtp = new Zend_Mail_Transport_Smtp(
                        $_conf->config->smtp ,
                        $_conf->config->toArray()
                ) ;
        Zend_Mail::setDefaultReplyTo(
                $_conf->email->contacto ,
                $_conf->data->contacto
        ) ;

        Zend_Mail::setDefaultTransport( $objSmtp ) ;

        $_confUrl = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/myConfig.ini' , 'url_desarrollo'
        ) ;

        $confUrl = $_confUrl->toArray() ;
        $urlBase = $confUrl['Url'] ;

        $m = new Mtt_Html_Mail_Mailer(
                        $_conf->email->contacto , $_conf->data->contacto
        ) ;
        $m->setSubject( $subject ) ;

        //$m->addTo( $confMail['emails'] );
        $m->addTo( $confMail['email'] ) ;
        foreach ( $emails as $email )
        {
            $m->addTo( $email->email ) ;
        }

        $m->setViewParam( 'equipo' ,
                          $data['equipo'] )
                ->setViewParam( 'url' ,
                                $urlBase . '/equipment/' . $data['url'] )

        ;

        $m->sendHtmlTemplate( "sendalert.phtml" ) ;


    }


    public function getAlertsToEmails( array $data )
    {
        $db = $this->getAdapter() ;
        $query =
                'SELECT email FROM  alerta ' .
                'INNER JOIN usuario ON alerta.usuario_id = usuario.id ' .
                'WHERE alerta.active = 1 AND tipo = 3 ' .
                ' UNION ' .
                'SELECT email FROM alerta ' .
                'INNER JOIN usuario ON alerta.usuario_id = usuario.id ' .
                'WHERE detalle IN (SELECT categoria_id FROM equipo WHERE id = ' .
                $data['id'] . ') ' .
                'UNION ' .
                'SELECT email FROM busqueda  ' .
                'INNER JOIN usuario ON busqueda.usuario_id = usuario.id ' .
                'WHERE "' .
                $data['nombre'] . '" LIKE ' .
                'CONCAT("%", busqueda.palabras_busqueda, "%") AND "' .
                $data['modelo'] . '" LIKE CONCAT("%", busqueda.modelo,"%") AND "' .
                $data['fabricante'] . '" LIKE CONCAT("%", busqueda.fabricante,"%")' .
                'AND CASE busqueda.categoria_id ' .
                'WHEN -1 THEN busqueda.categoria_id LIKE "%%" ELSE "' .
                $data['categoria_id'] . '" = busqueda.categoria_id END  ' .
                'AND ' . $data['fechafabricacion'] . ' > busqueda.anio_inicio AND ' .
                'CASE busqueda.anio_fin WHEN -1 THEN busqueda.anio_fin LIKE "%%" 
                ELSE ' . $data['fechafabricacion'] . '< busqueda.anio_fin END AND 
            CASE busqueda.precio_inicio WHEN -1 THEN busqueda.precio_inicio 
                LIKE "%%" ELSE busqueda.precio_inicio < ' .
                $data['precioventa'] . ' END 
            AND CASE busqueda.precio_fin WHEN -1 THEN busqueda.precio_fin 
                LIKE "%%" ELSE busqueda.precio_fin > ' . $data['precioventa'] .
                ' END 
            AND busqueda.active = 1 ' ;
        //return $query;
        return $db->query( $query )->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function deleteAlertaEmail( $id )
    {

        $this->delete( 'id = ?' ,
                       $id ) ;


    }


}
