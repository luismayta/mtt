<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Bussines_Page
        extends Mtt_Models_Table_Page
{


    public function __construct( $config = array( ) )
    {

        parent::__construct( $config ) ;


    }


    public function getFindId( $id , $active = null )
    {
        $value = ($active == null) ? array( 0 , 1 ) : 1 ;

        $db = $this->getAdapter() ;

        $query = $db->select()
                ->from( $this->_name )
                ->where( 'id = ?' ,
                         $id )
                ->where( 'active IN (?)' ,
                         $value )
                ->query()
        ;
        return $query->fetchObject() ;


    }


    /**
     * 
     */
    public function listPage( $active = null )
    {
        $value = ($active == null) ? array( 0 , 1 ) : 1 ;
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name ,
                        array(
                    'id' ,
                    'nombre' ,
                    'active'
                        )
                )
                ->where( 'page.active in (?)' ,
                         $value )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function updatePage( array $data , $id )
    {

        $this->update( $data ,
                       'id = ' . $id ) ;


    }


    public function savePage( array $data )
    {

        $this->insert( $data ) ;


    }


    public function deletePage( $id )
    {

        $this->delete( 'id =' . ( int ) $id ) ;


    }


    public function activarPage( $id )
    {

        $this->update( array( "active" => self::ACTIVE ) ,
                       'id = ' . $id ) ;


    }


    public function desactivarPage( $id )
    {

        $this->update( array( "active" => self::DESACTIVATE ) ,
                       'id = ' . $id ) ;


    }


}
