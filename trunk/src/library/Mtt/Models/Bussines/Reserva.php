<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Bussines_Reserva
        extends Mtt_Models_Table_Reserva
{


    public function __construct( $config = array( ) )
    {

        parent::__construct( $config ) ;


    }


    public function search( $term , $tipo )
    {
        $db = $this->getAdapter() ;

        $query = $db->select()
                ->from( $this->_name )
                ->joinInner( 'usuario'
                        ,
                             'reserva.usuario_id = usuario.id'
                        ,
                             array(
                    'CONCAT(usuario.nombre, " ", usuario.apellido ) as nombre'
                        )
                )
                ->where( "CONCAT(usuario.nombre, ' ', usuario.apellido ) LIKE '%$term%'" )
                ->where( 'reserva.tipo_reserva_id in (?)' ,
                         $tipo )
                ->where( 'reserva.active in (?)' ,
                         self::ACTIVE )
                ->group( 'usuario.id' )
                ->query()
        ;

        $filas = $query->fetchAll( Zend_Db::FETCH_OBJ ) ;

        $values = array( ) ;
        foreach ( $filas as $fila )
        {
            $values[] = $fila->nombre ;
        }
        return $values ;


    }


    public function findByNombre( $tipoReserva , $nombre = null )
    {

        $db = $this->getAdapter() ;
        $db->setFetchMode( Zend_Db::FETCH_OBJ ) ;
        $query = $db->select()
                ->from( $this->_name )
                ->joinInner( 'equipo' ,
                             'reserva.equipo_id = equipo.id' ,
                             array(
                    'equipo' => 'nombre' ,
                    'precio' => 'precioventa' ,
                    'modelo' ,
                    'tag' ,
                    'categoria_id' ,
                    'calidad' ,
                    'slug'
                        )
                )
                ->joinInner( 'categoria' ,
                             'categoria.id = equipo.categoria_id' ,
                             array(
                    'categoria' => 'categoria.nombre'
                        )
                )
                ->joinInner( 'estadoequipo' ,
                             'estadoequipo.id = equipo.estadoequipo_id' ,
                             array( 'estadoequipo.nombre as estadoequipo' )
                )
                ->joinInner(
                        'publicacionEquipo' ,
                        'publicacionEquipo.id = equipo.publicacionEquipo_id' ,
                        array(
                    'publicacionEquipo.nombre as publicacionequipo'
                        )
                )
                ->joinInner( 'moneda' ,
                             'moneda.id = equipo.moneda_id' ,
                             array( 'moneda.nombre as moneda' )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array( 'fabricantes.nombre as fabricante' )
                )
                ->joinInner( 'paises' ,
                             'paises.id = equipo.paises_id' ,
                             array( 'paises.nombre as pais' )
                )
                ->joinInner( 'usuario' ,
                             'reserva.usuario_id = usuario.id' ,
                             array( 'usuario.nombre' ,
                    'usuario.apellido' ) )
                ->joinLeft( 'imagen'
                        ,
                            'reserva.equipo_id = imagen.equipo_id and imagen.active = 1' ,
                            array( 'imagen.descripcion' ,
                    'imagen.imagen' ,
                    'imageNombre' => 'imagen.nombre' )
                )
                ->where( 'tipo_reserva_id = ?' ,
                         $tipoReserva )
                ->where( 'reserva.active in (?)' ,
                         self::ACTIVE ) ;

        if ( isset( $nombre ) )
        {
            $query->where( "CONCAT(usuario.nombre, ' ', usuario.apellido )  LIKE '$nombre%'" ) ;
        }

        ;
        $query->group( 'equipo.id' ) ;
        $query->query() ;
        return $db->fetchAll( $query ) ;


    }


    public function getFindId( $id )
    {
//        $db = $this->getAdapter();
//        $query = $db->select()
//                ->from( $this->_name )
//                ->where( 'id = ?' , $id )
//                ->where( 'active = ?' , '1' )
//                ->query()
//        ;
        return $this->fetchRow( 'id = ' . $id ) ;


    }


    public function listar()
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active = ?' ,
                         '1' )
                ->order( 'fechagrabacion desc' )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function getReservaByEquipUser( $idUsuario , $idEquipo , $tipoReserva )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'id' ,
                    'equipo_id' ,
                    'usuario_id' ,
                    'fechagrabacion' ,
                    'order' ,
                    'active'
                        )
                )
                ->joinInner( 'equipo' ,
                             'reserva.equipo_id = equipo.id' ,
                             array( 'nombre' ,
                    'precio' => 'precioventa' ,
                    'modelo' ,
                    'tag'
                        )
                )
                ->where( 'reserva.usuario_id = ?' ,
                         $idUsuario )
                ->where( 'reserva.equipo_id = ?' ,
                         $idEquipo )
                ->where( 'tipo_reserva_id = ?' ,
                         $tipoReserva )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function getReservaByEquipment( $idEquipo , $tipoReserva )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'id' ,
                    'equipo_id' ,
                    'usuario_id' ,
                    'fechagrabacion' ,
                    'order' ,
                    'active'
                        )
                )
                ->joinInner( 'equipo' ,
                             'reserva.equipo_id = equipo.id' ,
                             array(
                    'equipo' => 'nombre' ,
                    'precio' => 'precioventa' ,
                    'slug' ,
                    'modelo' ,
                    'tag' ,
                    'categoria_id' ,
                    'calidad'
                        )
                )
                ->joinInner( 'categoria' ,
                             'categoria.id = equipo.categoria_id' ,
                             array(
                    'categoria' => 'categoria.nombre'
                        )
                )
                ->joinInner( 'estadoequipo' ,
                             'estadoequipo.id = equipo.estadoequipo_id' ,
                             array( 'estadoequipo.nombre as estadoequipo' )
                )
                ->joinInner(
                        'publicacionEquipo' ,
                        'publicacionEquipo.id = equipo.publicacionEquipo_id' ,
                        array(
                    'publicacionEquipo.nombre as publicacionequipo'
                        )
                )
                ->joinInner( 'moneda' ,
                             'moneda.id = equipo.moneda_id' ,
                             array( 'moneda.nombre as moneda' )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array( 'fabricantes.nombre as fabricante' )
                )
                ->joinInner( 'paises' ,
                             'paises.id = equipo.paises_id' ,
                             array( 'paises.nombre as pais' )
                )
                ->joinLeft( 'imagen'
                        ,
                            'reserva.equipo_id = imagen.equipo_id and imagen.active = 1' ,
                            array( 'imagen.descripcion' ,
                    'imagen.imagen' ,
                    'imageNombre' => 'imagen.nombre' )
                )
                ->where( 'reserva.equipo_id = ?' ,
                         $idEquipo )
                ->where( 'tipo_reserva_id = ?' ,
                         $tipoReserva )
                ->where( 'reserva.active = ?' ,
                         self::ACTIVE )
                ->group( 'equipo.id' )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function getReservaByUser( $idUsuario , $tipoReserva , $limit = null )
    {
        $db = $this->getAdapter() ;
        $db->setFetchMode( Zend_Db::FETCH_OBJ ) ;
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'id' ,
                    'equipo_id' ,
                    'usuario_id' ,
                    'fechagrabacion' ,
                    'order' ,
                    'active'
                        )
                )
                ->joinInner( 'equipo' ,
                             'reserva.equipo_id = equipo.id' ,
                             array(
                    'equipo' => 'nombre' ,
                    'precio' => 'precioventa' ,
                    'slug' ,
                    'modelo' ,
                    'tag' ,
                    'categoria_id' ,
                    'calidad' ,
                    'publishdate'
                        )
                )
                ->joinInner( 'categoria' ,
                             'categoria.id = equipo.categoria_id' ,
                             array(
                    'categoria' => 'categoria.nombre'
                        )
                )
                ->joinInner( 'estadoequipo' ,
                             'estadoequipo.id = equipo.estadoequipo_id' ,
                             array( 'estadoequipo.nombre as estadoequipo' )
                )
                ->joinInner(
                        'publicacionEquipo' ,
                        'publicacionEquipo.id = equipo.publicacionEquipo_id' ,
                        array(
                    'publicacionEquipo.nombre as publicacionequipo'
                        )
                )
                ->joinInner( 'moneda' ,
                             'moneda.id = equipo.moneda_id' ,
                             array( 'moneda.nombre as moneda' )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array( 'fabricantes.nombre as fabricante' )
                )
                ->joinInner( 'paises' ,
                             'paises.id = equipo.paises_id' ,
                             array( 'paises.nombre as pais' )
                )
                ->joinLeft( 'imagen'
                        ,
                            'reserva.equipo_id = imagen.equipo_id and imagen.active = 1' ,
                            array( 'imagen.descripcion' ,
                    'imagen.imagen' ,
                    'imageNombre' => 'imagen.nombre' )
                )
                ->where( 'reserva.usuario_id = ?' ,
                         $idUsuario )
                ->where( 'tipo_reserva_id = ?' ,
                         $tipoReserva )
                ->where( 'reserva.active = ?' ,
                         self::ACTIVE )
                ->group( 'equipo.id' )

        //->query()
        ;

        if ( !is_null( $limit ) )
        {
            $query->limit( $limit ) ;
        }

        ;

        $query->query() ;
        return $db->fetchAll( $query ) ;


    }


    public function getReservaByType( $tipoReserva )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'id' ,
                    'equipo_id' ,
                    'usuario_id' ,
                    'fechagrabacion' ,
                    'order' ,
                    'active'
                        )
                )
                ->joinInner( 'equipo' ,
                             'reserva.equipo_id = equipo.id' ,
                             array(
                    'equipo' => 'nombre' ,
                    'precio' => 'precioventa' ,
                    'modelo' ,
                    'tag' ,
                    'categoria_id' ,
                    'calidad' ,
                    'slug'
                        )
                )
                ->joinInner( 'categoria' ,
                             'categoria.id = equipo.categoria_id' ,
                             array(
                    'categoria' => 'categoria.nombre'
                        )
                )
                ->joinInner( 'estadoequipo' ,
                             'estadoequipo.id = equipo.estadoequipo_id' ,
                             array( 'estadoequipo.nombre as estadoequipo' )
                )
                ->joinInner(
                        'publicacionEquipo' ,
                        'publicacionEquipo.id = equipo.publicacionEquipo_id' ,
                        array(
                    'publicacionEquipo.nombre as publicacionequipo'
                        )
                )
                ->joinInner( 'moneda' ,
                             'moneda.id = equipo.moneda_id' ,
                             array( 'moneda.nombre as moneda' )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array( 'fabricantes.nombre as fabricante' )
                )
                ->joinInner( 'paises' ,
                             'paises.id = equipo.paises_id' ,
                             array( 'paises.nombre as pais' )
                )
                ->joinInner( 'usuario' ,
                             'reserva.usuario_id = usuario.id' ,
                             array( 'usuario.nombre' ,
                    'usuario.apellido' ) )
                ->joinLeft( 'imagen'
                        ,
                            'reserva.equipo_id = imagen.equipo_id and imagen.active = 1' ,
                            array( 'imagen.descripcion' ,
                    'imagen.imagen' ,
                    'imageNombre' => 'imagen.nombre' )
                )
                ->where( 'tipo_reserva_id = ?' ,
                         $tipoReserva )
                ->where( 'reserva.active = ?' ,
                         self::ACTIVE )
                ->group( 'equipo.id' )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function pagListReservas( $tipo , $nombre = null )
    {
        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/myConfigAdmin.ini' ,
                        'reserva'
        ) ;
        $data = $_conf->toArray() ;

        $object = Zend_Paginator::factory(
                        $this->findByNombre( $tipo ,
                                             $nombre ) ) ;
        $object->setItemCountPerPage(
                $data['ItemCountPerPage']
        ) ;
        return $object ;


    }


    public function countReserved()
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name
                        ,
                        array( 'cantidad' => 'COUNT("id")' )
                )
                ->where( 'active = ?' ,
                         self::ACTIVE )
                ->where( 'tipo_reserva_id = ?' ,
                         Mtt_Models_Table_TipoReserva::RESERVED )
                ->query()
        ;

        return $query->fetchObject() ;


    }


    public function countReservaByUserTipo( $idUsuario , $idTipoReserva )
    {
        
    }


    public function pagListFavoritosByUser( $idUser , $tipo )
    {
        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/myConfigUser.ini' , 'favoritos'
        ) ;
        $data = $_conf->toArray() ;

        $object = Zend_Paginator::factory(
                        $this->getReservaByUser( $idUser ,
                                                 $tipo ) ) ;
        $object->setItemCountPerPage(
                $data['ItemCountPerPage']
        ) ;
        return $object ;


    }


    /**
     * 
     * 
     */
    public function listEquipMoreReserved( $limit , $tipoReserva )
    {

        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'cantidad' => 'COUNT(*)'
                        )
                )
                ->joinInner( 'equipo' ,
                             'reserva.equipo_id = equipo.id' ,
                             array( 'titulo' => 'nombre' )
                )
                ->where( 'equipo.active = ?' ,
                         self::ACTIVE )
                ->where( 'reserva.active = ?' ,
                         self::ACTIVE )
                ->where( 'reserva.tipo_reserva_id = ?' ,
                         $tipoReserva )
                ->group( 'equipo_id' )
                ->order( 'cantidad DESC' )
                ->limit( $limit )
                ->query()


        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function listEquipMoreReservedByCategory( $limit , $tipoReserva
    , $idCategoria )
    {

        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'cantidad' => 'COUNT(*)'
                        )
                )
                ->joinInner( 'equipo' ,
                             'reserva.equipo_id = equipo.id' ,
                             array( 'titulo' => 'nombre' )
                )
                ->where( 'equipo.active = ?' ,
                         self::ACTIVE )
                ->where( 'reserva.active = ?' ,
                         self::ACTIVE )
                ->where( 'reserva.tipo_reserva_id = ?' ,
                         $tipoReserva )
                ->where( 'equipo.categoria_id = ?' ,
                         $idCategoria )
                ->group( 'equipo_id' )
                ->order( 'cantidad DESC' )
                ->limit( $limit )
                ->query()


        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function updateReserva( array $data , $id )
    {

        $this->update( $data ,
                       'id = ' . $id ) ;


    }


    public function saveReserva( array $data )
    {

        $this->insert( $data ) ;


    }


    public function deleteReserva( $id )
    {

        $this->delete( 'id =' . ( int ) $id ) ;


    }


    /**
     *
     * @param type $idUser
     * @param type $idEquipment
     * @param type $tipoReserva_id
     * @return type 
     */
    public function getIdByEquipmentUser( $idUser , $idEquipment ,
                                          $tipoReserva_id )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()->from(
                        $this->_name ,
                        array( 'id' ,
                    'equipo_id' ,
                    'usuario_id' ,
                    'tipo_reserva_id' )
                )
                ->where( 'reserva.usuario_id = ?' ,
                         $idUser )
                ->where( 'reserva.equipo_id = ?' ,
                         $idEquipment )
                ->where( 'reserva.tipo_reserva_id = ?' ,
                         $tipoReserva_id )
                ->query() ;
        return $query->fetchObject() ;


    }


    public function activarReserva( $id )
    {

        $this->update( array( "active" => self::ACTIVE ) ,
                       'id = ' . $id ) ;


    }


    public function desactivarReserva( $id )
    {

        $this->update( array( "active" => self::DESACTIVATE ) ,
                       'id = ' . $id ) ;


    }


}

