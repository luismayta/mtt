<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Bussines_CuotasPago
        extends Mtt_Models_Table_CuotasPago
    {


    public function __construct( $config = array( ) )
        {

        parent::__construct( $config );
        }


    public function getComboValues()
        {
        $filas = $this->fetchAll( 'active=1' )->toArray();
        $values = array( );
        foreach ( $filas as $fila )
            {
            $values[$fila['id']] = $fila['nombre'];
            }
        return $values;
        }


    public function getFindId( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'id = ?' , $id )
                ->query()
        ;
        return $query->fetchObject();
        }


    public function getCuotasPagoById( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'id IN (?)' , $id )
                ->query()
        ;
        return $query->fetchObject();
        }


    public function getNroCuotas( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name , array( 'count(*)+1 as cantidad' )
                )
                ->where( 'operacion_has_equipo_id = ?' , $id )
                ->query()
        ;
        return $query->fetchObject();
        }


    public function listar()
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active = ?' , '1' )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ );
        }


    public function listByOperationHasEquip( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name ,
                        array( 'id' ,
                    'nrocuota' ,
                    'pago' ,
                    'cuotaspago.fechapago as fpagocuota' ,
                    'fechalimite' ,
                    'mora' )
                )
                ->joinInner( 'operacion_has_equipo' ,
                             'cuotaspago.operacion_has_equipo_id = 
                            operacion_has_equipo.id ' ,
                             array(
                    'operacion_has_equipo.id as op_equip_id'
                        )
                )
                ->joinInner( 'estadocuota' ,
                             'cuotaspago.estadocuota_id = estadocuota.id' ,
                             array( 'estadocuota.nombre as estadocuota' )
                )
                ->where( 'operacion_has_equipo.id  = ?' , $id
                )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ );
        }


    public function listByOperation( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name ,
                        array( 'id' ,
                    'nrocuota' ,
                    'pago' ,
                    'cuotaspago.fechapago as fpagocuota' ,
                    'fechalimite' ,
                    'mora' )
                )
                ->joinInner( 'operacion_has_equipo' ,
                             'cuotaspago.operacion_has_equipo_id = 
                            operacion_has_equipo.id ' ,
                             array(
                    'operacion_has_equipo.id as op_equip_id'
                        )
                )
                ->joinInner( 'estadocuota' ,
                             'cuotaspago.estadocuota_id = estadocuota.id' ,
                             array( 'estadocuota.nombre as estadocuota' )
                )
                ->where( 'operacion_has_equipo.operacion_id  = ?' , $id
                )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ );
        }


    public function updateCuotasPago( array $data , $id )
        {

        $this->update( $data , 'id = ' . $id );
        }


    public function saveCuotasPago( array $data )
        {

        $this->insert( $data );
        }


    public function deleteCuotasPago( $id )
        {

        $this->delete( 'id = ?' , $id );
        }


    public function activarCuotasPago( $id )
        {

        $this->update( array( "active" => self::ACTIVE ) , 'id = ' . $id );
        }


    public function desactivarCuotasPago( $id )
        {

        $this->update( array( "active" => self::DESACTIVATE ) , 'id = ' . $id );
        }


    }
