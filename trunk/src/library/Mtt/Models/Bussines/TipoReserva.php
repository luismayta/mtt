<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Bussines_TipoReserva
        extends Mtt_Models_Table_TipoReserva
{

    protected $objTraducciones ;


    public function __construct( $config = array( ) )
    {
        $this->objTraducciones = new Mtt_Models_Bussines_Traducciones() ;
        parent::__construct( $config ) ;


    }


    public function getComboValues()
    {
        $filas = $this->fetchAll( 'active=1' )->toArray() ;
        $values = array( ) ;
        $idlang = $this->sessionMtt->config->idlang ;
        foreach ( $filas as $fila )
        {
            $values[$fila['id']] = $this->objTraducciones->getTranslate(
                    $this->_entidad_id ,
                    $fila['id'] ,
                    $idlang ,
                    $fila['nombre']
            ) ;
        }
        return $values ;


    }


    public function getFindId( $id )
    {
//        $db = $this->getAdapter();
//        $query = $db->select()
//                ->from( $this->_name )
//                ->where( 'id = ?' , $id )
//                ->where( 'active = ?' , '1' )
//                ->query()
//        ;
        return $this->fetchRow( 'id = ' . $id ) ;


    }


    public function listar()
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active = ?' ,
                         '1' )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function updateTipoReserva( array $data , $id )
    {

        $this->update( $data ,
                       'id = ' . $id ) ;


    }


    public function saveTipoReserva( array $data )
    {

        $this->insert( $data ) ;


    }


    public function deleteTipoReserva( $id )
    {

        $this->delete( 'id = ?' ,
                       $id ) ;


    }


    public function activarTipoReserva( $id )
    {

        $this->update( array( "active" => self::ACTIVE ) ,
                       'id = ' . $id ) ;


    }


    public function desactivarTipoReserva( $id )
    {

        $this->update( array( "active" => self::DESACTIVATE ) ,
                       'id = ' . $id ) ;


    }


}
