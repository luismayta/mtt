<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Bussines_Contactus
    {


    public function __construct()
        {

        //parent::__construct( $config );


        }


    /**
     * para enviar correo de autorizacion
     * @param array $data
     * @param string $subject
     */
    public function sendMail( array $data , $subject )
        {
        $_conf = new Zend_Config_Ini( APPLICATION_PATH . '/configs/mail.ini' , APPLICATION_ENV );

        Zend_Mail::setDefaultFrom(
                $_conf->email->contacto , $_conf->data->contacto
        );

        $objSmtp = new Zend_Mail_Transport_Smtp(
                        $_conf->config->smtp ,
                        $_conf->config->toArray()
        );
        Zend_Mail::setDefaultReplyTo(
                $_conf->email->contacto , $_conf->data->contacto
        );

        Zend_Mail::setDefaultTransport( $objSmtp );


        $m = new Mtt_Html_Mail_Mailer(
                        $_conf->email->contacto , $_conf->data->contacto
        );
        $m->setSubject( $subject );

        $m->addTo( $_conf->email->contacto );

        $m->setViewParam( 'nombre' , $data['nombre'] )
                ->setViewParam( 'direccion' , $data['direccion'] )
                ->setViewParam( 'email' , $data['email'] )
                ->setViewParam( 'codpostal' , $data['codpostal'] )
                ->setViewParam( 'ciudad' , $data['ciudad'] )
                ->setViewParam( 'pais' , $data['pais'] )
                ->setViewParam( 'telefono' , $data['telefono'] )
                ->setViewParam( 'comentario' , $data['comentario'] )
        ;
        $m->sendHtmlTemplate( "contactus.phtml" );


        }


    }
