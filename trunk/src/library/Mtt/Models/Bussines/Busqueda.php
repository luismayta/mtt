<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Bussines_Busqueda
        extends Mtt_Models_Table_Busqueda
{


    public function __construct( $config = array( ) )
    {

        parent::__construct( $config ) ;


    }


    public function listSearchByUserId( $idUsuario )
    {

        $db = $this->getAdapter() ;
        $query = "SELECT id,
            palabras_busqueda,
            modelo,
            fabricante,
            categoria_id,
            CASE anio_inicio WHEN -1 THEN '' ELSE anio_inicio END AS anio_inicio,
            CASE anio_fin WHEN -1 THEN '' ELSE anio_fin END AS anio_fin,
            CASE precio_inicio WHEN -1 THEN '' ELSE precio_inicio END AS precio_inicio,
            CASE precio_fin WHEN -1 THEN '' ELSE precio_fin END precio_fin,
            usuario_id,
            CASE categoria_id 
                WHEN -1 THEN 'Todos' 
                ELSE (
                    SELECT nombre FROM categoria 
                    WHERE categoria.id = categoria_id
                ) END AS categoria
            FROM busqueda
            WHERE active = " . self::ACTIVE . " and usuario_id = " . $idUsuario ;

        return $db->query( $query )->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function pagListSearchByUserId( $idUsuario )
    {
        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/myConfigUser.ini' , 'busqueda'
        ) ;
        $data = $_conf->toArray() ;

        $object = Zend_Paginator::factory( $this->listSearchByUserId( $idUsuario ) ) ;
        $object->setItemCountPerPage(
                $data['ItemCountPerPage']
        ) ;
        return $object ;


    }


    public function getLastSearchByUser( $idUser )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name
                )
                ->where( 'usuario_id = ?' ,
                         $idUser )
                ->where( 'active = ?' ,
                         self::ACTIVE )
                ->order( 'fechamodificacion DESC ' )
                ->query()
        ;

        return $query->fetchObject() ;


    }


    public function setSearch( array $data )
    {
        //Definiendo variable search
        $search = new Zend_Session_Namespace( 'MTT' ) ;
        $search->Search->palabras_busqueda = $data['palabras_busqueda'] ;
        $search->Search->modelo = $data['modelo'] ;
        $search->Search->fabricante = $data['fabricante'] ;
        $search->Search->categoria_id = $data['categoria_id'] ;
        $search->Search->anio_inicio = $data['anio_inicio'] ;
        $search->Search->anio_fin = $data['anio_fin'] ;
        $search->Search->precio_inicio = $data['precio_inicio'] ;
        $search->Search->precio_fin = $data['precio_fin'] ;


    }


    public function isemptySearch()
    {
        //Definiendo variable search
        $search = new Zend_Session_Namespace( 'MTT' ) ;
        $count = 0 ;
        $count = ($search->Search->palabras_busqueda === '') ? ($count + 1) : $count ;
        $count = ($search->Search->modelo === '') ? ($count + 1) : $count ;
        $count = ($search->Search->fabricante === '') ? ($count + 1) : $count ;
        $count = ($search->Search->categoria_id == '-1') ? ($count + 1) : $count ;
        $count = ($search->Search->anio_inicio == '-1') ? ($count + 1) : $count ;
        $count = ($search->Search->anio_fin == '-1') ? ($count + 1) : $count ;
        $count = ($search->Search->precio_inicio == '-1') ? ($count + 1) : $count ;
        $count = ($search->Search->precio_fin == '-1') ? ($count + 1) : $count ;
        return $count ;


    }


    public function getSearch()
    {
        //Definiendo variable search
        $search = new Zend_Session_Namespace( 'MTT' ) ;
        return $search->Search ;


    }


    public function getFindId( $id )
    {
        return $this->fetchRow( 'id = ' . $id ) ;


    }


    public function updateBusqueda( array $data , $id )
    {
        $data['fechamodificacion'] = date( 'Y-m-d H:m:s' ) ;
        $this->update( $data ,
                       'id = ' . $id ) ;


    }


    public function saveBusqueda( array $data )
    {
        $data['fecharegistro'] = date( 'Y-m-d H:m:s' ) ;
        $data['fechamodificacion'] = date( 'Y-m-d H:m:s' ) ;
        $this->insert( $data ) ;


    }


    public function deleteBusqueda( $id )
    {

        $this->delete( 'id = ?' ,
                       $id ) ;


    }


    public function activarBusqueda( $id )
    {

        $this->update( array( "active" => self::ACTIVE ) ,
                       'id = ' . $id ) ;


    }


    public function desactivarBusqueda( $id )
    {

        $this->update( array( "active" => self::DESACTIVATE ) ,
                       'id = ' . $id ) ;


    }


}
