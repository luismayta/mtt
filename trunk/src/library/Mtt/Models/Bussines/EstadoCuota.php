<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Bussines_EstadoCuota
        extends Mtt_Models_Table_EstadoCuota
{

    protected $objTraducciones ;


    public function __construct( $config = array( ) )
    {
        $this->objTraducciones = new Mtt_Models_Bussines_Traducciones() ;
        parent::__construct( $config ) ;


    }


    public function getComboValues()
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active = ?' ,
                         self::ACTIVE )
                ->order( 'nombre' )
                ->query()
        ;
        $filas = $query->fetchAll( Zend_Db::FETCH_ASSOC ) ;
        $idlang = $this->sessionMtt->config->idlang ;
        $values = array( ) ;
        foreach ( $filas as $fila )
        {
            $values[$fila['id']] = $this->objTraducciones->getTranslate(
                    $this->_entidad_id ,
                    $fila['id'] ,
                    $idlang ,
                    $fila['nombre']
            ) ;
        }
        return $values ;


    }


    public function getFindId( $id )
    {
//        $db = $this->getAdapter();
//        $query = $db->select()
//                ->from( $this->_name )
//                ->where( 'id = ?' , $id )
//                ->where( 'active = ?' , '1' )
//                ->query()
//        ;
        return $this->fetchRow( 'id = ' . $id ) ;


    }


    public function listar()
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active = ?' ,
                         '1' )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


}
