<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Bussines_Fabricante
        extends Mtt_Models_Table_Fabricante
{


    public function __construct( $config = array( ) )
    {

        parent::__construct( $config ) ;


    }


    public function search( $term )
    {
        $db = $this->getAdapter() ;

        $query = $db->select()
                ->from( $this->_name ,
                        'nombre' )
                ->where( "nombre LIKE '$term%'" )
                ->where( 'active in (?)' ,
                         self::ACTIVE )
                ->query()
        ;

        $filas = $query->fetchAll( Zend_Db::FETCH_OBJ ) ;

        $values = array( ) ;
        foreach ( $filas as $fila )
        {
            $values[] = $fila->nombre ;
        }
        return $values ;


    }


    public function getFabricanteBySlug( $slug )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'slug IN (?)' ,
                         $slug )
                ->where( 'active = ?' ,
                         self::ACTIVE )
                ->query()
        ;
        return $query->fetchObject() ;


    }


    public function getComboValues()
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active = ?' ,
                         self::ACTIVE )
                ->order( 'nombre' )
                ->query()
        ;
        $filas = $query->fetchAll( Zend_Db::FETCH_ASSOC ) ;

        $values = array( ) ;
        foreach ( $filas as $fila )
        {
            $values[$fila['id']] = $fila['nombre'] ;
        }
        return $values ;


    }


//    public function getFindId( $id )
//        {
////        $db = $this->getAdapter();
////        $query = $db->select()
////                ->from( $this->_name )
////                ->where( 'id = ?' , $id )
////                ->where( 'active = ?' , '1' )
////                ->query()
////        ;
//        return $this->fetchRow( 'id = ' . $id );
//        }

    public function findByNombre( $active , $nombre = null )
    {

        $db = $this->getAdapter() ;
        $db->setFetchMode( Zend_Db::FETCH_OBJ ) ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active in (?)' ,
                         $active ) ;
        //->query();
        if ( isset( $nombre ) )
        {
            $query->where( "nombre LIKE '$nombre%'" ) ;
        }

        ;
        $query->order( 'nombre' ) ;
        $query->query() ;
        return $db->fetchAll( $query ) ;


    }


    public function getManufacturersPublishment( $active = self::ACTIVE )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( 'equipo' ,
                        array(
                    'equipoId' => 'id'
                        )
                )
                ->joinInner(
                        $this->_name ,
                        'equipo.fabricantes_id = fabricantes.id' ,
                        array(
                    'id' ,
                    'nombre' ,
                    'slug'
                        )
                )
                ->where( 'equipo.active in (?)' ,
                         $active )
                ->where( 'fabricantes.active in (?)' ,
                         $active )
                ->where( 'equipo.publicacionEquipo_id in (?)' ,
                         Mtt_Models_Bussines_PublicacionEquipo::Activada )
                ->group( 'fabricantes.id' )
                ->order( 'fabricantes.nombre' )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function listar( $active = self::ACTIVE )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active in (?)' ,
                         $active )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function pagListManufactur( $active = null , $nombre = null )
    {
        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/myConfigAdmin.ini'
                        , 'fabricante'
                ) ;
        $data = $_conf->toArray() ;

        $value = ($active == null) ? array( 0 , 1 ) : 1 ;

        $object = Zend_Paginator::factory(
                        $this->findByNombre( $value ,
                                             $nombre )
                ) ;
        $object->setItemCountPerPage(
                $data['ItemCountPerPage']
        ) ;
        return $object ;


    }


    public function updateFabricante( array $data , $id )
    {

        $this->update( $data ,
                       'id = ' . $id ) ;


    }


    public function saveFabricante( array $data )
    {

        $this->insert( $data ) ;


    }


    public function deleteFabricante( $id )
    {

        $this->delete( 'id =' . ( int ) $id ) ;


    }


    public function activarFabricante( $id )
    {

        $this->update( array( "active" => self::ACTIVE ) ,
                       'id = ' . $id ) ;


    }


    public function desactivarFabricante( $id )
    {

        $this->update( array( "active" => self::DESACTIVATE ) ,
                       'id = ' . $id ) ;


    }


    /**
     * 
     */
    public function getEquipments()
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name ,
                        array(
                    'fabricanteId' => 'id' ,
                    'fabricante' => 'nombre'
                        )
                )
                ->joinInner( 'equipo' ,
                             'equipo.fabricantes_id = fabricantes.id' ,
                             array(
                    'equipoId' => 'id' ,
                    'equipo' => 'nombre' ,
                    'slugEquipo' => 'slug' ,
                    'usuarioId' => 'usuario_id'
                        )
                )
                ->where( 'fabricante.active = ?' ,
                         self::ACTIVE )
                ->where( 'equipo.active = ?' ,
                         self::ACTIVE )
                ->order( 'fabricante.nombre' )
                ->query()
        ;
        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


}
