<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Bussines_EquipoRequerido
        extends Mtt_Models_Table_EquipoRequerido
{


    public function __construct( $config = array( ) )
    {

        parent::__construct( $config ) ;


    }


    public function search( $term )
    {
        $db = $this->getAdapter() ;

        $query = $db->select()
                ->from( $this->_name ,
                        'nombre' )
                ->where( "nombre LIKE '$term%'" )
                ->where( 'active in (?)' ,
                         self::ACTIVE )
                ->query()
        ;

        $filas = $query->fetchAll( Zend_Db::FETCH_OBJ ) ;

        $values = array( ) ;
        foreach ( $filas as $fila )
        {
            $values[] = $fila->nombre ;
        }
        return $values ;


    }


    public function findByNombre( $active = self::ACTIVE , $nombre = null )
    {

        $db = $this->getAdapter() ;
        $db->setFetchMode( Zend_Db::FETCH_OBJ ) ;
        $query = $db->select()
                ->from( $this->_name ,
                        array(
                    'id' , 'nombre' , 'modelo' , 'fabricante' ,
                    'categoria_id' , 'estado_id' , 'fechafabricacion' ,
                    'comentario' , 'active'
                        )
                )
                ->joinInner(
                        'categoria' ,
                        'categoria.id=equiporequerido.categoria_id'
                        ,
                        array( 'categoria' => 'nombre' )
                )
                ->joinInner(
                        'estadoequipo' ,
                        'estadoequipo.id=equiporequerido.estado_id' ,
                        array(
                    'estado' => 'nombre'
                        )
                )
                ->where( 'equiporequerido.active in (?)' ,
                         $active ) ;

        if ( isset( $nombre ) )
        {
            $query->where( "equiporequerido.nombre LIKE '$nombre%'" ) ;
        }

        ;
        $query->query() ;
        return $db->fetchAll( $query ) ;


    }


    public function pagList( $active = self::ACTIVE , $nombre = null )
    {
        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/myConfigAdmin.ini'
                        , 'equiporequerido'
                ) ;
        $data = $_conf->toArray() ;

        $object = Zend_Paginator::factory(
                        $this->findByNombre( $active ,
                                             $nombre )
                ) ;
        $object->setItemCountPerPage(
                $data['ItemCountPerPage']
        ) ;
        return $object ;


    }


    public function sendMailAdmin( array $data , $subject )
    {
        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/mail.ini' , APPLICATION_ENV
                ) ;

        Zend_Mail::setDefaultFrom(
                $_conf->email->request ,
                $_conf->data->request
        ) ;

        $objSmtp = new Zend_Mail_Transport_Smtp(
                        $_conf->config->smtp ,
                        $_conf->config->toArray()
                ) ;

        Zend_Mail::setDefaultReplyTo(
                $_conf->email->request ,
                $_conf->data->request
        ) ;

        Zend_Mail::setDefaultTransport( $objSmtp ) ;

        Zend_Mail::setDefaultFrom(
                $_conf->email->request ,
                $_conf->data->request
        ) ;

        $m = new Mtt_Html_Mail_Mailer(
                        $_conf->email->request , $_conf->data->request
                ) ;
        $m->setSubject( $subject ) ;

        $m->addTo( $_conf->email->admin ) ;

        if ( $data['toemail'] == 1 )
        {
            $m->addTo( $data['email'] ) ;
        }

        $m->setViewParam( 'nombre' ,
                          $data['nombre'] )
                ->setViewParam( 'organizacion' ,
                                $data['organizacion'] )
                ->setViewParam( 'direccion' ,
                                $data['direccion'] )
                ->setViewParam( 'codpostal' ,
                                $data['codpostal'] )
                ->setViewParam( 'ciudad' ,
                                $data['ciudad'] )
                ->setViewParam( 'email' ,
                                $data['email'] )
                ->setViewParam( 'asunto' ,
                                $data['asunto'] )
                ->setViewParam( 'pais' ,
                                $data['pais'] )
                ->setViewParam( 'mensaje' ,
                                $data['mensaje'] )
                ->setViewParam( 'equipo' ,
                                $data['equipo'] )
                ->setViewParam( 'fabricante' ,
                                $data['fabricante'] )
                ->setViewParam( 'modelo' ,
                                $data['modelo'] )
                ->setViewParam( 'fechafabricacion' ,
                                $data['fechafabricacion'] )
                ->setViewParam( 'equipo_id' ,
                                $data['equipo_id'] )
                ->setViewParam( 'codpostal' ,
                                $data['codpostal'] )


        ;

        $m->sendHtmlTemplate( "equiporequeridoAdmin.phtml" ) ;


    }


    public function getById( $id )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name ,
                        array(
                    'id' , 'nombre' , 'modelo' , 'fabricante' ,
                    'categoria_id' , 'estado_id' , 'fechafabricacion' ,
                    'comentario' , 'active'
                        )
                )
                ->joinInner(
                        'categoria' ,
                        'categoria.id=equiporequerido.categoria_id'
                        ,
                        array( 'categoria' => 'nombre' )
                )
                ->joinInner(
                        'estadoequipo' ,
                        'estadoequipo.id=equiporequerido.estado_id' ,
                        array( 'estado' => 'nombre' )
                )
                ->where( 'equiporequerido.id in (?)' ,
                         $id )
                ->where( 'equiporequerido.active in (?)' ,
                         self::ACTIVE )
                ->query()
        ;


        return $query->fetchObject() ;


    }


    public function listar( $active = self::ACTIVE )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name ,
                        array(
                    'id' , 'nombre' , 'modelo' , 'fabricante' ,
                    'categoria_id' , 'estado_id' , 'fechafabricacion' ,
                    'comentario' , 'active'
                        )
                )
                ->joinInner(
                        'categoria' ,
                        'categoria.id=equiporequerido.categoria_id'
                        ,
                        array( 'categoria' => 'nombre' )
                )
                ->joinInner(
                        'estadoequipo' ,
                        'estadoequipo.id=equiporequerido.estado_id' ,
                        array( 'estado' => 'nombre' )
                )
                ->where( 'equiporequerido.active in (?)' ,
                         $active )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function updateEquipoRequerido( array $data , $id )
    {

        $this->update( $data ,
                       'id = ' . $id ) ;


    }


    public function saveEquipoRequerido( array $data )
    {

        $this->insert( $data ) ;


    }


    public function deleteEquipoRequerido( $id )
    {

        $this->delete( 'id =' . ( int ) $id ) ;


    }


    public function activarEquipoRequerido( $id )
    {

        $this->update( array( "active" => self::ACTIVE ) ,
                       'id = ' . $id ) ;


    }


    public function desactivarEquipoRequerido( $id )
    {

        $this->update( array( "active" => self::DESACTIVATE ) ,
                       'id = ' . $id ) ;


    }


}
