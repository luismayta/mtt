<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Bussines_Pagina
        extends Mtt_Models_Table_Pagina
{


    public function __construct( $config = array( ) )
    {

        parent::__construct( $config ) ;


    }


    public function getFindId( $id , $active = null )
    {
        $value = ($active == null) ? array( 0 , 1 ) : 1 ;
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'idpagina = ?' ,
                         $id )
                ->where( 'active IN (?)' ,
                         $value )
                ->query()
        ;
        return $query->fetchObject() ;


    }


    /**
     * 
     */
    public function listPagina( $active = null )
    {
        $value = ($active == null) ? array( 0 , 1 ) : 1 ;
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name ,
                        array(
                    'id' => 'idpagina' ,
                    'pagina' => 'nombre' ,
                    'active' ,
                    'body'
                        )
                )
                ->joinInner( 'idiomas' ,
                             'idiomas.id = pagina.idiomas_id' ,
                             array(
                    'idioma' => 'idiomas.nombre'
                        )
                )
                ->joinInner( 'paises' ,
                             'paises.id = pagina.paises_id' ,
                             array(
                    'pais' => 'paises.nombre'
                        )
                )
                ->where( 'pagina.active in (?)' ,
                         $value )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function updatePagina( array $data , $id )
    {

        $this->update( $data ,
                       'idpagina = ' . $id ) ;


    }


    public function savePagina( array $data )
    {

        $this->insert( $data ) ;


    }


    public function deletePagina( $id )
    {

        $this->delete( 'idpagina =' . ( int ) $id ) ;


    }


    public function activarPagina( $id )
    {

        $this->update( array( "active" => self::ACTIVE ) ,
                       'idpagina = ' . $id ) ;


    }


    public function desactivarPagina( $id )
    {

        $this->update( array( "active" => self::DESACTIVATE ) ,
                       'idpagina = ' . $id ) ;


    }


    public function getByPageIdiomaPaisCountry(
    $page_id , $idiomas_id , $pais_code
    )
    {

        $db = $this->getAdapter() ;

        $query = $db->select()
                ->from( $this->_name ,
                        array(
                    'id' => 'idpagina' ,
                    'titulo' ,
                    'active' ,
                    'body'
                        )
                )
                ->joinInner( 'page' ,
                             'page.id = pagina.page_id' ,
                             array(
                    'page' => 'nombre'
                        )
                )
                ->joinInner( 'idiomas' ,
                             'idiomas.id = pagina.idiomas_id' ,
                             array(
                    'idioma' => 'nombre'
                        )
                )
                ->joinInner( 'paises' ,
                             'paises.id = pagina.paises_id' ,
                             array(
                    'pais' => 'nombre'
                        )
                )
                ->where( 'page.id in (?)' ,
                         $page_id )
                ->where( 'idiomas.id in (?)' ,
                         $idiomas_id )
                ->where( 'paises.code in (?)' ,
                         $pais_code )
                ->where( 'pagina.active in (?)' ,
                         self::ACTIVE )
                ->query()
        ;
        return $query->fetchObject() ;


    }


    public function getByPageIdiomasPaises( $page_id , $idiomas_id , $paises_id )
    {

        $db = $this->getAdapter() ;

        $query = $db->select()
                ->from( $this->_name ,
                        array(
                    'id' => 'idpagina' ,
                    'titulo' ,
                    'active' ,
                    'body'
                        )
                )
                ->joinInner( 'page' ,
                             'page.id = pagina.page_id' ,
                             array(
                    'page' => 'nombre'
                        )
                )
                ->joinInner( 'idiomas' ,
                             'idiomas.id = pagina.idiomas_id' ,
                             array(
                    'idioma' => 'nombre'
                        )
                )
                ->joinInner( 'paises' ,
                             'paises.id = pagina.paises_id' ,
                             array(
                    'pais' => 'nombre'
                        )
                )
                ->where( 'page.id in (?)' ,
                         $page_id )
                ->where( 'idiomas.id in (?)' ,
                         $idiomas_id )
                ->where( 'paises.id in (?)' ,
                         $paises_id )
                ->where( 'pagina.active in (?)' ,
                         self::ACTIVE )
                ->query()
        ;
        return $query->fetchObject() ;


    }


    public function getCheckExist( $datos )
    {

        foreach ( $datos as $data )
        {

            $dataPagina = $this->getByPageIdiomasPaises(
                    $data['page_id'] ,
                    $data['idiomas_id'] ,
                    $data['paises_id']
            ) ;

            unset( $data['idioma'] ) ;

            if (
                    !is_object(
                            $dataPagina
                    )
            )
            {

                $this->savePagina( $data ) ;
            }
            else
            {
                $this->updatePagina( $data ,
                                     $dataPagina->id ) ;
            }
        }
        return true ;


    }


}