<?php


/**
 * 
 */
class Mtt_Models_Bussines_Equipo
        extends Mtt_Models_Table_Equipo
    {

    private $objDate;


    public function __construct( $config = array( ) )
        {
        $this->objDate = new Zend_Date();
        parent::__construct( $config );


        }


    public function search( $term )
        {
        $db = $this->getAdapter();

        $query = $db->select()
                ->from( $this->_name , 'nombre' )
                ->where( "nombre LIKE '$term%'" )
                ->where( 'active in (?)' , self::ACTIVE )
                ->query()
        ;

        $filas = $query->fetchAll( Zend_Db::FETCH_OBJ );

        $values = array( );
        foreach ( $filas as $fila )
            {
            $values[] = $fila->nombre;
            }
        return $values;


        }


    public function getPrecioVenta( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name
                        , array( 'precioventa' , 'id' )
                )
                ->where( 'id = ?' , $id )
                ->query()
        ;
        return $query->fetchObject();


        }


    public function getLastEquipmentPublishedAllUsers( $limit = 5 ,
                                                       $order = 'desc' )
        {
        $date = new Zend_Date( );
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name ,
                        array(
                    'id' , 'nombre' ,
                    'slug' ,
                    'publishdate' ,
                    'modelo' ,
                    'fechafabricacion'
                        )
                )
                ->joinInner( 'categoria' ,
                             'categoria.id = equipo.categoria_id ' ,
                             array( 'categoria.nombre as categoria' )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array( 'fabricantes.nombre as fabricante' )
                )
                ->joinLeft( 'imagen'
                        , 'imagen.equipo_id = equipo.id and imagen.active = 1' ,
                            array( 'imagen' )
                )
                ->where( 'equipo.active IN (?)' , self::ACTIVE )
                ->where( 'EXTRACT(MONTH FROM equipo.publishdate) IN (?)' ,
                         date( 'm' ) )
                ->where( 'equipo.publicacionEquipo_id  = ?' ,
                         Mtt_Models_Table_PublicacionEquipo::Activada )
                ->group( 'equipo.id' )
                ->order( 'equipo.publishdate ' . $order )
                ->limit( $limit )
                ->query();

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    public function getLastEquipmentPublished( $usuarioId , $limit = 5 )
        {
        $date = new Zend_Date( );
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name ,
                        array(
                    'id' , 'nombre' ,
                    'slug' ,
                    'publishdate' ,
                    'modelo' ,
                    'fechafabricacion'
                        )
                )
                ->joinInner( 'categoria' ,
                             'categoria.id = equipo.categoria_id ' ,
                             array( 'categoria.nombre as categoria' )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array( 'fabricantes.nombre as fabricante' )
                )
                ->joinLeft( 'imagen'
                        , 'imagen.equipo_id = equipo.id and imagen.active = 1' ,
                            array( 'imagen.imagen as imagen' )
                )
                ->where( 'equipo.active IN (?)' , self::ACTIVE )
                ->where( 'EXTRACT(MONTH FROM equipo.publishdate) IN (?)' ,
                         date( 'm' ) )
                ->where( 'equipo.usuario_id IN (?)' , $usuarioId )
                ->where( 'equipo.publicacionEquipo_id  = ?' ,
                         Mtt_Models_Table_PublicacionEquipo::Activada )
                ->limit( $limit )
                ->query();

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    public function getEquipmentBySlug( $slug )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'id' , 'nombre' , 'precioventa' , 'preciocompra' , 'categoria_id' ,
                    'estadoequipo_id' , 'usuario_id' , 'fabricantes_id' ,
                    'especificaciones' , 'descripcion' , 'monedaventa_id' ,
                    'moneda_id' , 'paises_id' ,
                    'fechafabricacion' , 'slug' , 'modelo'
                        )
                )
                ->joinInner( 'categoria' ,
                             'categoria.id = equipo.categoria_id ' ,
                             array( 'categoria.nombre as categoria' )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array( 'fabricantes.nombre as fabricante' )
                )
                ->where( 'equipo.slug IN (?)' , $slug )
                ->where( 'equipo.active = ?' , self::ACTIVE )
                ->query()
        ;
        return $query->fetchObject();


        }


    public function getSlugById( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name , array( 'slug' ) )
                ->where( 'id IN (?)' , $id )
                ->where( 'active = ?' , self::ACTIVE )
                ->query()
        ;
        return $query->fetchObject();


        }


    public function listar()
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active = ?' , self::ACTIVE )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    /**
     *
     * @param type $id
     */
    public function addTopOfers( $id )
        {
        $data = array( 'topofers' => self::ACTIVE );
        $this->updateEquipo( $data , $id );


        }


    /**
     *
     * @param type $id
     */
    public function quitTopOfers( $id )
        {
        $data = array( 'topofers' => self::DESACTIVATE );
        $this->updateEquipo( $data , $id );


        }


    public function getComboValues()
        {
        $filas = $this->fetchAll( 'active=1' )->toArray();
        $values = array( );
        foreach ( $filas as $fila )
            {
            $values[$fila['id']] = $fila['nombre'];
            }
        return $values;


        }


    /**
     *
     * @param <type> $id
     */
    public function getProductMoneyVenta( $id )
        {
        $db = $this->getAdapter();

        $subSelelectImagen = $db->select()
                ->from( 'imagen' , array( 'id' ) )
                ->where( 'imagen.active = ?' , self::ACTIVE )
                ->where( 'imagen.equipo_id = equipo.id' )
                ->order( 'imagen.order' )
                ->limit( 1 )
        ;

        $query = $db->select()
                ->from(
                        $this->_name ,
                        array( 'id' , 'nombre' , 'precioventa' ,
                    'preciocompra' , 'tag' , 'calidad' ,
                    'cantidad' , 'modelo' , 'fechafabricacion' , 'descripcion' ,
                    'documento' , 'sourceDocumento' , 'pesoEstimado' , 'size' ,
                    'ancho' , 'alto' , 'sizeCaja' , 'especificaciones' ,
                    'slug' , 'addCart' , 'descuento' )
                )
                ->joinInner(
                        'categoria' , 'categoria.id = equipo.categoria_id' ,
                        array(
                    'categoria.nombre as categoria' ,
                    'categoria.id as categoria_id' ,
                    'slugCategory' => 'categoria.slug'
                        )
                )
                ->joinInner( 'estadoequipo'
                        ,
                             'estadoequipo.id = equipo.estadoequipo_id'
                        , array( 'estadoequipo.nombre as estadoequipo' )
                )
                ->joinInner( 'publicacionEquipo'
                        ,
                             'publicacionEquipo.id = equipo.publicacionEquipo_id'
                        ,
                             array( 'publicacionEquipo.nombre as publicacionequipo' ,
                    'publicacionEquipo.id as publicacionid' )
                )
                ->joinInner( 'moneda'
                        ,
                             'moneda.id = equipo.monedaventa_id'
                        ,
                             array(
                    'monedaid' => 'id' ,
                    'monedaventa' => 'nombre' ,
                    'ventacodepais' => 'codepais' ,
                    'ventacodegoogle' => 'codegoogle'
                        )
                )
                ->joinInner( 'fabricantes'
                        ,
                             'fabricantes.id = equipo.fabricantes_id'
                        , array( 'fabricantes.nombre as fabricante' )
                )
                ->joinInner( 'paises'
                        ,
                             'paises.id = equipo.paises_id'
                        , array( 'paises.nombre as pais' )
                )
                ->joinLeft( 'imagen' , 'imagen.equipo_id = equipo.id' ,
                            array(
                    'nombreimage' => 'nombre' ,
                    'imagen' )
                )
                ->where( 'equipo.active IN (?)' , self::ACTIVE )
                ->where( 'imagen.active IN (?)' , self::ACTIVE )
                ->where( 'imagen.id = (?)' , ($subSelelectImagen ) )
                ->where( 'equipo.id IN (?)' , $id )
                ->group( 'equipo.id' )
                ->query();

        return $query->fetchObject();


        }


    public function getProductMoneyVentaById( array $data )
        {

        $db = $this->getAdapter();
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array( 'id' , 'nombre' , 'precioventa' ,
                    'preciocompra' , 'tag' , 'calidad' ,
                    'cantidad' , 'modelo' , 'fechafabricacion' , 'descripcion' ,
                    'documento' , 'sourceDocumento' , 'pesoEstimado' , 'size' ,
                    'ancho' , 'alto' , 'sizeCaja' , 'especificaciones' ,
                    'slug' , 'addCart' , 'descuento' , 'active' )
                )
                ->joinInner(
                        'categoria' , 'categoria.id = equipo.categoria_id' ,
                        array(
                    'categoria.nombre as categoria' ,
                    'categoria.id as categoria_id' ,
                    'slugCategory' => 'categoria.slug'
                        )
                )
                ->joinInner( 'estadoequipo'
                        ,
                             'estadoequipo.id = equipo.estadoequipo_id'
                        , array( 'estadoequipo.nombre as estadoequipo' )
                )
                ->joinInner( 'publicacionEquipo'
                        ,
                             'publicacionEquipo.id = equipo.publicacionEquipo_id'
                        ,
                             array( 'publicacionEquipo.nombre as publicacionequipo' ,
                    'publicacionEquipo.id as publicacionid' )
                )
                ->joinInner( 'moneda'
                        ,
                             'moneda.id = equipo.monedaventa_id'
                        ,
                             array(
                    'monedaid' => 'id' ,
                    'monedaventa' => 'nombre' ,
                    'ventacodepais' => 'codepais' ,
                    'ventacodegoogle' => 'codegoogle'
                        )
                )
                ->joinInner( 'fabricantes'
                        ,
                             'fabricantes.id = equipo.fabricantes_id'
                        , array( 'fabricantes.nombre as fabricante' )
                )
                ->joinInner( 'paises'
                        ,
                             'paises.id = equipo.paises_id'
                        , array( 'paises.nombre as pais' )
                )
                ->joinLeft( 'imagen'
                        ,
                            'imagen.equipo_id = equipo.id and imagen.active = 1'
                        ,
                            array( 'imagen.nombre as nombreimage' ,
                    'imagen' )
                )
                ->where( 'equipo.id IN (' . Mtt_Tools_Text::getIn( $data ) . ')' )
                ->group( 'equipo.id' )
                ->query();

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    public function getProduct( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array( 'id' , 'nombre' , 'precioventa' ,
                    'preciocompra' , 'tag' , 'calidad' ,
                    'cantidad' , 'modelo' , 'fechafabricacion' , 'descripcion' ,
                    'documento' , 'sourceDocumento' , 'pesoEstimado' , 'size' ,
                    'ancho' , 'alto' , 'sizeCaja' , 'especificaciones' ,
                    'slug' )
                )
                ->joinInner(
                        'categoria' , 'categoria.id = equipo.categoria_id' ,
                        array(
                    'categoria.nombre as categoria' ,
                    'categoria.id as categoria_id' ,
                    'slugCategory' => 'categoria.slug'
                        )
                )
                ->joinInner( 'estadoequipo'
                        ,
                             'estadoequipo.id = equipo.estadoequipo_id'
                        , array( 'estadoequipo.nombre as estadoequipo' )
                )
                ->joinInner( 'publicacionEquipo'
                        ,
                             'publicacionEquipo.id = equipo.publicacionEquipo_id'
                        ,
                             array( 'publicacionEquipo.nombre as publicacionequipo' ,
                    'publicacionEquipo.id as publicacionid' )
                )
                ->joinInner( 'moneda'
                        ,
                             'moneda.id = equipo.moneda_id'
                        ,
                             array(
                    'moneda.nombre as moneda' ,
                    'moneda.codepais' ,
                    'moneda.codegoogle'
                        )
                )
                ->joinInner( 'fabricantes'
                        ,
                             'fabricantes.id = equipo.fabricantes_id'
                        , array( 'fabricantes.nombre as fabricante' )
                )
                ->joinInner( 'paises'
                        ,
                             'paises.id = equipo.paises_id'
                        , array( 'paises.nombre as pais' )
                )
                ->joinLeft( 'imagen'
                        ,
                            'imagen.equipo_id = equipo.id and imagen.active = 1'
                        ,
                            array( 'imagen.nombre as nombreimage' ,
                    'imagen' )
                )
                ->where( 'equipo.active IN (?)' , self::ACTIVE )
                ->where( 'equipo.id IN (?)' , $id )
                ->group( 'equipo.id' )
                ->query();

        return $query->fetchObject();


        }


    public function getNameDocumentById( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(
                        $this->_name , array( 'sourceDocumento' )
                )
                ->where( 'equipo.id IN (?)' , $id )
                ->query();

        return $query->fetchObject();


        }


    public function getImagenes( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name
                        , array( 'id' , 'nombre' )
                )
                ->joinInner( 'categoria'
                        ,
                             'categoria.id = equipo.categoria_id'
                        , array( 'categoria.nombre as categoria' )
                )
                ->joinInner( 'fabricantes'
                        ,
                             'fabricantes.id = equipo.fabricantes_id'
                        , array( 'fabricantes.nombre as fabricante' )
                )
                ->joinLeft( 'imagen'
                        ,
                            'imagen.equipo_id = equipo.id and imagen.active = 1'
                        ,
                            array( 'imagen.nombre as imagen' ,
                    'imagen.imagen as imagenurl' ,
                    'imagen.descripcion' ,
                    'imagen.id as idimagen' )
                )
                ->where( 'equipo.active IN (?)' , self::ACTIVE )
                ->where( 'equipo.id = ?' , $id )
                ->query();

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    public function getImage( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name , array( 'id' , 'nombre' ) )
                ->joinInner( 'categoria' , 'categoria.id = ' . $id ,
                             array( 'categoria.nombre as categoria' ) )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array( 'fabricantes.nombre as fabricante' ) )
                ->joinLeft( 'imagen'
                        , 'imagen.equipo_id = equipo.id and imagen.active = 1' ,
                            array( 'imagen.nombre as imagen' ) )
                ->where( 'equipo.active IN (?)' , self::ACTIVE )
                ->query();

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    public function getMoneyVentaById( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(
                        $this->_name
                        ,
                        array(
                    'id' ,
                    'nombre' ,
                    'precioventa' ,
                    'preciocompra' ,
                    'descripcion' ,
                    'moneda_id' ,
                    'paises_id' ,
                    'calidad' ,
                    'cantidad' ,
                    'modelo' ,
                    'fechafabricacion' ,
                    'slug'
                        )
                )
                ->joinInner(
                        'moneda' , 'moneda.id = equipo.monedaventa_id' ,
                        array(
                    'code' => 'moneda.codepais' ,
                    'codegoogle' => 'moneda.codegoogle'
                        )
                )
                ->where( 'equipo.id = ?' , $id )
                ->where( 'equipo.active = ?' , self::ACTIVE )
                ->query()
        ;
        return $query->fetchObject();


        }


    /**
     * @author slovacus@gmail.com
     * se devuelven los valores y la moneda de un equipo
     * @param type $id
     * @return type objeto
     */
    public function getById( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(
                        $this->_name
                        ,
                        array(
                    'id' ,
                    'nombre' ,
                    'precioventa' ,
                    'preciocompra' ,
                    'descripcion' ,
                    'moneda_id' ,
                    'paises_id' ,
                    'calidad' ,
                    'cantidad' ,
                    'modelo' ,
                    'fechafabricacion' ,
                    'slug'
                        )
                )
                ->joinInner( 'moneda' , 'moneda.id = equipo.moneda_id' ,
                             array(
                    'code' => 'moneda.codepais' ,
                    'codegoogle' => 'moneda.codegoogle'
                        )
                )
                ->where( 'equipo.id = ?' , $id )
                ->where( 'equipo.active = ?' , self::ACTIVE )
                ->query()
        ;
        return $query->fetchObject();


        }


    public function getFindId( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'id' , 'nombre' ,
                    'precioventa' ,
                    'preciocompra' ,
                    'categoria_id' ,
                    'estadoequipo_id' ,
                    'usuario_id' ,
                    'fabricantes_id' ,
                    'tag' ,
                    'especificaciones' ,
                    'descripcion' ,
                    'monedaventa_id' ,
                    'moneda_id' ,
                    'paises_id' ,
                    'calidad' ,
                    'cantidad' ,
                    'modelo' ,
                    new Zend_Db_Expr( 'year(fechafabricacion) as fechafabricacion' )
                    ,
                    'documento' ,
                    'sourceDocumento' ,
                    'pesoEstimado' ,
                    'size' ,
                    'ancho' ,
                    'alto' ,
                    'sizeCaja' ,
                    'active' ,
                    'publicacionEquipo_id' ,
                    'views' ,
                    'topofers' ,
                    'publishdate' ,
                    'slug' ,
                    'nombreOferente' ,
                    'articulo_id' ,
                    'ciudadOferente' ,
                    'codigoPostalOferente' ,
                    'addCart' ,
                    'sinonimo' ,
                    'descuento' )
                )
                ->where( 'id = ?' , $id )
                ->where( 'active = ?' , self::ACTIVE )
                ->query()
        ;
        return $query->fetchObject();


        }


    public function getFindIdByAdmin( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'id' , 'nombre' ,
                    'precioventa' ,
                    'preciocompra' ,
                    'categoria_id' ,
                    'estadoequipo_id' ,
                    'usuario_id' ,
                    'fabricantes_id' ,
                    'tag' ,
                    'especificaciones' ,
                    'descripcion' ,
                    'monedaventa_id' ,
                    'moneda_id' ,
                    'paises_id' ,
                    'calidad' ,
                    'cantidad' ,
                    'modelo' ,
                    new Zend_Db_Expr( 'year(fechafabricacion) as fechafabricacion' )
                    ,
                    'documento' ,
                    'sourceDocumento' ,
                    'pesoEstimado' ,
                    'size' ,
                    'ancho' ,
                    'alto' ,
                    'sizeCaja' ,
                    'active' ,
                    'publicacionEquipo_id' ,
                    'views' ,
                    'topofers' ,
                    'publishdate' ,
                    'slug' ,
                    'nombreOferente' ,
                    'articulo_id' ,
                    'ciudadOferente' ,
                    'codigoPostalOferente' ,
                    'addCart' ,
                    'sinonimo' ,
                    'descuento' )
                )
                ->where( 'id = ?' , $id )
                ->query()
        ;
        return $query->fetchObject();


        }


    public function getProductById( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name ,
                        array(
                    'id' ,
                    'nombre' ,
                    'modelo' ,
                    'fechafabricacion' ,
                    'slug'
                        )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array(
                    'fabricante' => 'fabricantes.nombre'
                        )
                )
                ->where( 'equipo.id = ?' , $id )
                ->where( 'equipo.active = ?' , self::ACTIVE )
                ->query()
        ;
        return $query->fetchObject();
//return $this->fetchRow( 'id = ' . $id );


        }


    public function getEquipmentsByManufacturesId( $manufacturerId )
        {
        $db = $this->getAdapter();

        $subSelelectProgramaEquipo = $db->select()
                ->from( 'programaequipo' , array( 'equipo_id' ) )
        ;

        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'id' , 'nombre' , 'slug' , 'modelo'
                        )
                )
                ->joinInner( 'categoria' ,
                             'categoria.id = equipo.categoria_id ' ,
                             array(
                    'categoria' => 'nombre'
                        )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array(
                    'fabricante' => 'nombre'
                        )
                )
                ->joinLeft( 'imagen'
                        , 'imagen.equipo_id = equipo.id and imagen.active = 1' ,
                            array( 'imagen' => 'imagen' )
                )
                ->joinLeft( 'programaequipo' ,
                            'equipo.id = programaequipo.equipo_id' ,
                            array(
                    'programaId' => 'id'
                        )
                )
                ->where( 'equipo.active IN (?)' , self::ACTIVE )
                ->where( 'equipo.publicacionEquipo_id  = ?' ,
                         Mtt_Models_Table_PublicacionEquipo::Activada )
                ->where( 'equipo.fabricantes_id = ?' , $manufacturerId )
                ->where( 'equipo.id NOT IN (?)' , ($subSelelectProgramaEquipo ) )
                ->group( 'equipo.id' )
                ->query();

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    public function getProducts()
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'id' , 'nombre' , 'slug' , 'modelo'
                        )
                )
                ->joinInner( 'categoria' ,
                             'categoria.id = equipo.categoria_id ' ,
                             array( 'categoria.nombre as categoria' )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array( 'fabricantes.nombre as fabricante' )
                )
                ->joinLeft( 'imagen'
                        , 'imagen.equipo_id = equipo.id and imagen.active = 1' ,
                            array( 'imagen.imagen as imagen' )
                )
                ->where( 'equipo.active IN (?)' , self::ACTIVE )
                ->where( 'equipo.publicacionEquipo_id  = ?' ,
                         Mtt_Models_Table_PublicacionEquipo::Activada )
                ->group( 'equipo.id' )
                ->query();

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    /*     * *
     */


    public function getRelatedEquipment( $equipoSlug , $limit = 4 )
        {
        $oldEquipoSlug = $equipoSlug;
        $long = 0;
        switch ( strlen( $equipoSlug ) )
            {
            case ( strlen( $equipoSlug ) > 30):
                $long = 4;
                break;
            case 20:
                $long = 4;
                break;
            case ( strlen( $equipoSlug ) < 10):
                $long = 2;
                break;
            case ( strlen( $equipoSlug ) < 5):
                $long = 2;
                break;
            default:
                $long = 3;
                break;
            }

        $equipoSlug = Mtt_Tools_Text::getCutText( $equipoSlug , $long );

        $db = $this->getAdapter();

        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'id' , 'nombre' , 'topofers' , 'slug' , 'modelo'
                        )
                )
                ->joinInner( 'categoria' ,
                             'categoria.id = equipo.categoria_id ' ,
                             array( 'categoria.nombre as categoria' )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array( 'fabricantes.nombre as fabricante' )
                )
                ->joinLeft( 'imagen'
                        , 'imagen.equipo_id = equipo.id and imagen.active = 1' ,
                            array( 'imagen.imagen as imagen' )
                )
                ->where( 'equipo.active IN (?)' , self::ACTIVE )
                ->where( 'equipo.publicacionEquipo_id = ?' ,
                         Mtt_Models_Table_PublicacionEquipo::Activada
                )
                ->where( "equipo.slug LIKE '$equipoSlug%'" )
                ->where( 'equipo.slug NOT IN (?)' , $oldEquipoSlug )
                ->group( 'equipo.id' )
                ->limit( $limit )
                ->query();

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    /**
     *
     * @return type Object
     */
    public function getProductsOfersAll( $limit = 0 )
        {

        $db = $this->getAdapter();

        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'id' , 'nombre' , 'topofers' , 'slug' , 'modelo'
                        )
                )
                ->joinInner( 'categoria' ,
                             'categoria.id = equipo.categoria_id ' ,
                             array( 'categoria.nombre as categoria' )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array( 'fabricantes.nombre as fabricante' )
                )
                ->joinLeft( 'imagen'
                        , 'imagen.equipo_id = equipo.id and imagen.active = 1' ,
                            array( 'imagen.imagen as imagen' )
                )
                ->where( 'equipo.topofers IN (?)' , self::ACTIVE )
                ->where( 'equipo.active IN (?)' , self::ACTIVE )
                ->where( 'equipo.publicacionEquipo_id = ?' ,
                         Mtt_Models_Table_PublicacionEquipo::Activada
                )
                ->group( 'equipo.id' )
                ->limit( $limit )
                ->query();

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    /**
     *
     * @param type $category_id
     * @param type $limit
     * @return type objects
     */
    public function getProductsOfersAllByCategory( $category_id , $limit = null )
        {
        $db = $this->getAdapter();

        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'id' ,
                    'nombre' ,
                    'topofers' ,
                    'slug' ,
                    'modelo'
                        )
                )
                ->joinInner( 'categoria' ,
                             'categoria.id = equipo.categoria_id ' ,
                             array( 'categoria.nombre as categoria' )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array( 'fabricantes.nombre as fabricante' )
                )
                ->joinLeft( 'imagen'
                        , 'imagen.equipo_id = equipo.id and imagen.active = 1' ,
                            array( 'imagen.imagen as imagen' )
                )
                ->where( 'equipo.topofers IN (?)' , self::ACTIVE )
                ->where( 'equipo.categoria_id IN (?)' , $category_id )
                ->where( 'equipo.active IN (?)' , self::ACTIVE )
                ->where( 'equipo.publicacionEquipo_id = ?'
                        , Mtt_Models_Table_PublicacionEquipo::Activada )
                ->group( 'equipo.id' )
                ->limit( $limit )
                ->query();

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    public function findByNombre( $active , $nombre = null )
        {

        $db = $this->getAdapter();
        $db->setFetchMode( Zend_Db::FETCH_OBJ );
        $query = $db->select()
                ->from( $this->_name )
                ->joinInner( 'categoria'
                        ,
                             'equipo.categoria_id = categoria.id'
                        , array( 'categoria' => 'nombre' )
                )
                ->joinInner( 'estadoequipo' ,
                             'estadoequipo.id = equipo.estadoequipo_id' ,
                             array( 'estadoequipo.nombre as estadoequipo' )
                )
                ->joinInner( 'usuario' , 'usuario.id = equipo.usuario_id' ,
                             array( 'usuario.nombre as usuario' )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array( 'fabricantes.nombre as fabricante' )
                )
                ->joinInner( 'paises' ,
                             'paises.id = equipo.paises_id'
                        , array( 'paises.nombre as paises' ) )
                ->where( 'equipo.active in (?)' , $active );
//->query();
        if ( isset( $nombre ) )
            {
            $query->where( "equipo.nombre LIKE '$nombre%'" );
            }

        ;
        $query->query();
        return $db->fetchAll( $query );


        }


    public function listEquipArray()
        {

        $db = $this->getAdapter();
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'id' ,
                    'nombre as equipo' ,
                    'precioventa' ,
                    'preciocompra' ,
                    'calidad' ,
                    'modelo' ,
                    'fechafabricacion' ,
                    'descripcion' ,
                    'documento' ,
                    'sourceDocumento' ,
                    'pesoEstimado' ,
                    'size' ,
                    'ancho' ,
                    'alto' ,
                    'sizeCaja' ,
                    'topofers' ,
                    'publishdate' ,
                    'active' )
                )
                ->joinInner(
                        'categoria' , 'categoria.id = equipo.categoria_id' ,
                        array( 'categoria.nombre as categoria' )
                )
                ->joinInner(
                        'publicacionEquipo' ,
                        'publicacionEquipo.id = equipo.publicacionEquipo_id' ,
                        array( 'publicacionEquipo.nombre as publicacionequipo' )
                )
                ->joinInner( 'usuario' , 'usuario.id = equipo.usuario_id' ,
                             array( 'usuario.nombre as usuario' )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array( 'fabricantes.nombre as fabricante' )
                )
                ->joinInner( 'moneda' , 'moneda.id = equipo.moneda_id' ,
                             array( 'moneda.nombre as moneda' )
                )
                ->joinInner( 'paises' ,
                             'paises.id = equipo.paises_id'
                        , array( 'paises.nombre as paises' ) )
                ->joinInner( 'estadoequipo' ,
                             'estadoequipo.id = equipo.estadoequipo_id' ,
                             array( 'estadoequipo.nombre as estadoequipo' ) )
                ->joinLeft( 'imagen'
                        , 'imagen.equipo_id = equipo.id and imagen.active = 1' ,
                            array( 'imagen.nombre as imageNombre' ,
                    'imagen.thumb as imageThumb' ,
                    'imagen.imagen as image' )
                )
                ->group( 'equipo.id' )
                ->query()
        ;

        return $query->fetchAll();


        }


    public function listEquip( $active = null )
        {
        $value = is_null( $active ) ? array( 0 , 1 ) : 1;
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'id' ,
                    'nombre as equipo' ,
                    'precioventa' ,
                    'preciocompra' ,
                    'calidad' ,
                    'modelo' ,
                    'fechafabricacion' ,
                    'descripcion' ,
                    'documento' ,
                    'sourceDocumento' ,
                    'pesoEstimado' ,
                    'size' ,
                    'ancho' ,
                    'alto' ,
                    'sizeCaja' ,
                    'topofers' ,
                    'publishdate' ,
                    'active' )
                )
                ->joinInner(
                        'categoria' , 'categoria.id = equipo.categoria_id' ,
                        array( 'categoria.nombre as categoria' )
                )
                ->joinInner(
                        'publicacionEquipo' ,
                        'publicacionEquipo.id = equipo.publicacionEquipo_id' ,
                        array( 'publicacionEquipo.nombre as publicacionequipo' )
                )
                ->joinInner( 'usuario' , 'usuario.id = equipo.usuario_id' ,
                             array( 'usuario.nombre as usuario' )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array( 'fabricantes.nombre as fabricante' )
                )
                ->joinInner( 'moneda' , 'moneda.id = equipo.moneda_id' ,
                             array( 'moneda.nombre as moneda' )
                )
                ->joinInner( 'paises' ,
                             'paises.id = equipo.paises_id'
                        , array( 'paises.nombre as paises' ) )
                ->joinInner( 'estadoequipo' ,
                             'estadoequipo.id = equipo.estadoequipo_id' ,
                             array( 'estadoequipo.nombre as estadoequipo' ) )
                ->joinLeft( 'imagen'
                        , 'imagen.equipo_id = equipo.id and imagen.active = 1' ,
                            array( 'imagen.nombre as imageNombre' ,
                    'imagen.thumb as imageThumb' ,
                    'imagen.imagen as image' )
                )
                ->where( 'equipo.active IN (?)' , $value )
                ->group( 'equipo.id' )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    /**
     *
     *
     */
    public function listEquipByUserStatus( $idUser , $status )
        {

        $db = $this->getAdapter();
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'equipo.id' , 'nombre as equipo' , 'precioventa' ,
                    'preciocompra' , 'calidad' , 'modelo' ,
                    'fechafabricacion' , 'documento' , 'sourceDocumento' ,
                    'pesoEstimado' ,
                    'descripcion' ,
                    'size' ,
                    'ancho' ,
                    'alto' ,
                    'sizeCaja' ,
                    'topofers' ,
                    'publishdate' ,
                    'active' ,
                    'slug' )
                )
                ->joinInner(
                        'categoria' , 'categoria.id = equipo.categoria_id' ,
                        array( 'categoria.nombre as categoria' )
                )
                ->joinInner(
                        'publicacionEquipo' ,
                        'publicacionEquipo.id = equipo.publicacionEquipo_id' ,
                        array( 'publicacionEquipo.nombre as publicacionequipo' )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array( 'fabricantes.nombre as fabricante' )
                )
                ->joinInner( 'moneda' , 'moneda.id = equipo.moneda_id' ,
                             array( 'moneda.nombre as moneda' )
                )
                ->joinInner( 'paises' ,
                             'paises.id = equipo.paises_id'
                        , array( 'paises.nombre as paises' ) )
                ->joinInner( 'estadoequipo' ,
                             'estadoequipo.id = equipo.estadoequipo_id' ,
                             array( 'estadoequipo.nombre as estadoequipo' ) )
                ->joinLeft( 'imagen'
                        , 'imagen.equipo_id = equipo.id and imagen.active = 1' ,
                            array( 'imagen.nombre as imageNombre' ,
                    'imagen.thumb as imageThumb' ,
                    'imagen.imagen as image' )
                )
                ->where( 'equipo.active = ?' , self::ACTIVE )
                ->where( 'equipo.publicacionEquipo_id = ?' , $status )
                ->where( 'equipo.usuario_id = ?' , $idUser )
                ->group( 'equipo.id' )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    public function countEquipmentsPendientes()
        {

        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name ,
                        array(
                    'cantidad' => 'COUNT(id)'
                        )
                )
                ->where( 'equipo.active = ?' , self::ACTIVE )
                ->where( 'equipo.publicacionEquipo_id = ?' ,
                         Mtt_Models_Bussines_PublicacionEquipo::Pendiente
                )
                ->query()
        ;

        return $query->fetchObject();


        }


    public function pagListEquipByUserStatus( $idUser , $status )
        {
        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/myConfigUser.ini' , 'paginator'
        );
        $data = $_conf->toArray();

        $object = Zend_Paginator::factory(
                        $this->listEquipByUserStatus( $idUser , $status )
        );
        $object->setItemCountPerPage(
                $data['ItemCountPerPage']
        );
        return $object;


        }


    public function pagListEquipByUser( $idUser )
        {
        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/myConfigUser.ini' , 'equipo'
        );
        $data = $_conf->toArray();

        $object = Zend_Paginator::factory( $this->listEquipByUser( $idUser ) );
        $object->setItemCountPerPage(
                $data['ItemCountPerPage']
        );
        return $object;


        }


    public function pagListEquip( $active = null , $nombre = null )
        {
        $value = is_null( $active ) ? array( 0 , 1 ) : 1;

        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/myConfigAdmin.ini' , 'equipo'
        );
        $data = $_conf->toArray();

        $object = Zend_Paginator::factory( $this->findByNombre( $value , $nombre ) );
        $object->setItemCountPerPage(
                $data['ItemCountPerPage']
        );
        return $object;


        }


    public function pagListResultSearch( $keywords , $modelo , $fabricante ,
                                         $categoria , $anioInicial ,
                                         $anioFinal , $precioInicial ,
                                         $precioFinal )
        {
        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/myConfigUser.ini' ,
                        'busqueda'
        );
        $data = $_conf->toArray();

        $object = Zend_Paginator::factory( $this->searchEquip( $keywords ,
                                                               $modelo ,
                                                               $fabricante ,
                                                               $categoria ,
                                                               $anioInicial ,
                                                               $anioFinal ,
                                                               $precioInicial ,
                                                               $precioFinal ) );
        $object->setItemCountPerPage(
                $data['ItemCountPerPage']
        );
        return $object;


        }


    public function listEquipByUser( $idUser )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'id' ,
                    'nombre as equipo' ,
                    'precioventa' ,
                    'preciocompra' ,
                    'calidad' ,
                    'modelo' ,
                    'fechafabricacion' ,
                    'descripcion' ,
                    'documento' ,
                    'sourceDocumento' ,
                    'pesoEstimado' ,
                    'size' ,
                    'ancho' ,
                    'alto' ,
                    'sizeCaja' ,
                    'topofers' ,
                    'publishdate' ,
                    'publicacionEquipo_id' ,
                    'slug' ,
                    'active' )
                )
                ->joinInner(
                        'categoria' , 'categoria.id = equipo.categoria_id' ,
                        array( 'categoria.nombre as categoria' )
                )
                ->joinInner(
                        'publicacionEquipo' ,
                        'publicacionEquipo.id = equipo.publicacionEquipo_id' ,
                        array(
                    'publicacionEquipo.nombre as publicacionequipo'
                        )
                )
                ->joinInner( 'usuario' , 'usuario.id = equipo.usuario_id' ,
                             array( 'usuario.nombre as usuario' )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array( 'fabricantes.nombre as fabricante' )
                )
                ->joinInner( 'moneda' , 'moneda.id = equipo.moneda_id' ,
                             array( 'moneda.nombre as moneda' )
                )
                ->joinInner( 'paises' ,
                             'paises.id = equipo.paises_id'
                        , array( 'paises.nombre as paises' ) )
                ->joinInner( 'estadoequipo' ,
                             'estadoequipo.id = equipo.estadoequipo_id' ,
                             array( 'estadoequipo.nombre as estadoequipo' ) )
                ->joinLeft( 'imagen'
                        , 'imagen.equipo_id = equipo.id and imagen.active = 1' ,
                            array( 'imagen.nombre as imageNombre' ,
                    'imagen.thumb as imageThumb' ,
                    'imagen.imagen as image' )
                )
                ->where( 'equipo.active = ?' , self::ACTIVE )
                ->where( 'usuario.id = ?' , $idUser )
                ->group( 'equipo.id' )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    public function listEquipUnresolved()
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'id' ,
                    'nombre as equipo' ,
                    'precioventa' ,
                    'preciocompra' ,
                    'modelo' ,
                    'fechafabricacion' ,
                    'active' )
                )
                ->joinInner(
                        'categoria' , 'categoria.id = equipo.categoria_id' ,
                        array( 'categoria.nombre as categoria' )
                )
                ->joinInner(
                        'moneda' , 'equipo.moneda_id = moneda.id' ,
                        array( 'simbolo' )
                )
                ->where( 'equipo.publicacionEquipo_id = ?' ,
                         Mtt_Models_Bussines_PublicacionEquipo::Pendiente )
                ->where( 'equipo.active = ?' , self::ACTIVE )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    /**
     *
     *
     */
    public function listEquipByStatus( $status )
        {

        $db = $this->getAdapter();
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'equipo.id' , 'nombre as equipo' , 'precioventa' ,
                    'preciocompra' , 'calidad' , 'modelo' ,
                    'fechafabricacion' , 'documento' , 'sourceDocumento' ,
                    'pesoEstimado' ,
                    'size' ,
                    'ancho' ,
                    'alto' ,
                    'sizeCaja' ,
                    'topofers' ,
                    'publishdate' ,
                    'active' )
                )
                ->joinInner(
                        'categoria' , 'categoria.id = equipo.categoria_id' ,
                        array( 'categoria.nombre as categoria' )
                )
                ->joinInner(
                        'publicacionEquipo' ,
                        'publicacionEquipo.id = equipo.publicacionEquipo_id' ,
                        array( 'publicacionEquipo.nombre as publicacionequipo' )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array( 'fabricantes.nombre as fabricante' )
                )
                ->joinInner( 'moneda' , 'moneda.id = equipo.moneda_id' ,
                             array( 'moneda.nombre as moneda' )
                )
                ->joinInner( 'paises' ,
                             'paises.id = equipo.paises_id'
                        , array( 'paises.nombre as paises' ) )
                ->joinInner( 'estadoequipo' ,
                             'estadoequipo.id = equipo.estadoequipo_id' ,
                             array( 'estadoequipo.nombre as estadoequipo' ) )
                ->joinLeft( 'imagen'
                        , 'imagen.equipo_id = equipo.id and imagen.active = 1' ,
                            array( 'imagen.nombre as imageNombre' ,
                    'imagen.thumb as imageThumb' ,
                    'imagen.imagen as image' )
                )
                ->where( 'equipo.active = ?' , self::ACTIVE )
                ->where( 'equipo.publicacionEquipo_id = ?' , $status )
                ->group( 'equipo.id' )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    /**
     * 
     * 
     */
    public function listEquipByCategory( $idCategoria , $status )
        {

        $db = $this->getAdapter();
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'equipo.id' , 'nombre as equipo' , 'precioventa' ,
                    'preciocompra' , 'calidad' , 'modelo' ,
                    'fechafabricacion' , 'documento' , 'sourceDocumento' ,
                    'pesoEstimado' ,
                    'size' ,
                    'ancho' ,
                    'alto' ,
                    'sizeCaja' ,
                    'topofers' ,
                    'publishdate' ,
                    'active' ,
                    'slug' )
                )
                ->joinInner(
                        'categoria' , 'categoria.id = equipo.categoria_id' ,
                        array( 'categoria.nombre as categoria' )
                )
                ->joinInner(
                        'publicacionEquipo' ,
                        'publicacionEquipo.id = equipo.publicacionEquipo_id' ,
                        array( 'publicacionEquipo.nombre as publicacionequipo' )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array( 'fabricantes.nombre as fabricante' )
                )
                ->joinInner( 'moneda' , 'moneda.id = equipo.moneda_id' ,
                             array( 'moneda.nombre as moneda' )
                )
                ->joinInner( 'paises' ,
                             'paises.id = equipo.paises_id'
                        , array( 'paises.nombre as paises' ) )
                ->joinInner( 'estadoequipo' ,
                             'estadoequipo.id = equipo.estadoequipo_id' ,
                             array( 'estadoequipo.nombre as estadoequipo' ) )
                ->joinLeft( 'imagen'
                        , 'imagen.equipo_id = equipo.id and imagen.active = 1' ,
                            array( 'imagen.nombre as imageNombre' ,
                    'imagen.thumb as imageThumb' ,
                    'imagen.imagen as image' )
                )
                ->where( 'equipo.active = ?' , self::ACTIVE )
                ->where( 'equipo.categoria_id = ?' , $idCategoria )
                ->where( 'equipo.publicacionEquipo_id = ?' , $status )
                ->group( 'equipo.id' )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    public function pagListEquipByCategory( $idCategoria , $status )
        {
        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/myConfig.ini' ,
                        'categoria_equipo'
        );
        $data = $_conf->toArray();

        $object = Zend_Paginator::factory(
                        $this->listEquipByCategory( $idCategoria , $status )
        );
        $object->setItemCountPerPage(
                $data['ItemCountPerPage']
        );
        return $object;


        }


    /**
     *
     *
     */
    public function listEquipMoreVisited( $limit )
        {

        $db = $this->getAdapter();
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'nombre as titulo' ,
                    'views as cantidad'
                        )
                )
                ->where( 'equipo.active = ?' , self::ACTIVE )
                ->limit( $limit )
                ->order( 'views DESC' )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    /**
     *
     *
     */
    public function listEquipMoreVisitedByCategory( $idCategoria , $limit )
        {

        $db = $this->getAdapter();
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'nombre as titulo' ,
                    'views as cantidad'
                        )
                )
                ->where( 'equipo.active = ?' , self::ACTIVE )
                ->where( 'equipo.categoria_id = ?' , $idCategoria )
                ->limit( $limit )
                ->order( 'views DESC' )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    /**
     *
     *
     */
    public function listEquipSalesUser( $idUser )
        {

        $db = $this->getAdapter();
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'equipo.id' ,
                    'equipo.nombre as equipo' ,
                    'precioventa AS precio' ,
                    'active'
                        )
                )
                ->joinLeft( 'operacion_has_equipo' ,
                            'operacion_has_equipo.equipo_id = equipo.id' ,
                            array( 'SUM(operacion_has_equipo.cantidad) AS cantidad' )
                )
                ->join( 'operacion' ,
                        'operacion_has_equipo.operacion_id = operacion.id' ,
                        array(
                    'operacion_id' => 'id' ,
                    'fecha'
                        )
                )
                ->join( 'categoria' , 'equipo.categoria_id = categoria.id' ,
                        array( 'categoria.nombre AS categoria' )
                )
                ->join( 'estadoequipo' ,
                        'equipo.estadoequipo_id = estadoequipo.id' ,
                        array( 'estadoequipo.nombre AS estadoequipo' )
                )
                ->join( 'fabricantes' ,
                        'equipo.fabricantes_id = fabricantes.id' ,
                        array( 'fabricantes.nombre AS fabricante' )
                )
                ->join( 'moneda' , 'equipo.moneda_id = moneda.id' ,
                        array( 'moneda.nombre AS moneda' ,
                    'moneda.simbolo AS simbolomoneda' )
                )
                ->join( 'paises' , 'equipo.paises_id = paises.id' ,
                        array( 'paises.nombre AS pais' )
                )
                ->where( 'estadooperacion_id = ?' ,
                         Mtt_Models_Bussines_EstadoOperacion::SALE )
                ->where( 'equipo.usuario_id = ?' , $idUser )
                ->where( 'equipo.publicacionEquipo_id = ?'
                        , Mtt_Models_Bussines_PublicacionEquipo::Vendido )
                ->group( 'equipo.id' )
                ->query()


        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    public function paglistEquipSalesUser( $idUser )
        {
        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/myConfigUser.ini' , 'paginator'
        );
        $data = $_conf->toArray();

        $object = Zend_Paginator::factory( $this->listEquipSalesUser( $idUser ) );
        $object->setItemCountPerPage(
                $data['ItemCountPerPage']
        );
        return $object;


        }


    /**
     *
     *
     */
    public function listEquipNoSalesUser( $idUser )
        {

        $db = $this->getAdapter();
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'equipo.id' ,
                    'equipo.nombre as equipo' ,
                    'precioventa AS precio' ,
                    'active'
                        )
                )
                ->join( 'categoria' , 'equipo.categoria_id = categoria.id' ,
                        array( 'categoria.nombre AS categoria' )
                )
                ->join( 'estadoequipo' ,
                        'equipo.estadoequipo_id = estadoequipo.id' ,
                        array( 'estadoequipo.nombre AS estadoequipo' )
                )
                ->join( 'fabricantes' ,
                        'equipo.fabricantes_id = fabricantes.id' ,
                        array( 'fabricantes.nombre AS fabricante' )
                )
                ->join( 'moneda' , 'equipo.moneda_id = moneda.id' ,
                        array( 'moneda.nombre AS moneda' ,
                    'moneda.simbolo AS simbolomoneda' )
                )
                ->join( 'paises' , 'equipo.paises_id = paises.id' ,
                        array( 'paises.nombre AS pais' )
                )
                ->where( 'equipo.usuario_id = ?' , $idUser )
                ->where( 'equipo.publicacionEquipo_id = ?' ,
                         Mtt_Models_Table_PublicacionEquipo::Activada )
                ->group( 'equipo.id' )
                ->query()


        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    public function pagListEquipNoSalesUser( $idUser )
        {
        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/myConfigUser.ini' , 'paginator'
        );
        $data = $_conf->toArray();

        $object = Zend_Paginator::factory( $this->listEquipNoSalesUser( $idUser ) );
        $object->setItemCountPerPage(
                $data['ItemCountPerPage']
        );
        return $object;


        }


    public function searchEquip( $keywords , $modelo , $fabricante ,
                                 $categoria , $anioInicial , $anioFinal ,
                                 $precioInicial , $precioFinal , $limit = null )
        {

        $db = $this->getAdapter();
        $db->setFetchMode( Zend_Db::FETCH_OBJ );
        $query = $db->select()
                ->from(
                        $this->_name ,
                        array(
                    'id' ,
                    'nombre as equipo' ,
                    'sinonimo' ,
                    'modelo' ,
                    'fechafabricacion' ,
                    'tag' ,
                    'slug' ,
                    'preciocompra' ,
                    'active' )
                )
                ->joinInner(
                        'categoria' , 'categoria.id = equipo.categoria_id' ,
                        array( 'categoria.nombre as categoria' )
                )
                ->joinInner( 'fabricantes' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array( 'fabricantes.nombre as fabricante' )
                )
                ->joinLeft( 'imagen'
                        ,
                            'imagen.equipo_id = equipo.id
                            and imagen.active = 1' ,
                            array( 'imagen.nombre as imageNombre' ,
                    'imagen.thumb as imageThumb' ,
                    'imagen.imagen as image' )
                )
                ->where( 'equipo.active = ?' , self::ACTIVE )
                ->where(
                        "equipo.nombre LIKE '%$keywords%'  OR " .
                        "equipo.modelo  LIKE '%$keywords%' OR " .
                        "equipo.id  LIKE '%$keywords%' OR " .
                        "equipo.descripcion  LIKE '%$keywords%' OR " .
                        "equipo.calidad  LIKE '%$keywords%' OR " .
                        "equipo.pesoEstimado  LIKE '%$keywords%' OR " .
                        "equipo.size  LIKE '%$keywords%' OR " .
                        "equipo.ancho  LIKE '%$keywords%' OR " .
                        "equipo.alto  LIKE '%$keywords%' OR " .
                        "equipo.sinonimo  LIKE '%$keywords%' OR " .
                        "equipo.sizeCaja  LIKE '%$keywords%' OR " .
                        "fabricantes.nombre LIKE '%$keywords%'"
                )
                ->where( "equipo.modelo LIKE '%$modelo%'" )
                ->where( "fabricantes.nombre LIKE '%$fabricante%'" )
                ->where( 'equipo.publicacionEquipo_id = ?' ,
                         Mtt_Models_Table_PublicacionEquipo::Activada
                )
                ->where( 'CASE ? WHEN -1
                    THEN equipo.categoria_id LIKE "%%" 
                    ELSE equipo.categoria_id = ? 
                    END' ,
                         $categoria )
                ->where( "equipo.fechafabricacion  > ? " , $anioInicial )
                ->where( "CASE ?
                    WHEN -1 
                    THEN equipo.fechafabricacion < 
                    (DATE_FORMAT(NOW(),'%Y')+1) 
                    ELSE equipo.fechafabricacion < ? 
                    END" ,
                         $anioFinal )
                ->where( 'equipo.preciocompra > ?' , $precioInicial )
                ->where( 'CASE ?
                    WHEN -1
                    THEN equipo.preciocompra > -1
                    ELSE equipo.preciocompra < ? END' ,
                         $precioFinal )
                ->where( 'equipo.publicacionEquipo_id = ?' ,
                         Mtt_Models_Table_PublicacionEquipo::Activada )
                ->group( 'equipo.id' );
        if ( isset( $limit ) )
            {
            $query->limit( $limit );
            }

        $query->query();
        return $db->fetchAll( $query );


        }


    /**
     * para enviar correo de autorizacion
     * @param array $data
     * @param string $subject
     */
    public function sendMailToRequest( array $data , $subject )
        {
        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/mail.ini' , APPLICATION_ENV
        );

        Zend_Mail::setDefaultFrom(
                $_conf->email->request , $_conf->data->request
        );

        $objSmtp = new Zend_Mail_Transport_Smtp(
                        $_conf->config->smtp ,
                        $_conf->config->toArray()
        );

        Zend_Mail::setDefaultReplyTo(
                $_conf->email->request , $_conf->data->request
        );

        Zend_Mail::setDefaultTransport( $objSmtp );

        Zend_Mail::setDefaultFrom(
                $_conf->email->request , $_conf->data->request
        );

        Zend_Mail::setDefaultReplyTo(
                $_conf->email->request , $_conf->data->request
        );

        $m = new Mtt_Html_Mail_Mailer(
                        $_conf->email->request , $_conf->data->request
        );
        $m->setSubject( $subject );

        $m->addTo( $_conf->email->request );

        if ( $data['toemail'] == 1 )
            {
            $m->addTo( $data['email'] );
            }


        $m->setViewParam( 'nombre' , $data['nombre'] )
                ->setViewParam( 'organizacion' , $data['organizacion'] )
                ->setViewParam( 'direccion' , $data['direccion'] )
                ->setViewParam( 'codpostal' , $data['codpostal'] )
                ->setViewParam( 'ciudad' , $data['ciudad'] )
                ->setViewParam( 'email' , $data['email'] )
                ->setViewParam( 'asunto' , $data['asunto'] )
                ->setViewParam( 'pais' , $data['pais'] )
                ->setViewParam( 'mensaje' , $data['mensaje'] )
                ->setViewParam( 'equipo' , $data['equipo'] )
                ->setViewParam( 'fabricante' , $data['fabricante'] )
                ->setViewParam( 'modelo' , $data['modelo'] )
                ->setViewParam( 'fechafabricacion' , $data['fechafabricacion'] )
                ->setViewParam( 'equipo_id' , $data['equipo_id'] )
                ->setViewParam( 'codpostal' , $data['codpostal'] )
                ->setViewParam( 'precio' ,
                                isset( $data['precio'] ) ? $data['precio'] : ''  )
                ->setViewParam( 'moneda' ,
                                isset( $data['moneda'] ) ? $data['moneda'] : ''  )
                ->setViewParam( 'link' , $data['link'] )
        ;

        $m->sendHtmlTemplate( "request.phtml" );


        }


    public function publicarEquipo( $id )
        {
        $data["publicacionEquipo_id"] = Mtt_Models_Table_PublicacionEquipo::Activada;
        $data["publishdate"] = date( 'Y-m-d H:m:s' );
        $this->update( $data
                , 'id = ' . $id );


        }


    public function updateEquipo( array $data , $id )
        {
        $this->update( $data , 'id = ' . $id );


        }


    public function saveEquipo( array $data )
        {
        $usuario = Zend_Auth::getInstance()->getStorage()->read();

        if ( is_null( $data['fechafabricacion'] ) )
            {
            unset( $data['fechafabricacion'] );
            }

        $slug = new Mtt_Filter_Slug( array(
                    'field' => 'slug' ,
                    'model' => $this
                        ) );

        $data['usuario_id'] = $usuario['usuario']->id;
        $data['publicacionEquipo_id'] = Mtt_Models_Table_PublicacionEquipo::Pendiente;
        $data['slug'] = $slug->filter( $data['nombre'] );

        return $this->insert( $data );


        }


    public function saveEquipoAdmin( array $data )
        {

        $usuario = Zend_Auth::getInstance()->getStorage()->read();

        if ( is_null( $data['fechafabricacion'] ) )
            {
            unset( $data['fechafabricacion'] );
            }

        $slug = new Mtt_Filter_Slug( array(
                    'field' => 'slug' ,
                    'model' => $this
                        ) );

        $data['usuario_id'] = $usuario['usuario']->id;
        $data['slug'] = $slug->filter( $data['nombre'] );
        return $this->insert( $data );


        }


    public function deleteEquipo( $id )
        {

        $this->delete( 'id = ?' , $id );


        }


    public function activarEquipo( $id )
        {

        $this->update( array(
            "active" => self::ACTIVE )
                , 'id = ' . $id );


        }


    public function desactivarEquipo( $id )
        {

        $this->update( array(
            "active" => self::DESACTIVATE )
                , 'id = ' . $id );


        }


    public function updateView( $id )
        {
        $equipo = $this->getFindId( $id );
//$equipo = $equipo->toArray();
        $newView = ( int ) $equipo->views + 1;
        $data = array( 'views' => $newView );

        $this->update( $data , 'id = ' . $id );


        }


    protected function validarFechaManufact( $fecha )
        {
//        if ( !$this->objDate->isDate( $fecha ) )
//            {
//        $this->objDate->set(
//                Mtt_Tools_Text::setDateYear(
//                        ( int ) $fecha
//                ) , "Y-MM-dd"
//        );
//        return $this->objDate->get( 'Y-MM-dd' );
        return Mtt_Tools_Text::setDateYear(
                        ( int ) $fecha
        );
//            }
//        else
//            {
//            
//            return $fecha;
//            }


        }


    }

