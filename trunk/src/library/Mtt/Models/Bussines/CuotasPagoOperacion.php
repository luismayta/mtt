<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Bussines_CuotasPagoOperacion
        extends Mtt_Models_Table_CuotasPagoOperacion
    {


    public function __construct( $config = array( ) )
        {

        parent::__construct( $config );
        }


    public function getFindId( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'id = ?' , $id )
                ->query()
        ;
        return $query->fetchObject();
        }


    public function getNroCuotas( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name , array( 'count(*)+1 as cantidad' )
                )
                ->where( 'operacion_id = ?' , $id )
                ->query()
        ;
        return $query->fetchObject();
        }


    public function getMontoCuotasByOperacion( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name , array( 'SUM(monto)  as subtotal' )
                )
                ->where( 'operacion_id = ?' , $id )
                ->where( 'active = ?' , self::ACTIVE )
                ->query()
        ;
        $valor = $query->fetchObject();
        return $valor->subtotal;
        }


    public function listar()
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active = ?' , '1' )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ );
        }


    public function listByOperation( $id )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name
                )
                ->joinInner( 'operacion' ,
                             'cuotaspagooperacion.operacion_id = 
                            operacion.id ' ,
                             array(
                    'operacion.id as op_equip_id'
                        )
                )
                ->where( 'cuotaspagooperacion.operacion_id  = ?' , $id
                )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ );
        }


    public function updateCuotasPagoOperacion( array $data , $id )
        {

        $this->update( $data , 'id = ' . $id );
        }


    public function saveCuotasPagoOperacion( array $data )
        {

        $this->insert( $data );
        }


    public function deleteCuotasPagoOperacion( $id )
        {

        $this->delete( 'id = ?' , $id );
        }


    public function activarCuotasPagoOperacion( $id )
        {

        $this->update( array( "active" => self::ACTIVE ) , 'id = ' . $id );
        }


    public function desactivarCuotasPagoOperacion( $id )
        {

        $this->update( array( "active" => self::DESACTIVATE ) , 'id = ' . $id );
        }


    }
