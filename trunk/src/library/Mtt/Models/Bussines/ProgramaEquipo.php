<?php


/**
 * 
 */
class Mtt_Models_Bussines_ProgramaEquipo
        extends Mtt_Models_Table_ProgramaEquipo
{


    public function __construct( $config = array( ) )
    {

        parent::__construct( $config ) ;


    }


    public function listar()
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active = ?' ,
                         self::ACTIVE )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    /**
     *
     * @param type $programaId
     * @param type $manufacturerId 
     */
    public function getIetmsByProgramaIdManufacturerId( $programaId ,
                                                        $manufacturerId )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( 'fabricantes' ,
                        array(
                    'fabricantes_id' => 'id' ,
                    'fabricante' => 'nombre'
                        )
                )
                ->joinInner( 'equipo' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array(
                    'equipo' => 'nombre' ,
                    'id' ,
                    'modelo' ,
                    'slug'
                        )
                ) ->joinInner( 'categoria' ,
                             'categoria.id = equipo.categoria_id ' ,
                             array(
                    'categoria' => 'nombre'
                        )
                )
                ->joinLeft( 'imagen'
                        ,
                            'imagen.equipo_id = equipo.id and imagen.active = 1' ,
                            array( 'imagen' => 'imagen' )
                )
                ->joinInner( $this->_name ,
                             'programaequipo.equipo_id = equipo.id' ,
                             array( 'programaequipo.id as programaequipo_id' ) )
                ->joinInner( 'programa' ,
                             'programa.id= programaequipo.programa_id' ,
                             array( 'programa.nombre as programa' ) )
                ->where( 'programaequipo.active = ?' ,
                         self::ACTIVE )
                ->where( 'equipo.active = ?' ,
                         self::ACTIVE )
                ->where( 'programaequipo.programa_id = ?' ,
                         $programaId )
                ->where( 'fabricantes.id = ?' ,
                         $manufacturerId )
                ->group( 'equipo.id' )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function getProgramaEquipoById( $id )
    {
        
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( 'fabricantes' ,
                        array(
                    'fabricante_id' => 'id' ,
                    'fabricante' => 'nombre' ,
                    'imagen' , 'slug'
                        )
                )
                ->joinInner( 'equipo' ,
                             'fabricantes.id = equipo.fabricantes_id' ,
                             array('equipo'=>'nombre','nroEquipos'=>'(COUNT(equipo.nombre))' ) )
                ->joinInner( $this->_name ,
                             'programaequipo.equipo_id = equipo.id' ,
                             array( 'programaequipo.id as programaequipo_id' ) )
                ->joinInner( 'programa' ,
                             'programa.id= programaequipo.programa_id' ,
                             array( 'programa.nombre as programa' )
                )
                ->where( 'programaequipo.active = ?' ,
                         self::ACTIVE )
                ->where( 'equipo.active = ?' ,
                         self::ACTIVE )
                ->where( 'programaequipo.programa_id = ?' ,
                         $id )
                ->group( 'fabricantes.id' )
                ->order( 'fabricantes.nombre' )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function addEquipmentByArray( $programaId , $equipments = array( ) )
    {
        $data = array( ) ;
        foreach ( $equipments as $equipment )
        {
            $data['equipo_id'] = $equipment ;
            $data['programa_id'] = $programaId ;
            $data['active'] = self::ACTIVE ;
            $this->saveProgramaEquipo( $data ) ;
        }


    }


    public function saveProgramaEquipo( array $data )
    {
        $this->insert( $data ) ;


    }


    public function updateProgramaEquipo( array $data , array $dataUsuario )
    {
        $registro = array( ) ;
        $alerta = array( ) ;
        foreach ( $dataUsuario as $fila )
        {
            $alerta[$fila->tipo] = $fila ;
        }
        for ( $i = 1 ; $i <= Mtt_Models_Table_Alerta::NAlertas ; $i++ )
        {

            $obj = "alerta" . $i ;
            $registro['active'] = $data[$obj] ;
            $registro['fechamodificacion'] = date( 'Y-m-d H:i:s' ) ;

            if ( isset( $data['categorias'] ) )
            {
                $registro['detalle'] = ($i == 2) ?
                        implode( ',' ,
                                 $data['categorias'] ) : null ;
            }
            $this->update( $registro ,
                           'id = ' . $alerta[$i]->id ) ;
        }


    }


    /**
     *
     * @param type $programaId
     * @param type $equipoId 
     */
    public function getIdByProgramaIdEquipoId( $programaId , $equipoId )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name ,
                        array(
                    'id' ,
                    'equipo_id' ,
                    'programa_id'
                        )
                )
                ->where( 'programa_id IN (?)' ,
                         $programaId )
                ->where( 'equipo_id IN (?)' ,
                         $equipoId )
                ->where( 'active = ?' ,
                         self::ACTIVE )
                ->query()
        ;
        return $query->fetchObject() ;


    }


    /**
     *
     * @param type $programaId
     * @param type $equipments 
     */
    public function deleteProgramaEquipoByArrayId( $programaId ,
                                                   $equipments = array( ) )
    {
        foreach ( $equipments as $equipment )
        {
            $_programaEquipo = $this->getIdByProgramaIdEquipoId( $programaId ,
                                                                 $equipment ) ;

            $this->deleteProgramaEquipo( $_programaEquipo->id ) ;
        }


    }


    public function deleteProgramaEquipo( $id )
    {


        $this->delete( "id = $id" ) ;


    }


    public function activarProgramaEquipo( $id )
    {

        $this->update( array( "active" => self::ACTIVE ) ,
                       'id = ' . $id ) ;


    }


    public function desactivarProgramaEquipo( $id )
    {

        $this->update( array( "active" => self::DESACTIVATE ) ,
                       'id = ' . $id ) ;


    }


}
