
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Bussines_Imagen
        extends Mtt_Models_Table_Imagen
    {


    public function __construct( $config = array( ) )
        {

        parent::__construct( $config );


        }


    public function getFindId( $id )
        {

        return $this->fetchRow( 'id = ' . $id );


        }


    public function listByEquip( $idEquipo )
        {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active = ?' , self::ACTIVE )
                ->where( 'equipo_id = ?' , $idEquipo )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ );


        }


    /**
     *
     * @param type $idEquipo
     * @param type $filtro
     * @param type $idImagen
     * @return type 
     */
    public function getImagesByEquip( $idEquipo , $filtro = null ,
                                      $idImagen = null )
        {
        $all = is_null( $filtro ) ? self::ACTIVE : array( 0 , 1 );
        $db = $this->getAdapter();
        $db->setFetchMode( Zend_Db::FETCH_OBJ );
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active in (?)' , $all )
                ->where( 'equipo_id = ?' , $idEquipo )
                ->order( 'order' );

        if ( !is_null( $idImagen ) )
            {
            $query->where( 'id != ?' , $idImagen );
            }

        $query->query();


        return $db->fetchAll( $query );


        }


    public function updateImagen( array $data , $id )
        {

        $this->update( $data , 'id = ' . $id );


        }


    public function saveImagen( array $data )
        {
        $data['thumb'] = $data['imagen'];
        $data['avatar'] = $data['imagen'];
        $data['imgequipo'] = $data['imagen'];
        $this->insert( $data );


        }


    public function deleteImagen( $id )
        {

        $this->delete( "id = $id" );


        }


    public function activarImagen( $id )
        {

        $this->update( array( "active" => self::ACTIVE ) , 'id = ' . $id );


        }


    public function desactivarImagen( $id )
        {

        $this->update(
                array( "active" => self::DESACTIVATE , "order" => 0 ) ,
                'id = ' . $id );


        }


    /**
     *
     * @param type $equipoId 
     */
    public function orderImagenById( $equipoId , $imagenId )
        {
        $objectImagenes = $this->getImagesByEquip( $equipoId , null , $imagenId );
        $this->updateImagen( array( 'order' => 1 ) , $imagenId );
        $cont = 1;
        foreach ( $objectImagenes as $imagen )
            {
            $cont++;
            $this->updateImagen(
                    array( 'order' => $cont ) , $imagen->id
            );
            }


        }


    }

