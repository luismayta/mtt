<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Bussines_Idioma
        extends Mtt_Models_Table_Idioma
{


    public function __construct( $config = array( ) )
    {

        parent::__construct( $config ) ;


    }


    public function getByPrefijo( $prefijo )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'prefijo = ?' ,
                         $prefijo )
                ->where( 'active = ?' ,
                         self::ACTIVE )
                ->query()
        ;
        return $query->fetchObject() ;


    }


    public function getComboValues()
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active = ?' ,
                         self::ACTIVE )
                ->order( 'nombre' )
                ->query()
        ;
        $filas = $query->fetchAll( Zend_Db::FETCH_ASSOC ) ;
        $values = array( ) ;

        foreach ( $filas as $fila )
        {

            $values[$fila['id']] = $fila['nombre'] ;
        }
        return $values ;


    }


    public function getComboValuesPrefijo()
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active = ?' ,
                         self::ACTIVE )
                ->order( 'nombre' )
                ->query()
        ;
        $filas = $query->fetchAll( Zend_Db::FETCH_ASSOC ) ;
        $values = array( ) ;
        foreach ( $filas as $fila )
        {
            $values[$fila['prefijo']] = $fila['nombre'] ;
        }
        return $values ;


    }


    public function getFindId( $id )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'id = ?' ,
                         $id )
                ->where( 'active = ?' ,
                         self::ACTIVE )
                ->query()
        ;
        return $query->fetchObject() ;


    }


    public function listar()
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active = ?' ,
                         self::ACTIVE )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function pagListLanguages()
    {
        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/myConfigAdmin.ini' ,
                        'idioma'
        ) ;
        $data = $_conf->toArray() ;

        $object = Zend_Paginator::factory( $this->listar() ) ;
        $object->setItemCountPerPage(
                $data['ItemCountPerPage']
        ) ;
        return $object ;


    }


    public function updateIdioma( array $data , $id )
    {

        $this->update( $data ,
                       'id = ' . $id ) ;


    }


    public function saveIdioma( array $data )
    {

        $this->insert( $data ) ;


    }


    public function deleteIdioma( $id )
    {

        $this->delete( 'id = ?' ,
                       $id ) ;


    }


    public function activarIdioma( $id )
    {

        $this->update( array( "active" => self::ACTIVE ) ,
                       'id = ' . $id ) ;


    }


    public function desactivarIdioma( $id )
    {

        $this->update( array( "active" => self::DESACTIVATE ) ,
                       'id = ' . $id ) ;


    }


}
