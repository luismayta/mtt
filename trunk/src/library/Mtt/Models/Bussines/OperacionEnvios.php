<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Mtt_Models_Bussines_OperacionEnvios
        extends Mtt_Models_Table_OperacionEnvios
{


    public function __construct( $config = array( ) )
    {

        parent::__construct( $config ) ;


    }


    public function getOperacionEnvios( $id )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'id IN (?)' ,
                         $id )
                ->query() ;

        return $query->fetchObject() ;


    }


    public function listar( $active )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'active in (?)' ,
                         $active )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function listByOperation( $idOperacion , $active )
    {
        $db = $this->getAdapter() ;
        $query = $db->select()
                ->from( $this->_name )
                ->where( 'operacion_id = ?' ,
                         $idOperacion )
                ->where( 'active in (?)' ,
                         $active )
                ->query()
        ;

        return $query->fetchAll( Zend_Db::FETCH_OBJ ) ;


    }


    public function pagListOperacionEnvios( $active = null )
    {
        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/myConfigAdmin.ini'
                        , 'operacion_envio'
        ) ;
        $data = $_conf->toArray() ;

        $value = ($active == null) ? array( 0 , 1 ) : 1 ;

        $object = Zend_Paginator::factory( $this->listar( $value ) ) ;
        $object->setItemCountPerPage(
                $data['ItemCountPerPage']
        ) ;
        return $object ;


    }


    public function pagListEnviosByOperation( $idOperacion , $active = null )
    {
        $_conf = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/myConfigAdmin.ini'
                        , 'operacion_envio'
        ) ;
        $data = $_conf->toArray() ;

        $value = ($active == null) ? array( 0 , 1 ) : 1 ;

        $object = Zend_Paginator::factory(
                        $this->listByOperation( $idOperacion ,
                                                $value )
        ) ;
        $object->setItemCountPerPage(
                $data['ItemCountPerPage']
        ) ;
        return $object ;


    }


    public function saveOperacionEnvios( array $data )
    {
        $data['fecha'] = date( 'Y-m-d H:i:s' ) ;
        $this->insert( $data ) ;


    }


    public function activarOperacionEnvios( $id )
    {

        $this->update( array( "active" => self::ACTIVE ) ,
                       'id = ' . $id ) ;


    }


    public function desactivarOperacionEnvios( $id )
    {

        $this->update( array( "active" => self::DESACTIVATE ) ,
                       'id = ' . $id ) ;


    }


}
