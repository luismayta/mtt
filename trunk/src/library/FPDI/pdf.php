<?

require_once(APPLICATION_PATH . "/../library/TCPDF/tcpdf.php");
require_once(APPLICATION_PATH . "/../library/FPDI/fpdi.php");

class PDF extends FPDI {

    /**
     * "Remembers" the template id of the imported page
     */
    var $_tplIdx;

    /**
     * include a background template for every page
     */
    function Header() {
        if (is_null($this->_tplIdx)) {
            $this->setSourceFile(APPLICATION_PATH . '/../templates/template.pdf');
            $this->_tplIdx = $this->importPage(1);
        }
        $this->useTemplate($this->_tplIdx);

        $this->SetFont('freesans', 'B', 9);
        $this->SetTextColor(255);
        $this->SetXY(60.5, 24.8);
        $this->Cell(0, 8.6, "TCPDF and FPDI");
    }

    function Footer() {
        
    }

    function SetFillColorCelda($r, $g, $b) {
        $this->SetFillColor($r, $g, $b);
    }

}
?>