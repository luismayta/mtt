/*
SQLyog Enterprise - MySQL GUI
MySQL - 5.5.8-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `moneda` (
	`id` double ,
	`nombre` varchar (300),
	`simbolo` varchar (30),
	`prefijo` varchar (135),
	`active` double ,
	`cambio` Decimal (10),
	`fechacambio` date ,
	`codepais` varchar (150)
); 
insert into `moneda` (`id`, `nombre`, `simbolo`, `prefijo`, `active`, `cambio`, `fechacambio`, `codepais`) values('1','Swiss Francs','Fr',NULL,'1',NULL,NULL,'CH');
insert into `moneda` (`id`, `nombre`, `simbolo`, `prefijo`, `active`, `cambio`, `fechacambio`, `codepais`) values('2','US Dollars','$',NULL,'1',NULL,NULL,'US');
insert into `moneda` (`id`, `nombre`, `simbolo`, `prefijo`, `active`, `cambio`, `fechacambio`, `codepais`) values('3','Peruvian Nuevo Sol','S/.',NULL,'1',NULL,NULL,'PE');
insert into `moneda` (`id`, `nombre`, `simbolo`, `prefijo`, `active`, `cambio`, `fechacambio`, `codepais`) values('4','Chilean Peso','$',NULL,'1',NULL,NULL,'CL');
insert into `moneda` (`id`, `nombre`, `simbolo`, `prefijo`, `active`, `cambio`, `fechacambio`, `codepais`) values('5','Euros','Ã¢â€šÂ¬',NULL,'1',NULL,NULL,'DE');
insert into `moneda` (`id`, `nombre`, `simbolo`, `prefijo`, `active`, `cambio`, `fechacambio`, `codepais`) values('6','Polish new Zloty','zÃ…â€š',NULL,'1',NULL,NULL,'PL');
