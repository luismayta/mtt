/*
SQLyog Enterprise - MySQL GUI
MySQL - 5.5.8-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `tipousuario` (
	`id` double ,
	`nombre` varchar (300),
	`active` double 
); 
insert into `tipousuario` (`id`, `nombre`, `active`) values('1','Manager','1');
insert into `tipousuario` (`id`, `nombre`, `active`) values('2','Registered','1');
insert into `tipousuario` (`id`, `nombre`, `active`) values('3','User','1');
