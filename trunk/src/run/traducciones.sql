/*
SQLyog Enterprise - MySQL GUI
MySQL - 5.5.8-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `traducciones` (
	`id` double ,
	`idiomas_id` double ,
	`entidades_id` double ,
	`campo_id` double ,
	`nombrecampo` varchar (450),
	`texto` blob ,
	`active` double 
); 
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('1','1','5','1','Efectivo','cash','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('2','2','5','1','Efectivo','gotÃ³wka','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('3','3','5','1','Efectivo','Efectivo','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('4','4','5','1','Efectivo','Bargeld','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('5','1','5','2','Credito','credit','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('6','2','5','2','Credito','kredyt','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('7','3','5','2','Credito','credito','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('8','4','5','2','Credito','Kredit','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('9','1','3','1','Pendiente Pago','ingles','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('10','2','3','1','Pendiente Pago','polaco','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('11','3','3','1','Pendiente Pago','espaÃ±ol','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('12','4','3','1','Pendiente Pago','aleman','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('13','1','3','2','Vendidos','ingles','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('14','2','3','2','Vendidos','polaco','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('15','3','3','2','Vendidos','espaÃ±ol','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('16','4','3','2','Vendidos','aleman','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('17','1','3','3','Reservado','ingles','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('18','2','3','3','Reservado','polaco','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('19','3','3','3','Reservado','espaÃ±ol','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('20','4','3','3','Reservado','aleman','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('21','1','3','4','Pendiente Entrega','ingles','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('22','2','3','4','Pendiente Entrega','polaco','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('23','3','3','4','Pendiente Entrega','espaÃ±ol','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('24','4','3','4','Pendiente Entrega','aleman','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('25','1','3','5','Entregado','ingles','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('26','2','3','5','Entregado','polaco','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('27','3','3','5','Entregado','espaÃ±ol','1');
insert into `traducciones` (`id`, `idiomas_id`, `entidades_id`, `campo_id`, `nombrecampo`, `texto`, `active`) values('28','4','3','5','Entregado','aleman','1');
