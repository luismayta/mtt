/*
SQLyog Enterprise - MySQL GUI
MySQL - 5.5.8-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `categoria` (
	`id` double ,
	`nombre` varchar (300),
	`title` varchar (450),
	`thumbnail` varchar (765),
	`descripcion` blob ,
	`published` double ,
	`order` double ,
	`active` double ,
	`slug` varchar (600)
); 
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('148','Others','Others',NULL,'',NULL,'19','1','others');
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('150','Apparatus / General Lab','Apparatus / General Lab',NULL,'',NULL,'2','1','apparatus-general-lab');
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('151','Cardio Devices','Cardio Devices',NULL,'',NULL,'5','1','cardio-devices');
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('152','Balances','Balances',NULL,'',NULL,'6','1','balances');
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('153','Centrifugues','Centrifugues',NULL,'',NULL,'7','1','centrifugues');
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('154','Respirators','Respirators',NULL,'',NULL,'8','1','respirators');
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('155','Endoscopy Devices','Endoscopy Devices',NULL,'',NULL,'10','1','endoscopy-devices');
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('156','Microscopes','Microscopes',NULL,'',NULL,'17','1','microscopes');
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('157','Imaging Systems','Imaging Systems',NULL,'',NULL,'12','1','imaging-systems');
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('158','Chirurgical Devices','Chirurgical Devices',NULL,'',NULL,'14','1','chirurgical-devices');
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('159','Monitoring Devices','Monitoring Devices',NULL,'',NULL,'9','1','monitoring-devices');
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('160','Perfusors & Pumps','Perfusors & Pumps',NULL,'',NULL,'16','1','perfusors-pumps');
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('161','Ultrasound','Ultrasound',NULL,'',NULL,'18','1','ultrasound');
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('162','Incubators','Incubators',NULL,'',NULL,'13','1','incubators');
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('163','Autoclaves','Autoclaves',NULL,'',NULL,'3','1','autoclaves');
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('164','Coolers & Freezers','Coolers & Freezers',NULL,'',NULL,'11','1','coolers-freezers');
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('165','Medical Furnitures / Beds','Medical Furnitures / Beds',NULL,'',NULL,'15','1','medical-furnitures-beds');
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('168','X-Ray','X-Ray',NULL,'',NULL,'4','1','x-ray');
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('169','NEU- & Demogeräte','NEU- & Demogeräte',NULL,'',NULL,'20','1','neu-demoger-te');
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('170','prueba','Prueba 2','123.jpg','esto es una vaina',NULL,NULL,'0','');
insert into `categoria` (`id`, `nombre`, `title`, `thumbnail`, `descripcion`, `published`, `order`, `active`, `slug`) values('171','prueba2','ddsfsd',NULL,'dfsdfdsfs',NULL,NULL,'0','');
