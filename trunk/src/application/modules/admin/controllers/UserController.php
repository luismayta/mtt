<?php


class Admin_UserController
        extends Mtt_Controller_Action
    {

    protected $_user;
    public $ajaxable = array(
        'searchusuario' => array( 'html' , 'json' )
    );


    public function init()
        {

        parent::init();
        $this->_user = new Mtt_Models_Bussines_Usuario();
        }


    public function indexAction()
        {


        $this->view->jQuery()
                ->addJavascriptFile(
                        '/js/jquery.contextMenu/jquery.contextMenu.js'
                )->addJavascriptFile(
                        '/theme/admin/js/contextmenu.js'
                )
                ->addStylesheet(
                        '/css/jquery.contextMenu/jquery.contextMenu.css'
        );

        $grid = new Mtt_JQuery_JqGrid( 'Usuarios' ,
                        new Mtt_JQuery_JqGrid_Adapter_Array(
                                $this->_user->listArray()
                        )
                        
        );

        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'id' , array( 'label' => 'id' )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'nombre' , array(
                    'label' => $this->_translate->translate( 'Nombre' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'apellido' , array(
                    'label' => $this->_translate->translate( 'Apellido' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'email' , array(
                    'label' => $this->_translate->translate( 'Email' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'login' , array(
                    'label' => $this->_translate->translate( 'Login' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'rol' , array(
                    'label' => $this->_translate->translate( 'Rol' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'fecharegistro' , array(
                    'label' => $this->_translate->translate( 'Fecha De Registro' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'ciudad' , array(
                    'label' => $this->_translate->translate( 'Ciudad' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'fabricante' , array(
                    'label' => $this->_translate->translate( 'manufacturer' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'codpostal' , array(
                    'label' => $this->_translate->translate( 'Cod. Postal' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'pais' , array(
                    'label' => $this->_translate->translate( 'Pais' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'active' , array(
                    'label' => $this->_translate->translate( 'activo' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'acreditado' , array(
                    'label' => $this->_translate->translate( 'acreditado' )
                        )
                )
        );



        $grid->registerPlugin( new Mtt_JQuery_JqGrid_Plugin_ToolbarFilter() );

        $this->view->assign( 'grid' , $grid->render() );

        /* $jForm = new Mtt_JForm_Search_Usuario(); */
        /* $usuarios = null;
          if ($this->_request->isPost() &&
          $jForm->isValid($this->_request->getPost())) {
          $usuarios = $this->_user->pagListUser(
          $active = null, $jForm->getValue('nombre')
          );
          } else {

          $usuarios = $this->_user->pagListUser(
          $active = null, null
          );
          $usuarios->setCurrentPageNumber(
          $this->_getParam('page', 1)
          );
          }
         */
        }


    public function detalleAction()
        {

        $this->_helper->layout->disableLayout();

        $id = intval( $this->_getParam( 'id' ) );

        $this->view->jQuery()
                ->addOnLoad(
                        ' $(document).ready(function() {
                            $("#tabs").tabs();
                          });'
                )
        ;
        //Editar datos del usuario
        $this->editarAction();

        //Listar equipos del Usuario
        $equipo = new Mtt_Models_Bussines_Equipo();
        $equipos_user = $equipo->listEquipByUser( $id );
        $this->view->assign( 'equipos' , $equipos_user );

        //Listar preguntas que el usuario formulo
        $pregunta = new Mtt_Models_Bussines_Pregunta();
        $preguntas_user = $pregunta->listByUser( $id );
        $this->view->assign( 'preguntas' , $preguntas_user );

        //Listar reservas
        $reserva = new Mtt_Models_Bussines_Reserva();
        $reservas_user = $reserva->getReservaByUser( $id
                , Mtt_Models_Table_TipoReserva::RESERVED );
        $this->view->assign( 'reservas' , $reservas_user );

        //Listar operaciones
        $operacion = new Mtt_Models_Bussines_Operacion();
        $operaciones_user = $operacion->
                listByUserOperation( $id ,
                                     Mtt_Models_Table_EstadoOperacion::SALE
        );
        $this->view->assign( 'operaciones' , $operaciones_user );
        }


    public function searchusuarioAction()
        {
        $results = $this->_user->search(
                $this->_getParam( 'term' )
        );
        $this->_helper->json(
                array_values( $results )
        );
        }


    public function activausuariosAction()
        {
        $this->view->assign(
                'titulo' , $this->_translate->translate( 'activar usuarios' )
        );

        $form = new Mtt_Form_ActivarUsuario();
        $this->view->assign( 'frmActiveUser' , $form );

        if ( $this->_request->isPost()
                &&
                $form->isValid( $this->_request->getPost() ) )
            {

            $usuarios = $form->getValues();
            $ids = $usuarios['usuarios'];

            foreach ( $ids as $item )
                {
                $this->_user->habilitarUsuario( $item );
                }

            $this->_helper->FlashMessenger(
                    $this->_translate->translate( 'se valido a los usuarios' ) );
            $this->_redirect(
                    $this->URL . "/" . $this->getRequest()->getActionName()
            );
            }
        }


    public function editarAction()
        {
        $id = intval( $this->_getParam( 'id' ) );

        $form = new Mtt_EditForm_Usuario( array( 'id' => $id ) );

        $usuario = $this->_user->getFindId( $id );

        if ( !is_null( $usuario ) )
            {
            if ( $this->_request->isPost()
                    &&
                    $form->isValid( $this->_request->getPost() ) )
                {
                $this->_user->updateUsuario(
                        $form->getValues() , $id
                );
                $this->_helper->FlashMessenger(
                        $this->_translate->translate( 'Changed a User' )
                );
                $this->_redirect( $this->URL );
                }
            $form->setDefaults( $usuario->toArray() );
            $this->view->assign( 'form' , $form );
            }
        else
            {
            $this->_helper->FlashMessenger(
                    $this->_translate->translate( 'no es usuario' ) );
            $this->_redirect( $this->URL );
            }
        }


    public function changePasswordAction()
        {

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'cambiar password' );

        $id = intval( $this->_getParam( 'id' ) );

        $form = new Mtt_Form_ChangePassword();

        $form->submit->setLabel(
                $this->_translate->translate(
                        'Change Password'
                )
        );

        $usuario = $this->_user->getFindId( $id );

        if ( !is_null( $usuario ) )
            {
            if ( $this->_request->isPost()
                    &&
                    $form->isValid( $this->_request->getPost() ) )
                {

                $data = $form->getValues();
                $newClave = Mtt_Auth_Adapter_DbTable_Mtt::generatePassword(
                                $data['clave']
                );
                $this->_user->changePassword(
                        $id , $newClave
                );
                $this->_helper->FlashMessenger(
                        $this->_translate->translate(
                                'Changed a Password'
                        )
                );
                $this->_redirect( $this->URL );
                }
            $form->setDefaults( $usuario->toArray() );
            $this->view->assign( 'formUsuario' , $form );
            }
        else
            {
            $this->_helper->FlashMessenger(
                    $this->_translate->translate(
                            'No se Pudo Cambiar'
                    )
            );
            $this->_redirect( $this->URL );
            }
        }


    public function borrarAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) );
        $this->_user->deleteUsuario( $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'usuario desactivado' )
        );
        $this->_redirect( $this->URL );
        }


    public function nuevoAction()
        {
        $form = new Mtt_Form_Usuario();
        if ( $this->_request->isPost() &&
                $form->isValid( $this->_request->getPost() ) )
            {
            $user = $form->getValues();
            $user['active'] = 1;

            $this->_user->saveUsuario( $user );

            $this->_helper->FlashMessenger(
                    $this->_translate->translate( 'se registro el usuario' )
            );
            $this->_redirect( $this->URL );
            }

        $this->view->assign( 'frmRegistrar' , $form );
        }


    public function verAction()
        {
        $id = intval( $this->_getParam( 'id' , null ) );
        $usuario = $this->_user->getFindId( $id );
        //$stmt = $this->_user->getCategoria( $id );
        $this->view->assign( 'usuario' , $usuario );
        }


    public function equipmentAction()
        {
        $id = intval( $this->_getParam( 'id' , null ) );

        $_equipo = new Mtt_Models_Bussines_Equipo();
        $stmt = $_equipo->listEquipByUser( $id );
        $this->view->assign( 'equipos' , $stmt );
        }


    public function operationAction()
        {
        $idUser = intval( $this->_getParam( 'id' , null ) );
        $_operacion = new Mtt_Models_Bussines_Operacion();
        $stmt = $_operacion->paglistByUser( $idUser );

        $stmt->setCurrentPageNumber(
                $this->_getParam( 'page' , 1 )
        );

        $this->view->assign( 'operaciones' , $stmt );
        }


    public function acreditarAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) );
        $this->_user->acreditarUsuario( $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'Usuario acreditado' )
        );
        $this->_redirect( $this->URL );
        }


    public function desacreditarAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) );
        $this->_user->desacreditarUsuario( $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'Usuario desacreditado' )
        );
        $this->_redirect( $this->URL );
        }


    public function activarAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) );
        $this->_user->activarUsuario( $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'Usuario activado' )
        );
        $this->_redirect( $this->URL );
        }


    public function desactivarAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) );
        $this->_user->desactivarUsuario( $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'Usuario desactivado' )
        );
        $this->_redirect( $this->URL );
        }


    }

