<?php


class Admin_CuotasPagoOperacionController
        extends Mtt_Controller_Action
{

    protected $_cuotaspagooperacion ;
    public $ajaxable = array(
        'getsubtotal' => array( 'html' , 'json' )
    ) ;


    public function init()
    {
        parent::init() ;
        $this->_cuotaspagooperacion = new Mtt_Models_Bussines_CuotasPagoOperacion() ;


    }


    public function indexAction()
    {
        
    }


    public function cuotasoperacionAction()
    {

        $id = intval( $this->_getParam( 'id' ) ) ;

        $this->view->jQuery()->addJavascriptFile( '/js/cuotaspago.js' ) ;
        $formCuotasPagoOperacion = new Mtt_Form_CuotasPagoOperacion() ;
        $formCuotasPagoOperacion->removeElement( 'fechapago' ) ;
        $this->view->assign(
                'formCuotasPago' ,
                $formCuotasPagoOperacion
        ) ;

        if ( $this->_request->isPost()
                &&
                $formCuotasPagoOperacion->isValid( $this->_request->getPost() ) )
        {

            $data = $formCuotasPagoOperacion->getValues() ;
            //$cuotasCantidad = $this->_cuotaspagooperacion->getNroCuotas($id);
            $data['operacion_id'] = $id ;
            //$data['nrocuota'] = $cuotasCantidad->cantidad;
            //$data['estadocuota_id'] = Mtt_Models_Table_EstadoCuota::Pendiente;
            unset( $data['restante'] ) ;
            $this->_cuotaspagooperacion->saveCuotasPagoOperacion( $data ) ;
        }


        $cuotasPago = $this->_cuotaspagooperacion->listByOperation( $id ) ;
        $operacion = new Mtt_Models_Bussines_Operacion() ;
        $total = $operacion->getTotalByOperation( $id ) ;
        $this->view->assign(
                'cuotasoperacion' ,
                $cuotasPago
        ) ;
        $this->view->assign(
                'id' ,
                $id
        ) ;
        $this->view->assign(
                'total' ,
                $total
        ) ;


    }


    public function editarAction()
    {

        $id = intval( $this->_getParam( 'id' ) ) ;

        $form = new Mtt_EditForm_CuotasPagoOperacion() ;

        $cuotasPago = $this->_cuotaspagooperacion->getFindId( $id ) ;

        if ( !is_null( $cuotasPago ) )
        {
            if ( $this->_request->isPost() && $form->isValid(
                            $this->_request->getPost() )
            )
            {
                $data = $form->getValues() ;
                unset( $data['restante'] ) ;
                $this->_cuotaspagooperacion->updateCuotasPagoOperacion( $data ,
                                                                        $id ) ;
                $this->_helper->FlashMessenger(
                        $this->_translate->translate( 'se modifico la cuota de pago' )
                ) ;
                $this->_redirect( 'admin/cuotaspagooperacion/cuotasoperacion/id/' . $cuotasPago->operacion_id ) ;
            }

            //$form->setDefaults($equipo->toArray());
            $form->setDefaults( ( array ) $cuotasPago ) ;
            $this->view->assign( 'form' ,
                                 $form ) ;
        }
        else
        {
            $this->_helper->FlashMessenger(
                    $this->_translate->translate( 'no existe la cuota' )
            ) ;
            $this->_redirect( $this->URL ) ;
        }


    }


    public function getsubtotalAction()
    {
        $results = $this->_equipo->getMontoCuotasByOperacion(
                $this->_getParam( 'id' )
        ) ;
        $this->_helper->json(
                ( $results )
        ) ;


    }


}

