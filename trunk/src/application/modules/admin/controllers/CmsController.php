<?php


class Admin_CmsController extends Mtt_Controller_Action
    {

    protected $_pagina ;
    protected $_page ;
    protected $_pais ;


    public function init()
        {
        parent::init() ;
        $this->_helper->layout->setLayout( 'layoutCms' ) ;
        $this->_pagina = new Mtt_Models_Bussines_Pagina() ;


        }


    public function indexAction()
        {
        
        }


    public function countryAction()
        {

        $idioma_id = $this->_getParam( 'idioma_id' , null ) ;
        $pais_id = $this->_getParam( 'pais_id' , null ) ;
        $titulo = $this->_getParam( 'titulo' , null ) ;

        $this->view->assign( 'countryActual' , $titulo ) ;

        $_data = array(
            'idioma_id' => $idioma_id ,
            'paises_id' => $pais_id
                ) ;

        $form = new Mtt_Form_ContentCountry( $_data ) ;

        if ( $this->_request->isPost() &&
                $form->isValid( $this->_request->getPost() ) )
            {

            $this->view->assign( 'dataForm' , $form->getValues() ) ;

            if ( $this->_pagina->getCheckExist( $form->getValues() ) )
                {

                $this->_helper->FlashMessenger(
                        $this->_translate->translate(
                                'se guardaron las acciones'
                        )
                ) ;
                }
            else
                {
                $this->_helper->FlashMessenger(
                        $this->_translate->translate( 'sucedieron errores' )
                ) ;
                }
            }

        $this->view->assign( 'formCountry' , $form ) ;


        }


    public function verAction()
        {

        $page_id = $this->_getParam( 'page_id' , null ) ;
        $pais_id = $this->_getParam( 'pais_id' , null ) ;
        $titulo = $this->_getParam( 'titulo' , null ) ;

        $this->view->assign( 'paginaActual' , $titulo ) ;

        $_data = array(
            'page_id' => $page_id ,
            'paises_id' => $pais_id
                ) ;

        $form = new Mtt_Form_Content( $_data ) ;


        if ( $this->_request->isPost() &&
                $form->isValid( $this->_request->getPost() ) )
            {
           
            if ( $this->_pagina->getCheckExist( $form->getValues() ) )
                {

                $this->_helper->FlashMessenger(
                        $this->_translate->translate(
                                'se guardaron las acciones'
                        )
                ) ;
                $this->_redirect( $this->URL ) ;
                }
            else
                {
                $this->_helper->FlashMessenger(
                        $this->_translate->translate( 'sucedieron errores' )
                ) ;
                }
            }
//        else {
//            $this->_helper->FlashMessenger(
//                    $this->_translate->translate(
//                            'por favor verifique los campos'
//                    )
//            );
//        }

        $this->view->assign( 'formContent' , $form ) ;

        }


    public function newAction()
        {
        $this->view->jQuery()
                ->addJavascriptFile(
                        '/js/jwysiwyg/jquery.wysiwyg.js'
                )
                ->addJavascriptFile(
                        '/js/cms.js'
                )
                ->addStylesheet(
                        '/js/jwysiwyg/jquery.wysiwyg.css'
                ) ;


        $form = new Mtt_Form_Cms() ;

        if ( $this->_request->isPost()
                && $form->isValid( $this->_request->getPost() ) )
            {

            $cms = $form->getValues() ;

            $this->_pagina->savePagina( $cms ) ;

            $this->_helper->FlashMessenger(
                    $this->_translate->translate( 'se registro el cms' )
            ) ;
            $this->_redirect( $this->URL ) ;
            }
        $this->view->assign( 'form' , $form ) ;


        }


    public function editAction()
        {
        $this->view->assign(
                'titulo' , $this->_translate->translate( 'editar cms' )
        ) ;

        $id = intval( $this->_getParam( 'id' ) ) ;

        $form = new Mtt_EditForm_Cms() ;

        $pagina = $this->_pagina->getFindId( $id ) ;

        if ( !is_null( $pagina ) )
            {
            if ( $this->_request->isPost() && $form->isValid(
                            $this->_request->getPost() )
            )
                {
                $this->_pagina->updatePagina( $form->getValues() , $id ) ;
                $this->_helper->FlashMessenger(
                        $this->_translate->translate( 'se modifico un cms' )
                ) ;
                $this->_redirect( $this->URL ) ;
                }
            //$form->setDefaults($equipo->toArray());
            $form->setDefaults( ( array ) $pagina ) ;
            $this->view->assign( 'form' , $form ) ;
            }
        else
            {
            $this->_helper->FlashMessenger(
                    $this->_translate->translate( 'No existe ese cms' ) ) ;
            $this->_redirect( $this->URL ) ;
            }


        }


    public function deleteAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) ) ;
        $this->_pagina->deletePagina( $id ) ;
        $this->_helper->FlashMessenger( $this->_translate->translate( 'pagina Borrada' ) ) ;
        $this->_redirect( $this->URL ) ;


        }


    public function activarAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) ) ;
        $this->_pagina->activarPagina( $id ) ;
        $this->_helper->FlashMessenger( $this->_translate->translate( 'pagina activada' ) ) ;
        $this->_redirect( $this->URL ) ;


        }


    public function desactivarAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) ) ;
        $this->_pagina->desactivarPagina( $id ) ;
        $this->_helper->FlashMessenger( $this->_translate->translate( 'pagina desactivado' ) ) ;
        $this->_redirect( $this->URL ) ;


        }


    }

