<?php

class Admin_CuotasPagoController 
    extends Mtt_Controller_Action {

    protected $_cuotaspago;
    public $ajaxable = array(
        'pay' => array( 'html' , 'json' )
    );

    public function init() {
        parent::init();
        $this->_cuotaspago = new Mtt_Models_Bussines_CuotasPago();
    }

    public function indexAction() {
        
    }

    public function cuotasoperacionAction() {

        $id = intval($this->_getParam('id'));

        $this->view->jQuery()->addJavascriptFile( '/js/cuotaspago.js' );
        $formCuotasPago = new Mtt_Form_CuotasPago();
        $this->view->assign(
                'formCuotasPago', $formCuotasPago
        );

        if ($this->_request->isPost()
                &&
                $formCuotasPago->isValid($this->_request->getPost())) {

            $data = $formCuotasPago->getValues();
            $cuotasCantidad = $this->_cuotaspago->getNroCuotas($id);
            $data['operacion_has_equipo_id'] = $id;
            $data['nrocuota'] = $cuotasCantidad->cantidad;
            $data['estadocuota_id'] = Mtt_Models_Table_EstadoCuota::Pendiente;

            $this->_cuotaspago->saveCuotasPago($data);
        }


        $cuotasPago = $this->_cuotaspago->listByOperation($id);
        $this->view->assign(
                'cuotasoperacion', $cuotasPago
        );
        $this->view->assign(
                'id', $id
        );
    }
    public function cuotasoperacionequipAction() {

        $id = intval($this->_getParam('id'));

        $this->view->jQuery()->addJavascriptFile( '/js/cuotaspago.js' );
        $formCuotasPago = new Mtt_Form_CuotasPago();
        $this->view->assign(
                'formCuotasPago', $formCuotasPago
        );

        if ($this->_request->isPost()
                &&
                $formCuotasPago->isValid($this->_request->getPost())) {

            $data = $formCuotasPago->getValues();
            $cuotasCantidad = $this->_cuotaspago->getNroCuotas($id);
            $data['operacion_has_equipo_id'] = $id;
            $data['nrocuota'] = $cuotasCantidad->cantidad;
            $data['estadocuota_id'] = Mtt_Models_Table_EstadoCuota::Pendiente;

            $this->_cuotaspago->saveCuotasPago($data);
        }


        $cuotasPago = $this->_cuotaspago->listByOperationHasEquip($id);
        $this->view->assign(
                'cuotasoperacion', $cuotasPago
        );
        $this->view->assign(
                'id', $id
        );
    }

    public function payAction() {
        $this->view->headTitle()->setSeparator(' - ');
        $this->view->headTitle()->prepend('cuotas');

        
        $id = (int) $this->_request->getParam('id', null);
        $data = $this->_cuotaspago->getCuotasPagoById($id);
        $data->fechapago = date('Y-m-d H:m:s');
        $this->_cuotaspago->updateCuotasPago((array)$data, $id);
        $this->view->assign('id', $id);
        if ($this->_request->isXmlHttpRequest()) {

            $this->view->assign('sms', $this->_translate->translate(
                            'realizar  pago'
                    )
            );
        }
       
    }
    
    
    public function editarAction()
        {

        $id = intval( $this->_getParam( 'id' ) );

        $form = new Mtt_EditForm_CuotasPago();

        $cuotasPago = $this->_cuotaspago->getFindId( $id );

        if ( !is_null( $cuotasPago ) )
            {
            if ( $this->_request->isPost() && $form->isValid(
                            $this->_request->getPost() )
            )
                {
                $this->_cuotaspago->updateCuotasPago( $form->getValues() , $id );
                $this->_helper->FlashMessenger(
                        $this->_translate->translate( 'se modifico la cuota de pago' )
                );
                //$this->_redirect( 'admin/cuotaspago/cuotasoperacion/id/'. $cuotasPago-> );
                }
           
            //$form->setDefaults($equipo->toArray());
            $form->setDefaults( ( array ) $cuotasPago );
            $this->view->assign( 'form' , $form );
            }
        else
            {
            $this->_helper->FlashMessenger(
                    $this->_translate->translate( 'no existe la cuota' )
            );
            $this->_redirect( $this->URL );
            }
        }

}

