<?php


class Admin_IndexController
        extends Mtt_Controller_Action
    {


    public function indexAction()
        {
        $_usuario = new Mtt_Models_Bussines_Usuario();
        $_equipo = new Mtt_Models_Bussines_Equipo();
        $_operation = new Mtt_Models_Bussines_Operacion();
        $_pregunta = new Mtt_Models_Bussines_Pregunta();
        $_reserva = new Mtt_Models_Bussines_Reserva();

        $this->view->assign( 'lastequipments' ,
                             $_equipo->getLastEquipmentPublishedAllUsers() );

        $this->view->assign(
                'lastsales' ,
                $_operation->listLastOperations(
                        Mtt_Models_Bussines_EstadoOperacion::SALE , 5 )
        );
        
        $this->view->assign(
                'lastUser'
                , $_usuario->getLastUserRegistred()
        );
        $this->view->assign(
                'nUserRegistrados'
                , $_usuario->countRegistrados()
        );
        $this->view->assign(
                'nEquiposPendientes'
                , $_equipo->countEquipmentsPendientes()
        );
        $this->view->assign(
                'nPreguntas'
                , $_pregunta->countQuestionUnresolved()
        );
        $this->view->assign(
                'nReservas'
                , $_reserva->countReserved()
        );
        
        }
        


    }