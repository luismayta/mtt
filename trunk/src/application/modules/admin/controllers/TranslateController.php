<?php


class Admin_TranslateController
        extends Mtt_Controller_Action
    {

    protected $_traducciones;
    protected $_entidades;


    public function init()
        {
        parent::init();
        $this->_helper->layout->setLayout( 'layoutTranslate' );
        $this->_traducciones = new Mtt_Models_Bussines_Traducciones();
        $this->_entidades = new Mtt_Models_Bussines_Entidades();
        }


    public function indexAction()
        {
        
        }


    public function verAction()
        {

        $entidad_id = $this->_getParam( 'entidad_id' , null );
        $titulo = $this->_getParam( 'titulo' , null );
        $rowEntidad = $this->_entidades->getId( $entidad_id );

        $this->view->assign(
                'EntidadActual' , $titulo
        );

        $_data = array(
            'entidad_id' => $entidad_id ,
            'entidad' => $rowEntidad->nombre
        );

        $form = new Mtt_Form_Translate( $_data );


        if ( $this->_request->isPost() &&
                $form->isValid( $this->_request->getPost() ) )
            {

//            $this->_traducciones->saveTraduccionesAll(
//                    $form->getValues()
//            );

            if ( $this->_traducciones->getCheckExist(
                            $form->getValues()
                    )
            )
                {

                $this->_helper->FlashMessenger(
                        $this->_translate->translate(
                                'se guardaron las acciones'
                        )
                );
                $this->_redirect( $this->URL );
                }
            else
                {
                $this->_helper->FlashMessenger(
                        $this->_translate->translate( 'sucedieron errores' )
                );
                }
            }
        $this->view->assign( 'formContent' , $form );
        }


    public function newAction()
        {
        $this->view->jQuery()
                ->addJavascriptFile(
                        '/js/jwysiwyg/jquery.wysiwyg.js'
                )
                ->addJavascriptFile(
                        '/js/cms.js'
                )
                ->addStylesheet(
                        '/js/jwysiwyg/jquery.wysiwyg.css'
        );


        $form = new Mtt_Form_Cms();

        if ( $this->_request->isPost()
                && $form->isValid( $this->_request->getPost() ) )
            {

            $cms = $form->getValues();

            $this->_traducciones->savePagina( $cms );

            $this->_helper->FlashMessenger(
                    $this->_translate->translate( 'se registro el cms' )
            );
            $this->_redirect( $this->URL );
            }
        $this->view->assign( 'form' , $form );
        }

    }

