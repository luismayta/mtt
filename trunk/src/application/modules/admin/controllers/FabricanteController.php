<?php


class Admin_FabricanteController
        extends Mtt_Controller_Action
    {

    protected $_fabricante;
    public $ajaxable = array(
        'searchfabricante' => array( 'html' , 'json' )
    );


    public function init()
        {
        parent::init();
        $this->_fabricante = new Mtt_Models_Bussines_Fabricante();
        $this->_helper->getHelper( 'ajaxContext' )->initContext();


        }


    public function searchfabricanteAction()
        {
        $results = $this->_fabricante->search(
                $this->_getParam( 'term' )
        );
        $this->_helper->json(
                array_values( $results )
        );


        }


    public function indexAction()
        {

        $jForm = new Mtt_JForm_Search_Fabricante();
        $fabricantes = null;
        if ( $this->_request->isPost() &&
                $jForm->isValid( $this->_request->getPost() ) )
            {
            $fabricantes = $this->_fabricante->pagListManufactur(
                    $active = null , $jForm->getValue( 'nombre' )
            );
            }
        else
            {

            $fabricantes = $this->_fabricante->pagListManufactur(
                    $active = null , null
            );
            $fabricantes->setCurrentPageNumber(
                    $this->_getParam( 'page' , 1 )
            );
            }

        $this->view->assign( 'searchForm' , $jForm );

        $this->view->assign( 'fabricantes'
                , $fabricantes );


        }


    public function nuevoAction()
        {
        $form = new Mtt_Form_Fabricante();
        if ( $this->_request->isPost() &&
                $form->isValid( $this->_request->getPost() ) )
            {

            $dataForm = $form->getValues();


            $upload = $form->imagen->getTransferAdapter();
            $slugger = new Mtt_Filter_Slug(
                            array(
                                'field' => 'nombre' ,
                                'model' => new Mtt_Models_Bussines_Fabricante()
                            )
            );
            $target = $slugger->filter( $dataForm['nombre'] ) . '.jpg';

            $f = new Zend_Filter_File_Rename(
                            array(
                                'target' => $target ,
                                'overwrite' => true
                            )
            );

            $upload->addFilter( $f );


            if ( $form->imagen->receive() )
                {

                $serviceImage = new Mtt_Service_Image(
                                array( 'element' => 'manufacturer' )
                );
                $serviceImage
                        ->processImageAvata(
                                $form->imagen->getFileName() , $target
                );
                $serviceImage
                        ->processImageThumb(
                                $form->imagen->getFileName() , $target
                );
                $serviceImage
                        ->processImageProduct(
                                $form->imagen->getFileName() , $target );

                $dataForm['imagen'] = $target;

                $this->_fabricante->saveFabricante( $dataForm );
                $this->_helper->FlashMessenger(
                        $this->_translate->translate( 'se registro el fabricante' )
                );
                $this->_redirect( $this->URL );
                }
            }
        $this->view->assign( 'frmRegistrar' , $form );


        }


    public function editarAction()
        {

        $id = ( int ) $this->_getParam( 'id' );

        $form = new Mtt_EditForm_Fabricante( array( 'id' => $id ) );

        $fabricante = $this->_fabricante->getFindId( $id );



        if ( is_object( $fabricante ) )
            {


            if ( $this->_request->isPost()
                    && $form->isValid( $this->_request->getPost() ) )
                {

                $dataForm = $form->getValues();


                $upload = $form->imagen->getTransferAdapter();

                $target = $fabricante->slug . Mtt_Tools_Text::getExtension(
                                $dataForm['imagen'] );


                $f = new Zend_Filter_File_Rename(
                                array(
                                    'target' => $target ,
                                    'overwrite' => true
                                )
                );

                $upload->addFilter( $f );


                if ( $form->imagen->receive() )
                    {

                    $serviceImage = new Mtt_Service_Image(
                                    array( 'element' => 'manufacturer' )
                    );
                    $serviceImage
                            ->processImageAvata(
                                    $form->imagen->getFileName() , $target
                    );
                    $serviceImage
                            ->processImageThumb(
                                    $form->imagen->getFileName() , $target
                    );
                    $serviceImage
                            ->processImageProduct(
                                    $form->imagen->getFileName() , $target );

                    $dataForm['imagen'] = $target;

                    $this->_fabricante->updateFabricante(
                            $dataForm , $id
                    );
                    $this->_helper->FlashMessenger(
                            $this->_translate->translate( 'se modifico el fabricante' )
                    );
                    $this->_redirect( $this->URL );
                    }
                }
            $form->setDefaults( $fabricante->toArray() );
            $this->view->assign( 'form' , $form );
            }
        else
            {
            $this->_helper->FlashMessenger(
                    $this->_translate->translate( 'no existe ese fabricante' )
            );
            $this->_redirect( $this->URL );
            }


        }


    public function borrarAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) );
        $this->_fabricante->deleteFabricante( $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'fabricante borrado' )
        );
        $this->_redirect( $this->URL );


        }


    public function activarAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) );
        $this->_fabricante->update( array( 'active' => 1 ) , 'id=' . $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'fabricante activado' )
        );
        $this->_redirect( $this->URL );


        }


    public function desactivarAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) );
        $this->_fabricante->update( array( 'active' => 0 ) , 'id=' . $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'fabricante desactivado' )
        );
        $this->_redirect( $this->URL );


        }


    }

