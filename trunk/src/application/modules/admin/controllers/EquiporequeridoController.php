<?php


class Admin_EquipoRequeridoController
		extends Mtt_Controller_Action
{

	protected $_equipoRequerido ;
	public $ajaxable = array(
		'searchequiporequerido' => array( 'html' , 'json' )
			) ;


	public function init()
	{
		parent::init() ;
		$this->_equipoRequerido = new Mtt_Models_Bussines_EquipoRequerido() ;
		$this->_helper->getHelper( 'ajaxContext' )->initContext() ;


	}


	public function indexAction()
	{
		$jForm = new Mtt_JForm_Search_EquipoRequerido() ;
		$equipoRequerido = null ;
		if ( $this->_request->isPost() &&
				$jForm->isValid( $this->_request->getPost() ) )
		{
			$equipoRequerido = $this->_equipoRequerido->pagList(
							Mtt_Models_Bussines_EquipoRequerido::ACTIVE ,
							$jForm->getValue( 'nombre' )
					) ;
		}
		else
		{

			$equipoRequerido = $this->_equipoRequerido->pagList(
							Mtt_Models_Bussines_EquipoRequerido::ACTIVE ,
							null
					) ;
			$equipoRequerido->setCurrentPageNumber(
					$this->_getParam( 'page' , 1 )
			) ;
		}

		$this->view->assign( 'searchForm' , $jForm ) ;

		$this->view->assign( 'equiporequeridos'
				, $equipoRequerido ) ;


	}


	public function searchequiporequeridoAction()
	{
		$results = $this->_equipoRequerido->search(
						$this->_getParam( 'term' )
				) ;
		$this->_helper->json(
				array_values( $results )
		) ;


	}


	public function newAction()
	{
		$form = new Mtt_Form_EquipoRequerido( ) ;

		if ( $this->_request->isPost() &&
				$form->isValid( $this->_request->getPost() ) )
		{

			$equipoRequerido = $form->getValues() ;

			$this->_equipoRequerido->saveEquipoRequerido( $equipoRequerido ) ;

			$this->_helper->FlashMessenger(
					$this->_translate->translate( 'se registro el equipo requerido' )
			) ;
			$this->_redirect( $this->URL ) ;
		}
		$this->view->assign( 'frmRegistrar' , $form ) ;


	}


	public function editAction()
	{

		$id = ( int ) $this->_getParam( 'id' ) ;

		$form = new Mtt_EditForm_EquipoRequerido() ;

		$equipoRequerido = $this->_equipoRequerido->getFindId( $id ) ;

		if ( is_object( $equipoRequerido ) )
		{


			if ( $this->_request->isPost()
					&& $form->isValid( $this->_request->getPost() ) )
			{
				$this->_equipoRequerido->updateEquipoRequerido(
						$form->getValues() , $id
				) ;
				$this->_helper->FlashMessenger(
						$this->_translate->translate( 'se modifico el equipo requerido' )
				) ;
				$this->_redirect( $this->URL ) ;
			}
			$form->setDefaults( $equipoRequerido->toArray() ) ;
			$this->view->assign( 'form' , $form ) ;
		}
		else
		{
			$this->_helper->FlashMessenger(
					$this->_translate->translate( 'no existe ese equipo requerido' )
			) ;
			$this->_redirect( $this->URL ) ;
		}


	}


	public function deleteAction()
	{
		$id = intval( $this->_request->getParam( 'id' ) ) ;
		$this->_equipoRequerido->deleteEquipoRequerido( $id ) ;
		$this->_helper->FlashMessenger(
				$this->_translate->translate( 'equipo requerido borrado' )
		) ;
		$this->_redirect( $this->URL ) ;


	}


	public function activeAction()
	{
		$id = intval( $this->_request->getParam( 'id' ) ) ;
		$this->_equipoRequerido->updateEquipoRequerido( array( 'active' => 1 ) , 'id=' . $id ) ;
		$this->_helper->FlashMessenger(
				$this->_translate->translate( 'equipo requerido activado' )
		) ;
		$this->_redirect( $this->URL ) ;


	}


	public function desactivarAction()
	{
		$id = intval( $this->_request->getParam( 'id' ) ) ;
		$this->_equipoRequerido->updateEquipoRequerido(
				array( 'active' => 0 ) , 'id=' . $id
		) ;
		$this->_helper->FlashMessenger(
				$this->_translate->translate(
						'equipo requerido desactivado'
				)
		) ;
		$this->_redirect( $this->URL ) ;


	}


}

