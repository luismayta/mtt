<?php

class Admin_EquipoFormaPagoController 
    extends Mtt_Controller_Action {

    protected $_equipoFormaPago;
    public $ajaxable = array(
        'borrar' => array('html', 'json')
    );

    public function init() {
        parent::init();
        $this->_equipoFormaPago = new Mtt_Models_Bussines_EquipoFormaPago();
        $this->_helper->getHelper('ajaxContext')->initContext();
    }

    public function indexAction() {
        
    }

    public function nuevoAction() {

        $this->view->jQuery()->addJavascriptFile('/js/equipoformapago.js');

        $idEquipo = (int) ( $this->_getParam('id', null) );

        $equipo = new Mtt_Models_Bussines_Equipo();
        $dataEquipo = $equipo->getProduct($idEquipo);

        $form = new Mtt_Form_EquipoFormaPago();

        if (!is_null($dataEquipo)) {
            if ($this->_request->isPost()
                    && $form->isValid($this->_request->getPost())) {

                $equipoFormaPago = $form->getValues();
                $equipoFormaPago['equipo_id'] = $idEquipo;

                $this->_equipoFormaPago->saveEquipoFormaPago($equipoFormaPago);

                $this->_helper->FlashMessenger(
                        $this->_translate->translate('se registro la forma de pago')
                );
                //$this->_redirect($this->URL);
            }
            $equiposFormasPago = $this->_equipoFormaPago->listByEquipo($idEquipo);
            
            $this->view->assign('equipo', $dataEquipo);
            $this->view->assign('equiposFormasPago', $equiposFormasPago);
            $this->view->assign('frmRegistrar', $form);
        } else {
            $this->_helper->FlashMessenger(
                    $this->_translate->translate('no existe esa forma de pago')
            );
            $this->_redirect($this->URL);
        }
    }

    public function verAction() {
        $id = intval($this->_getParam('id', null));
        //$stmt = $this->_moneda->getCategoria( $id );
        //$this->view->assign( 'categoria' , $stmt );
    }

    public function editarAction() {

        $id = intval($this->_getParam('id'));

        $form = new Mtt_EditForm_EquipoFormaPago();

        $equipoFormaPago = $this->_equipoFormaPago->getFindId($id);
        $this->view->assign('equipoFormaPago', $equipoFormaPago);

        if (!is_null($equipoFormaPago)) {
            if ($this->_request->isPost() && $form->isValid(
                            $this->_request->getPost())
            ) {
                $this->_equipoFormaPago->updateEquipoFormaPago($form->getValues(), $id);
                $this->_helper->FlashMessenger(
                        $this->_translate->translate('se modifico la forma de pago')
                );
                $this->_redirect('/admin/equipo');
            }
            $form->setDefaults($equipoFormaPago->toArray());
            $this->view->assign('form', $form);
            $this->view->assign('equipo', $equipoFormaPago);
        } else {
            $this->_helper->FlashMessenger(
                    $this->_translate->translate('no existe esa forma de pago')
            );
            $this->_redirect($this->URL);
        }
    }

    public function borrarAction() {


        $id = (int) $this->_request->getParam('id', null);
        $this->_equipoFormaPago->desactivarEquipoFormaPago($id);
        $this->view->assign('id', $id);
        if ($this->_request->isXmlHttpRequest()) {

            $this->view->assign('sms', $this->_translate->translate(
                            ' eliminada'
                    )
            );
        }

        $this->_redirect($this->URL);
    }

}

