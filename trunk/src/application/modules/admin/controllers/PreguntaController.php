<?php


class Admin_PreguntaController
        extends Mtt_Controller_Action
    {

    protected $_pregunta;
    public $ajaxable = array(
        'searchpregunta' => array( 'html' , 'json' )
    );


    public function init()
        {
        parent::init();
        $this->_pregunta = new Mtt_Models_Bussines_Pregunta();
        }


    public function indexAction()
        {

        $jForm = new Mtt_JForm_Search_Pregunta();
        $preguntas = null;
        
        $form = new Mtt_Form_AnswerQuestion();
        $this->view->assign( 'formAnswer' , $form );

        $this->view->jQuery()
                ->addOnLoad(
                        ' 
                        $("a.rpta").click(function(){ 
                            
                            var id = ($(this).attr("id")); 
                            $("#id").attr("value", id);
                            $( "#dg-rpta" ).dialog({
                                height: 320,
                                width: 580,
                                modal: true
                            });
                        });  
                        $("#dg-rpta").css("display", "none");
                        '
        );

        $this->view->assign(
                'titulo' , $this->_translate->translate( 'preguntas' )
        );

        if ( $this->_request->isPost() &&
                $jForm->isValid( $this->_request->getPost() ) )
            {
            $preguntas = $this->_pregunta->pagListQuestion(
                    null
                    , $jForm->getValue( 'nombre' )
            );
            }
        else
            {

            $preguntas = $this->_pregunta->pagListQuestion(
                    null
                    , null
            );
            $preguntas->setCurrentPageNumber(
                    $this->_getParam( 'page' , 1 )
            );
            }

        $this->view->assign( 'searchForm' , $jForm );

        $this->view->assign(
                'preguntas' , $preguntas );
       

        }


    public function answerquestionAction()
        {
        //$this->_helper->layout()->disableLayout();
        $form = new Mtt_Form_AnswerQuestion();
        $id = intval( $this->_request->getParam( 'id' ) );
        if ( $this->_request->isPost()
                && $form->isValid( $this->_request->getPost() ) )
            {

            $data = $form->getValues();
            unset( $data['submit'] );
            $this->_pregunta->responderPregunta( $data , $id );
            }

        $this->_redirect( $this->URL );
        }


    public function questionunresolvedAction()
        {
        $this->view->assign(
                'titulo' ,
                $this->_translate->translate( 'preguntas por responder' )
        );
        $this->view->jQuery()
                ->addOnLoad(
                        ' 
                        $("a.rpta").click(function(){ 
                            
                            var id = ($(this).attr("id")); 
                            $("#id").attr("value", id);
                            $( "#dg-rpta" ).dialog({
                                height: 320,
                                width: 580,
                                modal: true
                            });
                        });  
                        $("#dg-rpta").css("display", "none");
                        '
        );

        $form = new Mtt_Form_AnswerQuestion();
        $this->view->assign( 'formAnswer' , $form );

        $preguntas = $this->_pregunta->pagListQuestionUnresolved();
        $preguntas->setCurrentPageNumber(
                $this->_getParam( 'page' , 1 )
        );
        $this->view->assign(
                'preguntas' , $preguntas );
        }


    public function searchpreguntaAction()
        {
        $results = $this->_pregunta->search(
                $this->_getParam( 'term' )
        );
        $this->_helper->json(
                array_values( $results )
        );
        }


    public function borrarAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) );
        $this->_pregunta->desactivaPregunta( $id );
        $this->_helper->flashMessenger( $this->_translate->translate( 'Pregunta Borrado' ) );
        $this->_redirect( $this->URL );
        }


    public function activarAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) );
        $this->_pregunta->activarPregunta( $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'pregunta activada' )
        );
        $this->_redirect( $this->URL );
        }


    public function desactivarAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) );
        $this->_pregunta->desactivarPregunta( $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'pregunta desactivada' )
        );
        $this->_redirect( $this->URL );
        }


    }

