<?php

class Admin_PaisesController 
    extends Mtt_Controller_Action
    {

    protected $_pais;
    public $ajaxable = array(
        'searchpais' => array( 'html' , 'json' )
    );         

    public function init()
        {
        parent::init();
        $this->_pais = new Mtt_Models_Bussines_Paises();
        }

    public function indexAction()
        {
        
        $jForm = new Mtt_JForm_Search_Pais();
        $paises = null;
        if ( $this->_request->isPost() &&
                $jForm->isValid( $this->_request->getPost() ) )
            {
            $paises = $this->_pais->pagListCountries(
                    $active = null , $jForm->getValue( 'nombre' )
            );
            $paises->setCurrentPageNumber(
                $this->_getParam('page', 1)
            );
            }
        else
            {
            
            $paises = $this->_pais->pagListCountries(
                    $active = null , null
            );
            $paises->setCurrentPageNumber(
                $this->_getParam('page', 1)
            );
            }

        $this->view->assign( 'searchForm' , $jForm );

        $this->view->assign(
                'paises', $paises
        );
        
        

        }
        
   
    public function searchpaisAction()
        {
        $results = $this->_pais->search(
                $this->_getParam( 'term' )
        );
        $this->_helper->json(
                array_values( $results )
        );
        }
        

    
    public function nuevoAction()
        {
        $form = new Mtt_Form_Pais();
        if ( $this->_request->isPost() && 
                $form->isValid( $this->_request->getPost() ) )
            {

            $pais= $form->getValues();

            $this->_pais->insert( $pais );

            $this->_helper->FlashMessenger( 
                    $this->_translate->translate('se registro el pais' )
            );
            
            $this->_redirect( $this->URL );
            }
        $this->view->assign( 'frmRegistrar' , $form );
        }

    public function verAction()
        {
        $id = intval( $this->_getParam( 'id' , null ) );
        $stmt = $this->_pais->getCategoria( $id );
        $this->view->assign( 'categoria' , $stmt );
        }
        
        

    public function editarAction()
        {
        $id = intval( $this->_getParam( 'id' ) );

        $form = new Mtt_EditForm_Pais();
        
        $form->nombre->addValidator(
                new Zend_Validate_Db_NoRecordExists(
                        array(
                            'table' =>'paises',
                            'field' => 'nombre',
                            'exclude' => array (
                                'field' => 'id', 
                                'value' => $id)
                        )
                )
        );
        
        $pais = $this->_pais->getFindId( $id );

        if ( !is_null( $pais ) )
            {
            if ( $this->_request->isPost()
                    &&
                    $form->isValid( $this->_request->getPost() ) )
                {
                $this->_pais->updatePais(
                        $form->getValues() , $id
                );
                $this->_helper->FlashMessenger(
                        $this->_translate->translate( 'se modifico el pais' )
                );
                $this->_redirect( $this->URL );
                }
            //$form->setDefaults( $pais->toArray() );
            $form->setDefaults((array) $pais);
            $this->view->assign( 'form' , $form );
            }
        else
            {
            $this->_helper->FlashMessenger( 
                    $this->_translate->translate( 'no existe' ) );
            $this->_redirect( $this->URL );
            }
        }        
        
        

    public function borrarAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) );
        $this->_pais->deletePais( $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'pais eliminado' )
        );
        $this->_redirect( $this->URL );
        }        
        

    public function activarAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) );
        $this->_pais->activarPais(  $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'pais activado' )
        );
        $this->_redirect( $this->URL );
        }


    public function desactivarAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) );
        $this->_pais->desactivarPais( $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'pais desactivado' )
        );
        $this->_redirect( $this->URL );
        }

    }

