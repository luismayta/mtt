<?php


class Admin_OperacionController
        extends Mtt_Controller_Action
    {

    protected $_operacion;
    public $ajaxable = array(
        'searchoperacion' => array( 'html' , 'json' )
    );

    public function init()
        {
        parent::init();
        $this->_operacion = new Mtt_Models_Bussines_Operacion();
        }


    public function indexAction()
        {
        $jForm = new Mtt_JForm_Search_Operacion();
        $operaciones = null;
        
        $this->view->jQuery()->addJavascriptFile( '/js/operacion.js' );
        $this->view->assign(
                'titulo' , $this->_translate->translate( 'nuevas ventas' )
        );

        if ( $this->_request->isPost() &&
                $jForm->isValid( $this->_request->getPost() ) )
            {
            $operaciones = $this->_operacion->paglistByOperations(
                    Mtt_Models_Bussines_EstadoOperacion::SALE 
                    , $jForm->getValue( 'nombre' )
            );
            }
        else
            {

            $operaciones = $this->_operacion->paglistByOperations(
                    Mtt_Models_Bussines_EstadoOperacion::SALE 
                    , null
            );
            $operaciones->setCurrentPageNumber(
                    $this->_getParam( 'page' , 1 )
            );
            }

        $this->view->assign( 'searchForm' , $jForm );

        $this->view->assign(
                'operaciones' , $operaciones
        );


        
        }


    public function verAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) );
        $this->view->assign(
                'operacion' , $this->_operacion->verDetalle( $id )
        );
        }


    public function detallaroperacionAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) );
        $operationEquipo = new Mtt_Models_Bussines_OperationEquipo;
        $detalles = $operationEquipo->getEquipmentsByOperation( $id );
        $this->view->assign(
                'operaciones' , $detalles
        );
        }


    public function salesAction()
        {

        $operaciones = $this->_operacion->pagListByOperation(
                Mtt_Models_Bussines_EstadoOperacion::SALE
        );

        $operaciones->setCurrentPageNumber(
                $this->_getParam( 'page' , 1 ) );
        $this->view->assign(
                'operaciones' , $operaciones
        );
        }


        
    public function searchoperacionAction()
        {
        $results = $this->_operacion->search(
                $this->_getParam( 'term' )
        );
        $this->_helper->json(
                array_values( $results )
        );
        }

    }