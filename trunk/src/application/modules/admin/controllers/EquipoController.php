<?php


class Admin_EquipoController
        extends Mtt_Controller_Action
    {

    protected $_equipo;
    public $ajaxable = array(
        'searchequipo' => array( 'html' , 'json' ) ,
        'checkprecioventa' => array( 'html' , 'json' ) ,
        'exportarsuiza' => array( 'html' , 'json' ) ,
        'exportar' => array( 'html' , 'json' )
    );


    public function init()
        {
        parent::init();
        $this->_equipo = new Mtt_Models_Catalog_Equipo();
        $this->_helper->getHelper( 'ajaxContext' )->initContext();


        }


    public function indexAction()
        {

        $this->view->jQuery()
                ->addJavascriptFile(
                        '/js/jquery.contextMenu/jquery.contextMenu.js'
                )->addJavascriptFile(
                        '/theme/admin/js/contextmenuEquipo.js'
                )
                ->addJavascriptFile(
                        '/theme/admin/js/exportar.js'
                )
                ->addStylesheet(
                        '/css/jquery.contextMenu/jquery.contextMenu.css'
        );

        $grid = new Mtt_JQuery_JqGrid( 'equipos' ,
                        new Mtt_JQuery_JqGrid_Adapter_Array(
                                $this->_equipo->listEquipArray()
                        ) , array( 'multiselect' => true )
        );

        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'id' , array(
                    'label' => $this->_translate->translate( 'id' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'equipo' , array(
                    'label' => $this->_translate->translate( 'nombre' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'modelo' , array(
                    'label' => $this->_translate->translate( 'modelo' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'categoria' , array(
                    'label' => $this->_translate->translate( 'categoria' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'estadoequipo' , array(
                    'label' => $this->_translate->translate( 'estado' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'usuario' , array(
                    'label' => $this->_translate->translate( 'usuario' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'fechafabricacion' , array(
                    'label' => $this->_translate->translate( 'fabricacion' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'publishdate' , array(
                    'label' => $this->_translate->translate( 'publicacion' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'fabricante' , array(
                    'label' => $this->_translate->translate( 'fabricante' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'paises' , array(
                    'label' => $this->_translate->translate( 'pais' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'topofers' , array(
                    'label' => $this->_translate->translate( 'top ofers' )
                        )
                )
        );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column(
                        'active' , array(
                    'label' => $this->_translate->translate( 'activo' )
                        )
                )
        );


        $grid->registerPlugin( new Mtt_JQuery_JqGrid_Plugin_ToolbarFilter() );

        $this->view->assign( 'grid' , $grid->render() );

        /* $this->view->jQuery()->addJavascriptFile(
          '/js/equipo.js'
          );
          $jForm = new Mtt_JForm_Search_Equipo();

          $formEquipoPrecio = new Mtt_JxForm_DialogPrecioVenta();
          $equipos = null;
          if ( $this->_request->isPost() &&
          $jForm->isValid( $this->_request->getPost() ) )
          {
          $equipos = $this->_equipo->pagListEquip(
          $active = null , $jForm->getValue( 'nombre' )
          );
          }
          else
          {

          $equipos = $this->_equipo->pagListEquip(
          $active = null , null
          );
          $equipos->setCurrentPageNumber(
          $this->_getParam( 'page' , 1 )
          );
          }

          $this->view->assign( 'searchForm' , $jForm );
          $this->view->assign( 'formEquipoPrecio' , $formEquipoPrecio );

          $this->view->assign(
          'equipos' , $equipos ); */


        }


    public function jqgridAction()
        {


        $grid = new Mtt_JQuery_JqGrid( 'equipos' ,
                        new Mtt_JQuery_JqGrid_Adapter_Array(
                                $this->_equipo->listEquipArray()
                        )
        );

        $grid->addColumn( new Mtt_JQuery_JqGrid_Column( 'id' ) );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column( 'equipo' ) );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column( 'modelo' ) );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column( 'categoria' ) );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column( 'estadoequipo' ) );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column( 'usuario' ) );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column( 'fechafabricacion' ) );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column( 'publishdate' ) );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column( 'fabricante' ) );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column( 'paises' ) );
        $grid->addColumn( new Mtt_JQuery_JqGrid_Column( 'acciones' ) );


        $grid->registerPlugin( new Mtt_JQuery_JqGrid_Plugin_ToolbarFilter() );
        $this->view->assign( 'grid' , $grid->render() );

        /* $this->view->jQuery()->addJavascriptFile(
          '/js/equipo.js'
          );
          $jForm = new Mtt_JForm_Search_Equipo();

          $formEquipoPrecio = new Mtt_JxForm_DialogPrecioVenta();
          $equipos = null;
          if ( $this->_request->isPost() &&
          $jForm->isValid( $this->_request->getPost() ) )
          {
          $equipos = $this->_equipo->pagListEquip(
          $active = null , $jForm->getValue( 'nombre' )
          );
          }
          else
          {

          $equipos = $this->_equipo->pagListEquip(
          $active = null , null
          );
          $equipos->setCurrentPageNumber(
          $this->_getParam( 'page' , 1 )
          );
          }

          $this->view->assign( 'searchForm' , $jForm );
          $this->view->assign( 'formEquipoPrecio' , $formEquipoPrecio );

          $this->view->assign(
          'equipos' , $equipos ); */


        }


    public function backAction()
        {
        $url = new Zend_Session_Namespace( 'MTT' );
        $this->_redirect( $url->url );


        }


    public function detalleAction()
        {

        $this->_helper->layout->disableLayout();
        $id = intval( $this->_getParam( 'id' ) );
        $this->view->jQuery()
                ->addOnLoad(
                        ' $(document).ready(function() {
                            $("#tabs").tabs();
                          });'
                )
        ;

//Editar datos del equipo
        $this->editarAction();

//Listar preguntas sobre el equipo
        $pregunta = new Mtt_Models_Bussines_Pregunta();
        $preguntas_equip = $pregunta->listByEquip( $id );
        $this->view->assign( 'preguntas' , $preguntas_equip );

//Listar reservas
        $reserva = new Mtt_Models_Bussines_Reserva();
        $reservas_equip = $reserva->getReservaByEquipment( $id
                , Mtt_Models_Table_TipoReserva::RESERVED );
        $this->view->assign( 'reservas' , $reservas_equip );

//Listar operaciones
        $operacion = new Mtt_Models_Bussines_Operacion();
        $operaciones_user = $operacion->
                listByUserOperation( $id ,
                                     Mtt_Models_Table_EstadoOperacion::SALE
        );
        $this->view->assign( 'operaciones' , $operaciones_user );


        }


    public function searchequipoAction()
        {
        $results = $this->_equipo->search(
                $this->_getParam( 'term' )
        );
        $this->_helper->json(
                array_values( $results )
        );


        }


    public function saveprecioAction()
        {
        $equipoPrecio = null;
        $equipo = new Mtt_Models_Bussines_Equipo();

        $form = new Mtt_JxForm_DialogPrecioVenta();
        if ( $this->_request->isPost()
                &&
                $form->isValid( $this->_request->getPost() ) )
            {

            $equipoPrecio = $form->getValues();
            if ( is_numeric( $equipoPrecio['precioventa'] ) )
                {
                $equipoPrecio['publicacionEquipo_id'] =
                        Mtt_Models_Bussines_PublicacionEquipo::Activada;
                $equipo->updateEquipo( $equipoPrecio , $equipoPrecio['id'] );
                $this->_redirect( 'admin/equipo' );
                }
            }


        }


    public function checkprecioventaAction()
        {
        $results = $this->_equipo->getPrecioVenta(
                $this->_getParam( 'id' )
        );
        if ( $results->precioventa > 0 )
            {
            $dataEquipo['publicacionEquipo_id'] =
                    Mtt_Models_Bussines_PublicacionEquipo::Activada;
            $this->_equipo->updateEquipo( $dataEquipo , $results->id );
            }
        $data = $this->_helper->json(
                $results
        );

        $this->view->assign( 'data' , $data );


        }


    public function activaequiposAction()
        {

        $this->view->assign(
                'titulo' , $this->_translate->translate( 'activar equipos' )
        );
        $form = new Mtt_Form_ActivarEquipo();
        $this->view->assign( 'frmActivarEquipo' , $form );

//$alertas = new Mtt_Models_Bussines_Alerta();
        $alertasEmail = new Mtt_Models_Bussines_AlertaEmail();

        if ( $this->_request->isPost()
                &&
                $form->isValid( $this->_request->getPost() ) )
            {

            $equipos = $form->getValues();
            $ids = $equipos['equipos'];

            foreach ( $ids as $item )
                {
                $this->_equipo->publicarEquipo( $item );
                $data['equipo_id'] = $item;
                $alertasEmail->saveAlertaEmail( $data );
//$equipment = $this->_equipo->getProduct( $item );
//$alertasEquipo = $alertas->getAlertsToEmails( ( array ) $equipment );
//                if ( count( $alertasEquipo > 0 ) )
//                    {
//                    $data['url'] = $equipment->slug;
//                    $data['equipo'] = $equipment->nombre;
//
//
//                    $alertas->sendMailToAlert( $data
//                            , 'send to mail'
//                            , ( array ) $alertasEquipo );
//                    }
                }


            $this->_helper->FlashMessenger(
                    $this->_translate->translate( 'se valido los equipos' ) );
            $this->_redirect(
                    $this->URL . "/" . $this->getRequest()->getActionName()
            );
            }


        }


    public function activeequipmentAction()
        {
        $this->view->assign(
                'titulo' , $this->_translate->translate( 'activar equipos' )
        );
        $form = new Mtt_Form_ActivarEquipos(
                        $this->_equipo->listEquipUnresolved()
        );

        if ( $this->_request->isPost() && $form->isValid( $this->_request->getPost() ) )
            {
            $this->view->assign( 'data' , $form->getValues() );
            }

        $this->view->assign( 'form' , $form );


        }


    public function questionAction()
        {
        
        }


    public function addtopofersAction()
        {
        $id = intval( $this->_getParam( 'id' , null ) );
        $this->_equipo->addTopOfers( $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'se agrego como top ofers  ' ) . $id );
        $this->_redirect( $_SERVER['HTTP_REFERER'] );


        }


    public function quittopofersAction()
        {
        $id = intval( $this->_getParam( 'id' , null ) );
        $this->_equipo->quitTopOfers( $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'se a quitado de top ofers  ' )
                . $id );
        $this->_redirect( $_SERVER['HTTP_REFERER'] );


        }


    public function stadisticsequipoAction()
        {

        $this->_helper->layout->setLayout( 'layoutstadistica' ); /* no olvidar */

        $this->view->jQuery()->addJavascriptFile(
                        '/js/elycharts/elycharts.min.js'
                )
                ->addJavascriptFile(
                        '/js/elycharts/raphael-min.js'
                )
                ->addJavascriptFile( '/js/elycharts/cake-chart.js' );

        $this->view->assign( 'equipos' ,
                             $this->_equipo->listEquipMoreVisited( 10 ) );


        }


    public function verAction()
        {
        $id = intval( $this->_getParam( 'id' , null ) );
        $stmt = $this->_equipo->getCategoria( $id );
        $this->view->assign( 'categoria' , $stmt );


        }


    public function exportarAction()
        {

        include(APPLICATION_PATH . "/../library/PHPExcel.php");

        $parametro = $this->_getParam( 'id' );
        $codePais = $this->_getParam( 'codePais' );
        $codeExportar = $this->_getParam( 'codeExportar' );
        $pais = $this->_getParam( 'pais' );


        $objPHPExcel = new PHPExcel();

// Set properties
        $objPHPExcel->getProperties()->setCreator( "Maarten Balliauw" );
        $objPHPExcel->getProperties()->setLastModifiedBy( "Maarten Balliauw" );
        $objPHPExcel->getProperties()->setTitle( "Office 2007 XLSX Test Document" );
        $objPHPExcel->getProperties()->setSubject( "Office 2007 XLSX Test Document" );
        $objPHPExcel->getProperties()->setDescription( "Test document for Office 2007 XLSX, generated using PHP classes." );


//		$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(12);
//		$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);
//		
        $objPHPExcel->getActiveSheet()->getStyle( 'A8:B8' )->getFont()->getColor()->setARGB( PHPExcel_Style_Color::COLOR_RED );
        $objPHPExcel->getActiveSheet()->getStyle( 'A8:B8' )->getAlignment()->setHorizontal( PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS );
        $objPHPExcel->getActiveSheet()->getStyle( 'A8:B8' )->getBorders()->getTop()->setBorderStyle( PHPExcel_Style_Border::BORDER_THIN );
        $objPHPExcel->getActiveSheet()->getStyle( 'A8:B8' )->getBorders()->getBottom()->setBorderStyle( PHPExcel_Style_Border::BORDER_THIN );
        $objPHPExcel->getActiveSheet()->getStyle( 'A8:B8' )->getBorders()->getLeft()->setBorderStyle( PHPExcel_Style_Border::BORDER_THIN );
        $objPHPExcel->getActiveSheet()->getStyle( 'A8:B8' )->getBorders()->getRight()->setBorderStyle( PHPExcel_Style_Border::BORDER_THIN );
        $objPHPExcel->getActiveSheet()->getStyle( 'A8:B8' )->getFill()->setFillType( PHPExcel_Style_Fill::FILL_SOLID );
        $objPHPExcel->getActiveSheet()->getStyle( 'A8:B8' )->getFill()->getStartColor()->setARGB( 'FAFB60' );



        $objPHPExcel->getActiveSheet()->getStyle( 'C8:Y8' )->getFont()->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK );
        $objPHPExcel->getActiveSheet()->getStyle( 'C8:Y8' )->getAlignment()->setHorizontal( PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS );
        $objPHPExcel->getActiveSheet()->getStyle( 'C8:Y8' )->getBorders()->getTop()->setBorderStyle( PHPExcel_Style_Border::BORDER_THIN );
        $objPHPExcel->getActiveSheet()->getStyle( 'C8:Y8' )->getBorders()->getBottom()->setBorderStyle( PHPExcel_Style_Border::BORDER_THIN );
        $objPHPExcel->getActiveSheet()->getStyle( 'C8:Y8' )->getBorders()->getLeft()->setBorderStyle( PHPExcel_Style_Border::BORDER_THIN );
        $objPHPExcel->getActiveSheet()->getStyle( 'C8:Y8' )->getBorders()->getRight()->setBorderStyle( PHPExcel_Style_Border::BORDER_THIN );
        $objPHPExcel->getActiveSheet()->getStyle( 'C8:Y8' )->getFill()->setFillType( PHPExcel_Style_Fill::FILL_SOLID );
        $objPHPExcel->getActiveSheet()->getStyle( 'C8:Y8' )->getFill()->getStartColor()->setARGB( 'FFFF5D' );


        $objPHPExcel->getActiveSheet()->getColumnDimension( 'A' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'B' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'C' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'D' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'E' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'F' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'G' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'H' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'I' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'J' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'K' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'L' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'M' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'N' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'O' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'P' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'Q' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'R' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'S' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'T' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'U' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'V' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'W' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'X' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'Y' )->setAutoSize( true );

// Add some data
        $objPHPExcel->setActiveSheetIndex( 0 );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'C1' ,
                                                      $this->_translate->translate( 'Lista de precios para ' ) . $pais );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'C2' ,
                                                      date( 'l jS \of F Y h:i:s A' ) );

        /* encabezado */
        $objPHPExcel->getActiveSheet()->SetCellValue( 'A8' , 'POSTED A¡' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'B8' ,
                                                      'Availability asked on' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'C8' , 'on portal' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'D8' , 'ID' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'E8' ,
                                                      'Product name / Description' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'F8' , 'Description SP' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'G8' , 'Typ/Model' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'H8' , 'Manufacturer' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'I8' , 'Quantity' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'J8' , 'Category ' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'K8' ,
                                                      'Condition of device' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'L8' ,
                                                      'Manu-facturing Year' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'M8' , 'Age of Device' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'N8' ,
                                                      'Country of Origin / Device location' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'O8' ,
                                                      'Selling Price EXW see column P ' . $codePais );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'P8' ,
                                                      'Market Value of device ' . $codeExportar );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'Q8' ,
                                                      'CHILE Lead Success Rating [%]' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'R8' ,
                                                      'Approx.Weight [kg]' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'S8' ,
                                                      'Dimension LxWxH cm' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'T8' , 'Box size LxWxH cm' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'U8' , 'Infos(no Manual)' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'V8' , 'Frec.Hz' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'W8' , 'Description' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'X8' , 'Accessories ' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'Y8' ,
                                                      'Offer to posibleBuyer ' );
        /* encabezado */


        /* enviando los datos */
        $objEquipoEquipos = $this->_equipo->getProductMoneyVentaById( $parametro );


        $rowNumber = 8;
        foreach ( $objEquipoEquipos as $equipo )
            {
            $rowNumber++;
            $objPHPExcel->getActiveSheet()->getRowDimension( $rowNumber )->setRowHeight( 30 );
            $objPHPExcel->getActiveSheet()->SetCellValue( 'A' . ( string ) $rowNumber ,
                                                          '' );
            $objPHPExcel->getActiveSheet()->SetCellValue( 'B' . ( string ) $rowNumber ,
                                                          '' );

            if (
                    $equipo->active && $equipo->publicacionid == Mtt_Models_Bussines_PublicacionEquipo::Activada
            )
                {

                $objPHPExcel->getActiveSheet()->SetCellValue( 'C' . ( string ) $rowNumber ,
                                                              'yes' );
                }
            else
                {
                $objPHPExcel->getActiveSheet()->SetCellValue( 'C' . ( string ) $rowNumber ,
                                                              'no' );
                }

            $objPHPExcel->getActiveSheet()->SetCellValue( 'D' . ( string ) $rowNumber ,
                                                          $equipo->id );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'E' . ( string ) $rowNumber ,
                                                          $equipo->nombre );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'F' . ( string ) $rowNumber ,
                                                          strip_tags( $equipo->descripcion ) );


            $objPHPExcel->getActiveSheet()->SetCellValue( 'G' . ( string ) $rowNumber ,
                                                          $equipo->modelo );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'H' . ( string ) $rowNumber ,
                                                          $equipo->fabricante );
            $objPHPExcel->getActiveSheet()->SetCellValue( 'I' . ( string ) $rowNumber ,
                                                          $equipo->cantidad );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'J' . ( string ) $rowNumber ,
                                                          $equipo->categoria );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'K' . ( string ) $rowNumber ,
                                                          $equipo->calidad );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'L' . ( string ) $rowNumber ,
                                                          $equipo->fechafabricacion );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'M' . ( string ) $rowNumber ,
                                                          '' );


            $objPHPExcel->getActiveSheet()->SetCellValue( 'N' . ( string ) $rowNumber ,
                                                          $equipo->pais );


            if ( $equipo->precioventa <> 0 && !empty( $equipo->precioventa ) && is_numeric( $equipo->precioventa ) )
                {
                $precio = Mtt_Service_Finance::currency(
                                $equipo->ventacodegoogle , $codePais ,
                                $equipo->precioventa
                );

                $objPHPExcel->getActiveSheet()->SetCellValue(
                        'O' . ( string ) $rowNumber ,
                        round( $precio , 0 , PHP_ROUND_HALF_UP )
                );
                }
            else
                {
                $objPHPExcel->getActiveSheet()->SetCellValue(
                        'O' . ( string ) $rowNumber , $equipo->precioventa
                );
                }


            if ( $equipo->precioventa <> 0 && !empty( $equipo->precioventa ) && is_numeric( $equipo->precioventa ) )
                {
                $precio = Mtt_Service_Finance::currency(
                                $equipo->ventacodegoogle , $codeExportar ,
                                $equipo->precioventa
                );

                $objPHPExcel->getActiveSheet()->SetCellValue(
                        'P' . ( string ) $rowNumber ,
                        round( $precio , 0 , PHP_ROUND_HALF_UP )
                );
                }
            else
                {
                $objPHPExcel->getActiveSheet()->SetCellValue(
                        'P' . ( string ) $rowNumber , $equipo->precioventa
                );
                }



            $objPHPExcel->getActiveSheet()->SetCellValue( 'Q' . ( string ) $rowNumber ,
                                                          '' );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'R' . ( string ) $rowNumber ,
                                                          $equipo->pesoEstimado );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'S' . ( string ) $rowNumber ,
                                                          $equipo->ancho . '*' . $equipo->alto );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'T' . ( string ) $rowNumber ,
                                                          $equipo->sizeCaja );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'U' . ( string ) $rowNumber ,
                                                          '' );
            $objPHPExcel->getActiveSheet()->SetCellValue( 'V' . ( string ) $rowNumber ,
                                                          '' );
            $objPHPExcel->getActiveSheet()->SetCellValue( 'W' . ( string ) $rowNumber ,
                                                          '' );
            $objPHPExcel->getActiveSheet()->SetCellValue( 'X' . ( string ) $rowNumber ,
                                                          '' );
            $objPHPExcel->getActiveSheet()->SetCellValue( 'Y' . ( string ) $rowNumber ,
                                                          '' );
            }
        /* enviando los datos */

// Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle( 'Simple' );


// Save Excel 2007 file

        $objWriter = new PHPExcel_Writer_Excel2007( $objPHPExcel );
        $rutaNombre = DS . 'excel' . DS . 'informe.xlsx';
        $ruta = APPLICATION_PUBLIC . DS . $rutaNombre;
        $objWriter->save( $ruta );

        $this->view->assign( 'parametro' , $rutaNombre );


        }


    public function exportarsuizaAction()
        {

        include(APPLICATION_PATH . "/../library/PHPExcel.php");
        $objPHPExcel = new PHPExcel();


        $parametro = $this->_getParam( 'id' );
        $pais = $this->_getParam( 'pais' );


// Set properties
        $objPHPExcel->getProperties()->setCreator( "Maarten Balliauw" );
        $objPHPExcel->getProperties()->setLastModifiedBy( "Maarten Balliauw" );
        $objPHPExcel->getProperties()->setTitle( "Office 2007 XLSX Test Document" );
        $objPHPExcel->getProperties()->setSubject( "Office 2007 XLSX Test Document" );
        $objPHPExcel->getProperties()->setDescription( "Test document for Office 2007 XLSX, generated using PHP classes." );


// Add some data
        $objPHPExcel->setActiveSheetIndex( 0 );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'C1' ,
                                                      $this->_translate->translate( 'Lista de precios para ' ) . $pais );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'C2' ,
                                                      date( 'l jS \of F Y h:i:s A' ) );

//		$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(12);
//		$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);

        $objPHPExcel->getActiveSheet()->getColumnDimension( 'A' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'B' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'C' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'D' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'E' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'F' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'G' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'H' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'I' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'J' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'K' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'L' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'M' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'N' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'O' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'P' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'Q' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'R' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'S' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'T' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'U' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'V' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'W' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'X' )->setAutoSize( true );
        $objPHPExcel->getActiveSheet()->getColumnDimension( 'Y' )->setAutoSize( true );


        $objPHPExcel->getActiveSheet()->getStyle( 'A8:B8' )->getFont()->getColor()->setARGB( PHPExcel_Style_Color::COLOR_RED );
        $objPHPExcel->getActiveSheet()->getStyle( 'A8:B8' )->getAlignment()->setHorizontal( PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS );
        $objPHPExcel->getActiveSheet()->getStyle( 'A8:B8' )->getBorders()->getTop()->setBorderStyle( PHPExcel_Style_Border::BORDER_THIN );
        $objPHPExcel->getActiveSheet()->getStyle( 'A8:B8' )->getBorders()->getBottom()->setBorderStyle( PHPExcel_Style_Border::BORDER_THIN );
        $objPHPExcel->getActiveSheet()->getStyle( 'A8:B8' )->getBorders()->getLeft()->setBorderStyle( PHPExcel_Style_Border::BORDER_THIN );
        $objPHPExcel->getActiveSheet()->getStyle( 'A8:B8' )->getBorders()->getRight()->setBorderStyle( PHPExcel_Style_Border::BORDER_THIN );
        $objPHPExcel->getActiveSheet()->getStyle( 'A8:B8' )->getFill()->setFillType( PHPExcel_Style_Fill::FILL_SOLID );
        $objPHPExcel->getActiveSheet()->getStyle( 'A8:B8' )->getFill()->getStartColor()->setARGB( 'FAFB60' );



        $objPHPExcel->getActiveSheet()->getStyle( 'C8:Y8' )->getFont()->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK );
        $objPHPExcel->getActiveSheet()->getStyle( 'C8:Y8' )->getAlignment()->setHorizontal( PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS );
        $objPHPExcel->getActiveSheet()->getStyle( 'C8:Y8' )->getBorders()->getTop()->setBorderStyle( PHPExcel_Style_Border::BORDER_THIN );
        $objPHPExcel->getActiveSheet()->getStyle( 'C8:Y8' )->getBorders()->getBottom()->setBorderStyle( PHPExcel_Style_Border::BORDER_THIN );
        $objPHPExcel->getActiveSheet()->getStyle( 'C8:Y8' )->getBorders()->getLeft()->setBorderStyle( PHPExcel_Style_Border::BORDER_THIN );
        $objPHPExcel->getActiveSheet()->getStyle( 'C8:Y8' )->getBorders()->getRight()->setBorderStyle( PHPExcel_Style_Border::BORDER_THIN );
        $objPHPExcel->getActiveSheet()->getStyle( 'C8:Y8' )->getFill()->setFillType( PHPExcel_Style_Fill::FILL_SOLID );
        $objPHPExcel->getActiveSheet()->getStyle( 'C8:Y8' )->getFill()->getStartColor()->setARGB( 'FFFF5D' );

        /* encabezado */
        $objPHPExcel->getActiveSheet()->SetCellValue( 'A8' , 'POSTED A¡' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'B8' ,
                                                      'Availability asked on' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'C8' , 'on portal' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'D8' , 'ID' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'E8' ,
                                                      'Product name / Description' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'F8' , 'Description SP' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'G8' , 'Typ/Model' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'H8' , 'Manufacturer' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'I8' , 'Quantity' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'J8' , 'Category ' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'K8' ,
                                                      'Condition of device' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'L8' ,
                                                      'Manu-facturing Year' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'M8' , 'Age of Device' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'N8' ,
                                                      'Country of Origin / Device location' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'O8' ,
                                                      'Selling Price EXW see column P [CHF]' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'P8' ,
                                                      'Market Value of deviceChile [USD]' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'Q8' ,
                                                      'CHILE Lead Success Rating [%]' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'R8' ,
                                                      'Approx.Weight [kg]' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'S8' ,
                                                      'Dimension LxWxH cm' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'T8' , 'Box size LxWxH cm' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'U8' , 'Infos(no Manual)' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'V8' , 'Frec.Hz' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'W8' , 'Description' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'X8' , 'Accessories ' );
        $objPHPExcel->getActiveSheet()->SetCellValue( 'Y8' ,
                                                      'Offer to posibleBuyer ' );
        /* encabezado */


        /* enviando los datos */
        $objEquipoEquipos = $this->_equipo->getProductMoneyVentaById( $parametro );


        $rowNumber = 8;
        foreach ( $objEquipoEquipos as $equipo )
            {
            $rowNumber++;

            $objPHPExcel->getActiveSheet()->getRowDimension( $rowNumber )->setRowHeight( 30 );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'A' . ( string ) $rowNumber ,
                                                          '' );
            $objPHPExcel->getActiveSheet()->SetCellValue( 'B' . ( string ) $rowNumber ,
                                                          '' );

            if (
                    $equipo->active && $equipo->publicacionid == Mtt_Models_Bussines_PublicacionEquipo::Activada
            )
                {

                $objPHPExcel->getActiveSheet()->SetCellValue( 'C' . ( string ) $rowNumber ,
                                                              'yes' );
                }
            else
                {
                $objPHPExcel->getActiveSheet()->SetCellValue( 'C' . ( string ) $rowNumber ,
                                                              'no' );
                }

            $objPHPExcel->getActiveSheet()->SetCellValue( 'D' . ( string ) $rowNumber ,
                                                          $equipo->id );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'E' . ( string ) $rowNumber ,
                                                          $equipo->nombre );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'F' . ( string ) $rowNumber ,
                                                          strip_tags( $equipo->descripcion ) );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'G' . ( string ) $rowNumber ,
                                                          $equipo->modelo );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'H' . ( string ) $rowNumber ,
                                                          $equipo->fabricante );
            $objPHPExcel->getActiveSheet()->SetCellValue( 'I' . ( string ) $rowNumber ,
                                                          $equipo->cantidad );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'J' . ( string ) $rowNumber ,
                                                          $equipo->categoria );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'K' . ( string ) $rowNumber ,
                                                          $equipo->calidad );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'L' . ( string ) $rowNumber ,
                                                          $equipo->fechafabricacion );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'M' . ( string ) $rowNumber ,
                                                          '' );


            $objPHPExcel->getActiveSheet()->SetCellValue( 'N' . ( string ) $rowNumber ,
                                                          $equipo->pais );

            if ( $equipo->precioventa <> 0 && !empty( $equipo->precioventa ) )
                {
                $precio = Mtt_Service_Finance::currency(
                                $equipo->ventacodegoogle , 'CHF' ,
                                $equipo->precioventa
                );

                $objPHPExcel->getActiveSheet()->SetCellValue(
                        'O' . ( string ) $rowNumber ,
                        round( $precio , 0 , PHP_ROUND_HALF_UP )
                );
                }
            else
                {
                $objPHPExcel->getActiveSheet()->SetCellValue(
                        'O' . ( string ) $rowNumber , $equipo->precioventa
                );
                }


            $objPHPExcel->getActiveSheet()->SetCellValue( 'P' . ( string ) $rowNumber ,
                                                          '' );
            $objPHPExcel->getActiveSheet()->SetCellValue( 'Q' . ( string ) $rowNumber ,
                                                          '' );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'R' . ( string ) $rowNumber ,
                                                          $equipo->pesoEstimado );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'S' . ( string ) $rowNumber ,
                                                          $equipo->ancho . '*' . $equipo->alto );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'T' . ( string ) $rowNumber ,
                                                          $equipo->sizeCaja );

            $objPHPExcel->getActiveSheet()->SetCellValue( 'U' . ( string ) $rowNumber ,
                                                          '' );
            $objPHPExcel->getActiveSheet()->SetCellValue( 'V' . ( string ) $rowNumber ,
                                                          '' );
            $objPHPExcel->getActiveSheet()->SetCellValue( 'W' . ( string ) $rowNumber ,
                                                          '' );
            $objPHPExcel->getActiveSheet()->SetCellValue( 'X' . ( string ) $rowNumber ,
                                                          '' );
            $objPHPExcel->getActiveSheet()->SetCellValue( 'Y' . ( string ) $rowNumber ,
                                                          '' );
            }
        /* enviando los datos */

// Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle( 'Simple' );


// Save Excel 2007 file

        $objWriter = new PHPExcel_Writer_Excel2007( $objPHPExcel );
        $rutaNombre = DS . 'excel' . DS . 'informe.xlsx';
        $ruta = APPLICATION_PUBLIC . DS . $rutaNombre;
        $objWriter->save( $ruta );

        $this->view->assign( 'parametro' , $rutaNombre );


        }


    public function nuevoAction()
        {

        $form = new Mtt_Form_Equipo();
        $this->view->jQuery()
                ->addJavascriptFile(
                        '/js/jwysiwyg/jquery.wysiwyg.js'
                )
                ->addJavascriptFile(
                        '/js/equipo_txtarea.js'
                )
                ->addStylesheet(
                        '/js/jwysiwyg/jquery.wysiwyg.css'
        );
        if ( $this->_request->isPost()
                &&
                $form->isValid( $this->_request->getPost() )
        )
            {

            $location = $form->sourceDocumento->getFileName();

            $equipo = $form->getValues();

            $equipo['usuario_id'] = $this->authData['usuario']->id;

            

            $idEquipo = $this->_equipo->saveEquipoAdmin( $equipo );

            $upload = $form->sourceDocumento->getTransferAdapter();

            $nombreArchivoEquipo = $this->_equipo->getNameDocumentById(
                    $idEquipo
            );

            $upload->addFilter( 'Rename'
                    ,
                                array(
                'target' => APPLICATION_PUBLIC . '/resources/files/' .
                $idEquipo . '.' . $nombreArchivoEquipo->sourceDocumento ,
                'overwrite' => true
            ) );

            $upload->receive();

            $data['sourceDocumento'] =
                    $idEquipo . '.' . $nombreArchivoEquipo->sourceDocumento;
            $this->_equipo->updateEquipo( $data , $idEquipo );


            $this->_helper->FlashMessenger(
                    $this->_translate->translate( 'se registro el equipo' )
            );
            $this->_redirect( $this->URL );
            }
        $this->view->assign( 'frmRegistrar' , $form );


        }


    public function editarAction()
        {

        $this->view->jQuery()
                ->addJavascriptFile(
                        '/js/jwysiwyg/jquery.wysiwyg.js'
                )
                ->addJavascriptFile(
                        '/js/equipo_txtarea.js'
                )
                ->addStylesheet(
                        '/js/jwysiwyg/jquery.wysiwyg.css'
        );

        $id = intval( $this->_getParam( 'id' ) );

        $form = new Mtt_EditForm_Equipo();



        $equipo = $this->_equipo->getFindIdByAdmin( $id );

        if ( !is_null( $equipo ) )
            {
            if ( $this->_request->isPost() && $form->isValid(
                            $this->_request->getPost() )
            )
                {
                $this->_equipo->updateEquipo( $form->getValues() , $id );
                $this->_helper->FlashMessenger(
                        $this->_translate->translate( 'se modifico el equipo' )
                );
                $this->_redirect( $this->URL );
                }
            else
                {
                $this->_helper->FlashMessenger(
                        $this->_translate->translate( 'ingrese adecuadamente
		los valores marcados con rojo' )
                );
                }
            //$form->setDefaults($equipo->toArray());
            $form->setDefaults( ( array ) $equipo );
            $this->view->assign( 'form' , $form );
            }
        else
            {
            $this->_helper->FlashMessenger(
                    $this->_translate->translate( 'no existe el equipo' )
            );
            $this->_redirect( $this->URL );
            }


        }


    public function borrarAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) );
        $this->_equipo->desactivarEquipo( $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'equipo borrado' )
        );
        $this->_redirect( $this->URL );


        }


    public function activarAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) );
        $this->_equipo->activarEquipo( $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'equipo activado' )
        );
        $this->_redirect( $this->URL );


        }


    public function desactivarAction()
        {
        $id = intval( $this->_request->getParam( 'id' ) );
        $this->_equipo->desactivarEquipo( $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'equipo desactivado' )
        );
        $this->_redirect( $this->URL );


        }


    }

