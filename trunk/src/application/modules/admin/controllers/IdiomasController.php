<?php

class Admin_IdiomasController extends Mtt_Controller_Action {

    protected $_idioma;

    public function init() {
        parent::init();
        $this->_idioma = new Mtt_Models_Bussines_Idioma();
    }

    public function indexAction() {

        $idiomas = $this->_idioma->pagListLanguages();
        $idiomas->setCurrentPageNumber(
                $this->_getParam('page', 1)
        );
        $this->view->assign(
                'idiomas', $idiomas
        );
    }

    public function newAction() {
        $this->view->assign(
                'titulo', $this->_translate->translate('nuevo idioma')
        );

        $form = new Mtt_Form_Idioma();
        if ($this->_request->isPost()
                &&
                $form->isValid($this->_request->getPost())
        ) {

            $idioma = $form->getValues();

            $this->_idioma->saveIdioma($idioma);

            $this->_helper->FlashMessenger(
                    $this->_translate->translate('se registro el idioma'));
            $this->_redirect($this->URL);
        }
        $this->view->assign('frmRegistrar', $form);
    }

    public function editAction() {

        $id = intval($this->_getParam('id'));

        $form = new Mtt_Form_Idioma($options = null, $id);

        $idioma = $this->_idioma->getFindId($id);

        if (is_object($idioma)) {
            if ($this->_request->isPost() && $form->isValid(
                            $this->_request->getPost()
                    )
            ) {

                $this->_idioma->updateIdioma(
                        $form->getValues(), $id
                );

                $this->_helper->FlashMessenger(
                        $this->_translate->translate('Se modificó el idioma')
                );
                $this->_redirect($this->URL);
            }
           
            $form->setDefaults((array) $idioma);
            $this->view->assign('form', $form);
        } else {
            $this->_helper->FlashMessenger(
                    $this->_translate->translate('No existe el idioma'));
            $this->_redirect($this->URL);
        }
    }

    public function deleteAction() {
        $id = intval($this->_request->getParam('id'));
        $this->_idioma->desactivarIdioma($id);
        $this->_helper->FlashMessenger(
                $this->_translate->translate('se elimino el idioma')
        );
        $this->_redirect($this->URL);
    }

    public function activarAction() {
        $id = intval($this->_request->getParam('id'));
        $this->_idioma->activarIdioma($id);
        $this->_helper->FlashMessenger(
                $this->_translate->translate('idioma activado')
        );
        $this->_redirect($this->URL);
    }

    public function desactivarAction() {
        $id = intval($this->_request->getParam('id'));
        $this->_idioma->desactivarIdioma($id);
        $this->_helper->FlashMessenger(
                $this->_translate->translate('Idioma desactivado')
        );
        $this->_redirect($this->URL);
    }

    public function changeidiomaAction() {

        $_idioma = new Mtt_Models_Bussines_Idioma();

        if ($this->_request->isPost()
        ) {
            $data = $this->_request->getPost();

            if ($data['recordar'] == 1) {
                $dataConfig = $_config->checkUserConfig(
                        $this->authData['usuario']->id
                );
                if (is_object($dataConfig)) {
                    $_config->updateConfig($data, $dataConfig->id);
                } else {
                    $_config->saveConfig($data);
                }
            }
            if ((int) $data['idioma_id'] > 0) {
                $dataIdioma = $_idioma->getFindId($data['idioma_id']);
                $this->mtt->config->lang = $dataIdioma->prefijo;
                $this->mtt->config->idlang = $dataIdioma->id;
            }

            $this->_redirect($_SERVER['HTTP_REFERER']);
        }
    }

}

