<?php

class Admin_OperacionEnviosController 
    extends Mtt_Controller_Action {

    protected $_operacionEnvios;

    public function init() {
        parent::init();
        $this->_operacionEnvios = new Mtt_Models_Bussines_OperacionEnvios();
    }

    public function indexAction() {
        
    }

    public function detalleAction() {
        
        $id = intval($this->_getParam('id', null));
        $form = new Mtt_Form_OperacionEnvio();

        if ($this->_request->isPost()
                &&
                $form->isValid($this->_request->getPost())) {

            $data = $form->getValues();
            $data['operacion_id'] = $id;
            $this->_operacionEnvios->saveOperacionEnvios($data);
        }

        $operacionesEnvios = $this->_operacionEnvios->pagListEnviosByOperation(
                $id);
        $operacionesEnvios->setCurrentPageNumber(
                $this->_getParam('page', 1)
        );
        $this->view->assign(
                'envios', $operacionesEnvios
        );
        
        $this->view->assign(
                'form', $form
        );
    }

    public function nuevoAction() {
   
    }

    public function verAction() {
       
    }

    public function editarAction() {

 
    }

    public function borrarAction() {
       
    }

    public function activarAction() {
        $id = intval($this->_request->getParam('id'));
        $operacionEnvio = $this->_operacionEnvios->getOperacionEnvios($id);
        $this->_operacionEnvios->activarOperacionEnvios($id);
        $this->_helper->FlashMessenger(
                $this->_translate->translate('envio activado')
        );
        $this->_redirect($this->URL. '/detalle/id/' .$operacionEnvio->operacion_id);
    }

    public function desactivarAction() {
        $id = intval($this->_request->getParam('id'));
        $operacionEnvio = $this->_operacionEnvios->getOperacionEnvios($id);
        $this->_operacionEnvios->desactivarOperacionEnvios($id);
        $this->_helper->FlashMessenger(
                $this->_translate->translate('envio desactivada')
        );
        $this->_redirect($this->URL. '/detalle/id/' .$operacionEnvio->operacion_id);
    }

}

