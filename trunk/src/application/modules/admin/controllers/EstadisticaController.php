<?php


class Admin_EstadisticaController
		extends Mtt_Controller_Action
{

	protected $_categoria ;
	public $ajaxable = array(
		'equipo' => array( 'html' , 'json' ) ,
		'favoritocategoria' => array( 'html' , 'json' ) ,
		'reservacategoria' => array( 'html' , 'json' )
	) ;


	public function init()
	{
		parent::init() ;
		$this->_helper->layout->setLayout( 'layoutstadistica' ) ;

		$this->view->jQuery()->addJavascriptFile(
						'/js/taffydb/taffy-min.js'
				)->addJavascriptFile(
						'/js/elycharts/elycharts.min.js'
				)
				->addJavascriptFile(
						'/js/elycharts/raphael-min.js'
		) ;
		//->addJavascriptFile( '/js/elycharts/cake-chart.js' );


		$this->_helper->getHelper( 'ajaxContext' )->initContext() ;

		$this->_categoria = new Mtt_Models_Bussines_Categoria() ;


	}


	public function indexAction()
	{

		$equipo = new Mtt_Models_Bussines_Equipo() ;

		$categorias = $this->_categoria->listCategory( 1 ) ;
		$this->view->assign( 'categoria' , $categorias ) ;
		$this->view->assign( 'titulo' ,
				$this->_translate->translate( 'los equipos mas visitados' )
		) ;
		$this->view->assign( 'dataEstadistica'
				, $equipo->listEquipMoreVisited( 10 ) ) ;


	}


	public function favoritosAction()
	{

		$categorias = $this->_categoria->listCategory( 1 ) ;
		$this->view->assign( 'categoria' , $categorias ) ;
		$this->view->assign( 'titulo' ,
				$this->_translate->translate(
						'los equipos top, agregados a favoritos'
				)
		) ;


	}


	public function reservasAction()
	{

		$categorias = $this->_categoria->listCategory( 1 ) ;
		$this->view->assign( 'categoria' , $categorias ) ;
		$this->view->assign( 'titulo' ,
				$this->_translate->translate(
						'los equipos mas reservados'
				)
		) ;


	}


	public function equipoAction()
	{
		$id = $this->_getParam( 'id' , null ) ;
		$equipo = new Mtt_Models_Bussines_Equipo() ;
		$equipVisited = $equipo->listEquipMoreVisitedByCategory( $id , 10 ) ;
		$this->view->assign( 'equipos' , $equipVisited ) ;
		$this->view->assign( 'listCategoria' , $id ) ;


	}


	public function favoritocategoriaAction()
	{
		$id = $this->_getParam( 'id' , null ) ;
		$reserva = new Mtt_Models_Bussines_Reserva() ;
		$favoritocategoria = $reserva->listEquipMoreReservedByCategory( 10
						, Mtt_Models_Table_TipoReserva::FAVORITE , $id ) ;

		$this->view->assign( 'equipos' , $favoritocategoria ) ;
		$this->view->assign( 'listCategoria' , $id ) ;


	}


	public function reservacategoriaAction()
	{
		$id = $this->_getParam( 'id' , null ) ;
		$reserva = new Mtt_Models_Bussines_Reserva() ;
		$equipReservados = $reserva->listEquipMoreReservedByCategory( 10
						, Mtt_Models_Table_TipoReserva::RESERVED , $id ) ;

		$this->view->assign( 'equipos' , $equipReservados ) ;
		$this->view->assign( 'listCategoria' , $id ) ;


	}


}

