<?php


class Admin_ProgramaController
        extends Mtt_Controller_Action
{

    protected $_programaEquipo = null ;
    protected $_manufacturer = null ;


    public function init()
    {
        parent::init() ;
        $ajaxContext = $this->_helper->getHelper( 'AjaxContext' ) ;
        $ajaxContext->addActionContext( 'showmanufacturerequipment' ,
                                        'html' )
                ->addActionContext( 'addequipment' ,
                                    'html' )
                ->addActionContext( 'showequiposmanufacturer' ,
                                    'html' )
                ->addActionContext( 'showequiposnuevos' ,
                                    'html' )
                ->addActionContext( 'quitequiposnuevos' ,
                                    'html' )
                ->initContext() ;



        $this->_programaEquipo = new Mtt_Models_Bussines_ProgramaEquipo() ;
        $this->_manufacturer = new Mtt_Models_Bussines_Fabricante() ;

        /* Initialize action controller here */


    }


    public function indexAction()
    {
        $this->view->jQuery()->addJavascriptFile(
                        '/js/jquery.checkboxtree/jquery.checkboxtree.min.js'
                )->addJavaScriptFile( '/theme/admin/js/programa.js' )
                ->addStylesheet(
                        '/js/jquery.checkboxtree/jquery.checkboxtree.min.css'
                ) ;
        $this->view->assign(
                'titulo' ,
                $this->_translate->translate( 'Add Equipos Nuevos' )
        ) ;


    }


    public function addequipmentAction()
    {

        $equipments = $this->_getParam( 'equipo' ) ;

        $this->_programaEquipo->addEquipmentByArray(
                Mtt_Models_Bussines_ProgramaEquipo::EQUIPOS_NUEVOS ,
                $equipments
        ) ;


    }


    public function showequiposmanufacturerAction()
    {

        // action body


    }


    public function showmanufacturerequipmentAction()
    {

        $manufacturers = $this->_manufacturer->getManufacturersPublishment() ;
        $this->view->assign( 'manufacturers' ,
                             $manufacturers ) ;


    }


    public function showequiposnuevosAction()
    {

        $manufacturers = $this->_programaEquipo->getProgramaEquipoById(
                Mtt_Models_Bussines_ProgramaEquipo::EQUIPOS_NUEVOS
                ) ;
        $this->view->assign( 'manufacturers' ,
                             $manufacturers ) ;


    }


    public function quitequiposnuevosAction()
    {
        $equipos = $this->_getParam( 'equipo' ) ;

        $this->_programaEquipo->deleteProgramaEquipoByArrayId(
                Mtt_Models_Bussines_ProgramaEquipo::EQUIPOS_NUEVOS ,
                $equipos ) ;


    }


}

