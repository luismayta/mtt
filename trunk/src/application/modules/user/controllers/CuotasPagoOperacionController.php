<?php


class User_CuotasPagoOperacionController
        extends Mtt_Controller_Action
    {

    protected $_cuotaspagooperacion;


    public function init()
        {
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'cuotas de pago' );
        
        parent::init();
        $this->_cuotaspagooperacion = new Mtt_Models_Bussines_CuotasPagoOperacion();
        }


    public function indexAction()
        {
        
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'cuotas' );
        $id = ( int ) ( $this->_getParam( 'id' , null ) );
        
        $this->view->assign(
                'cuotasoperacion' , $this->_cuotaspagooperacion->listByOperation(
                $id)
        );
        }
        

 
        
    }

