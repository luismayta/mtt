<?php


class User_AlertaController extends Mtt_Controller_Action
    {

    protected $_alerta ;


    public function init()
        {
        $this->view->headTitle()->setSeparator( ' - ' ) ;
        $this->view->headTitle()->prepend( 'Alerta' ) ;


        parent::init() ;
        $this->_alerta = new Mtt_Models_Bussines_Alerta() ;


        }


    public function indexAction()
        {
        $this->view->headTitle()->setSeparator( ' - ' ) ;
        $this->view->headTitle()->prepend( 'configuracion de Alertas' ) ;


        }


    public function editarAction()
        {
        $this->view->headTitle()->setSeparator( ' - ' ) ;
        $this->view->headTitle()->prepend( 'editar Alertas' ) ;


        }


    public function configurationAction()
        {
        $this->view->headTitle()->setSeparator( ' - ' ) ;
        $this->view->headTitle()->prepend( 'configuration de alertas' ) ;
        $this->view->jQuery()->addJavascriptFile( '/js/alerta.js' ) ;
        $this->view->jQuery()->addStylesheet( '/css/alerta.css'
        ) ;

        $alertasUsuario = $this->_alerta->getAlertaByUser(
                $this->authData['usuario']->id
        ) ;


        $form = new Mtt_Form_ConfigurarAlertas(
                        $this->_alerta->comprobarActivoAlerta( $alertasUsuario )
        ) ;

        if ( $this->_request->isPost()
                &&
                $form->isValid( $this->_request->getPost() )
        )
            {
            $alertas = $this->_request->getPost() ;
            $alertas['usuario_id'] = $this->authData['usuario']->id ;

            if ( empty( $alertasUsuario ) )
                {
                $this->_alerta->saveAlerta( $alertas ) ;
                }
            else
                {
                $this->_alerta->updateAlerta( $alertas , $alertasUsuario ) ;
                }
            }

        $this->view->assign( 'form' , $form ) ;
        $this->view->assign( 'alerta' ,
                             $this->_alerta->comprobarActivoAlerta( $alertasUsuario ) ) ;


        }


    }

