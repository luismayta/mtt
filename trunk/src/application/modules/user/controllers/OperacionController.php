<?php


class User_OperacionController
        extends Mtt_Controller_Action
    {

    protected $_operacion;


    public function init()
        {
        
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'mis operaciones' );
        
        parent::init();
        $this->_operacion = new Mtt_Models_Bussines_Operacion();
        }


    public function indexAction()
        {
        
        }


    public function verAction()
        {
        
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'ver operaciones' );
        
        $id = intval( $this->_request->getParam( 'id' ) );


        $this->view->assign(
                'operacion' , $this->_operacion->getOperacion( $id )
        );

  

        $this->view->assign(
                'detalles' , $this->_operacion->verDetalle( $id )
        );
        }


    public function comprasactivasAction()
        {
        
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'mis compras activas' );
        
        $this->view->jQuery()
                ->addOnLoad(
                        ' $(document).ready(function() {
                         
                          });'
        );

        $operaciones = $this->_operacion->pagGetOperations(
                $this->authData['usuario']->id ,
                Mtt_Models_Bussines_EstadoOperacion::SALE
        );



        $operaciones->setCurrentPageNumber(
                $this->_getParam( 'page' , 1 ) );
        $this->view->assign(
                'operaciones' , $operaciones
        );
        }


    public function comprasAction()
        {
        
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'mis compras' );
        
        
        //$this->_helper->layout->setLayout('layoutListado');
        //$id = intval( $this->_request->getParam( 'id' ) );
        $this->view->assign(
                'operaciones' ,
                $this->_operacion->listByUserOperation(
                        $this->authData['usuario']->id
                        , Mtt_Models_Bussines_EstadoOperacion::SALE
                )
        );
        }


    }