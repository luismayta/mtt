<?php


class User_EquipoController
        extends Mtt_Controller_Action
    {

    protected $_equipo;


    public function init()
        {

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'equipment' );

        parent::init();
        $this->_equipo = new Mtt_Models_Bussines_Equipo();
        $this->URL = $this->getRequest()->getModuleName() . '/' .
                $this->getRequest()->getControllerName();


        }


    public function indexAction()
        {

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'equipment' );

        $equipos = $this->_equipo->pagListEquipByUser(
                $this->authData['usuario']->id
        );
        $equipos->setCurrentPageNumber(
                $this->_getParam( 'page' , 1 )
        );

        $this->view->assign( 'equipos' , $equipos );

        $url = new Zend_Session_Namespace( 'MTT' );
        $url->url = $this->URL . '/' . $this->getRequest()->getActionName();


        }


    public function questionAction()
        {
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'preguntas' );


        }


    public function addtopofersAction()
        {
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'add a topofers' );


        }


    public function verAction()
        {

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'ver mis equipos' );

        $id = intval( $this->_getParam( 'id' , null ) );
        $stmt = $this->_equipo->getProduct( $id );
        $this->view->assign( 'equipo' , $stmt );

        $modImagen = new Mtt_Models_Bussines_Imagen();
        $imagenes = $modImagen->listByEquip( $id );
        $this->view->assign( 'imagenes' , $imagenes );


        }


    public function verpendientesAction()
        {
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'ver mis pendientes' );

        $equipos = $this->_equipo->pagListEquipByUserStatus(
                $this->authData['usuario']->id ,
                Mtt_Models_Bussines_PublicacionEquipo::Pendiente
        );

        $equipos->setCurrentPageNumber(
                $this->_getParam( 'page' , 1 )
        );
        $this->view->assign(
                'equipos' , $equipos
        );

        $url = new Zend_Session_Namespace( 'MTT' );
        $url->url = $this->URL . '/' . $this->getRequest()->getActionName();


        }


    public function veractivosAction()
        {

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'ver activos' );

        $equipos = $this->_equipo->pagListEquipByUserStatus(
                $this->authData['usuario']->id ,
                Mtt_Models_Bussines_PublicacionEquipo::Activada
        );

        $equipos->setCurrentPageNumber(
                $this->_getParam( 'page' , 1 )
        );
        $this->view->assign(
                'equipos' , $equipos
        );
        $url = new Zend_Session_Namespace( 'MTT' );
        $url->url = $this->URL . '/' . $this->getRequest()->getActionName();
        $url->urlBack = $this->URL . '/' . $this->getRequest()->getActionName();


        }


    public function vervendidosAction()
        {
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'ver vendidos' );

        $equipos = $this->_equipo->paglistEquipSalesUser(
                $this->authData['usuario']->id
        );
        $equipos->setCurrentPageNumber(
                $this->_getParam( 'page' , 1 )
        );

        $this->view->assign(
                'equipos' , $equipos
        );
        $url = new Zend_Session_Namespace( 'MTT' );
        $url->url = $this->URL . '/' . $this->getRequest()->getActionName();


        }


    public function vernovendidosAction()
        {

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'ver no vendidos' );

        $equipos = $this->_equipo->pagListEquipNoSalesUser(
                $this->authData['usuario']->id );
        $equipos->setCurrentPageNumber(
                $this->_getParam( 'page' , 1 )
        );
        $this->view->assign(
                'equipos' , $equipos
        );


        }


    public function favoritosAction()
        {
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'mis favoritos' );


        $this->view->assign(
                'equipos' ,
                $this->_equipo->listEquipFavoriteByUser(
                        $this->authData['usuario']->id , 10 )
        );


        }


    public function reservasAction()
        {

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'ver mis reservas' );

        $this->view->assign(
                'equipos' ,
                $this->_equipo->listEquipReservedUser(
                        $this->authData['usuario']->id )
        );


        }


    public function cotizarAction()
        {

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'cotizar' );

        $this->view->jQuery()
                ->addJavascriptFile(
                        '/js/jwysiwyg/jquery.wysiwyg.js'
                )
                ->addJavascriptFile(
                        '/js/cotizar.js'
                )
                ->addStylesheet(
                        '/js/jwysiwyg/jquery.wysiwyg.css'
                )
                ->addOnLoad(
                        ' $(document).ready(function() {
                             $("#mensaje").wysiwyg();
                          });'
                )
        ;

        $id = intval( $this->_request->getParam( 'id' ) );

        $equipo = $this->_equipo->getFindId( $id );

        $form = new Mtt_Form_Cotizar();

        if ( !is_null( $equipo ) )
            {

            if ( $this->_request->isPost()
                    &&
                    $form->isValid( $this->_request->getPost() ) )
                {

                $cotizacion = $form->getValues();

                $paises = new Mtt_Models_Bussines_Paises();
                $pais = $paises->getFindId( $cotizacion['paises_id'] );
                $cotizacion['pais'] = $pais->nombre;
                $cotizacion['equipo'] = $equipo->nombre;
                $this->_equipo->sendMailToRequest( $cotizacion , 'cotizar' );
                //$this->view->assign( 'equipo' , $equipo );
                }
            $this->view->assign( 'frmCotizar' , $form );
            }
        else
            {

            $this->_helper->FlashMessenger(
                    $this->_translate->translate( 'no existe' )
            );
            $this->_redirect( $this->URL );
            }


        }


    public function nuevoAction()
        {

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'nuevo equipo' );

        $this->view->jQuery()
                ->addJavascriptFile(
                        '/js/jwysiwyg/jquery.wysiwyg.js'
                )
                ->addJavascriptFile(
                        '/js/equipo_txtarea.js'
                )
                ->addStylesheet(
                        '/js/jwysiwyg/jquery.wysiwyg.css'
        );
        $form = new Mtt_Form_Equipo();
        $form->removeElement( 'precioventa' );
        $form->removeElement( 'monedaventa_id' );
        $form->removeElement( 'nombreOferente' );
        $form->removeElement( 'articuloId' );
        $form->removeElement( 'ciudadOferente' );
        $form->removeElement( 'codigoPostalOferente' );
        $form->removeElement( 'addCart' );
        $form->removeElement( 'sinonimo' );
        $form->removeElement( 'descuento' );
        $form->removeElement( 'publicacionEquipo_id' );
        $form->removeElement( 'tag' );
        $form->articulo_id->setAttrib( 'disable' , 'true' );
        $form->preciocompra->setLabel( $this->_translate->translate( 'precio' ) );


        if ( $this->_request->isPost() &&
                $form->isValid( $this->_request->getPost() )
        )
            {

            $location = $form->sourceDocumento->getFileName();

            $equipo = $form->getValues();

            $idEquipo = $this->_equipo->saveEquipo( $equipo );

            $upload = $form->sourceDocumento->getTransferAdapter();

            $nombreArchivoEquipo = $this->_equipo->getNameDocumentById(
                    $idEquipo
            );

            $upload->addFilter( 'Rename'
                    ,
                                array(
                'target' => APPLICATION_PATH . '/../public/resources/files/' .
                $idEquipo . '.' . $nombreArchivoEquipo->sourceDocumento ,
                'overwrite' => true
            ) );

            $upload->receive();
            $data['sourceDocumento'] =
                    $idEquipo . '.' . $nombreArchivoEquipo->sourceDocumento;

            $this->_equipo->updateEquipo( $data , $idEquipo );

            $this->_helper->FlashMessenger(
                    $this->_translate->translate(
                            'Se Registro El Equipo'
                    )
            );
            $this->_redirect( $this->URL );
            }
        $this->view->assign( 'frmRegistrar' , $form );


        }


    public function editarAction()
        {

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'editar equipment' );

        $this->view->jQuery()
                ->addJavascriptFile(
                        '/js/jwysiwyg/jquery.wysiwyg.js'
                )
                ->addJavascriptFile(
                        '/js/equipo_txtarea.js'
                )
                ->addStylesheet(
                        '/js/jwysiwyg/jquery.wysiwyg.css'
        );

        $id = intval( $this->_getParam( 'id' ) );

        $form = new Mtt_EditForm_Equipo();
        $form->removeElement( 'precioventa' );
        $form->removeElement( 'monedaventa_id' );
        $form->removeElement( 'nombreOferente' );
        $form->removeElement( 'articuloId' );
        $form->removeElement( 'ciudadOferente' );
        $form->removeElement( 'codigoPostalOferente' );
        $form->removeElement( 'addCart' );
        $form->removeElement( 'publicacionEquipo_id' );
        $form->removeElement( 'sinonimo' );
        $form->removeElement( 'descuento' );
        $form->articulo_id->setAttrib( 'readOnly' , 'true' );

        $equipo = $this->_equipo->getFindId( $id );

        if ( !is_null( $equipo ) )
            {
            if ( $this->_request->isPost() && $form->isValid(
                            $this->_request->getPost() )
            )
                {
                $this->_equipo->updateEquipo( $form->getValues() , $id );
                $this->_helper->FlashMessenger(
                        $this->_translate->translate( 'se modifico el equipo' )
                );
                $url = new Zend_Session_Namespace( 'MTT' );

                $this->_redirect( $url->url );
                }

            //$form->setDefaults( $equipo->toArray() );
            $form->setDefaults( ( array ) $equipo );
            $this->view->assign( 'form' , $form );
            }
        else
            {
            $this->_helper->FlashMessenger(
                    $this->_translate->translate( 'no se encuentra el equipo ' )
            );
            $this->_redirect( $this->URL );
            }


        }


    public function borrarAction()
        {

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'desactivar un equipo' );


        $id = intval( $this->_request->getParam( 'id' ) );
        $this->_equipo->desactivarReserva( $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'Equipo Borrado' )
        );
        $this->getRequest()->
        $this->_redirect( $this->URL );


        }


    }

