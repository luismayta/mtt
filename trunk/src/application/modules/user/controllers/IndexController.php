<?php


class User_IndexController
        extends Mtt_Controller_Action
    {

    protected $_equipo;
    protected $_reserva;


    public function init()
        {
       
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'zona usuario' );

        parent::init();
        }


    public function indexAction()
        {
        $this->_reserva = new Mtt_Models_Bussines_Reserva();
        $this->_equipo = new Mtt_Models_Bussines_Equipo();
        $this->_busqueda = new Mtt_Models_Bussines_Busqueda();

        $this->view->assign(
                'lastequipos' ,
                $this->_equipo->getLastEquipmentPublished(
                        $this->authData['usuario']->id
                )
        );

        $this->view->assign(
                'favoritos' ,
                $this->_reserva->getReservaByUser(
                        $this->authData['usuario']->id ,
                        Mtt_Models_Table_TipoReserva::FAVORITE,
                        3
                )
        );


        $lastSearch = $this->_busqueda->getLastSearchByUser(
                $this->authData['usuario']->id
        );
        $equiposLastSearch = null;
        if ( !empty( $lastSearch ) )
            {
            $equiposLastSearch = $this->_equipo->searchEquip(
                    $lastSearch->palabras_busqueda , $lastSearch->modelo ,
                    $lastSearch->fabricante , $lastSearch->categoria_id ,
                    $lastSearch->anio_inicio , $lastSearch->anio_fin ,
                    $lastSearch->precio_inicio , $lastSearch->precio_fin, 3 );
            }
        $this->view->assign( 'equiposlastSearch' , $equiposLastSearch );
        }


    }