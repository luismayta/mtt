<?php


class User_ImagenController
        extends Mtt_Controller_Action
    {

    protected $_imagen = null;


    public function init()
        {
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'imagenes de equipo' );

        parent::init();
        $ajaxContext = $this->_helper->getHelper( 'AjaxContext' );
        $ajaxContext->addActionContext( 'borrar' , array( 'json' , 'html' ) )
                ->initContext();

        $this->_imagen = new Mtt_Models_Bussines_Imagen();


        }


    public function indexAction()
        {
        
        }


    public function editarAction()
        {
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'edicion' );


        }


    public function nuevoAction()
        {

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'agregar imagen' );

        $idEquipo = ( int ) ( $this->_getParam( 'id' , null ) );

        $this->view->jQuery()->addJavascriptFile( '/js/imagen.js' );

        //Traer datos del equipo
        $equipo = new Mtt_Models_Bussines_Equipo();
        $dataEquipo = $equipo->getProduct( $idEquipo );

        $this->view->assign( 'equipo' , $dataEquipo );

        $imagenes = $this->_imagen->getImagesByEquip(
                $idEquipo
        );
        $inicial = 97;
        $nImagenes = count( $imagenes );
        $caracter = chr( $inicial + $nImagenes );
        $this->view->assign( 'imagenes' , $imagenes );

        $form = new Mtt_Form_Imagen();

        if ( $this->_request->isPost()
                &&
                $form->isValid( $this->_request->getPost() )
        )
            {

            $imagen = $form->getValues();
            $imagen['imagen'] = $idEquipo . $caracter . ".jpg";

            $upload = $form->imagen->getTransferAdapter();
            $_imagen = new Mtt_Models_Bussines_Imagen();

            $slugger = new Mtt_Filter_Slug(
                            array(
                                'field' => 'nombre' ,
                                'model' => $_imagen
                            )
            );

            $target = $slugger->filter( $idEquipo . $caracter ) . '.jpg';

            $f = new Zend_Filter_File_Rename(
                            array(
                                'target' => $target ,
                                'overwrite' => true
                            )
            );

            $upload->addFilter( $f );


            $imagen_new = array(
                'equipo_id' => $idEquipo ,
                'order' => count( $imagenes ) + 1 ,
                'imagen' => $target
            );

            if ( $form->imagen->receive() )
                {

                $serviceImage = new Mtt_Service_Image(
                                array( 'element' => 'equipment' )
                );
                $serviceImage->processImageAvata( $form->imagen->getFileName() ,
                                                  $target );
                $serviceImage->processImageEquipo( $form->imagen->getFileName() ,
                                                   $target );
                $serviceImage->processImageThumb( $form->imagen->getFileName() ,
                                                  $target );
                $serviceImage->processImageProduct( $form->imagen->getFileName() ,
                                                    $target );

                $imagen = array_merge( $imagen , $imagen_new );

                $this->_imagen->saveImagen( $imagen );
                $this->_helper->FlashMessenger(
                        $this->_translate->translate(
                                'se agrego la imagen al equipo'
                        )
                );
                $this->_redirect( 'user/imagen/nuevo/id/' . $idEquipo );
                }
            }
        $this->view->assign( 'frmRegistrar' , $form );


        }


    public function borrarAction()
        {

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'eliminar una imagen' );

        $id = ( int ) $this->_request->getParam( 'id' , null );

        $this->_imagen->desactivarImagen( $id );
        $this->view->assign( 'id' , $id );
        if ( $this->_request->isXmlHttpRequest() )
            {

            $this->view->assign( 'sms' ,
                                 $this->_translate->translate(
                            ' eliminada'
                    )
            );
            }


        }


    public function orderAction()
        {
        $imagenId = $this->_getParam( 'imageId' );
        $equipoId = $this->_getParam( 'equipoId' );

        if ( !is_null( $imagenId ) )
            {

            $this->_imagen->orderImagenById( $equipoId , $imagenId );
            }

        $this->_redirect( $_SERVER['HTTP_REFERER'] );


        }


    }

