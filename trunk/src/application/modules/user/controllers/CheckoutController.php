<?php


class User_CheckoutController
        extends Mtt_Controller_Action
    {

    protected $_operacionEquipo;
    protected $_operacion;


    public function init()
        {

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'compras de equipos' );

        parent::init();
        }


    public function preDispatch()
        {
        $this->_operacionEquipo = new Mtt_Models_Catalog_OperationEquipo();
        $this->_operacion = new Mtt_Models_Bussines_Operacion;
        parent::preDispatch();
        }


    public function indexAction()
        {
        $equipo = new Mtt_Models_Bussines_Equipo();
        $id = ( int ) $this->getRequest()->getParam( 'id' , null );

        $dataEquipo = $equipo->getMoneyVentaById( $id );

        if ( is_object( $dataEquipo ) )
            {
            $carrito = new Mtt_Store_Cart(
                            $dataEquipo , $this->mtt->currency
            );



            $this->_operacionEquipo->addOperacionDetalle( $carrito );
            }


        $form = new Mtt_Form_Checkout(
                        array(
                            'data'
                            =>
                            $this->_operacionEquipo->getOperacionDetalles()
                        )
        );

        $this->view->assign( 'checkout' , $form );

        if ( $this->_request->isPost() && $form->isValid( $this->_request->getPost() ) )
            {
            $data = $form->getValues();
            $this->_operacionEquipo->fillDetalle( $data['carro'] );

            $data = $form->getValues();
            $this->view->assign( 'data' , $data );
            }


        $this->view->assign( 'checkoutdata' ,
                $this->_operacionEquipo->getOperacionDetalles() );
        $this->view->assign( 'total' , $this->_operacionEquipo->getTotal() );
        }


    public function cartAction()
        {
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'checkout items' );

        if ( !count( $this->_operacionEquipo->getOperacionDetalles() ) )
            {
            $this->_helper->FlashMessenger(
                    'debe de agregar productos al carro de compra'
            );
            $this->_redirect( '/equipo/' );
            }

        if ( !$this->_operacionEquipo->checkFormaPagoExist() )
            {
            $dataOperacion = array(
                'usuario_id' => $this->authData['usuario']->id ,
                'fecha' => date( "Y-m-d H:i:s" ) ,
                'fechainicio' => date( "Y-m-d H:i:s" ) ,
                'estadooperacion_id' => Mtt_Models_Bussines_EstadoOperacion::SALE ,
                'total' => $this->_operacionEquipo->getTotal()
            );

            $lastInsertId = $this->_operacion->saveOperacion( $dataOperacion );

            if ( !is_null( $lastInsertId ) )
                {

                $this->_operacionEquipo->saveOperacionDetalle(
                        $this->authData['usuario']->id , $lastInsertId ,
                        $this->_operacionEquipo->getOperacionDetalles()
                );
                $this->_operacionEquipo->clearOperacionDetalles();
                $this->_helper->FlashMessenger(
                        $this->_translate->translate( 'se realizo la compra' )
                );
                $this->_redirect( '/user/operacion/ver/id/' . $lastInsertId );
                }
            }
        else
            {
            $this->_helper->FlashMessenger(
                    'escoga la forma de pago y haga click en actualizar'
            );
            $this->_redirect( '/user/checkout/index/' );
            }
        }


    public function deleteAction()
        {

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'eliminar items de checkout' );

        $this->_helper->FlashMessenger(
                $this->_translate->translate(
                        'se borro todo los items de la bolsa de compras'
                )
        );

        $this->_operacionEquipo->clearOperacionDetalles();
        $this->_redirect( '/default/equipo/' );
        }


    }

