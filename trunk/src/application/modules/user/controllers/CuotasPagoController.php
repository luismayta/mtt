<?php


class User_CuotasPagoController
        extends Mtt_Controller_Action
    {

    protected $_cuotaspago;


    public function init()
        {
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'cuotas de pago' );
        
        parent::init();
        $this->_cuotaspago = new Mtt_Models_Bussines_CuotasPago();
        }


    public function indexAction()
        {
        
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'cuotas' );
        
        $this->view->assign(
                'cuotaspago' , $this->_cuotaspago->listByUser(
                $this->authData['usuario']->id)
        );
        }
        

    public function listarAction()
        {
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'listado de pagos' );
        $idOperacion = ( int ) ( $this->_getParam( 'id' , null ) );
        
        $this->view->assign(
                'cuotaspago' , $this->_cuotaspago->listByOperationHasEquip(
                $idOperacion)
        );
        }
        
    }

