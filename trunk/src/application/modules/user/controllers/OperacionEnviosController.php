<?php


class User_OperacionEnviosController
        extends Mtt_Controller_Action
    {

    protected $_operacionEnvios;


    public function init()
        {
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'envio de operacion' );
        parent::init();
        $this->_operacionEnvios = new Mtt_Models_Bussines_OperacionEnvios();
        }


    public function indexAction()
        {
        
        }


    public function detalleAction()
        {

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'detalles de operacion envios' );

        $id = intval( $this->_getParam( 'id' , null ) );

        $operacionesEnvios = $this->_operacionEnvios->pagListEnviosByOperation(
                $id );
        $operacionesEnvios->setCurrentPageNumber(
                $this->_getParam( 'page' , 1 )
        );
        $this->view->assign(
                'envios' , $operacionesEnvios
        );
        }


    public function nuevoAction()
        {
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'nuevo detalle de envio' );
        }


    public function verAction()
        {
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'ver operacion envios' );
        }


    public function editarAction()
        {
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'edicion de envios' );
        }


    public function borrarAction()
        {
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'desactivar envios' );
        }


    public function activarAction()
        {
        
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'activar operacion' );
        
        $id = intval( $this->_request->getParam( 'id' ) );
        $operacionEnvio = $this->_operacionEnvios->getOperacionEnvios( $id );
        $this->_operacionEnvios->activarOperacionEnvios( $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'envio activado' )
        );
        $this->_redirect( $this->URL . '/detalle/id/' . $operacionEnvio->operacion_id );
        }


    public function desactivarAction()
        {
        
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'desactivar' );
        
        $id = intval( $this->_request->getParam( 'id' ) );
        $operacionEnvio = $this->_operacionEnvios->getOperacionEnvios( $id );
        $this->_operacionEnvios->desactivarOperacionEnvios( $id );
        $this->_helper->FlashMessenger(
                $this->_translate->translate( 'envio desactivada' )
        );
        $this->_redirect( $this->URL . '/detalle/id/' . $operacionEnvio->operacion_id );
        }


    }

