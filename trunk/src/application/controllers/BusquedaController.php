<?php


class BusquedaController
		extends Mtt_Controller_Action
{

	protected $_busqueda ;
	protected $URL ;


	public function init()
	{
		parent::init() ;
		$this->view->headTitle()->setSeparator( ' - ' ) ;
		$this->view->headTitle()->prepend( 'Busqueda' ) ;


		$this->_busqueda = new Mtt_Models_Bussines_Busqueda() ;
		$this->URL = '/' . $this->getRequest()->getControllerName() ;
		parent::init() ;


	}


	public function indexAction()
	{
		$this->view->headTitle()->setSeparator( ' - ' ) ;
		$this->view->headTitle()->prepend( 'index' ) ;


	}


	public function resultsearchAction()
	{
		$this->view->headTitle()->setSeparator( ' - ' ) ;
		$this->view->headTitle()->prepend( 'resultado de busqueda' ) ;

		$search = new Zend_Session_Namespace( 'MTT' ) ;

		if ( $this->_request->isPost() || !($search->Search === NULL) )
		{

			if ( $this->_request->isPost() )
			{
				$criterio = $this->_request->getPost() ;
				$criterio['anio_fin'] = '-1' ;
				$criterio['precio_inicio'] = '-1' ;
				$criterio['precio_fin'] = '-1' ;
				//asignar valores a Session
				$this->_busqueda->setSearch( $criterio ) ;
			}
			$busqueda = $this->_busqueda->getSearch() ;

			$equipo = new Mtt_Models_Bussines_Equipo() ;
			$resultados = $equipo->pagListResultSearch(
							$busqueda->palabras_busqueda
							, $busqueda->modelo
							, $busqueda->fabricante
							, $busqueda->categoria_id
							, $busqueda->anio_inicio
							, $busqueda->anio_fin
							, $busqueda->precio_inicio
							, $busqueda->precio_fin ) ;
			$resultados->setCurrentPageNumber(
					$this->_getParam( 'page' , 1 )
			) ;
			if ( $this->_busqueda->isemptySearch() < 8 &&
					Zend_Auth::getInstance()->hasIdentity() )
			{
				$form = new Mtt_Form_SaveSearch() ;
				$this->view->assign( 'frmSaveSearch' , $form ) ;
			}
			$this->view->assign( 'productos' , $resultados ) ;
			$url = new Zend_Session_Namespace( 'MTT' ) ;
			$url->urlBack = '/busqueda/resultsearch' ;
			$this->view->assign( 'id' , $this->_busqueda->isemptySearch() ) ;
		}
		else
		{
			$this->_helper->FlashMessenger( $this->_translate->translate( 'no efectuo la busqueda' ) ) ;
			$this->_redirect( '/index' ) ;
		}


	}


	public function listsearchAction()
	{
		$this->view->headTitle()->setSeparator( ' - ' ) ;
		$this->view->headTitle()->prepend( 'listado de busqueda' ) ;

		$listadoBusqueda = $this->_busqueda->listSearchByUserId(
						$this->authData['usuario']->id
				) ;

		$this->view->assign( 'busquedas' , $listadoBusqueda ) ;


	}


}

