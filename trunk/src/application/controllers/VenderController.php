<?php


class VenderController
        extends Mtt_Controller_Action
    {

    protected $_pagina;

    public function init()
        {
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'vender equipos' );

        parent::init();
        $this->_pagina = new Mtt_Models_Bussines_Page();
        }


    public function indexAction()
        {

        $this->_pagina = new Mtt_Models_Bussines_Pagina();

        $pagina = $this->_pagina->getByPageIdiomasPaises(
                    Mtt_Models_Bussines_Pagina::SELLNTRADE ,
                    $this->mtt->config->idlang , '14'
            );

       $this->view->headTitle()->prepend( $pagina->titulo );


        $this->view->assign( 'country' , $pagina->body );

        }


    }

