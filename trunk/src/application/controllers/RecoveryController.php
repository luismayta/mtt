<?php


class RecoveryController
		extends Mtt_Controller_Action
{

	protected $_usuario ;


	public function init()
	{
		$this->view->headTitle()->setSeparator( ' - ' ) ;
		$this->view->headTitle()->prepend( 'Recovery' ) ;

		parent::init() ;
		$this->_usuario = new Mtt_Models_Bussines_Usuario( ) ;


	}


	public function indexAction()
	{
		$this->view->headTitle()->setSeparator( ' - ' ) ;
		$this->view->headTitle()->prepend( 'index' ) ;

		$form = new Mtt_Form_RecoveryPassword() ;

		if ( $this->_request->isPost() &&
				$form->isValid( $this->_request->getPost() ) )
		{

			$frmrecovery = $form->getValues() ;

			if (  $this->_usuario->recoveryPassword( $frmrecovery['email']  ) )
			{

				
				$this->_helper->MyFlashMessenger(
						$this->_translate->translate(
								'se le envio la nueva clave a su buzon de correo'
						)
						, Mtt_Controller_Action_Helper_MyFlashMessenger::SUCCESS
				) ;
			}
			else
			{
				$this->_helper->MyFlashMessenger(
						$this->_translate->translate(
								'no se encontro el email en la base de datos'
						)
						, Mtt_Controller_Action_Helper_MyFlashMessenger::ERROR
				) ;
			}

			$this->_redirect( $this->URL ) ;
		}
		else
		{
			$this->view->assign( 'formRecoveryPassword' , $form ) ;
		}


	}


}

