<?php


class IndexController
        extends Mtt_Controller_Action
{

    protected $_equipo ;
    protected $_pagina ;


    public function init()
    {
//        $this->view->headTitle()->setSeparator( ' - ' );
//        $this->view->headTitle()->prepend( 'medtechtrade.com' );

        parent::init() ;
        $this->_equipo = new Mtt_Models_Catalog_Equipo() ;
        $this->_pagina = new Mtt_Models_Bussines_Page() ;


    }


    public function aboutusAction()
    {

        $titulo = "" ;
        $body = "" ;
        $this->view->headTitle()->setSeparator( ' - ' ) ;

        $country = isset( $this->mtt->config->paisCode ) ? $this->mtt->config->paisCode
                    : Mtt_Service_Finance::getCountryFromIP() ;

        $this->_pagina = new Mtt_Models_Bussines_Pagina() ;

        $pagina = $this->_pagina->getByPageIdiomaPaisCountry(
                Mtt_Models_Bussines_Pagina::ABOUTUS ,
                $this->mtt->config->idlang ,
                $country
                ) ;

        if ( is_object( $pagina ) )
        {
            if ( isset( $pagina->titulo ) )
            {

                $titulo = $pagina->titulo ;
            }
            if ( isset( $pagina->body ) )
            {

                $body = $pagina->body ;
            }
        }
        else
        {
            $pagina = $this->_pagina->getByPageIdiomasPaises(
                    Mtt_Models_Bussines_Pagina::ABOUTUS ,
                    $this->mtt->config->idlang ,
                    '14'
                    ) ;


            if ( isset( $pagina->titulo ) )
            {

                $titulo = $pagina->titulo ;
            }
            if ( isset( $pagina->body ) )
            {

                $body = $pagina->body ;
            }
        }

        $this->view->headTitle()->prepend( $titulo ) ;
        $this->view->assign( 'country' ,
                             $body ) ;


    }


    public function jobsAction()
    {
        $this->_pagina = new Mtt_Models_Bussines_Pagina() ;

        $pagina = $this->_pagina->getByPageIdiomasPaises(
                Mtt_Models_Bussines_Pagina::JOBS ,
                $this->mtt->config->idlang ,
                '14'
                ) ;

        $this->view->headTitle()->prepend( $pagina->titulo ) ;


        $this->view->assign( 'country' ,
                             $pagina->body ) ;


    }


    public function teamAction()
    {
        $this->_pagina = new Mtt_Models_Bussines_Pagina() ;

        /* $country = isset($this->mtt->config->paisCode)? $this->mtt->config->paisCode : Mtt_Service_Finance::getCountryFromIP(); 

          $pagina = $this->_pagina->getByPageIdiomaPaisCountry(
          Mtt_Models_Bussines_Pagina::TEAM , $this->mtt->config->idlang ,
          $country
          );
         */

        $pagina = $this->_pagina->getByPageIdiomasPaises(
                Mtt_Models_Bussines_Pagina::TEAM ,
                $this->mtt->config->idlang ,
                '14'
                ) ;

        $this->view->headTitle()->prepend( $pagina->titulo ) ;


        $this->view->assign( 'country' ,
                             $pagina->body ) ;


    }


    public function referencesAction()
    {
        $this->_pagina = new Mtt_Models_Bussines_Pagina() ;

        /*

          $country = isset($this->mtt->config->paisCode)? $this->mtt->config->paisCode : Mtt_Service_Finance::getCountryFromIP();

          $pagina = $this->_pagina->getByPageIdiomaPaisCountry(
          Mtt_Models_Bussines_Pagina::REFERENCES , $this->mtt->config->idlang ,
          $country
          );
         */

        $pagina = $this->_pagina->getByPageIdiomasPaises(
                Mtt_Models_Bussines_Pagina::REFERENCES ,
                $this->mtt->config->idlang ,
                '14'
                ) ;

        $this->view->headTitle()->prepend( $pagina->titulo ) ;



        $this->view->assign( 'country' ,
                             $pagina->body ) ;


    }


    public function legalnoticeAction()
    {
        $this->_pagina = new Mtt_Models_Bussines_Pagina() ;

        /* $country = isset($this->mtt->config->paisCode)? $this->mtt->config->paisCode : Mtt_Service_Finance::getCountryFromIP(); */

        /* $pagina = $this->_pagina->getByPageIdiomaPaisCountry(
          Mtt_Models_Bussines_Pagina::LEGALNOTICE , $this->mtt->config->idlang ,
          $country
          ); */

        $pagina = $this->_pagina->getByPageIdiomasPaises(
                Mtt_Models_Bussines_Pagina::LEGALNOTICE ,
                $this->mtt->config->idlang ,
                '14'
                ) ;

        $this->view->headTitle()->prepend( $pagina->titulo ) ;


        $this->view->assign( 'country' ,
                             $pagina->body ) ;


    }


    public function indexAction()
    {

        $titulo = "" ;
        $body = "" ;

        $this->view->headTitle()->setSeparator( ' - ' ) ;

        $this->_pagina = new Mtt_Models_Bussines_Pagina() ;

        $country = isset( $this->mtt->config->paisCode ) ? $this->mtt->config->paisCode
                    : Mtt_Service_Finance::getCountryFromIP() ;

        $pagina = $this->_pagina->getByPageIdiomaPaisCountry(
                Mtt_Models_Bussines_Pagina::WELCOME ,
                $this->mtt->config->idlang ,
                $country
                ) ;


        if ( is_object( $pagina ) )
        {
            if ( isset( $pagina->titulo ) )
            {

                $titulo = $pagina->titulo ;
            }
            if ( isset( $pagina->body ) )
            {

                $body = $pagina->body ;
            }
        }
        else
        {
            $pagina = $this->_pagina->getByPageIdiomasPaises(
                    Mtt_Models_Bussines_Pagina::WELCOME ,
                    $this->mtt->config->idlang ,
                    '14'
                    ) ;


            if ( isset( $pagina->titulo ) )
            {

                $titulo = $pagina->titulo ;
            }
            if ( isset( $pagina->body ) )
            {

                $body = $pagina->body ;
            }
        }

        $this->view->headTitle()->prepend( $titulo ) ;
        $this->view->assign( 'country' ,
                             $body ) ;



        /* salida */


        $this->view->assign(
                'oferEquipo' ,
                $this->_equipo->showEquiposOfers( 4 )
        ) ;


    }


}

