<?php


class EquipoRequeridoController
		extends Mtt_Controller_Action
{

	protected $_equipoRequerido ;

	public $ajaxable = array(
        'ver' => array( 'html' , 'json' ),
        'sendmail' => array('html','json')
    );

	public function init()
	{
		parent::init() ;
		$this->_equipoRequerido = new Mtt_Models_Bussines_EquipoRequerido( ) ;
		$this->URL = '/' . $this->getRequest()->getControllerName() ;

		$this->_helper->getHelper( 'ajaxContext' )->initContext();


	}


	public function preDispatch()
	{
		$this->view->headTitle()->setSeparator( ' - ' ) ;

		$this->view->headTitle()->prepend(
				$this->_translate->translate( 'equipment' )
		) ;

		parent::preDispatch() ;


	}


	public function verAction()
	{
		$id = intval( $this->_request->getParam( 'id' ) ) ;
		$dataEquipoRequerido = $this->_equipoRequerido->getById( $id ) ;


		$response = "" ;
		if ( is_object( $dataEquipoRequerido ) )
		{
			$response = $dataEquipoRequerido ;
		}
		else
		{
			$response = $this->_translate->translate( 'el registro no existe' ) ;
		}

		$this->view->assign( 'response' , $response ) ;


	}


	public function sendmailAction()
	{

		$this->view->jQuery()
				->addJavascriptFile(
						'/js/jwysiwyg/jquery.wysiwyg.js'
				)
				->addJavascriptFile(
						'/js/cotizar.js'
				)
				->addStylesheet(
						'/js/jwysiwyg/jquery.wysiwyg.css'
				)
				->addOnLoad(
						' $(document).ready(function() {
                             $("#mensaje").wysiwyg();
                          });'
				)
		;
		$this->view->headTitle()->setSeparator( ' - ' ) ;
		$this->view->headTitle()->prepend(
				$this->_translate->translate( 'send email' )
		) ;

		$id = intval( $this->_request->getParam( 'id' ) ) ;

		$equipoRequerido = $this->_equipoRequerido->getById( $id ) ;

		$form = (Zend_Auth::getInstance()->hasIdentity())
				?
				new Mtt_Form_CotizarUsuario()
				: new Mtt_Form_Cotizar() ;
		$form->removeElement('precioCheck');
		$form->removeElement('precio');
		$form->removeElement('moneda_id');

		if ( Zend_Auth::getInstance()->hasIdentity() )
		{
			$form->email->setValue( $this->authData['usuario']->email ) ;
			$form->nombre->setValue( $this->authData['usuario']->nombre . ' ' .
					$this->authData['usuario']->apellido ) ;
		}
		if ( !is_null( $equipoRequerido ) )
		{

			if ( $this->_request->isPost()
					&&
					$form->isValid( $this->_request->getPost() ) )
			{

				$paises = new Mtt_Models_Bussines_Paises() ;
				$cotizacion = array( ) ;


				if ( Zend_Auth::getInstance()->hasIdentity() )
				{

					$cotizacion['nombre'] = $this->authData['usuario']->nombre . ' ' .
							$this->authData['usuario']->apellido ;
					$cotizacion['organizacion'] = $this->authData['usuario']->institucion ;
					$cotizacion['direccion'] = $this->authData['usuario']->direccion ;
					$cotizacion['codpostal'] = $this->authData['usuario']->codpostal ;
					$cotizacion['ciudad'] = $this->authData['usuario']->ciudad ;
					$cotizacion['email'] = $this->authData['usuario']->email ;
				}
				else
				{
					$cotizacion = $form->getValues() ;
				}

				$idPais = (Zend_Auth::getInstance()->hasIdentity())
						?
						$this->authData['usuario']->paises_id
						:
						$cotizacion['paises_id'] ;

				$pais = $paises->getFindId( $idPais ) ;


				$cotizacion['equipo'] = $equipoRequerido->nombre ;
				$cotizacion['equipo_id'] = $equipoRequerido->id ;
				$cotizacion['modelo'] = $equipoRequerido->modelo ;
				$cotizacion['fechafabricacion'] = $equipoRequerido->fechafabricacion ;
				$cotizacion['fabricante'] = $equipoRequerido->fabricante ;
				$cotizacion['pais'] = $pais->nombre ;



				$this->_equipoRequerido->sendMailAdmin(
						$cotizacion ,
						$this->_translate->translate( 'A quote request for device' ) .
						" N° " . $equipoRequerido->id
				) ;

				$this->_helper->FlashMessenger(
						$this->_translate->translate( 'se ha enviado el
                        email'
						)
				) ;

				$this->_redirect( '/default/equiporequerido/' ) ;
			}
			$this->view->assign( 'frmEquipoRequerido' , $form ) ;
		}
		else
		{

			$this->_helper->FlashMessenger(
					$this->_translate->translate( 'no existe' )
			) ;
			$this->_redirect( $this->URL ) ;
		}


	}


	public function indexAction()
	{
		$this->view->headTitle()->setSeparator( ' - ' ) ;
		$this->view->headTitle()->prepend(
				$this->_translate->translate( 'equipo requeridos' )
		) ;

		$equipoRequeridos = $this->_equipoRequerido->pagList(
						Mtt_Models_Bussines_EquipoRequerido::ACTIVE , null
				) ;


		$equipoRequeridos->setCurrentPageNumber(
				$this->_getParam( 'page' , 1 )
		) ;


		$this->view->assign(
				'equiposRequeridos' , $equipoRequeridos
		) ;


	}


}

