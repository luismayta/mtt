<?php


class IdiomaController
        extends Mtt_Controller_Action
{


    public function init()
    {

        $this->view->headTitle()->setSeparator( ' - ' ) ;
        $this->view->headTitle()->prepend( 'idioma' ) ;

        parent::init() ;
        /* Initialize action controller here */


    }


    public function indexAction()
    {

        $this->view->headTitle()->setSeparator( ' - ' ) ;
        $this->view->headTitle()->prepend( 'idiomas' ) ;

        //

        $_config = new Mtt_Models_Bussines_Config() ;
        $_idioma = new Mtt_Models_Bussines_Idioma() ;
        $_pais = new Mtt_Models_Bussines_Paises() ;

        if ( $this->_request->isPost()
        )
        {
            $data = $this->_request->getPost() ;

            if ( $data['recordar'] == 1 )
            {
                $dataConfig = $_config->checkUserConfig(
                        $this->authData['usuario']->id
                ) ;
                if ( is_object( $dataConfig ) )
                {
                    $_config->updateConfig( $data ,
                                            $dataConfig->id ) ;
                }
                else
                {
                    $_config->saveConfig( $data ) ;
                }
            }
            if ( ( int ) $data['idioma_id'] > 0 )
            {
                $dataIdioma = $_idioma->getFindId( $data['idioma_id'] ) ;
                $this->mtt->config->lang = $dataIdioma->prefijo ;
                $this->mtt->config->idlang = $dataIdioma->id ;
            }

            if ( ( int ) $data['pais_id'] <> 500 )
            {
                $dataPais = $_pais->getFindId( $data['pais_id'] ) ;
                $this->mtt->config->paisId = $dataPais->id ;
                $this->mtt->config->paisCode = $dataPais->code ;
            }

            $this->view->assign( 'form' ,
                                 $this->_request->getPost() ) ;
            $this->view->assign( 'data' ,
                                 $this->mtt->config ) ;

            $this->_redirect( $_SERVER['HTTP_REFERER'] ) ;
        }


    }


    public function englishAction()
    {
        $mtt = new Zend_Session_Namespace( 'MTT' ) ;
        $mtt->lang = 'en' ;
        $this->view->assign( 'mtt' ,
                             $mtt ) ;
        $this->_redirect( $_SERVER['HTTP_REFERER'] ) ;


    }


    public function spanishAction()
    {
        $mtt = new Zend_Session_Namespace( 'MTT' ) ;
        $mtt->lang = 'es' ;
        $this->view->assign( 'mtt' ,
                             $mtt ) ;
        $this->_redirect( $_SERVER['HTTP_REFERER'] ) ;


    }


    public function alemanAction()
    {
        $mtt = new Zend_Session_Namespace( 'MTT' ) ;
        $mtt->lang = 'de' ;
        $this->view->assign( 'mtt' ,
                             $mtt ) ;
        $this->_redirect( $_SERVER['HTTP_REFERER'] ) ;


    }


    public function poloniaAction()
    {
        $mtt = new Zend_Session_Namespace( 'MTT' ) ;
        $mtt->lang = 'pl' ;
        $this->view->assign( 'mtt' ,
                             $mtt ) ;
        $this->_redirect( $_SERVER['HTTP_REFERER'] ) ;


    }


}

