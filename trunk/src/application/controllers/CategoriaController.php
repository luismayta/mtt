<?php


class CategoriaController
        extends Mtt_Controller_Action
{

    protected $_categoria ;
    protected $URL ;


    public function init()
    {
        parent::init() ;

        $this->_categoria = new Mtt_Models_Bussines_Categoria() ;
        $this->URL = '/' . $this->getRequest()->getControllerName() ;


    }


    public function preDispatch()
    {
        $this->view->headTitle()->setSeparator( ' - ' ) ;

        $this->view->headTitle()->prepend(
                $this->_translate->translate( 'categoria' )
        ) ;
        parent::preDispatch() ;


    }


    public function verAction()
    {
        $objTraducciones = new Mtt_Models_Bussines_Traducciones() ;

        $slug = $this->_getParam( 'slug' ,
                                  NULL ) ;

        $data = $this->_categoria->getCategoriaBySlug( $slug ) ;

        $this->view->headTitle()->setSeparator( ' - ' ) ;

        $translateCategoria = $objTraducciones->getTranslate(
                Mtt_Models_Bussines_Categoria::ENTIDAD_ID ,
                $data->id ,
                $this->mtt->config->idlang ,
                $data->nombre
        ) ;

        $this->view->headTitle()->prepend(
                $translateCategoria
        ) ;

        $id = $data->id ;


        $order = $this->_getParam( 'order' ,
                                   'modelo' ) ;


        $stmt = $this->_categoria->pagGetProducts( $id ,
                                                   $order ) ;
        $stmt->setCurrentPageNumber(
                $this->_getParam( 'page' ,
                                  1 )
        ) ;

        $this->view->assign( 'productos' ,
                             $stmt ) ;

        $stmtCategoria = $this->_categoria->getCategoria( $id ) ;

        $this->view->assign( 'categoria' ,
                             $stmtCategoria ) ;

        $this->view->assign( 'translateCategoria' ,
                             $translateCategoria ) ;

        $formOrder = new Mtt_Form_OrderEquipo() ;
        $this->view->assign( 'formOrder' ,
                             $formOrder ) ;

        $_equipo = new Mtt_Models_Catalog_Equipo() ;
        $this->view->assign(
                'equipoOfert'
                ,
                $_equipo->showEquiposOfersByCategory( $id ,
                                                      4 )
        ) ;

        $url = new Zend_Session_Namespace( 'MTT' ) ;
        $url->urlBack = '/category/' . $slug ;


    }


    public function indexAction()
    {
//  $this->view->headTitle()->setSeparator( ' - ' );
//  $this->view->headTitle()->prepend(
//          $this->_translate->translate( 'categoria' )
//  );


        $id = intval( $this->_getParam( 'id' ,
                                        null ) ) ;


        $order = $this->_getParam( 'order' ,
                                   'modelo' ) ;


        $stmt = $this->_categoria->getProducts( $id ,
                                                $order ) ;

        $this->view->assign( 'productos' ,
                             $stmt ) ;



        $stmtCategoria = $this->_categoria->getCategoria( $id ) ;
        $this->view->assign( 'categoria' ,
                             $stmtCategoria ) ;



        $formOrder = new Mtt_Form_OrderEquipo() ;
        $this->view->assign( 'formOrder' ,
                             $formOrder ) ;

        $_equipo = new Mtt_Models_Catalog_Equipo() ;
        $this->view->assign(
                'equipoOfert'
                ,
                $_equipo->showEquiposOfersByCategory( $id ,
                                                      4 )
        ) ;


    }


}

