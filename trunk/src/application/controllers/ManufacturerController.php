<?php


class ManufacturerController
        extends Mtt_Controller_Action
    {

    protected $fabricante;
    protected $programaEquipo;


    public function init()
        {
        $this->fabricante = new Mtt_Models_Bussines_Fabricante();
        $this->programaEquipo = new Mtt_Models_Bussines_ProgramaEquipo();
        parent::init();
        /* Initialize action controller here */


        }


    public function indexAction()
        {
        $slugManufacturer = $this->_getParam( 'slug' );
        $manufacturer = $this->fabricante->getFabricanteBySlug(
                $slugManufacturer
        );

        $equipos =
                $this->programaEquipo->getIetmsByProgramaIdManufacturerId(
                1 , $manufacturer->id
        );

        $this->view->assign( 'equipos' , $equipos );
        $this->view->assign( 'manufacturer' , $manufacturer );


        }


    public function verAction()
        {
        // action body


        }


    }

