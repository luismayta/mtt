<?php


class MonedaController
        extends Mtt_Controller_Action
{


    public function init()
    {
        parent::init() ;


    }


    public function indexAction()
    {

        if ( $this->_request->isPost() )
        {
            $data = $this->_request->getPost() ;
            $_moneda = new Mtt_Models_Bussines_Moneda() ;
            $objectMoneda = $_moneda->getFindId( $data["moneda"] ) ;

            if ( is_object( $objectMoneda ) )
            {
                $this->mtt->currency->id = $objectMoneda->id ;
                $this->mtt->currency->code = $objectMoneda->codepais ;
                $this->mtt->currency->codeGoogle = $objectMoneda->codegoogle ;
            }

            $this->_redirect( $_SERVER['HTTP_REFERER'] ) ;
        }


    }


}

