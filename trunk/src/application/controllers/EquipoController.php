<?php


class EquipoController
        extends Mtt_Controller_Action
    {

    protected $_equipo;


    public function init()
        {
        parent::init();
        $this->_equipo = new Mtt_Models_Catalog_Equipo();
        $this->URL = '/' . $this->getRequest()->getControllerName();


        }


    public function preDispatch()
        {


        parent::preDispatch();


        }


    public function indexAction()
        {
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend(
                $this->_translate->translate( 'equipo' )
        );

        $formOrder = new Mtt_Form_OrderEquipo();

        $productos = $this->_equipo->showEquipos();

        $productos->setCurrentPageNumber(
                $this->_getParam( 'page' , 1 )
        );

        $this->view->assign( 'formOrder' , $formOrder );
        $this->view->assign(
                'productos' , $productos
        );
        $url = new Zend_Session_Namespace( 'MTT' );
        $url->urlBack = '/equipo/';


        }


    public function backAction()
        {
        $url = new Zend_Session_Namespace( 'MTT' );
        $this->_redirect( $url->urlBack );


        }


    public function printAction()
        {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $idEquipo = $this->_request->getParam( 'id' );
        //$pdfData = $this->imprimirPDF($idEquipo);
        $pdfData = $this->pdfgenerator( $idEquipo );
        $objEquipo = $this->_equipo->getProduct( $idEquipo );
        $nombrePdf = Mtt_Tools_Text::getJoinText(
                        array(
                    $objEquipo->fabricante ,
                    $objEquipo->modelo ,
                    $objEquipo->nombre
                        ) , "-"
                );

        header( 'Cache-Control: public' );
        header( "Content-type:application/pdf" );

        header( "Content-Disposition: inline; filename=" . $nombrePdf . ".pdf" );

        $pdfData->Output( $nombrePdf . '.pdf' , 'I' );

//        header("Content-Disposition: inline; filename=informeMedtechtrade.pdf");
//        $this->_response->appendBody($pdfData);
//        $this->_response->setBody($pdfData);
        //readfile("informeMedtechtrade.pdf");


        }


    public function verAction()
        {

        $slug = $this->_getParam( 'slug' , null );

        $data = $this->_equipo->getEquipmentBySlug( $slug );

        $id = $data->id;

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( $data->categoria );
        $this->view->headTitle()->setSeparator( ' , ' );
        $this->view->headTitle()->prepend( $data->fabricante );
        $this->view->headTitle()->setSeparator( ' , ' );
        $this->view->headTitle()->prepend( $data->modelo );
        $this->view->headTitle()->setSeparator( ' , ' );
        $this->view->headTitle()->prepend( $data->nombre );
        $this->view->headTitle()->setSeparator( ' - ' );

        $this->view->jQuery()
                ->addJavascriptFile(
                        '/js/jquery.jPrint/jquery.jPrint.0.3.js'
                )
                ->addJavascriptFile(
                        '/js/jquery.lightbox/jquery.lightbox-0.5.js'
                )
                ->addStylesheet(
                        '/js/jquery.lightbox/jquery.lightbox-0.5.css'
                );
        $this->view->jQuery()
                ->addOnLoad(
                        '$(document).ready(function() {
                            $("#device-foto-galery .img-thumb").lightBox(
                            {fixedNavigation:true}
                            );
                            $("#device-foto-galery .product").lightBox(
                            {fixedNavigation:false}
                            );

                        });


                        $(".device-action-friend").click(function(){
                            $("#dialogToSendFriend").dialog({
                             position: "top",
                                height: 160,
                                width: 360,
                                modal: true
                            });
                        });
                        $(".device-action-help").click(function(){
                            $("#dialogHelp").dialog({
                                position: "top",
                                height: 200,
                                width: 540,
                                modal: true

                            });

                        });
                        
                        '
                );

        $producto = $this->_equipo->getProductMoneyVenta( $id );

        $imagen = new Mtt_Models_Bussines_Imagen();
        $imagenes = $imagen->getImagesByEquip( $id );

        if ( $producto->publicacionid ==
                Mtt_Models_Table_PublicacionEquipo::Activada )
            {
            $this->_equipo->updateView( $id );

            $producto->imagenes = $imagenes;


            $this->view->assign(
                    'producto' , $producto
            );


            $formEnviarAmigo = new Mtt_Form_EnviarAmigo();

            $this->view->assign( 'formEnviarAmigo' , $formEnviarAmigo );

            $this->view->assign(
                    'relatedEquipment' ,
                    $this->_equipo->getRelatedEquipment( $slug )
            );

            if ( Zend_Auth::getInstance()->hasIdentity() )
                {
                $_reserva = new Mtt_Models_Bussines_Reserva();

                $dataReservado = $_reserva->getIdByEquipmentUser(
                        $this->authData['usuario']->id , $id ,
                        Mtt_Models_Table_TipoReserva::RESERVED );

                $dataFavorito = $_reserva->getIdByEquipmentUser(
                        $this->authData['usuario']->id , $id ,
                        Mtt_Models_Table_TipoReserva::FAVORITE );

                $this->view->assign( 'reservado' , $dataReservado );

                $this->view->assign( 'favorito' , $dataFavorito );

                $url = new Zend_Session_Namespace( 'MTT' );
                $link =
                        '/equipment/' .
                        $slug;
                $url->url = $link;
                }
            /**/
            }
        else
            {
            $this->_redirect( '/default/usuario/no-autorizado' );
            }


        }


    public function verdisableAction()
        {

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend(
                $this->_translate->translate(
                        'equipo pendiente de autorizacion'
                )
        );

        $slug = $this->_getParam( 'slug' , null );

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( $slug );


        $id = $this->_equipo->getEquipmentBySlug( $slug )->id;

        $this->view->jQuery()
                ->addJavascriptFile(
                        '/js/jquery.lightbox/jquery.lightbox-0.5.js'
                )
                ->addStylesheet(
                        '/js/jquery.lightbox/jquery.lightbox-0.5.css'
                );
        $this->view->jQuery()
                ->addOnLoad(
                        '$(document).ready(function() {
                            $("#device-foto-galery .img-thumb").lightBox(
                            {fixedNavigation:true}
                            );
                            $("#device-foto-galery .product").lightBox(
                            {fixedNavigation:false}
                            );

                        });
                        '
                );

        $producto = $this->_equipo->getProduct( $id );

        $imagen = new Mtt_Models_Bussines_Imagen();
        $imagenes = $imagen->getImagesByEquip( $id );

        if ( $producto->publicacionid !=
                Mtt_Models_Table_PublicacionEquipo::Activada )
            {
            //$this->_equipo->updateView($id);

            $producto->imagenes = $imagenes;
            $this->view->assign(
                    'producto' , $producto
            );

            //$form = new Mtt_Form_SearchGeneral();
            //$this->view->assign( 'formSearch' , $form );
            }
        else
            {
            $this->_redirect( '/default/usuario/no-autorizado' );
            }


        }


    public function vercompraAction()
        {
        $slug = $this->_getParam( 'slug' , null );

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend(
                $this->_translate->translate( 'equipo adquirido' )
        );


        $id = $this->_equipo->getEquipmentBySlug( $slug )->id;


        $this->view->jQuery()
                ->addJavascriptFile(
                        '/js/jquery.lightbox/jquery.lightbox-0.5.js'
                )
                ->addStylesheet(
                        '/js/jquery.lightbox/jquery.lightbox-0.5.css'
                );
        $this->view->jQuery()
                ->addOnLoad(
                        '$(document).ready(function() {
                            $("#device-foto-galery .img-thumb").lightBox(
                            {fixedNavigation:true}
                            );
                            $("#device-foto-galery .product").lightBox(
                            {fixedNavigation:false}
                            );

                        });
                        '
                );

        $producto = $this->_equipo->getProduct( $id );

        $imagen = new Mtt_Models_Bussines_Imagen();
        $imagenes = $imagen->getImagesByEquip( $id );

        if ( $producto->publicacionid !=
                Mtt_Models_Table_PublicacionEquipo::Activada )
            {
            //$this->_equipo->updateView($id);

            $producto->imagenes = $imagenes;
            $this->view->assign(
                    'producto' , $producto
            );

            //$form = new Mtt_Form_SearchGeneral();
            //$this->view->assign( 'formSearch' , $form );
            }
        else
            {
            $this->_redirect( '/default/usuario/no-autorizado' );
            }


        }


    public function equipcategoriaAction()
        {

        $objTraducciones = new Mtt_Models_Bussines_Traducciones();

        $slug = $this->_getParam( 'slug' , null );

        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'equipment category' );

        //$this->view->headTitle()->setSeparator( ' - ' );
        //$this->view->headTitle()->prepend( $slug );

        $categoria = new Mtt_Models_Bussines_Categoria();
        $data = $categoria->getCategoriaBySlug( $slug );

        $id = $data->id;

        $this->view->jQuery()
                ->addJavascriptFile(
                        '/js/jquery.lightbox/jquery.lightbox-0.5.js'
                )
                ->addStylesheet(
                        '/js/jquery.lightbox/jquery.lightbox-0.5.css'
                );
        $this->view->jQuery()
                ->addOnLoad(
                        '$(document).ready(function() {
                            $("#device-foto-galery a").lightBox(
                            {fixedNavigation:true}
                            );
                        });'
                );

        //$this->_equipo->updateView( $id );
        $equipos = $this->_equipo->pagListEquipByCategory(
                $id , Mtt_Models_Bussines_PublicacionEquipo::Activada
                );
        $equipos->setCurrentPageNumber(
                $this->_getParam( 'page' , 1 )
        );
        $this->view->assign(
                'productos' , $equipos
        );
        $this->view->assign(
                'slug' , $slug
        );
        $url = new Zend_Session_Namespace( 'MTT' );
        $url->urlBack = '/categoryall/' . $slug;


        $translateCategoria = $objTraducciones->getTranslate(
                Mtt_Models_Bussines_Categoria::ENTIDAD_ID , $data->id ,
                $this->mtt->config->idlang , $data->nombre
                );


        $this->view->assign( 'translateCategoria' , $translateCategoria );


        }


    public function cotizarAction()
        {
        $this->view->jQuery()
                ->addJavascriptFile(
                        '/js/jwysiwyg/jquery.wysiwyg.js'
                )
                ->addJavascriptFile(
                        '/js/cotizar.js'
                )
                ->addStylesheet(
                        '/js/jwysiwyg/jquery.wysiwyg.css'
                )
                ->addOnLoad(
                        ' $(document).ready(function() {
                             $("#mensaje").wysiwyg();
                          });'
                )
        ;
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend(
                $this->_translate->translate( 'cotizar' )
        );

        $id = intval( $this->_request->getParam( 'id' ) );

        $equipo = $this->_equipo->getProductById( $id );

        $form = (Zend_Auth::getInstance()->hasIdentity()) ?
                new Mtt_Form_CotizarUsuario() : new Mtt_Form_Cotizar();

        if ( Zend_Auth::getInstance()->hasIdentity() )
            {
            $form->email->setValue( $this->authData['usuario']->email );
            $form->nombre->setValue( $this->authData['usuario']->nombre . ' ' .
                    $this->authData['usuario']->apellido );
            }
        if ( !is_null( $equipo ) )
            {

            if ( $this->_request->isPost()
                    &&
                    $form->isValid( $this->_request->getPost() ) )
                {

                $paises = new Mtt_Models_Bussines_Paises();
                $cotizacion = array( );


                if ( Zend_Auth::getInstance()->hasIdentity() )
                    {

                    $cotizacion['nombre'] = $this->authData['usuario']->nombre . ' ' .
                            $this->authData['usuario']->apellido;
                    $cotizacion['organizacion'] = $this->authData['usuario']->institucion;
                    $cotizacion['direccion'] = $this->authData['usuario']->direccion;
                    $cotizacion['codpostal'] = $this->authData['usuario']->codpostal;
                    $cotizacion['ciudad'] = $this->authData['usuario']->ciudad;
                    $cotizacion['email'] = $this->authData['usuario']->email;
                    $cotizacion['asunto'] = $form->getValue( 'asunto' );
                    $cotizacion['mensaje'] = $form->getValue( 'mensaje' );
                    $cotizacion['toemail'] = $form->getValue( 'toemail' );
                    }
                else
                    {
                    $cotizacion = $form->getValues();
                    }

                $idPais = (Zend_Auth::getInstance()->hasIdentity()) ?
                        $this->authData['usuario']->paises_id :
                        $cotizacion['paises_id'];
                $pais = $paises->getFindId( $idPais );


                $cotizacion['equipo'] = $equipo->nombre;
                $cotizacion['equipo_id'] = $equipo->id;
                $cotizacion['modelo'] = $equipo->modelo;
                $cotizacion['fechafabricacion'] = $equipo->fechafabricacion;
                $cotizacion['fabricante'] = $equipo->fabricante;
                $cotizacion['pais'] = $pais->nombre;

                $cotizacion['link'] = "http://" .
                        $_SERVER['SERVER_NAME'] .
                        "/equipment/" .
                        $equipo->slug;


                if ( $form->getValue( 'precioCheck' ) )
                    {
                    if ( !$form->getValue( 'precio' ) )
                        {
                        unset( $cotizacion['precio'] );
                        }
                    else
                        {
                        $cotizacion['precio'] = $form->getValue( 'precio' );
                        }
                    if ( !$form->getValue( 'moneda_id' ) )
                        {
                        unset( $cotizacion['moneda_id'] );
                        }
                    else
                        {
                        $moneda = new Mtt_Models_Bussines_Moneda();

                        $cotizacion['moneda'] = $moneda->getFindId(
                                        $form->getValue( 'moneda_id' )
                                )->nombre;
                        }
                    }
                $this->_equipo->sendMailToRequest(
                        $cotizacion ,
                        $this->_translate->translate( 'A quote request for device' ) .
                        " N° " . $equipo->id
                );


                $this->_helper->FlashMessenger(
                        $this->_translate->translate( 'se ha enviado el
                        requerimiento para la cotizacion del equipo'
                        )
                );

                $this->_redirect( $cotizacion['link'] );
                }
            $this->view->assign( 'frmCotizar' , $form );
            }
        else
            {

            $this->_helper->FlashMessenger(
                    $this->_translate->translate( 'no existe' )
            );
            $this->_redirect( $this->URL );
            }


        }


    public function pdfAction()
        {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $idEquipo = $this->_request->getParam( 'id' );
        $objEquipo = $this->_equipo->getProduct( $idEquipo );
        $pdfData = $this->pdfgenerator( $idEquipo );

        $nombrePdf = Mtt_Tools_Text::getJoinText(
                        array(
                    $objEquipo->fabricante ,
                    $objEquipo->modelo ,
                    $objEquipo->nombre
                        ) , "-"
                );

        header( "Content-type: application/x-pdf" );
        header( "Content-Disposition: inline; filename=" . $nombrePdf . ".pdf" );

        $pdfData->Output( $nombrePdf . '.pdf' , 'D' );


        }


    public function imprimirPDF( $idEquipo )
        {

        $_equipo = new Mtt_Models_Bussines_Equipo();
        $dataEquipo = $_equipo->getProduct( $idEquipo );

        $pdf1 = Zend_Pdf::load(
                        APPLICATION_PATH . '/../templates/template.pdf'
                );

        $encode = 'UTF-8';

        $page = $pdf1->newPage( Zend_Pdf_Page::SIZE_A4 ); // 595 x842
        $font = Zend_Pdf_Font::fontWithName( Zend_Pdf_Font::FONT_HELVETICA );

        $page = $pdf1->pages[0];

        /* ficha de equipo */
        $page->setFont( $font , 14 );
        $page->setFillColor( new Zend_Pdf_Color_Html( '#B91E1D' ) );
        $page->drawText(
                $this->_translate->translate( 'ficha del equipo' ) , 23 , 696 ,
                                              $encode
        );
        /* end ficha de equipo */

        /* cuerpo del equipo */

        $pdfX = 672.5;
        $aumento = 15.1;
        $pdfY = 175;
        $pdfYCabecera = 33;
        $page->setFont( $font , 11 );
        $page->setFillColor( new Zend_Pdf_Color_Html( '#355F91' ) );
        /* cabecera */

        $page->drawText( $this->_translate->translate( 'manufactur' ) ,
                                                       $pdfYCabecera , $pdfX ,
                                                       $encode
        );
        /* valor del manufacturer */
        $page->drawText( $dataEquipo->fabricante , $pdfY , $pdfX , $encode );
        /**/

        $pdfX -= $aumento;
        /* cabecera */
        $page->drawText( $this->_translate->translate( 'nombre del producto' ) ,
                                                       $pdfYCabecera , $pdfX ,
                                                       $encode );
        /* valor del manufacturer */
        $page->drawText( $dataEquipo->nombre , $pdfY , $pdfX , $encode );
        /**/
        $pdfX -= $aumento - 0.5;

        /* cabecera */
        $page->drawText( $this->_translate->translate( 'modelo' ) ,
                                                       $pdfYCabecera , $pdfX ,
                                                       $encode );
        /* valor del manufacturer */
        $page->drawText( $dataEquipo->modelo , $pdfY , $pdfX , $encode );
        /**/
        $pdfX -= $aumento;
        /* cabecera */
        $page->drawText( $this->_translate->translate( 'origen' ) ,
                                                       $pdfYCabecera , $pdfX ,
                                                       $encode );
        /* valor del manufacturer */
        $page->drawText( $dataEquipo->pais , $pdfY , $pdfX , $encode );
        /**/
        $pdfX -= $aumento;
        /* cabecera */
        $page->drawText( $this->_translate->translate( 'articulo id' ) ,
                                                       $pdfYCabecera , $pdfX ,
                                                       $encode );
        /* valor del manufacturer */
        $page->drawText( $dataEquipo->id , $pdfY , $pdfX , $encode );
        /**/
        $pdfX -= $aumento - 1;
        /* cabecera */
        $page->drawText( $this->_translate->translate( 'fecha de creacion' ) ,
                                                       $pdfYCabecera , $pdfX ,
                                                       $encode );
        /* valor del manufacturer */
        $page->drawText( $dataEquipo->fechafabricacion , $pdfY , $pdfX , $encode );
        /**/
        $pdfX -= $aumento;
        /* cabecera */
        $page->drawText( $this->_translate->translate( 'peso estimado' ) ,
                                                       $pdfYCabecera , $pdfX ,
                                                       $encode );
        /* valor del manufacturer */
        $page->drawText( $dataEquipo->pesoEstimado , $pdfY , $pdfX , $encode );
        /**/
        $pdfX -= $aumento;
        /* cabecera */
        $page->drawText( $this->_translate->translate( 'lenght' ) ,
                                                       $pdfYCabecera , $pdfX ,
                                                       $encode );
        /* valor del manufacturer */
        $page->drawText( $dataEquipo->sizeCaja , $pdfY , $pdfX , $encode );
        /**/
        $pdfX -= $aumento;
        /* cabecera */
        $page->drawText( $this->_translate->translate( 'width' ) ,
                                                       $pdfYCabecera , $pdfX ,
                                                       $encode );
        /* valor del manufacturer */
        $page->drawText( $dataEquipo->ancho , $pdfY , $pdfX , $encode );
        /**/
        $pdfX -= $aumento - 0.5;
        /* cabecera */
        $page->drawText( $this->_translate->translate( 'height' ) ,
                                                       $pdfYCabecera , $pdfX ,
                                                       $encode );
        /* valor del manufacturer */
        $page->drawText( $dataEquipo->alto , $pdfY , $pdfX , $encode );
        /**/
        $pdfX -= $aumento;
        /* cabecera */
        $page->drawText( $this->_translate->translate( 'box size' ) ,
                                                       $pdfYCabecera , $pdfX ,
                                                       $encode );
        /* valor del manufacturer */
        $page->drawText( $dataEquipo->sizeCaja , $pdfY , $pdfX , $encode );
        /**/
        $pdfX -= $aumento;
        /* cabecera */
        $page->drawText(
                $this->_translate->translate(
                        'condicion de calidad'
                ) , $pdfYCabecera , $pdfX , $encode
        );
        /* valor del manufacturer */
        $page->drawText(
                $dataEquipo->calidad , $pdfY , $pdfX , $encode
        );
        /**/
        $pdfX -= $aumento - 1;
        /* cabecera */
        /* /$page->drawText(
          $this->_translate->translate( 'offerer name' ) , $pdfYCabecera , $pdfX , $encode );
          /* valor del manufacturer */
        /* $page->drawText( '' , $pdfY , $pdfX );
          /* */
        $pdfX -= $aumento + 0.5;
        /* cabecera */
        /* $page->drawText( $this->_translate->translate( 'offerer city' ) , $pdfYCabecera , $pdfX , $encode );
          /* valor del manufacturer */
        /* $page->drawText( '' , $pdfY , $pdfX );
          /* */
        $pdfX -= $aumento;
        /* cabecera */
        $page->drawText( $this->_translate->translate( 'amount of equipment' ) ,
                                                       $pdfYCabecera , 463 ,
                                                       $encode );
        /* valor del manufacturer */
        $page->drawText( $dataEquipo->cantidad , $pdfY , 463 , $encode );
        /**/
        $pdfX -= $aumento;
        /* cabecera */
        $page->drawText( $this->_translate->translate( 'descripcion' ) ,
                                                       $pdfYCabecera , 433 ,
                                                       $encode );

        //$page->drawText( $dataEquipo->descripcion , $pdfY , 421 , $encode );
        /**/
        $lineDescripcion = 419;
        $heightPerLine = 10;

        $text = $this->_translate->translate( $dataEquipo->descripcion );
        //$lines = array();
        $ncaracteres = strlen( $text );

        for ( $i = 0; $i < ($ncaracteres / 97); $i++ )
            {
            $txt = substr( $text , $i * 97 , 97 );
            $page->drawText(
                    $txt
                    , $pdfYCabecera
                    , $lineDescripcion
                    , $encode
            );
            $lineDescripcion -= $heightPerLine;
            }
        $pdfX -= $aumento;
        /* cabecera */
        if ( Zend_Auth::getInstance()->hasIdentity() )
            {

            $page->drawText(
                    $this->_translate->translate(
                            'selling price'
                    ) , $pdfYCabecera , 283.5
            );
            /* valor del manufacturer */
            $page->drawText( $dataEquipo->precioventa , $pdfY , 283.5 );
            /**/
            }

        /* fin del equipo */
        /* colocar la parte de imagenes */
        /* final de la parte */

        $imagenes = $_equipo->getImagenes( $idEquipo );
        $imagenX = 25;
        $imagenY = 190;
        $espacioX = 15;
        $espacioY = 15;
        $imagenAlto = 80;
        $imagenAncho = 80;
        $n = 0;
        foreach ( $imagenes as $imagen ):

            if ( !is_null( $imagen->imagenurl ) ):


                $image = Zend_Pdf_Image::imageWithPath(
                                "media/catalog/product/avatar/" . $imagen->imagenurl
                        );
                $page->drawImage(
                        $image , $imagenX , $imagenY
                        , $imagenX + $imagenAncho
                        , $imagenY + $imagenAlto
                );
                $n++;
                $imagenX += $imagenAncho + $espacioX;
                if ( $n == 6 )
                    {
                    $imagenY = 100;
                    $imagenX = 25;
                    }
            endif;
        endforeach;



        $pdfData = $pdf1->render();
        return $pdfData;


        }


    public function sendtofriendAction()
        {
        $this->view->headTitle()->setSeparator( ' - ' );
        $this->view->headTitle()->prepend( 'enviar a un amigo' );

        $form = new Mtt_Form_EnviarAmigo();
        $parametros = $this->_request->getPost();

        $parametros['url'] = $_SERVER['HTTP_REFERER'];
        $parametros['test'] = $this->getRequest();

        $this->_equipo->sendMail(
                $parametros ,
                $this->_translate->translate( 'enviar a un amigo' )
        );

        $this->view->assign( 'parametros' , $parametros );

        $this->_redirect( $_SERVER['HTTP_REFERER'] );


        }


    public function pdfgenerator( $idEquipo )
        {
        $_equipo = new Mtt_Models_Bussines_Equipo();
        $dataEquipo = $_equipo->getProductMoneyVenta( $idEquipo );

        require_once(APPLICATION_PATH . "/../library/FPDI/pdf.php");
        // initiate PDF

        $pdf = new PDF();
        $pdf->SetMargins( 5 , 35 , 5 , true );
        $pdf->SetAutoPageBreak( true , 35 );


        $colorLetra = array( 59 , 95 , 131 );
        $colorLetraTitulo = array( 169 , 37 , 25 );
        $colorCelda[0] = array( 211 , 223 , 235 ); //gris
        $colorCelda[1] = array( 255 , 255 , 255 ); //blanco
        $xTitulo = 7.5;
        $yTitulo = 59.42;
        $width = 197;
        $height = 5;
        $widthLabel = 50;
        $widthText = 146;

        //$pdf->SetMargins(PDF_MARGIN_LEFT, 40, PDF_MARGIN_RIGHT);
        //$pdf->SetAutoPageBreak(true, 40);
        $pdf->setFontSubsetting( false );

        // add a page
        $pdf->AddPage();

        // get esternal file content
        //$utf8text = file_get_contents("cache/utf8test.txt", true);


        $idLang = $this->mtt->config->idlang;
        $font = ($this->mtt->config->idlang == 2) ? 'freeserif' : 'helvetica';
        $pdf->SetFont( $font , "" , 14 );
        // now write some text above the imported page
        $pdf->Write( 5 , $utf8text );
        $style = array(
            'width' => 0.5 ,
            'cap' => 'butt' ,
            'join' => 'miter' ,
            'dash' => 0 ,
            'color' => $colorCelda[0]
                );

        //encabezado
        $pdf->SetTextColor(
                $colorLetraTitulo[0]
                , $colorLetraTitulo[1]
                , $colorLetraTitulo[2]
        );
        $pdf->Text( 7.2 , 52 ,
                    $this->_translate->translate( 'ficha del equipo' ) );
        $pdf->Line( 7.2 , 59 , 203.5 , 59 , $style );

        //ficha del producto
        $pdf->SetFont( $font , "" , 12 );
        $pdf->SetTextColor( $colorLetra[0] , $colorLetra[1] , $colorLetra[2] );
        $pdf->SetXY( $xTitulo , $yTitulo );
        $pdf->SetFillColorCelda(
                $colorCelda[1][0] , $colorCelda[1][1] , $colorCelda[1][2]
        );

        $pdf->Cell( $widthLabel , $height
                , $this->_translate->translate( 'manufactur' )
                , 0 , 0 , '' , 1 , '' , 1
        );

        $pdf->Cell(
                $widthText , $height
                , $dataEquipo->fabricante , 0 , 1 , '' , 1 , '' , 1 );
        $yTitulo += $height;

        $pdf->SetXY( $xTitulo , $yTitulo );
        $pdf->SetFillColorCelda(
                $colorCelda[0][0] , $colorCelda[0][1] , $colorCelda[0][2]
        );

        $pdf->Cell(
                $widthLabel , $height
                , $this->_translate->translate( 'nombre del producto' )
                , 0 , 0 , '' , 1 , '' , 1 );
        $pdf->Cell(
                $widthText , $height
                , $dataEquipo->nombre
                , 0 , 1 , '' , 1 , '' , 1 );
        $yTitulo += $height;

        $pdf->SetXY( $xTitulo , $yTitulo );
        $pdf->SetFillColorCelda(
                $colorCelda[1][0] , $colorCelda[1][1] , $colorCelda[1][2]
        );

        $pdf->Cell(
                $widthLabel , $height
                , $this->_translate->translate( 'modelo' )
                , 0 , 0 , '' , 1 , '' , 1 );
        $pdf->Cell(
                $widthText , $height
                , $dataEquipo->modelo
                , 0 , 1 , '' , 1 , '' , 1 );
        $yTitulo += $height;

        $pdf->SetXY( $xTitulo , $yTitulo );
        $pdf->SetFillColorCelda(
                $colorCelda[0][0] , $colorCelda[0][1] , $colorCelda[0][2]
        );

        $pdf->Cell(
                $widthLabel , $height
                , $this->_translate->translate( 'origen' )
                , 0 , 0 , '' , 1 , '' , 1 );
        $pdf->Cell(
                $widthText , $height
                , $dataEquipo->pais
                , 0 , 1 , '' , 1 , '' , 1 );
        $yTitulo += $height;

        $pdf->SetXY( $xTitulo , $yTitulo );
        $pdf->SetFillColorCelda(
                $colorCelda[1][0] , $colorCelda[1][1] , $colorCelda[1][2]
        );

        $pdf->Cell(
                $widthLabel , $height
                , $this->_translate->translate( 'articulo id' )
                , 0 , 0 , '' , 1 , '' , 1 );
        $pdf->Cell(
                $widthText , $height
                , $dataEquipo->id
                , 0 , 1 , '' , 1 , '' , 1 );
        $yTitulo += $height;

        $pdf->SetXY( $xTitulo , $yTitulo );
        $pdf->SetFillColorCelda(
                $colorCelda[0][0] , $colorCelda[0][1] , $colorCelda[0][2]
        );

        if ( !is_null( $dataEquipo->fechafabricacion )
                && $dataEquipo->fechafabricacion > 0 )
            {
            $pdf->Cell(
                    $widthLabel , $height
                    , $this->_translate->translate( 'fecha de creacion' )
                    , 0 , 0 , '' , 1 , '' , 1 );

            $pdf->Cell(
                    $widthText , $height
                    , $dataEquipo->fechafabricacion
                    , 0 , 1 , '' , 1 , '' , 1 );
            $yTitulo += $height;

            $pdf->SetXY( $xTitulo , $yTitulo );
            }

        $flag = 1;

        if ( !is_null( $dataEquipo->pesoEstimado )
                && $dataEquipo->pesoEstimado != '0.00'
                && $dataEquipo->pesoEstimado != '' )
            {

            $pdf->SetFillColorCelda(
                    $colorCelda[$flag][0] , $colorCelda[$flag][1]
                    , $colorCelda[$flag][2]
            );
            $pdf->Cell(
                    $widthLabel , $height
                    , $this->_translate->translate( 'peso estimado' )
                    , 0 , 0 , '' , 1 , '' , 1 );
            $pdf->Cell(
                    $widthText , $height
                    , $dataEquipo->pesoEstimado
                    , 0 , 1 , '' , 1 , '' , 1 );
            $yTitulo += $height;
            $pdf->SetXY( $xTitulo , $yTitulo );
            $flag = ($flag == 0) ? 1 : 0;
            }


        if ( !is_null( $dataEquipo->size )
                && $dataEquipo->size != '0.00'
                && $dataEquipo->size != '' )
            {
            $pdf->SetFillColorCelda(
                    $colorCelda[$flag][0] , $colorCelda[$flag][1]
                    , $colorCelda[$flag][2]
            );
            $pdf->Cell(
                    $widthLabel , $height
                    , $this->_translate->translate( 'lenght' )
                    , 0 , 0 , '' , 1 , '' , 1 );
            $pdf->Cell(
                    $widthText , $height
                    , $dataEquipo->size
                    , 0 , 1 , '' , 1 , '' , 1 );
            $yTitulo += $height;

            $pdf->SetXY( $xTitulo , $yTitulo );

            $flag = ($flag == 0) ? 1 : 0;
            }

        if ( !is_null( $dataEquipo->ancho )
                && $dataEquipo->ancho != '0.00'
                && $dataEquipo->ancho != '' )
            {
            $pdf->SetFillColorCelda(
                    $colorCelda[$flag][0]
                    , $colorCelda[$flag][1] , $colorCelda[$flag][2]
            );
            $pdf->Cell(
                    $widthLabel , $height
                    , $this->_translate->translate( 'width' )
                    , 0 , 0 , '' , 1 , '' , 1 );
            $pdf->Cell(
                    $widthText , $height
                    , $dataEquipo->ancho
                    , 0 , 1 , '' , 1 , '' , 1 );
            $yTitulo += $height;

            $pdf->SetXY( $xTitulo , $yTitulo );
            $flag = ($flag == 0) ? 1 : 0;
            }

        if ( !is_null( $dataEquipo->alto )
                && $dataEquipo->alto != '0.00'
                && $dataEquipo->alto != '' )
            {
            $pdf->SetFillColorCelda(
                    $colorCelda[$flag][0] , $colorCelda[$flag][1]
                    , $colorCelda[$flag][2]
            );

            $pdf->Cell(
                    $widthLabel , $height
                    , $this->_translate->translate( 'height' )
                    , 0 , 0 , '' , 1 , '' , 1 );
            $pdf->Cell(
                    $widthText , $height
                    , $dataEquipo->alto
                    , 0 , 1 , '' , 1 , '' , 1 );
            $yTitulo += $height;

            $pdf->SetXY( $xTitulo , $yTitulo );
            $flag = ($flag == 0) ? 1 : 0;
            }
        if ( !is_null( $dataEquipo->sizeCaja )
                && $dataEquipo->sizeCaja != '0.00'
                && $dataEquipo->sizeCaja != '' )
            {
            $pdf->SetFillColorCelda( $colorCelda[$flag][0]
                    , $colorCelda[$flag][1] , $colorCelda[$flag][2] );

            $pdf->Cell(
                    $widthLabel , $height
                    , $this->_translate->translate( 'box size' )
                    , 0 , 0 , '' , 1 , '' , 1 );
            $pdf->Cell(
                    $widthText , $height
                    , $dataEquipo->sizeCaja
                    , 0 , 1 , '' , 1 , '' , 1 );
            $yTitulo += $height;

            $pdf->SetXY( $xTitulo , $yTitulo );
            $flag = ($flag == 0) ? 1 : 0;
            }

        $pdf->SetFillColorCelda( $colorCelda[$flag][0]
                , $colorCelda[$flag][1] , $colorCelda[$flag][2] );

        $pdf->Cell(
                $widthLabel , $height
                , $this->_translate->translate( 'condicion de calidad' )
                , 0 , 0 , '' , 1 , '' , 1 );
        $pdf->Cell(
                $widthText , $height
                , $dataEquipo->calidad
                , 0 , 1 , '' , 1 , '' , 1 );
        $yTitulo += $height;

        $pdf->SetXY( $xTitulo , $yTitulo );
        $flag = ($flag == 0) ? 1 : 0;
        $pdf->SetFillColorCelda( $colorCelda[$flag][0]
                , $colorCelda[$flag][1] , $colorCelda[$flag][2] );

        $pdf->Cell(
                $widthLabel , $height
                , $this->_translate->translate( 'amount of equipment' )
                , 0 , 0 , '' , 1 , '' , 1 );
        $pdf->Cell(
                $widthText , $height
                , $dataEquipo->cantidad
                , 0 , 1 , '' , 1 , '' , 1 );
        $yTitulo += $height;

        $pdf->SetXY( $xTitulo , $yTitulo );
        $flag = ($flag == 0) ? 1 : 0;
        $pdf->SetFillColorCelda(
                $colorCelda[$flag][0]
                , $colorCelda[$flag][1] , $colorCelda[$flag][2]
        );

        $pdf->Cell(
                $width , $height
                , $this->_translate->translate( 'descripcion' )
                , 0 , 1 , '' , 1 , '' , 1 );
        $yTitulo += $height;
        $pdf->SetXY( $xTitulo , $yTitulo );
        $flag = ($flag == 0) ? 1 : 0;
        $pdf->SetFillColorCelda( $colorCelda[$flag][0]
                , $colorCelda[$flag][1] , $colorCelda[$flag][2] );
        $pdf->writeHTMLCell(
                $width , 5 , $xTitulo , $yTitulo
                , $dataEquipo->descripcion
                , 0 , 1 , 1 , 0 , 'L' , 1 );
        $yTitulo += $height;
        $flag = ($flag == 0) ? 1 : 0;

        $pdf->SetXY( $xTitulo , $pdf->GetY() );

        if ( Zend_Auth::getInstance()->hasIdentity() )
            {

            $pdf->SetFillColorCelda(
                    $colorCelda[$flag][0] , $colorCelda[$flag][1]
                    , $colorCelda[$flag][2]
            );


            if ( $dataEquipo->monedaid > 0 && $dataEquipo->precioventa > 0 ):
                if ( !is_null( $dataEquipo->precioventa ) ):
                    $pdf->Cell(
                            $widthLabel , $height
                            ,
                            $this->_translate->translate( 'selling price' )
                            , 0 , 0 , '' , 1 , '' , 1 );

                    $valor = 0;
                    $descuento = 0;

                    $currency = new Zend_Currency( array( 'precision' => 0 ) ,
                                    isset( $this->mtt->currency->code ) ?
                                            $this->mtt->currency->code : $dataEquipo->ventacodepais
                            );

                    if ( isset( $this->mtt->currency->codeGoogle ) )
                        {
                        if ( $this->mtt->currency->codeGoogle == 'CLP' )
                            {
                            $valor = Mtt_Service_Finance::Currency(
                                            $dataEquipo->ventacodegoogle
                                            ,
                                            $this->mtt->currency->codeGoogle
                                            , $dataEquipo->precioventa
                                    );
                            $descuento = Mtt_Service_Finance::Currency(
                                            $dataEquipo->ventacodegoogle
                                            ,
                                            $this->mtt->currency->codeGoogle
                                            , $dataEquipo->descuento
                                    );
                            }
                        else
                            {
                            $valor = Mtt_Service_Finance::Currency(
                                            $dataEquipo->ventacodegoogle
                                            ,
                                            $this->mtt->currency->codeGoogle
                                            , $dataEquipo->precioventa
                                    );
                            $descuento = Mtt_Service_Finance::Currency(
                                            $dataEquipo->ventacodegoogle
                                            ,
                                            $this->mtt->currency->codeGoogle
                                            , $dataEquipo->descuento
                                    );
                            }
                        }
                    else
                        {
                        $valor = $dataEquipo->precioventa;
                        $descuento = $dataEquipo->descuento;
                        }
                    $priceHTML = '';
                    if ( $this->mtt->currency->codeGoogle == 'CLP' )
                        {
                        if ( $dataEquipo->descuento > 0 )
                            {
                            $priceHTML .= '<span style="font-style: italic;text-decoration: line-through;"> ' . $valor . '</span> ';
                            $priceHTML.= ' <span> ' . $descuento . '</span>';
                            }
                        else
                            {
                            $priceHTML .= '<span>' . $valor . '</span>';
                            }
                        }
                    else
                        {
                        if ( $dataEquipo->descuento > 0 )
                            {
                            $priceHTML .= '<span style="font-style: italic;text-decoration: line-through;">' . $currency->toCurrency(
                                            $valor
                                    ) . '</span> ';
                            $priceHTML .= ' <span>' . $currency->toCurrency(
                                            $descuento
                                    ) . '</span>';
                            }
                        else
                            {
                            $priceHTML .= '<span>' . $currency->toCurrency(
                                            $valor
                                    ) . '</span>';
                            }
                        }


                endif;
            endif;

            $pdf->writeHTMLCell(
                    $widthText , $height , $xTitulo + $widthLabel
                    , $pdf->GetY()
                    , $priceHTML
                    , 0 , 0 , 1 , 0 , 'L' , 1 );

//			$pdf->Cell(
//					$widthText , $height
//					, $priceHTML
//					, 0 , 1 , '' , 1 , '' , 1 ) ;
            $yTitulo += $height;
            $pdf->SetXY( $xTitulo , $pdf->GetY() + 6 );
            }

        $pdf->Line( 7.2 , $pdf->GetY() + 2 , 203.5 , $pdf->GetY() + 2 , $style );

        $imagenes = $_equipo->getImagenes( $idEquipo );
        $imagenX = 7.2;
        $imagenY = 72;
        $espacioX = 2.5;
        $espacioY = 2.5;
        $imagenAlto = 30;
        $imagenAncho = 30;
        $n = 0;



        if ( $pdf->GetY() > 200 && count( $imagenes > 6 ) )
            {
            //imagenes
            $pdf->AddPage();
            //$pdf->SetMargins(5, 35, 5,true );
            $imagenX = 7.2;
            $imagenY = 50;
            }
        else
            {
            $imagenY = $pdf->GetY() + 8;
            }



        foreach ( $imagenes as $imagen ):

            if ( !is_null( $imagen->imagenurl ) ):

                $image = $pdf->Image(
                        "media/catalog/product/avatar/" . $imagen->imagenurl
                        , $imagenX , $imagenY , $imagenAncho , $imagenAlto
                        );

                $n++;
                $imagenX += $imagenAncho + $espacioX;
                if ( $n == 6 )
                    {
                    $imagenY = $imagenY + $imagenAlto + $espacioY;
                    $imagenX = 7.2;
                    }
            endif;
        endforeach;


        return $pdf;


        }


    }
