<?php


class Bootstrap
        extends Zend_Application_Bootstrap_Bootstrap
{


    protected function _initSession()
    {

//        Zend_Session::setOptions(
//                array(
//                    'cookie_secure' => true ,
//                    'name' => 'mySSL' ,
//                    'save_path' => APPLICATION_PUBLIC . '/session/mySSL' ,
//                    'cookie_httponly' => true ,
//                    'gc_maxlifetime' => 15 * 60
//                )
//        );
        Zend_Session::start() ;


    }


    protected function _initAutoload()
    {
        $this->options = $this->getOptions() ;
        Zend_Registry::set( 'config.recaptcha' ,
                            $this->options['recaptcha'] ) ;


    }


    protected function _initView()
    {

        $docTypeHelper = new Zend_View_Helper_Doctype() ;
        $docTypeHelper->doctype(
                Zend_View_Helper_Doctype::HTML5
        ) ;


    }


    protected function _initViewHelpers()
    {

        $this->bootstrap( 'layout' ) ;
        $layout = $this->getResource( 'layout' ) ;
        $view = $layout->getView() ;

        //ZendX_JQuery_View_Helper_JQuery::enableNoConflictMode();
        $view->addHelperPath( 'ZendX/JQuery/View/Helper/' ,
                              'ZendX_JQuery_View_Helper' ) ;

        $view->addHelperPath( 'ZendX/JQuery/View/Helper/JQuery' ,
                              'ZendX_JQuery_View_Helper_JQuery' ) ;

        $view->addHelperPath( 'Mtt/View/Helper/' ,
                              'Mtt_View_Helper' ) ;


        $view->headTitle( 'Medtechtrade.com ' )
                ->setSeparator( ' - ' ) ;


        $view->headMeta()->appendHttpEquiv(
                'Content-Type' ,
                'text/html; charset=UTF-8'
        ) ;


    }


    protected function _initViewJQuery()
    {
        $this->bootstrap( 'layout' ) ;
        $layout = $this->getResource( 'layout' ) ;
        $view = $layout->getView() ;
        $view->jQuery()->setVersion( '1.6.4' )
                ->setUiVersion( '1.8.16' )
                ->addStylesheet( '/css/Aristo/Aristo.css' )
                ->addStylesheet( '/css/uniform.aristo/uniform.aristo.css' )
                ->addStylesheet( '/css/jquery.jqGrid.min/ui.jqgrid.css' )
                ->addStylesheet( '/js/fancybox/jquery.fancybox-1.3.4.css' )
                /* Javascript */
                ->addJavascriptFile( '/js/fancybox/jquery.fancybox-1.3.4.js' )
                ->addJavascriptFile( '/js/jquery.jqGrid.min/i18n/grid.locale-en.js' )
                ->addJavascriptFile( '/js/jquery.jqGrid.min/jquery.jqGrid.min.js' )
                ->addJavascriptFile( '/js/jquery.tools/jquery.tools.min.js' )
                ->addJavascriptFile( '/js/jquery.formalize/jquery.formalize.js' )
                ->addJavascriptFile( '/js/jquery.uniform/jquery.uniform.min.js' )
                ->addJavascriptFile( '/theme/default/js/global.js' )
                ->addJavascriptFile( '/js/mtt/tooltip.js' )
                ->enable()
                ->uiEnable() ;


    }


    protected function _initActionHelpers()
    {
        Zend_Controller_Action_HelperBroker::addHelper(
                new Mtt_Controller_Action_Helper_Auth()
        ) ;

        Zend_Controller_Action_HelperBroker::addHelper(
                new Mtt_Controller_Action_Helper_MyFlashMessenger()
        ) ;


    }


    protected function _initZFDebug()
    {
        if ( 'development' == APPLICATION_ENV )
        {
            $options = array(
                'plugins' => array( 'Variables' ,
                    'File' => array( 'base_path' => APPLICATION_PATH ) ,
                    'Memory' ,
                    'Time' ,
                    'Registry' ,
                    'Exception' )
                    ) ;

            if ( $this->hasPluginResource( 'db' ) )
            {
                $this->bootstrap( 'db' ) ;
                $db = $this->getPluginResource( 'db' )->getDbAdapter() ;
                $options['plugins']['Database']['adapter'] = $db ;
            }

            if ( $this->hasPluginResource( 'cache' ) )
            {
                $this->bootstrap( 'cache' ) ;
                $cache = $this - getPluginResource( 'cache' )->getDbAdapter() ;
                $options['plugins']['Cache']['backend'] = $cache->getBackend() ;
            }

            $debug = new ZFDebug_Controller_Plugin_Debug( $options ) ;

            $this->bootstrap( 'frontController' ) ;
            $frontController = $this->getResource( 'frontController' ) ;
            $frontController->registerPlugin( $debug ) ;
        }


    }


}

