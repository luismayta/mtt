<?php


class Delta_0033
        extends Mtt_Migration_Delta
    {

    protected $_author = "Teresa Chunga";
    protected $_desc = "add campo columna active a equipo_has_formapago";


    public function up()
        {
        $sql = "
            ALTER TABLE `medtechtrade`.`equipo_has_formapago`     
            ADD COLUMN `active` INT DEFAULT '1' NOT NULL AFTER `moraxdia`;
            ";

        $this->_db->query( $sql );

        return true;
        }


    }
