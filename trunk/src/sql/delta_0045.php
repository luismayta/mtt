<?php

class Delta_0045 extends Mtt_Migration_Delta {

    protected $_author = self::SLOVACUS;
    protected $_desc = "add idiomas a paises";

    public function up() {
        $sql = "
           UPDATE paises SET idioma = 3 WHERE paises.id = 1;
UPDATE paises SET idioma = 4 WHERE paises.id = 24;
UPDATE paises SET idioma = 4 WHERE paises.id = 99;
UPDATE paises SET idioma = 3 WHERE paises.id = 161;
UPDATE paises SET idioma = 2 WHERE paises.id = 79;
            ";

        $this->_db->query($sql);

        return true;
    }

}
