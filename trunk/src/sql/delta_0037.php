<?php


class Delta_0037
        extends Mtt_Migration_Delta
    {

    protected $_author = "Teresa Chunga E.";
    protected $_desc = "set active a 1 por defecto";


    public function up()
        {
        $sql = "
            ALTER TABLE `medtechtrade`.`pagina`     
                CHANGE `active` `active` INT(11) DEFAULT '1' NULL ;
            ";

        $this->_db->query( $sql );

        return true;
        }


    }
