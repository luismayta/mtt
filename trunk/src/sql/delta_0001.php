<?php


class Delta_0001
        extends Mtt_Migration_Delta
    {

    protected $_author = "Luis Alberto Mayta Mamani";
    protected $_desc = "agregar tabla busqueda";


    public function up()
        {
        $sql =
                "CREATE  TABLE IF NOT EXISTS `medtechtrade`.`busqueda` (

  `id` INT NOT NULL AUTO_INCREMENT ,

  `palabras_busqueda` VARCHAR(255) NULL ,

  `modelo` VARCHAR(255) NULL ,

  `fabricante` VARCHAR(255) NULL ,

  `categoria` INT NULL ,

  `anio_inicio` INT NULL ,

  `anio_fin` INT NULL ,

  `precio_inicio` INT NULL ,

  `precio_fin` INT NULL ,

  `usuario_id` INT NOT NULL ,

  PRIMARY KEY (`id`) ,

  CONSTRAINT `fk_busqueda_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION

)ENGINE = INNODB;";
        $this->_db->query( $sql );
        
        $sql =
            "
               CREATE  TABLE IF NOT EXISTS `categoriapregunta` (
	`id` double ,
	`descripcion` varchar (135),
	`active` double 
); 
insert into `categoriapregunta` (`id`, `descripcion`, `active`) values('1','Otros','1');
                ";
        $this->_db->query($sql);
        
        
        $sql =
            "
               insert into `tipo_reserva` (`id`, `nombre`, `active`) values('1','Reserva','1');
insert into `tipo_reserva` (`id`, `nombre`, `active`) values('2','Favorito','1');
                ";
        $this->_db->query($sql);
        $sql =
            "
               insert into `tipousuario` (`id`, `nombre`, `active`) values('1','Manager','1');
insert into `tipousuario` (`id`, `nombre`, `active`) values('2','Registered','1');
insert into `tipousuario` (`id`, `nombre`, `active`) values('3','User','1');
                ";
        $this->_db->query($sql);
        
        return true;
        }


    }
