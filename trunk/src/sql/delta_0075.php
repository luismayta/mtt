<?php


class Delta_0075
        extends Mtt_Migration_Delta
    {

    protected $_author = "Teresa Chunga";
    protected $_desc = "agregar campo: agregar a carro";


    public function up()
        {
        $sql = "
            ALTER TABLE `equipo`     
                ADD COLUMN `addCart` INT(11) DEFAULT '0' 
                NULL AFTER `codigoPostalOferente`;
            ";
        $this->_db->query( $sql );


        
        return true;
        }


    }
