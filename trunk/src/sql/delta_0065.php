<?php


class Delta_0065
        extends Mtt_Migration_Delta
    {

    protected $_author = self::SLOVACUS;
    protected $_desc = "crear tabla entidades";

    public function up()
        {
        $sql = "
          CREATE TABLE `entidades`( 
          `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'primarykey de la tabla',
          `nombre` VARCHAR(200) NOT NULL ,
          `active` INT(1) ,     PRIMARY KEY (`id`)  )  ;
            ";
        $this->_db->query( $sql );
        $sql = "
           ALTER TABLE `traducciones`    
           ADD COLUMN `campo_id` INT(11) NULL AFTER `entidades_id`,
           ADD COLUMN `active` INT(1) NULL AFTER `texto`, 
           CHANGE `nombretabla` `entidades_id` INT(11) NOT NULL;
            ";
        $this->_db->query( $sql );
        $sql = "
          ALTER TABLE `traducciones` 
          ADD CONSTRAINT `FK_traducciones` FOREIGN KEY (`entidades_id`) 
          REFERENCES `entidades` (`id`) ON DELETE NO ACTION  ON UPDATE NO ACTION ;
            ";
        $this->_db->query( $sql );
        
        $sql = "
          insert into `entidades` (`id`, `nombre`, `active`) values('1','categoria','1');
insert into `entidades` (`id`, `nombre`, `active`) values('2','estadoequipo','1');
insert into `entidades` (`id`, `nombre`, `active`) values('3','estadooperacion','1');
insert into `entidades` (`id`, `nombre`, `active`) values('4','formaenvio','1');
insert into `entidades` (`id`, `nombre`, `active`) values('5','formapago','1');
insert into `entidades` (`id`, `nombre`, `active`) values('6','paises','1');
insert into `entidades` (`id`, `nombre`, `active`) values('7','publicacionequipo','1');
insert into `entidades` (`id`, `nombre`, `active`) values('8','tipo_reserva','1');
insert into `entidades` (`id`, `nombre`, `active`) values('9','tipousuario','1');
            ";
        $this->_db->query( $sql );
        $sql = "
          insert into `page` (`id`, `nombre`, `fecha`, `active`) values('6','team',NULL,'1');
insert into `page` (`id`, `nombre`, `fecha`, `active`) values('7','references',NULL,'1');
insert into `page` (`id`, `nombre`, `fecha`, `active`) values('8','legal notice',NULL,'1');

            ";
        $this->_db->query( $sql );
        $sql = "
          ALTER TABLE `traducciones`  CHANGE `active` `active` INT(1) DEFAULT '1' NULL ;
            ";
        $this->_db->query( $sql );

        return true;
        }


    }
