<?php

class Delta_0063 extends Mtt_Migration_Delta {

protected $_author = "Teresa chunga";
protected $_desc = "setear apellido";


    public function up() {
        $sql = "
           UPDATE 
usuario SET 
apellido = SUBSTRING(nombre, LOCATE(' ',nombre)+1, LENGTH(nombre)),
nombre = SUBSTRING(nombre, 1, LOCATE(' ',nombre))
WHERE LOCATE(' ',nombre)>0
AND id > 10
ORDER BY id ASC;

UPDATE usuario SET apellido = '' WHERE apellido IS null;

            ";
        $this->_db->query($sql);

        return true;
    }

}
