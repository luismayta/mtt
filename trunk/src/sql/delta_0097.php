<?php


class Delta_0097
        extends Mtt_Migration_Delta
    {

    protected $_author = self::SLOVACUS;
    protected $_desc = "cambiando el tipo de fechadefabricacion de la tabla equipos";


    public function up()
        {
        $sql = "
            ALTER TABLE `equipo`     CHANGE `fechafabricacion` `fechafabricacion` YEAR NULL ;
            ";
        $this->_db->query( $sql );
       
        
        return true;
        }


    }



