<?php


class Delta_0087
        extends Mtt_Migration_Delta
    {

    protected $_author = self::SLOVACUS;
    protected $_desc = "change de fechafabricacion";


    public function up()
        {
        $sql = "
            ALTER TABLE `equiporequerido` CHANGE `fechafabricacion` `fechafabricacion` VARCHAR(200) NULL ;
            ";
        $this->_db->query( $sql );
       
        
        return true;
        }


    }
