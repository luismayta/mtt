<?php


class Delta_0049
        extends Mtt_Migration_Delta
    {

    protected $_author = self::SLOVACUS;
    protected $_desc = "add data moneda";


    public function up()
        {
        $sql = "
            insert into `moneda` (`id`, `nombre`, `simbolo`, `prefijo`, `active`, `cambio`, `fechacambio`, `codepais`) values('1','Swiss Francs','Fr',NULL,'1',NULL,NULL,'CH');
insert into `moneda` (`id`, `nombre`, `simbolo`, `prefijo`, `active`, `cambio`, `fechacambio`, `codepais`) values('2','US Dollars','$',NULL,'1',NULL,NULL,'US');
insert into `moneda` (`id`, `nombre`, `simbolo`, `prefijo`, `active`, `cambio`, `fechacambio`, `codepais`) values('3','Peruvian Nuevo Sol','S/.',NULL,'1',NULL,NULL,'PE');
           insert into `moneda` (`id`, `nombre`, `simbolo`, `prefijo`, `active`, `cambio`, `fechacambio`, `codepais`) values('4','Chilean Peso','$',NULL,'1',NULL,NULL,'CL');
insert into `moneda` (`id`, `nombre`, `simbolo`, `prefijo`, `active`, `cambio`, `fechacambio`, `codepais`) values('5','Euros','â‚¬',NULL,'1',NULL,NULL,'DE');
insert into `moneda` (`id`, `nombre`, `simbolo`, `prefijo`, `active`, `cambio`, `fechacambio`, `codepais`) values('6','Polish new Zloty','zÅ‚',NULL,'1',NULL,NULL,'PL');

            ";
        $this->_db->query( $sql );
       
        return true;
        }


    }
