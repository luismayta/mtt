<?php


class Delta_0038
        extends Mtt_Migration_Delta
    {

    protected $_author = "Teresa Chunga E.";
    protected $_desc = "creando tabla operacion_envio";


    public function up()
        {
        $sql = "
            CREATE  TABLE IF NOT EXISTS `medtechtrade`.`operacion_envios` (
                `id` INT NOT NULL ,
                `operacion_id` INT(11) NOT NULL ,
                `descripcion` VARCHAR(120) NOT NULL ,
                `fecha` DATETIME NOT NULL ,
                `active` INT NOT NULL DEFAULT 1  ,  
            PRIMARY KEY (`id`) ,
            KEY `fk_operacion1` (`operacion_id`),
                CONSTRAINT `fk_operacion1` 
                FOREIGN KEY (`operacion_id`) 
                REFERENCES `operacion` (`id`) 
                ON DELETE NO ACTION ON UPDATE NO ACTION
            )
            ENGINE = INNODB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
            ";

        $this->_db->query( $sql );

        return true;
        }


    }
