<?php


class Delta_0012
        extends Mtt_Migration_Delta
    {

    protected $_author = "Teresa Chunga Estrada";
    protected $_desc = "agregando campos a tabla usuario";


    public function up()
        {
       
        $sql = " ALTER TABLE `usuario`     
            CHANGE `tratamiento` `tratamiento` INT NULL  
            COMMENT '0 Sr, 1 Sra/Srta'";
        $this->_db->query( $sql );
       
        return true;
        }


    }
