<?php


class Delta_0100
        extends Mtt_Migration_Delta
{

    protected $_author = self::SLOVACUS ;
    protected $_desc = "add campos para manufacturers" ;


    public function up()
    {
        $sql = "
           ALTER TABLE `fabricantes`     
           ADD COLUMN `imagen` VARCHAR(250) NULL AFTER `active`,
           ADD COLUMN `slug` VARCHAR(250) NULL AFTER `imagen`;
            " ;
        $this->_db->query( $sql ) ;


        return true ;


    }


}

