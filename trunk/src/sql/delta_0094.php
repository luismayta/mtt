<?php


class Delta_0094
        extends Mtt_Migration_Delta
    {

    protected $_author = self::SLOVACUS;
    protected $_desc = "agregando campo acreditado a usuario";


    public function up()
        {
        $sql = "
            ALTER TABLE 
            `usuario` ADD COLUMN `acreditado` INT(1) NULL AFTER `direccion2`;

            ";
        $this->_db->query( $sql );
       
        
        return true;
        }


    }



