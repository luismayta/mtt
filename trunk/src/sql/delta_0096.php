<?php


class Delta_0096
        extends Mtt_Migration_Delta
    {

    protected $_author = self::SLOVACUS;
    protected $_desc = "cambiando el tipo de descuento";


    public function up()
        {
        $sql = "
            ALTER TABLE `equipo`     
            CHANGE `descuento` `descuento` NUMERIC(8,2) NULL ;
            ";
        $this->_db->query( $sql );
       
        
        return true;
        }


    }



