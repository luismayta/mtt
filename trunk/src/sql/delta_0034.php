<?php


class Delta_0034
        extends Mtt_Migration_Delta
    {

    protected $_author = "Teresa Chunga";
    protected $_desc = "add campo columna descripcion a equipo";


    public function up()
        {
        $sql = "
           ALTER TABLE `medtechtrade`.`equipo`     
           ADD COLUMN `descripcion` TEXT 
           CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `especificaciones`;
            ";

        $this->_db->query( $sql );

        return true;
        }


    }
