<?php

class Delta_0044 extends Mtt_Migration_Delta {

    protected $_author = self::SLOVACUS;
    protected $_desc = "add un campo de idioma a la tabla paises";

    public function up() {
        $sql = "
           ALTER TABLE `paises`     
           ADD COLUMN `idioma` INT(11) NULL AFTER `integrate`;
            ";

        $this->_db->query($sql);

        return true;
    }

}
