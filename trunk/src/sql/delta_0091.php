<?php


class Delta_0091
        extends Mtt_Migration_Delta
    {

    protected $_author = self::SLOVACUS;
    protected $_desc = "add nuevos equipos";


    public function up()
        {
        $sql = "
insert into `equipo` (`id`, `nombre`, `precioventa`, `preciocompra`, `categoria_id`, `estadoequipo_id`, `usuario_id`, `fabricantes_id`, `tag`, `especificaciones`, `descripcion`, `monedaventa_id`, `moneda_id`, `paises_id`, `calidad`, `cantidad`, `modelo`, `fechafabricacion`, `documento`, `sourceDocumento`, `pesoEstimado`, `size`, `ancho`, `alto`, `sizeCaja`, `active`, `publicacionEquipo_id`, `views`, `topofers`, `publishdate`, `slug`, `nombreOferente`, `articulo_id`, `ciudadOferente`, `codigoPostalOferente`, `addCart`) values('392','Ultrasound Veterinary','n.a.','6220','161','1','62','55','','','Included in price:\r\nMain unit\r\nElectronic convex array probe (2.0/2.5/3.5/4.5/5.0/6.0MHz)\r\nElectronic linear array probe (6.5/7.5/9.0/10.0MHz)\r\n\r\nFeatures:\r\n2 Probe connectors\r\n265 gray scales 10.4\"LCD display\r\n365 frames cine loop memory\r\nAC and battery power supply and rechargeable\r\nWeight: 4.5kg for easier carrying\r\nMulti-frequency probes \r\nStandard portable bag\r\nSoftware upgradeable\r\nHundreds of Cine loop\r\nMin.1600 frames permanent storage \r\nVideo and VGA output\r\n2 USB ports,1 Video output,1 VGA output\r\nMeasurement & calculation software packages\r\n\r\n*Pictures for reference only','0','2','101','new','1','EMP-820Plus vet','2011-01-01','Brochure EMP-820 Plus Vet.pdf','392.EMP-820 Plus Vet.pdf','4.5','','','','','1','2','125','0','2011-11-16','ultrasound-veterinary','',NULL,'','','0');
insert into `equipo` (`id`, `nombre`, `precioventa`, `preciocompra`, `categoria_id`, `estadoequipo_id`, `usuario_id`, `fabricantes_id`, `tag`, `especificaciones`, `descripcion`, `monedaventa_id`, `moneda_id`, `paises_id`, `calidad`, `cantidad`, `modelo`, `fechafabricacion`, `documento`, `sourceDocumento`, `pesoEstimado`, `size`, `ancho`, `alto`, `sizeCaja`, `active`, `publicacionEquipo_id`, `views`, `topofers`, `publishdate`, `slug`, `nombreOferente`, `articulo_id`, `ciudadOferente`, `codigoPostalOferente`, `addCart`) values('393','CR system','n.a.','10511','168','1','62','56','','','Konika-Minolta CR-System \"REGIUS 190 MAMMO\"\r\nDOM: 2005\r\n3  Cassette 18x24 cm\r\n3  Cassettes 24x30 cm\r\nincluded: computer and Software to work together with Mammomat 3000 Nova.','0','0','24','very good','1','Regius 190 Mammo','2005-01-01','Brochure Regius190.pdf','393.Regius190.pdf','','','','','','1','2','56','0','2011-11-16','cr-system','UMS',NULL,'','','0');
insert into `equipo` (`id`, `nombre`, `precioventa`, `preciocompra`, `categoria_id`, `estadoequipo_id`, `usuario_id`, `fabricantes_id`, `tag`, `especificaciones`, `descripcion`, `monedaventa_id`, `moneda_id`, `paises_id`, `calidad`, `cantidad`, `modelo`, `fechafabricacion`, `documento`, `sourceDocumento`, `pesoEstimado`, `size`, `ancho`, `alto`, `sizeCaja`, `active`, `publicacionEquipo_id`, `views`, `topofers`, `publishdate`, `slug`, `nombreOferente`, `articulo_id`, `ciudadOferente`, `codigoPostalOferente`, `addCart`) values('394','Ultrasound system','n.a.','21474','161','1','62','17','','','Ultrasound system Vivid 7 Cardiac vascular GE\r\nDOM: 02/2003\r\nSystem software 1.52. Installed 2007. (latest software possible). Application software 2.6.4.\r\nOptions included:\r\n1 x M3 S convex transducer.\r\n1 x 5 S sector transducer.\r\n1 x Sony VCR SVO 8500 ADP.\r\n1 x Sony B/w printer. UP 895.\r\nDicom, Tissue tracking, Archive, Plug and scam, Vivid 7 Exp.\r\nSystem had full maintenance contract OEM and will be tested by GE.','0','0','19','very good','1','Vivid 7 Cardiac vascular','2003-01-01','','','','','','','','1','2','57','0','2011-11-16','ultrasound-system-30','Ext 199 - Medi-Trade',NULL,'','','0');

            ";
        $this->_db->query( $sql );
       
        $sql = "
INSERT INTO imagen (id, equipo_id, nombre, thumb, imagen, published, descripcion)
VALUES
(381, 392, 'a', '392a.jpg', '392a.jpg', 0, NULL),
(382, 393, 'a', '393a.jpg', '393a.jpg', 0, NULL);
            ";
        $this->_db->query( $sql );       
        return true;
        }


    }
