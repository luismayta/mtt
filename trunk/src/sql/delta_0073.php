<?php


class Delta_0073
        extends Mtt_Migration_Delta
    {

    protected $_author = "Teresa Chunga";
    protected $_desc = "cambiar campos a alfanumericos, campos requeridos Daniel";


    public function up()
        {
        $sql = "
            ALTER TABLE `equipo`     
                CHANGE `precioventa` `precioventa` VARCHAR(45) NOT NULL,     
                CHANGE `preciocompra` `preciocompra` VARCHAR(45) NOT NULL;
            ";
        $this->_db->query( $sql );


        $sql = "
            ALTER TABLE `equipo`     
                CHANGE `articulo_id` `articulo_id` INT NULL ;
            ";
        $this->_db->query( $sql );
        return true;
        }


    }
