<?php

class Delta_0043 extends Mtt_Migration_Delta {

    protected $_author = self::SLOVACUS;
    protected $_desc = "cambiando el campo nombre por titulo";

    public function up() {
        $sql = "
            ALTER TABLE `pagina`     CHANGE `nombre` `titulo` VARCHAR(150)
            CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
            ";

        $this->_db->query($sql);

        return true;
    }

}
