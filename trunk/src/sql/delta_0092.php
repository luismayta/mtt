<?php


class Delta_0092
        extends Mtt_Migration_Delta
    {

    protected $_author = self::SLOVACUS;
    protected $_desc = "actualizando fabricantes_id de equipo";


    public function up()
        {
        $sql = "
update equipo set fabricantes_id=57 where id =307;
update equipo set fabricantes_id=58 where id =339;
update equipo set fabricantes_id=58 where id =340;
update equipo set fabricantes_id=59 where id =240;
update equipo set fabricantes_id=60 where id =384;
update equipo set fabricantes_id=61 where id =386;
update equipo set fabricantes_id=62 where id =388;
update equipo set fabricantes_id=63 where id =387;
update equipo set fabricantes_id=64 where id =251;
update equipo set fabricantes_id=64 where id =255;
update equipo set fabricantes_id=64 where id =256;
update equipo set fabricantes_id=65 where id =381;
update equipo set fabricantes_id=66 where id =194;
update equipo set fabricantes_id=55 where id =389;
update equipo set fabricantes_id=67 where id =237;
update equipo set fabricantes_id=68 where id =342;
update equipo set fabricantes_id=69 where id =250;
update equipo set fabricantes_id=70 where id =303;
update equipo set fabricantes_id=71 where id =320;
update equipo set fabricantes_id=72 where id =385;
update equipo set fabricantes_id=73 where id =379;
update equipo set fabricantes_id=74 where id =241;
update equipo set fabricantes_id=75 where id =243;
update equipo set fabricantes_id=76 where id =288;
update equipo set fabricantes_id=77 where id =242;
update equipo set fabricantes_id=78 where id =355;
update equipo set fabricantes_id=79 where id =246;
update equipo set fabricantes_id=80 where id =382;
update equipo set fabricantes_id=81 where id =308;
update equipo set fabricantes_id=82 where id =295;
update equipo set fabricantes_id=82 where id =302;
update equipo set fabricantes_id=82 where id =328;
update equipo set fabricantes_id=82 where id =329;
update equipo set fabricantes_id=82 where id =330;
update equipo set fabricantes_id=82 where id =331;
update equipo set fabricantes_id=82 where id =332;
update equipo set fabricantes_id=82 where id =333;
update equipo set fabricantes_id=83 where id =348;
update equipo set fabricantes_id=84 where id =368;
update equipo set fabricantes_id=85 where id =311;
update equipo set fabricantes_id=85 where id =325;
update equipo set fabricantes_id=85 where id =323;
update equipo set fabricantes_id=85 where id =324;
update equipo set fabricantes_id=85 where id =326;
update equipo set fabricantes_id=85 where id =327;
update equipo set fabricantes_id=85 where id =343;
update equipo set fabricantes_id=86 where id =380;
update equipo set fabricantes_id=87 where id =290;
update equipo set fabricantes_id=87 where id =291;
update equipo set fabricantes_id=87 where id =292;
update equipo set fabricantes_id=88 where id =261;
update equipo set fabricantes_id=89 where id =383;
update equipo set fabricantes_id=90 where id =314;
update equipo set fabricantes_id=91 where id =282;
update equipo set fabricantes_id=92 where id =266;
update equipo set fabricantes_id=92 where id =305;
update equipo set fabricantes_id=93 where id =390;
update equipo set fabricantes_id=94 where id =391;
update equipo set fabricantes_id=95 where id =244;

            ";
        $this->_db->query( $sql );
       
        
        return true;
        }


    }
