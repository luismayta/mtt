<?php


class Delta_0069
        extends Mtt_Migration_Delta
    {

    protected $_author = self::SLOVACUS;
    protected $_desc = "add tabla para equipos Requeridos";


    public function up()
        {
        $sql = "
         CREATE TABLE `equiporequerido`( `id` INT(11) NOT NULL AUTO_INCREMENT , `nombre` VARCHAR(250) , `Modelo` VARCHAR(250) , `categoria_id` INT(11) , `estado_id` INT(11) , `fechafabricacion` DATE , `comentario` TEXT , PRIMARY KEY (`id`))  ;
            ";
        $this->_db->query( $sql );

        $sql = "
         ALTER TABLE `equiporequerido` ADD COLUMN `active` INT(1) DEFAULT '1' NULL AFTER `comentario`;
            ";
        $this->_db->query( $sql );


        return true;
        }


    }
