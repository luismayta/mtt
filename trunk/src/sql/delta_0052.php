<?php

class Delta_0052 extends Mtt_Migration_Delta {

    protected $_author = "self::SLOVACUS";
    protected $_desc = "ingresar data fabricantes";

    public function up() {
        $sql = "
insert into `fabricantes` (`id`, `nombre`, `active`) values('9','Karl Storz','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('10','Hamilton','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('11','Olympus','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('12','Sony','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('13','Carbamed','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('14','BBraun','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('15','Alaris Medical','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('16','Schiller','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('17','GE Healthcare','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('18','Fisher & Paykel','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('19','Siemens','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('20','Mitsubishi','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('21','Hill-Rom','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('22','Ohmeda','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('23','Medela','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('24','Johnson & Johnson','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('25','Agfa','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('26','Hellige','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('27','Imed','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('28','Laerdal','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('29','Graseby Medical','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('30','Kontron Medical','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('31','Masimo','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('32','Lamprecht','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('33','Dräger Medical','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('34','Wisap','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('35','HP','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('36','Linvatec','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('37','Bipolar','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('38','Dörr','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('39','Lifepak','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('40','Linde Medical Sensors','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('41','Roche','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('42','Biotrans','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('43','Datex-Ohmeda GE Healthcare','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('44','IVAC','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('45','Anklin','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('46','Hess','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('47','Topcon Japan','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('48','New Brunswick Scientific','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('49','Warm-Air','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('50','Schaerer Medical','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('51','Datex-Engstrom (Ohmeda)','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('52','Zeiss','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('53','Physio Control','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('54','Thermo Fisher Scientific','1');
insert into `fabricantes` (`id`, `nombre`, `active`) values('500','Others','1');
            ";
        $this->_db->query($sql);

        return true;
    }

}
