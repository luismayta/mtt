<?php


class Delta_0081
        extends Mtt_Migration_Delta
    {

    protected $_author = "Teresa Chunga";
    protected $_desc = "update archivos pdf";


    public function up()
        {
        $sql = "
update equipo set sourceDocumento='120.Perfusor Compact.pdf', documento='' where id=120;
update equipo set sourceDocumento='123.Perfusor Compact.pdf', documento='' where id=123;
update equipo set sourceDocumento='177.IVAC770 Manual_D.pdf', documento='' where id=177;
update equipo set sourceDocumento='87.Dinamap 8100 Crikiton Manual_E.pdf', documento='' where id=87;
update equipo set sourceDocumento='137.MR410 Operators Manual_E.pdf', documento='' where id=137;
update equipo set sourceDocumento='138.MR410 Operators Manual_E.pdf', documento='' where id=138;
update equipo set sourceDocumento='139.MR410 Operators Manual_E.pdf', documento='' where id=139;
update equipo set sourceDocumento='140.MR410 Operators Manual_E.pdf', documento='' where id=140;
update equipo set sourceDocumento='141.MR410 Operators Manual_E.pdf', documento='' where id=141;
update equipo set sourceDocumento='142.MR410 Operators Manual_E.pdf', documento='' where id=142;
update equipo set sourceDocumento='143.MR410 Operators Manual_E.pdf', documento='' where id=143;
update equipo set sourceDocumento='144.MR410 Operators Manual_E.pdf', documento='' where id=144;
update equipo set sourceDocumento='100.MR410 Operators Manual_E.pdf', documento='' where id=100;
update equipo set sourceDocumento='101.MR410 Operators Manual_E.pdf', documento='' where id=101;
update equipo set sourceDocumento='178.Perfusor fm Manual_E.pdf', documento='' where id=178;
update equipo set sourceDocumento='134.MR410 Operators Manual_E.pdf', documento='' where id=134;
update equipo set sourceDocumento='148.Dinamap XL 9302 Manual_D.pdf', documento='' where id=148;
update equipo set sourceDocumento='162.Saluta Super 2 Manual_D.pdf', documento='' where id=162;
update equipo set sourceDocumento='179.Offer - Solar 8000 CPU Modell - MedTechTrade.pdf', documento='Solar 8000 CPU Modell - MedTechTrade.pdf' where id=179;
update equipo set sourceDocumento='196.MicroGas 7650 Service manual Feb 05.pdf', documento='' where id=196;
update equipo set sourceDocumento='220.MR410 Operators Manual_E.pdf', documento='MR410 Operators Manual_E' where id=220;
update equipo set sourceDocumento='222.Datex - Anasthesiesystem - MedTechTrade.pdf', documento='' where id=222;
update equipo set sourceDocumento='228.Offer - Ear Surgery set Karl Storz - MedTechTrade.pdf', documento='Ear surgery set Karl Storz - MedTechTrade.pdf' where id=228;
update equipo set sourceDocumento='229.Offer -Surgery Stapedectomie set Karl Storz - MedTechTrade.pdf', documento='Surgery Stapedectomie set Karl Storz - MedTechTrade.pdf' where id=229;
update equipo set sourceDocumento='231.Offer - Life Pak 300 Physio Control - MedTechTrade.pdf', documento='Life Pak 300 Physio Control - MedTechTrade.pdf' where id=231;
update equipo set sourceDocumento='236.Offer -Ultra-Low Temperature Upright Freezers ULT2586-10-V - MedTechTrade.pdf', documento='Ultra-Low Temperature Upright Freezers ULT2586-10-V - MedTechTrade.pdf' where id=236;
update equipo set sourceDocumento='237.XL brochure.pdf', documento='Brochure Filtair XL 1646' where id=237;
update equipo set sourceDocumento='241.Offer -Surgery 3 - MedTechTrade.pdf', documento='PDF Surgery 3 - MedTechTrade' where id=241;
update equipo set sourceDocumento='242.Offer -Surgery 4 - MedTechTrade -.pdf', documento='Surgery 4 - MedTechTrade PDF' where id=242;
update equipo set sourceDocumento='243.Offer -Surgery 5 - MedTechTrade - .pdf', documento='PDF Surgery 5 - MedTechTrade' where id=243;
update equipo set sourceDocumento='254.Offer - Zeiss ORL Microscop - MedTechTrade.pdf', documento='Zeiss ORL Microscop - MedTechTrade.pdf' where id=254;
update equipo set sourceDocumento='257.Offer - Respirator Cato - MedTechTrade.pdf', documento='Respirator Cato - MedTechTrade.pdf' where id=257;
update equipo set sourceDocumento='282.rk1028h.pdf', documento='Sonorex Super RK 1028 H' where id=282;
update equipo set sourceDocumento='295.MindRay-DP-6600-Brochure.pdf', documento='Prospecto DP6600 (EN)' where id=295;
update equipo set sourceDocumento='297.Offer - Schaerer Autoclave - MedTechTrade.pdf', documento='Schaerer Autoclave - MedTechTrade.pdf' where id=297;
update equipo set sourceDocumento='302.Mindray-M5-DataSheet.pdf', documento='Mindray-M5-DataSheet.pdf' where id=302;
update equipo set sourceDocumento='328.DP2200_brochure.pdf', documento='DP2200_brochure.pdf' where id=328;
update equipo set sourceDocumento='333.m5 Brochure.pdf', documento='M5 Brochure.pdf' where id=333;
update equipo set sourceDocumento='378.GE_Vivid_ i.pdf', documento='Brochure GE_Vivid_ i.pdf' where id=378;
update equipo set sourceDocumento='377.Sequoia C512 brochure.pdf', documento='Sequoia C512 brochure.pdf' where id=377;
update equipo set sourceDocumento='389.EMP-2100.pdf', documento='Brochure EMP-2100.pdf' where id=389;
update equipo set sourceDocumento='390.HM-32.pdf', documento='Brochure HM-32.pdf' where id=390;
update equipo set sourceDocumento='392.EMP-820 Plus Vet.pdf', documento='Brochure EMP-820 Plus Vet.pdf' where id=392;
update equipo set sourceDocumento='393.Regius190.pdf', documento='Brochure Regius190.pdf' where id=393;

            ";
        $this->_db->query( $sql );
       

        return true;
        }


    }
