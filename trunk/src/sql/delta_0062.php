<?php

class Delta_0062 extends Mtt_Migration_Delta {

protected $_author = "Teresa chunga";
protected $_desc = "agregar foreign operacion";


    public function up() {
        $sql = "
           CREATE  TABLE IF NOT EXISTS `cuotaspagooperacion` (
              `id` INT NOT NULL ,
              `operacion_id` INT(11) NOT NULL ,
              `pago` DOUBLE NULL ,
              `monto` DOUBLE NULL ,
              `fechapago` DATETIME NULL ,
              `fechaLimite` DATE NULL ,
              `active` INT NULL ,
              PRIMARY KEY (`id`),
                CONSTRAINT `fk_cuotas_oper` FOREIGN KEY (`operacion_id`) 
                    REFERENCES `operacion` (`id`) 
                    ON DELETE NO ACTION ON UPDATE NO ACTION)
              
            ENGINE = InnoDB;
            
ALTER TABLE `cuotaspagooperacion`     
CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

            ";
        $this->_db->query($sql);

        return true;
    }

}
