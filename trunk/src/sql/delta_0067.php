<?php


class Delta_0067
        extends Mtt_Migration_Delta
    {

    protected $_author = self::SLOVACUS;
    protected $_desc = "add active a tabla forma de envio";


    public function up()
        {
        $sql = "
          ALTER TABLE `formaenvio` ADD COLUMN `active` INT(1) DEFAULT '1' NOT NULL AFTER `nombre`;
            ";
        $this->_db->query( $sql );


        return true;
        }


    }
