<?php

class Delta_0040 extends Mtt_Migration_Delta {

    protected $_author = self::SLOVACUS;
    protected $_desc = "add table page";

    public function up() {
        $sql = "
            CREATE  TABLE IF NOT EXISTS `page`(
            `id` INT(11) NOT NULL AUTO_INCREMENT ,
            `nombre` VARCHAR(200) NOT NULL ,
            `fecha` DATETIME ,
            `active` INT(1) DEFAULT '1' ,
            PRIMARY KEY (`id`)  )  ;
            ";

        $this->_db->query($sql);

        return true;
    }

}
