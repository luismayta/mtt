<?php


class Delta_0071
        extends Mtt_Migration_Delta
    {

    protected $_author = "Teresa Chunga";
    protected $_desc = "add tabla para alertas por email";


    public function up()
        {
        $sql = "
            ALTER TABLE `alerta_email` 
                DROP COLUMN `id_categoria`,    
                ADD COLUMN `id` BIGINT NOT NULL AUTO_INCREMENT FIRST,    
                CHANGE `id_equipo` `equipo_id` INT(11) NOT NULL,    
                ADD PRIMARY KEY(`id`);
            ";
        $this->_db->query( $sql );


        return true;
        }


    }
