<?php


class Delta_0095
        extends Mtt_Migration_Delta
    {

    protected $_author = self::SLOVACUS;
    protected $_desc = "agregando nuevos campos";


    public function up()
        {
        $sql = "
            ALTER TABLE `equipo`
			ADD COLUMN `sinonimo` VARCHAR(200) NULL AFTER `addCart`,
			ADD COLUMN `descuento` NUMERIC(8,6) NULL AFTER `sinonimo`;
            ";
        $this->_db->query( $sql );
       
        
        return true;
        }


    }



