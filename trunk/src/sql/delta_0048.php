<?php


class Delta_0048
        extends Mtt_Migration_Delta
    {

    protected $_author = self::SLOVACUS;
    protected $_desc = "add column codepais";


    public function up()
        {
        $sql = "
           ALTER TABLE `moneda`     ADD COLUMN `codepais` VARCHAR(50) NULL AFTER `fechacambio`;
            ";
        $this->_db->query( $sql );

        return true;
        }


    }
