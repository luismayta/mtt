<?php


class Delta_0072
        extends Mtt_Migration_Delta
    {

    protected $_author = "Teresa Chunga";
    protected $_desc = "add tabla equipos, campos requeridos Daniel";


    public function up()
        {
        $sql = "
            ALTER TABLE equipo
                ADD `monedaventa_id` INT NOT NULL AFTER `descripcion`,
                ADD `nombreOferente` VARCHAR(200) NULL ,
                ADD `articulo_id` DATETIME NULL ,
                ADD `ciudadOferente` VARCHAR(45) NULL ,
                ADD `codigoPostalOferente` VARCHAR(45) NULL ;
            ";
        $this->_db->query( $sql );


        return true;
        }


    }
