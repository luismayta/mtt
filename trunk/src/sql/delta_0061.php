<?php

class Delta_0061 extends Mtt_Migration_Delta {

protected $_author = "Teresa chunga";
protected $_desc = "agregar foreign operacion";


    public function up() {
        $sql = "
            ALTER TABLE `operacion`
            ADD COLUMN `moneda_id` INT(11) NOT NULL AFTER `id`;
            
    ALTER TABLE `operacion` 
            ADD CONSTRAINT `FK_operacion_moneda` FOREIGN KEY (`moneda_id`)
            REFERENCES `moneda` (`id`) ON DELETE NO ACTION  ON UPDATE NO ACTION ;

            ";
        $this->_db->query($sql);

        return true;
    }

}
