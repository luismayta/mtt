<?php


class Delta_0032
        extends Mtt_Migration_Delta
    {

    protected $_author = "Teresa Chunga";
    protected $_desc = "add campo columna fecharegistro, fechamodificacion a 
        busqueda";


    public function up()
        {
        $sql = "
            ALTER TABLE `medtechtrade`.`busqueda`     
                ADD COLUMN `fecharegistro` DATETIME NULL AFTER `active`,     
                ADD COLUMN `fechamodificacion` DATETIME NULL AFTER `fecharegistro`;
            ";

        $this->_db->query( $sql );

        return true;
        }


    }
