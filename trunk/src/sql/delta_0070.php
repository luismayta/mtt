<?php


class Delta_0070
        extends Mtt_Migration_Delta
    {

    protected $_author = "Teresa Chunga";
    protected $_desc = "add tabla para alertas por email";


    public function up()
        {
        $sql = "
CREATE TABLE `alerta_email` (
  `id_equipo` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8
            ";
        $this->_db->query( $sql );


        return true;
        }


    }
