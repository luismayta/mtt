<?php


class Delta_0088
        extends Mtt_Migration_Delta
    {

    protected $_author = self::SLOVACUS;
    protected $_desc = "agregando fabricantes";


    public function up()
        {
        $sql = "
INSERT INTO fabricantes(id, nombre) VALUES (55, 'Emperor');
INSERT INTO fabricantes(id, nombre) VALUES (56, 'Minolta');
insert into fabricantes(id, nombre) values (57,'Alara');
insert into fabricantes(id, nombre) values (58,'Aloca');
insert into fabricantes(id, nombre) values (59,'Arjo Sweden');
insert into fabricantes(id, nombre) values (60,'ATMOS Medical');
insert into fabricantes(id, nombre) values (61,'Atmos Medizintechnik');
insert into fabricantes(id, nombre) values (62,'Borcad');
insert into fabricantes(id, nombre) values (63,'Dantschke');
insert into fabricantes(id, nombre) values (64,'Datex');
insert into fabricantes(id, nombre) values (65,'Dewert');
insert into fabricantes(id, nombre) values (66,'Dürr');
insert into fabricantes(id, nombre) values (67,'Erlab');
insert into fabricantes(id, nombre) values (68,'ESAOTE');
insert into fabricantes(id, nombre) values (69,'Haemonetics');
insert into fabricantes(id, nombre) values (70,'Heinen + Löwenstein');
insert into fabricantes(id, nombre) values (71,'Hologic');
insert into fabricantes(id, nombre) values (72,'ILO Elektronik');
insert into fabricantes(id, nombre) values (73,'Isele');
insert into fabricantes(id, nombre) values (74,'Karl Storz, Access');
insert into fabricantes(id, nombre) values (75,'Karl Storz, Ulrich');
insert into fabricantes(id, nombre) values (76,'Kips');
insert into fabricantes(id, nombre) values (77,'Leibinger, Ulrich');
insert into fabricantes(id, nombre) values (78,'manufacture');
insert into fabricantes(id, nombre) values (79,'Manufacturer Dummy');
insert into fabricantes(id, nombre) values (80,'MC');
insert into fabricantes(id, nombre) values (81,'Medtronic');
insert into fabricantes(id, nombre) values (82,'Mindray');
insert into fabricantes(id, nombre) values (83,'MPV TRUMA');
insert into fabricantes(id, nombre) values (84,'Pentax');
insert into fabricantes(id, nombre) values (85,'Philips');
insert into fabricantes(id, nombre) values (86,'Promotal');
insert into fabricantes(id, nombre) values (87,'Radiometer A/S');
insert into fabricantes(id, nombre) values (88,'Schmid');
insert into fabricantes(id, nombre) values (89,'sele');
insert into fabricantes(id, nombre) values (90,'Siemens Acuson');
insert into fabricantes(id, nombre) values (91,'Sonorex Super');
insert into fabricantes(id, nombre) values (92,'Toshiba');
insert into fabricantes(id, nombre) values (93,'Wandong');
insert into fabricantes(id, nombre) values (94,'Well-D');
insert into fabricantes(id, nombre) values (95,'Wolf System AG');


            ";
        $this->_db->query( $sql );
       
        
        return true;
        }


    }
