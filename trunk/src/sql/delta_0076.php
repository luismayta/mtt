<?php


class Delta_0076
        extends Mtt_Migration_Delta
    {

    protected $_author = self::SLOVACUS;
    protected $_desc = "cambiando los valores decimales por varchar";


    public function up()
        {
        $sql = "
            ALTER TABLE `equipo`
            CHANGE `size` `size` VARCHAR(30) NULL ;
            ";
        $this->_db->query( $sql );
        $sql = "
           ALTER TABLE `equipo`
           CHANGE `pesoEstimado` `pesoEstimado` VARCHAR(30) NULL ,
           CHANGE `ancho` `ancho` VARCHAR(30) NULL ,
           CHANGE `alto` `alto` VARCHAR(30) NULL ,
           CHANGE `sizeCaja` `sizeCaja` VARCHAR(30) NULL ;
            ";
        $this->_db->query( $sql );
        $sql = "
           ALTER TABLE `equiporequerido`     ADD COLUMN `fabricante` VARCHAR(250) NULL AFTER `comentario`;
            ";
        $this->_db->query( $sql );
        $sql = "
           ALTER TABLE `equiporequerido`     CHANGE `Modelo` `modelo` VARCHAR(250);
            ";
        $this->_db->query( $sql );

        return true;
        }


    }
