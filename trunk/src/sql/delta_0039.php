<?php


class Delta_0039
        extends Mtt_Migration_Delta
    {

    protected $_author = "Teresa Chunga E.";
    protected $_desc = "modificando tabla operacion_envio";


    public function up()
        {
        $sql = "
            ALTER TABLE `medtechtrade`.`operacion_envios`     
            CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
            ";

        $this->_db->query( $sql );

        return true;
        }


    }
