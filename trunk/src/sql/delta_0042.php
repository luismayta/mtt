<?php

class Delta_0042 extends Mtt_Migration_Delta {

    protected $_author = self::SLOVACUS;
    protected $_desc = "add relacion de paginas";

    public function up() {
        $sql = "
            ALTER TABLE `pagina` 
ADD CONSTRAINT `FK_pagina` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) 
ON DELETE NO ACTION  ON UPDATE NO ACTION ;
            ";

        $this->_db->query($sql);

        return true;
    }

}
