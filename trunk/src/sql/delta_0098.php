<?php


class Delta_0098
        extends Mtt_Migration_Delta
    {

    protected $_author = self::SLOVACUS;
    protected $_desc = "add tabla programa";


    public function up()
        {
        $sql = "
            CREATE TABLE 
            `programa`(     `id` INT(11) NOT NULL AUTO_INCREMENT 
            COMMENT 'clave primary key',     
            `nombre` VARCHAR(150) NOT NULL COMMENT 'nombre de la tabla ',
            `active` INT(1) COMMENT '1 esta activo, 0 desactivado',
            PRIMARY KEY (`id`)  );
            ";
        $this->_db->query( $sql );


        $sql = "
            CREATE TABLE `programaequipo`(     `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT
            'id de programa', 
            `equipo_id` INT(11) NOT NULL COMMENT 'id de la tabla de equipo',
            `programa_id` INT(11) NOT NULL COMMENT 'id de la tabla programa',
            `active` INT(1) DEFAULT '1' COMMENT 'active 1, desactive 0',
            PRIMARY KEY (`id`)  );
            ";
        $this->_db->query( $sql );


        $sql = "
            ALTER TABLE `programa` 
            CHANGE `active` `active` INT(1) DEFAULT '1'
            NULL COMMENT '1 esta activo, 0 desactivado';
            ";
        $this->_db->query( $sql );


        $sql = "
            ALTER TABLE `programaequipo` 
            ADD CONSTRAINT `FK_programaequipo` FOREIGN KEY (`programa_id`)
            REFERENCES `programa` (`id`) ON DELETE NO ACTION  ON UPDATE NO ACTION ;
            ";
        $this->_db->query( $sql );
        
        $sql = "
            ALTER TABLE `programaequipo` ADD CONSTRAINT `FK_equipoprograma`
            FOREIGN KEY (`equipo_id`) REFERENCES `equipo` (`id`)
            ON DELETE NO ACTION  ON UPDATE NO ACTION ;
            ";
        $this->_db->query( $sql );


        return true;


        }


    }

