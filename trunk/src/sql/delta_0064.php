<?php


class Delta_0064
        extends Mtt_Migration_Delta
    {

    protected $_author = self::SLOVACUS;
    protected $_desc = "agregar un campo para cambio de monedas";

    public function up()
        {
        $sql = "
           ALTER TABLE `moneda` ADD COLUMN `codegoogle` VARCHAR(50) NULL AFTER `codepais`;

            ";
        $this->_db->query( $sql );
        $sql = "
           UPDATE moneda SET codegoogle = 'CHF' WHERE moneda.id = 1;
UPDATE moneda SET codegoogle = 'USD' WHERE moneda.id = 2;
UPDATE moneda SET codegoogle = 'PEN' WHERE moneda.id = 3;
UPDATE moneda SET codegoogle = 'CLP' WHERE moneda.id = 4;
UPDATE moneda SET codegoogle = 'EUR' WHERE moneda.id = 5;
UPDATE moneda SET codegoogle = 'PLN' WHERE moneda.id = 6;

            ";
        $this->_db->query( $sql );

        return true;
        }


    }
