<?php

class Delta_0041 extends Mtt_Migration_Delta {

    protected $_author = self::SLOVACUS;
    protected $_desc = "campos en pagina";

    public function up() {
        $sql = "
            ALTER TABLE `pagina`
            ADD COLUMN `page_id` INT(11) NOT NULL AFTER `body`;
            ";

        $this->_db->query($sql);

        return true;
    }

}
