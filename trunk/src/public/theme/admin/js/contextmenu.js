
/*menu para usuarios*/
$(document).ready( function() {

	$('#Usuarios').jqGrid(
		'setGridParam', {
			loadComplete: function(id){
				alert(id);
			}
		}
	);



/*desactivar formularios*/
//$('#myMenu').disableContextMenuItems('#operaciones,#equipos');
	/*desactivar formularios*/

	$('#Usuarios').contextMenu({
		menu: 'myMenu'
	},
	function(action, el, pos)
	{
		var id = jQuery("#Usuarios").jqGrid('getGridParam','selrow');
            
		switch(action){
			case 'operaciones':

				$(location).attr('href',"/admin/user/operation/id/"+id);
				break;
			case 'equipos':

				$(location).attr('href',"/admin/user/equipment/id/"+id);
				break;
			case 'edit':

				$(location).attr('href',"/admin/user/editar/id/"+id);
				break;
			case 'changePassword':

				$(location).attr('href',"/admin/user/change-password/id/"+id);
				break;
			case 'inactive':

				$(location).attr('href',"/admin/user/desactivar/id/"+id);
				break;
			case 'active':

				$(location).attr('href',"/admin/user/activar/id/"+id);
				break;
            case 'acreditar':

				$(location).attr('href',"/admin/user/acreditar/id/"+id);
				break;
			case 'desacreditar':

				$(location).attr('href',"/admin/user/desacreditar/id/"+id);
				break;
                    
		}

		

		
	}
	);
	
});
