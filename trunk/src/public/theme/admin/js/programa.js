function showEquiposNuevos(){
    $.ajax({
        url: "/admin/programa/showequiposnuevos/format/html",
        type: "POST",
        dataType: "html",
        async:false,
        success: function(data){
            $("#equiposNuevosData").html(data);
            $('#equiposNuevos').checkboxTree();
                        
        }
    }
    );
}

function showManufacturersEquipments(){
    $.ajax({
        url: "/admin/programa/showmanufacturerequipment/format/html",
        type: "POST",
        dataType: "html",
        async:false,
        success: function(data){
            $("#divFrmManufacturer").html(data);
            $('#equiposManufacturers').checkboxTree();
        }
    }
    );
}


function showAllPrograma(){
    this.showEquiposNuevos();
    this.showManufacturersEquipments();
}

$(document).ready(
    function(){
        
        $("#manufacturerAjax").bind(
            'click',function(){
                
                showManufacturersEquipments();
           
            });
            
        $("#equiposNuevosAjax").bind(
            'click',function(){
                showEquiposNuevos()
            
            });    

        $('#frmManufacturer').live('submit',
            function(){
                event.preventDefault();
                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    // Mostramos un mensaje con la respuesta de PHP
                    success: function(data) {
                        $('#response').html(data);
                        showAllPrograma();
                    }
                })        
                return false;  
            }

            );
        $('#frmEquiposNuevos').live('submit',
            function(){
                event.preventDefault();
                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    // Mostramos un mensaje con la respuesta de PHP
                    success: function(data) {
                        $('#response').html(data);
                        showAllPrograma();
                        
                    }
                })        
                return false;  
            }

            );
                
           
    }
    
    );
