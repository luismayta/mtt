$(document).ready(
	function(){
		$('span #exportarPeru').click(function(){
			$('#downloadExcel').show("slow");
			imagen = '<img src="/images/acciones/ajax/ajax-loader.gif" />';
			$('#downloadExcel').html(imagen);
			var selr = $('#equipos').jqGrid('getGridParam','selarrrow');
			$.post("/admin/equipo/exportar/format/html", {
				id:selr,
				codePais:'USD',
				codeExportar:'PEN',
				pais:'Peru'
			},
			function(data){
				enlace = '<a href=' + data + '> ';
				enlace +=  '<img src="/images/acciones/22/excel.png" />';
				enlace +=  'download report excel';
				enlace += '</a> ';
				$('#downloadExcel').hide("fast");
				$('#downloadExcel').html(enlace);
				$('#downloadExcel').show("slow");
			});
			
		})

		$('span #exportarChile').click(function(){
			$('#downloadExcel').show("slow");
			imagen = '<img src="/images/acciones/ajax/ajax-loader.gif" />';
			$('#downloadExcel').html(imagen);
			var selr = $('#equipos').jqGrid('getGridParam','selarrrow');
			$.post("/admin/equipo/exportar/format/html", {
				id:selr,
				codePais:'USD',
				codeExportar:'CLP',
				pais:'Chile'
			},
			function(data){
				enlace = '<a href=' + data + '> ';
				enlace +=  '<img src="/images/acciones/22/excel.png" />';
				enlace +=  'download report excel';
				enlace += '</a> ';
				$('#downloadExcel').html(enlace);
				$('#downloadExcel').show("slow");
			});

		})
		$('span #exportarSuiza').click(function(){
			$('#downloadExcel').show("slow");
			imagen = '<img src="/images/acciones/ajax/ajax-loader.gif" />';
			$('#downloadExcel').html(imagen);
			var selr = $('#equipos').jqGrid('getGridParam','selarrrow');

			$.post("/admin/equipo/exportarsuiza/format/html", {
				id: selr,
				pais:'Suiza'
			},
			function(data){
				enlace = '<a href=' + data + '> ';
				enlace +=  '<img src="/images/acciones/22/excel.png" />';
				enlace +=  'download report excel';
				enlace += '</a> ';
				$('#downloadExcel').html(enlace);
				$('#downloadExcel').show("slow");
			});

		})
		$('span #exportarPolonia').click(function(){
			$('#downloadExcel').show("slow");
			imagen = '<img src="/images/acciones/ajax/ajax-loader.gif" />';
			$('#downloadExcel').html(imagen);
			var selr = $('#equipos').jqGrid('getGridParam','selarrrow');
			$.post("/admin/equipo/exportar/format/html", {
				id:selr,
				codePais:'EUR',
				codeExportar:'PLN',
				pais:'Polonia'
			},
			function(data){
				enlace = '<a href=' + data + '> ';
				enlace +=  '<img src="/images/acciones/22/excel.png" />';
				enlace +=  'download report excel';
				enlace += '</a> ';
				$('#downloadExcel').html(enlace);
				$('#downloadExcel').show("slow");
			});

		})
    
	}
	);