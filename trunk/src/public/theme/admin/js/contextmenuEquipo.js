$(document).ready( function() {

	// Show menu when #myDiv is clicked
	$("#equipos").contextMenu({
		menu: 'myMenu'
	},
	function(action, el, pos)
	{
            var id = jQuery("#equipos").jqGrid('getGridParam','selrow');
            
            switch(action){
                case 'addTopOffers':

                    $(location).attr('href',"/admin/equipo/addtopofers/id/"+id);
					break;
                case 'quitOffers':

                    $(location).attr('href',"/admin/equipo/quittopofers/id/"+id);
					break;
                case 'payment':

                    $(location).attr('href',"/admin/equipoformapago/nuevo/id/"+id);
					break;
                case 'edit':

                    $(location).attr('href',"/admin/equipo/editar/id/"+id);
					break;
                case 'publish':

                    //$(location).attr('href',"/admin/equipo/addtopofers/id/"+id);
					break;
                case 'imagen':

                    $(location).attr('href',"/admin/imagen/nuevo/id/"+id);
					break;
                case 'active':

                    $(location).attr('href',"/admin/equipo/activar/id/"+id);
					break;
                case 'inactive':

                    $(location).attr('href',"/admin/equipo/desactivar/id/"+id);
					break;
                    
            }

	}
	);


});
