var titulo= new Array();
var cantidad=new Array();
var otitulo = TAFFY();
var ocantidad  = TAFFY();


$.elycharts.templates['line_basic_6'] = {
	type: "line",
	margins: [10, 40, 40, 30],
	defaultSeries: {
		highlight: {
			newProps: {
				r: 8,
				opacity: 1
			},
			overlayProps: {
				fill: "white",
				opacity: 0.2
			}
		}
	},
	series: {
		serie1: {
			color: "90-#003000-#009000",
			tooltip: {
				frameProps: {
					stroke: "green"
				}
			}
		}
    
	},
	defaultAxis: {
		labels: true
	},
	axis: {
		x: {
			labelsRotate: 25,
			labelsProps: {
				font: "12px Verdana"
			}
		}
	},
	features: {
		grid: {
			draw: true,
			forceBorder: true,
			ny: 5
		}
	},
	barMargins: 15
};



function myBeforeSendCallbackJsFunc(){
	$("#chart").chart("clear");
	this.titulo = new Array();
	this.cantidad  = new Array();
}

function myCompleteCallbackJsFunc(data){

	this.otitulo =TAFFY()
	this.ocantidad =TAFFY()
    
	var datos = TAFFY(data.equipos);
	if(datos().count()== 0){
		return;
	}
	datos().each(function (record,recordnumber)
	{
        
		this.titulo[recordnumber]=record["titulo"];
		this.cantidad[recordnumber]=record["cantidad"];

	});

	this.otitulo = TAFFY(this.titulo);
	this.ocantidad = TAFFY(this.cantidad);
  
	var newcantidades = eval('[' + this.ocantidad().get() + ']');

	$("#chart").chart({
		template :"line_basic_6",
		values : {
			serie1 : newcantidades
		},
		labels : this.otitulo().get(),
		tooltips : {
			serie1 : this.otitulo().get()
		},
		defaultSeries: {
			type: "bar",
			stacked: true
		},
		series: {
			serie3: {
				type: "line",
				stacked: false,
				axis: "r"
			}
		},
		axis: {
			r: {
				max: 100,
				suffix: "%"
			}
		}
	});
    

	generarTable(datos);



}


function loadJson(titulos, cantidades)
{
	data="";
	var newcantidades = eval('[' + cantidades().get() + ']');
    
    
	data={
		template: "line_basic_6",
		values: {
			serie1: newcantidades
		},
		labels: titulos().get(),
		tooltips: {
			serie1:titulos().get()
		},
		defaultSeries: {
			type: "bar",
			stacked: true
		},
		series: {
			serie3: {
				type: "line",
				stacked: false,
				axis: "r"
			}
		},
		axis: {
			r: {
				max: 100,
				suffix: "%"
			}
		}
	}
	
    
	return data;
}

function generarTable(data){
		
	var tabla = '<table cellpadding="0" cellspacing="0" border="1" class="listado" id="listProduct">';
	tabla += '<thead>';
	tabla += '<tr>';
	tabla += '<th>Name</th>';
	tabla += '<th>Cantidad</th>';
	tabla += '</tr>';
	tabla += '</thead>';
	tabla += '<tbody>';
	tr = '';

	data().each(function (record,recordnumber)
	{

		tr += '<tr>';
		tr += '<td>'+record["titulo"]+'</td>';
		tr += '<td>'+record["cantidad"]+'</td>';
		tr += '</tr>';
		
	});
	
	tabla += tr;
	tabla += '</tbody></table>';

	$('#detalleChart').html(tabla);
}

function btnExportar()
{
	
}

