

$(document).ready(function(){
    //$("img[title]").tooltip();
    $("span[title]").tooltip(
    {

	// place tooltip on the right edge
	position: "center right",

	// a little tweaking of the position
	offset: [-2, 10],

	// use the built-in fadeIn/fadeOut effect
	effect: "fade",

	// custom opacity setting
	opacity: 0.7

}
        );
  
});