/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.1.53-community-log : Database - medtechtrade
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`medtechtrade` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `medtechtrade`;

/*Data for the table `paises` */

insert  into `paises`(`id`,`nombre`,`code`,`active`,`integrate`) values (1,'Peru','PE',1,1),(2,'FRANCE','FR',1,NULL),(3,'ITALY','IT',1,NULL),(4,'UNITED ARAB EMIRATES','AE',1,NULL),(5,'SWEDEN','SE',1,1),(6,'KAZAKHSTAN','KZ',1,NULL),(7,'PORTUGAL','PT',1,NULL),(8,'GREECE','GR',1,NULL),(9,'SAUDI ARABIA','SA',1,NULL),(10,'RUSSIAN FEDERATION','RU',1,NULL),(11,'UNITED KINGDOM','GB',1,NULL),(12,'DENMARK','DK',1,NULL),(13,'HONG KONG','HK',1,NULL),(14,'UNITED STATES','US',1,NULL),(15,'INDIA','IN',1,NULL),(16,'BRAZIL','BR',1,NULL),(17,'CANADA','CA',1,NULL),(18,'PUERTO RICO','PR',1,NULL),(19,'NETHERLANDS','NL',1,NULL),(500,'Others','OT',1,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
