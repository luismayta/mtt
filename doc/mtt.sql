/*
SQLyog Enterprise - MySQL GUI v8.1 
MySQL - 5.5.16-log : Database - medtechtrade
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `alerta` */

DROP TABLE IF EXISTS `alerta`;

CREATE TABLE `alerta` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` INT(11) NOT NULL,
  `tipo` INT(11) DEFAULT NULL,
  `detalle` VARCHAR(255) DEFAULT NULL COMMENT 'detalle de configuracion de alertas ',
  `active` INT(11) DEFAULT '1',
  `fecharegistro` DATETIME DEFAULT NULL,
  `fechamodificacion` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_conf_alertas_usuario1` (`usuario_id`),
  CONSTRAINT `fk_alerta_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB DEFAULT CHARSET=utf8;

/*Data for the table `alerta` */

/*Table structure for table `busqueda` */

DROP TABLE IF EXISTS `busqueda`;

CREATE TABLE `busqueda` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `palabras_busqueda` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modelo` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fabricante` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `categoria_id` INT(11) DEFAULT NULL,
  `anio_inicio` INT(11) DEFAULT NULL,
  `anio_fin` INT(11) DEFAULT NULL,
  `precio_inicio` INT(11) DEFAULT NULL,
  `precio_fin` INT(11) DEFAULT NULL,
  `usuario_id` INT(11) NOT NULL,
  `active` INT(11) NOT NULL DEFAULT '1',
  `fecharegistro` DATETIME DEFAULT NULL,
  `fechamodificacion` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_busqueda_usuario1` (`usuario_id`),
  CONSTRAINT `fk_busqueda_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `busqueda` */

INSERT  INTO `busqueda`(id,palabras_busqueda,modelo,fabricante,categoria_id,anio_inicio,anio_fin,precio_inicio,precio_fin,usuario_id,active,fecharegistro,fechamodificacion) VALUES (1,'equipo','','',-1,-1,-1,-1,-1,6,1,'2011-10-25 23:10:31','2011-10-25 23:10:31');

/*Table structure for table `categoria` */

DROP TABLE IF EXISTS `categoria`;

CREATE TABLE `categoria` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` VARCHAR(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` TEXT COLLATE utf8_unicode_ci,
  `published` INT(11) DEFAULT NULL,
  `order` INT(11) DEFAULT NULL,
  `active` INT(11) DEFAULT '1',
  `slug` VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=172 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `categoria` */

INSERT  INTO `categoria`(id,nombre,title,thumbnail,descripcion,published,categoria.order,active,slug) VALUES 
(148,'Others','Others',NULL,'',NULL,19,1,'others'),
(150,'Apparatus / General Lab','Apparatus / General Lab',NULL,'',NULL,2,1,'apparatus-general-lab'),
(151,'Cardio Devices','Cardio Devices',NULL,'',NULL,5,1,'cardio-devices'),
(152,'Balances','Balances',NULL,'',NULL,6,1,'balances'),
(153,'Centrifugues','Centrifugues',NULL,'',NULL,7,1,'centrifugues'),
(154,'Respirators','Respirators',NULL,'',NULL,8,1,'respirators'),
(155,'Endoscopy Devices','Endoscopy Devices',NULL,'',NULL,10,1,'endoscopy-devices'),
(156,'Microscopes','Microscopes',NULL,'',NULL,17,1,'microscopes'),
(157,'Imaging Systems','Imaging Systems',NULL,'',NULL,12,1,'imaging-systems'),
(158,'Chirurgical Devices','Chirurgical Devices',NULL,'',NULL,14,1,'chirurgical-devices'),
(159,'Monitoring Devices','Monitoring Devices',NULL,'',NULL,9,1,'monitoring-devices'),
(160,'Perfusors & Pumps','Perfusors & Pumps',NULL,'',NULL,16,1,'perfusors-pumps'),
(161,'Ultrasound','Ultrasound',NULL,'',NULL,18,1,'ultrasound'),
(162,'Incubators','Incubators',NULL,'',NULL,13,1,'incubators'),
(163,'Autoclaves','Autoclaves',NULL,'',NULL,3,1,'autoclaves'),
(164,'Coolers & Freezers','Coolers & Freezers',NULL,'',NULL,11,1,'coolers-freezers'),
(165,'Medical Furnitures / Beds','Medical Furnitures / Beds',NULL,'',NULL,15,1,'medical-furnitures-beds'),
(168,'X-Ray','X-Ray',NULL,'',NULL,4,1,'x-ray'),
(169,'NEU- & Demogeräte','NEU- & Demogeräte',NULL,'',NULL,20,1,'neu-demoger-te'),
(170,'prueba','Prueba 2','123.jpg','esto es una vaina',NULL,NULL,0,''),
(171,'prueba2','ddsfsd',NULL,'dfsdfdsfs',NULL,NULL,0,'');

/*Table structure for table `categoriapregunta` */

DROP TABLE IF EXISTS `categoriapregunta`;

CREATE TABLE `categoriapregunta` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(45) DEFAULT NULL,
  `active` INT(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `categoriapregunta` */

INSERT  INTO `categoriapregunta`(id,descripcion,active) VALUES (1,'Otros',1);

/*Table structure for table `config` */

DROP TABLE IF EXISTS `config`;

CREATE TABLE `config` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` INT(11) NOT NULL,
  `pais_id` INT(11) DEFAULT NULL,
  `idioma_id` INT(11) NOT NULL,
  `active` INT(1) DEFAULT '1',
  `fechaactualizacion` DATE DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_config_idiomas` (`idioma_id`),
  KEY `FK_config_usuario` (`usuario_id`),
  KEY `FK_config_paises` (`pais_id`),
  CONSTRAINT `FK_config_idiomas` FOREIGN KEY (`idioma_id`) REFERENCES `idiomas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_config_paises` FOREIGN KEY (`pais_id`) REFERENCES `paises` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_config_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `config` */

INSERT  INTO `config`(id,usuario_id,pais_id,idioma_id,active,fechaactualizacion) VALUES (1,6,500,4,1,NULL);

/*Table structure for table `contacto` */

DROP TABLE IF EXISTS `contacto`;

CREATE TABLE `contacto` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `contacto` */

/*Table structure for table `cuotaspago` */

DROP TABLE IF EXISTS `cuotaspago`;

CREATE TABLE `cuotaspago` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `operacion_has_equipo_id` INT(11) NOT NULL,
  `estadocuota_id` INT(11) NOT NULL,
  `nrocuota` INT(11) NOT NULL,
  `pago` FLOAT NOT NULL,
  `fechapago` DATETIME NOT NULL,
  `fechalimite` DATETIME NOT NULL,
  `mora` FLOAT NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cuotaspago_operacion_has_equipo1` (`operacion_has_equipo_id`),
  KEY `fk_cuotaspago_estadocuota1` (`estadocuota_id`),
  CONSTRAINT `fk_cuotaspago_estadocuota1` FOREIGN KEY (`estadocuota_id`) REFERENCES `estadocuota` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cuotaspago_operacion_has_equipo1` FOREIGN KEY (`operacion_has_equipo_id`) REFERENCES `operacion_has_equipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `cuotaspago` */

INSERT  INTO `cuotaspago`(id,operacion_has_equipo_id,estadocuota_id,nrocuota,pago,fechapago,fechalimite,mora) VALUES (3,4,2,1,58,'2011-09-26 15:01:02','2011-09-26 15:01:02',0.58),(4,4,2,2,58,'2011-09-26 15:01:04','2011-09-26 15:01:04',0),(5,4,1,3,42,'2011-09-26 15:00:58','2011-09-26 15:00:58',0);

/*Table structure for table `dbversion` */

DROP TABLE IF EXISTS `dbversion`;

CREATE TABLE `dbversion` (
  `version` INT(11) NOT NULL
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `dbversion` */

INSERT  INTO `dbversion`(VERSION) VALUES (39);

/*Table structure for table `equipo` */

DROP TABLE IF EXISTS `equipo`;

CREATE TABLE `equipo` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `precioventa` FLOAT NOT NULL,
  `preciocompra` FLOAT NOT NULL,
  `categoria_id` INT(11) NOT NULL,
  `estadoequipo_id` INT(11) NOT NULL,
  `publicacionEquipo_id` INT(11) NOT NULL,
  `usuario_id` INT(11) NOT NULL,
  `fabricantes_id` INT(11) NOT NULL,
  `tag` TEXT COLLATE utf8_unicode_ci,
  `especificaciones` TEXT COLLATE utf8_unicode_ci,
  `descripcion` TEXT COLLATE utf8_unicode_ci,
  `moneda_id` INT(11) NOT NULL,
  `paises_id` INT(11) NOT NULL,
  `calidad` VARCHAR(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cantidad` INT(11) NOT NULL,
  `modelo` VARCHAR(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechafabricacion` DATE DEFAULT NULL,
  `documento` VARCHAR(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sourceDocumento` VARCHAR(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pesoEstimado` DECIMAL(10,2) DEFAULT NULL,
  `size` DECIMAL(10,2) DEFAULT NULL,
  `ancho` DECIMAL(10,2) DEFAULT NULL,
  `alto` DECIMAL(10,2) DEFAULT NULL,
  `sizeCaja` DECIMAL(10,2) DEFAULT NULL,
  `active` INT(11) DEFAULT '1',
  `views` INT(11) DEFAULT NULL COMMENT 'es para la parte de productos mas visitados',
  `topofers` INT(2) DEFAULT '0' COMMENT 'si es un producto top offers',
  `publishdate` DATE DEFAULT NULL COMMENT 'fecha de publicacion',
  `slug` VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_equipo_categoria` (`categoria_id`),
  KEY `fk_equipo_estadoequipo1` (`estadoequipo_id`),
  KEY `fk_equipo_publicacionEquipo1` (`publicacionEquipo_id`),
  KEY `fk_equipo_usuario1` (`usuario_id`),
  KEY `fk_equipo_fabricantes1` (`fabricantes_id`),
  KEY `fk_equipo_moneda1` (`moneda_id`),
  KEY `fk_equipo_paises1` (`paises_id`),
  CONSTRAINT `fk_equipo_categoria` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_equipo_estadoequipo1` FOREIGN KEY (`estadoequipo_id`) REFERENCES `estadoequipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_equipo_fabricantes1` FOREIGN KEY (`fabricantes_id`) REFERENCES `fabricantes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_equipo_moneda1` FOREIGN KEY (`moneda_id`) REFERENCES `moneda` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_equipo_paises1` FOREIGN KEY (`paises_id`) REFERENCES `paises` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_equipo_publicacionEquipo1` FOREIGN KEY (`publicacionEquipo_id`) REFERENCES `publicacionequipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_equipo_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `equipo` */

INSERT  INTO `equipo`(id,nombre,precioventa,preciocompra,categoria_id,estadoequipo_id,publicacionEquipo_id,usuario_id,fabricantes_id,tag,especificaciones,descripcion,moneda_id,paises_id,calidad,cantidad,modelo,fechafabricacion,documento,sourceDocumento,pesoEstimado,size,ancho,alto,sizeCaja,active,views,topofers,publishdate,slug) VALUES (2,'Equipo 1',12,12,148,1,2,3,2,'sas',NULL,NULL,1,1,'buena',122,'23123','0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,79,0,'2011-10-10','equipo-1'),(3,'equipo 2',123,111,148,2,2,3,2,'kusa,kusa',NULL,NULL,1,1,'muy mala',123,'123we','0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,552,0,'2011-10-10','equipo-2'),(4,'equipo 3',125,140,151,1,2,5,2,'1245',NULL,NULL,1,1,'buena',145,'1455214','0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,664,1,'2011-10-10','equipo-3'),(5,'equipo 4',1234,12,162,1,2,3,2,'qwq',NULL,NULL,1,1,'muy buena',12,'2122','0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,481,1,'2011-10-10','equipo-4'),(6,'test tsfs',120,240,150,2,4,6,2,'sssss',NULL,NULL,2,1,'Alta calidad',5,'s','0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,724,0,'2011-10-10','test-tsfs'),(9,'prueba equivo',15,1285,151,2,2,6,2,'',NULL,NULL,1,1,'Buena Calidad',10,'120vb','0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,111,0,'2011-10-10','prueba-equivo'),(10,'test 2 x',9,1258,152,2,2,6,2,'dgdfgdgfd',NULL,NULL,1,1,'Buena Calidad',15,'xxxxxxxxxxxxxxx','0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,34,1,'2011-10-10','test-2-x'),(11,'test tsfs2',5,245,150,1,4,6,2,'sfsf',NULL,NULL,1,1,'Buena Calidad',35,'xxxxxxxxxxxxxxx','2015-03-20',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,54,0,'2011-10-10','test-tsfs2'),(12,'test tsfs3',14,654,159,1,4,6,2,'sgadgsg',NULL,NULL,2,1,'Buena Calidad',10,'tst','2023-03-20',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,12,0,'2011-10-10','test-tsfs3'),(13,'stereo xxxx',58,958,150,1,4,6,2,'sdsssssssssssssss',NULL,NULL,1,1,'Buena Calidad',25,'sfs','2011-03-17',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,184,1,'2011-10-10','stereo-xxxx'),(14,'electrocardiograma',0,915,151,2,2,6,2,'Equipo de Electro',NULL,NULL,2,1,'Excelente',10,'varuadis','2011-03-24',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,36,0,'2011-10-10','electrocardiograma'),(15,'Equipo-X-Ray',0,1452,168,1,2,6,2,'Equipo,',NULL,NULL,3,1,'Excelente',1452,'mdmfsdf','2011-03-30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,4,0,'2011-10-10','equipo-x-ray'),(16,'Equipo numero 15',0,1452,161,1,2,19,2,'gsggs',NULL,NULL,2,1,'muy buena',10,'231231ddd','2011-04-27',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,0,'2011-10-10','equipo-numero-15'),(17,'nuevo equipo 152',0,14,151,1,2,20,2,'tag , tag',NULL,NULL,2,1,'muy bueno',145,'sgg --ggg','2011-04-06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,25,0,'2011-10-10','nuevo-equipo-152'),(18,'master',0,2344,156,1,2,6,2,'dsd',NULL,NULL,2,1,'muy buena',1452,'dsadasd3213131','2017-04-25',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,19,0,'2011-10-10','master'),(19,'nombre',0,1452,148,1,1,6,2,NULL,NULL,NULL,2,305,'buena',12,'smm','2017-04-04',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,0,'2011-10-10','nombre'),(20,'equipo',0,1452,148,1,1,6,2,NULL,NULL,NULL,1,148,'buena',145,'bueno','2017-05-11',NULL,NULL,'145.00','12.00','1452.00','145.00','122.00',1,NULL,0,'2011-10-10','equipo'),(21,'Investigation unit Ear Nose Throat / ORL',0,150,148,1,2,6,2,NULL,NULL,'equipo en buen estado',1,260,'buena',1,'muy bueno','2011-10-11',NULL,NULL,'145.00','12.00','10.00','15.00','15.00',1,NULL,0,NULL,'investigation-unit-ear-nose-throat-orl'),(22,'Investigation unit Ear Nose Throat / ORL',0,14523,152,1,2,3,2,NULL,NULL,'<b style=\"padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \"><span style=\"padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; \">&nbsp;</span></b><span class=\"Apple-style-span\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; \">Investigation unit Ear Nose Throat / ORL Compact Dantschke</span><br style=\"padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \"><span class=\"Apple-style-span\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \">Manufactured in: DE (Leiptzig)</span><br style=\"padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \"><span class=\"Apple-style-span\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \">Date of manufacture: 2008</span><br style=\"padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \"><span class=\"Apple-style-span\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \">Condition: New (Demo Model)</span><br style=\"padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \"><span class=\"Apple-style-span\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \">Quantity: 1</span><br style=\"padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \"><span class=\"Apple-style-span\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \">Description: secretion pump out and automatic secretion drainage,</span><br style=\"padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \"><span class=\"Apple-style-span\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \">with air pressure for spray function,</span><br style=\"padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \"><span class=\"Apple-style-span\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \">with 1 Temp. water-ear flushing 37Â° C</span><br style=\"padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \"><span class=\"Apple-style-span\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \">with vacuum &amp; air pressure manometer</span><br style=\"padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \"><span class=\"Apple-style-span\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \">with headlamp hanger assembly and automatic halogen light-connection</span><br style=\"padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \"><span class=\"Apple-style-span\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \">with 2 channel Halogen light source 15V 150W</span><br style=\"padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \"><span class=\"Apple-style-span\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \">Pre heated endoscope tray</span><br style=\"padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \"><span class=\"Apple-style-span\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \">12 month waranty in Switzerland</span><br style=\"padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \"><span class=\"Apple-style-span\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \">Water connection &amp; sewer hookups</span><br style=\"padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \"><span class=\"Apple-style-span\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \">Voltage: 230V&nbsp;</span><br style=\"padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \"><span class=\"Apple-style-span\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: left; \">Weight: aprox. 85 Kg.</span>',1,354,'buena',0,'swedfer','1995-01-04',NULL,NULL,'85.00','120.00','150.00','145.00','145.00',1,12,0,NULL,'investigation-unit-ear-nose-throat-orl-1');

/*Table structure for table `equipo_has_formapago` */

DROP TABLE IF EXISTS `equipo_has_formapago`;

CREATE TABLE `equipo_has_formapago` (
  `equipo_id` INT(11) NOT NULL,
  `formapago_id` INT(11) NOT NULL,
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nrocuotas` INT(11) NOT NULL,
  `pago` FLOAT DEFAULT NULL,
  `dias` INT(11) DEFAULT NULL,
  `totalpago` FLOAT DEFAULT NULL,
  `moraxdia` FLOAT NOT NULL,
  `active` INT(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_equipo_has_formapago_formapago1` (`formapago_id`),
  KEY `fk_equipo_has_formapago_equipo1` (`equipo_id`),
  CONSTRAINT `fk_equipo_has_formapago_equipo1` FOREIGN KEY (`equipo_id`) REFERENCES `equipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_equipo_has_formapago_formapago1` FOREIGN KEY (`formapago_id`) REFERENCES `formapago` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `equipo_has_formapago` */

INSERT  INTO `equipo_has_formapago`(equipo_id,formapago_id,id,nrocuotas,pago,dias,totalpago,moraxdia,active) VALUES (13,2,1,3,15,2,0,15,1),(12,2,2,2,20,5,0,10,1);

/*Table structure for table `equipodescripcion` */

DROP TABLE IF EXISTS `equipodescripcion`;

CREATE TABLE `equipodescripcion` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `idiomas_id` INT(11) NOT NULL,
  `descripcion` TEXT COLLATE utf8_unicode_ci NOT NULL,
  `equipo_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_equipodescripcion_idiomas1` (`idiomas_id`),
  KEY `fk_equipodescripcion_equipo1` (`equipo_id`),
  CONSTRAINT `fk_equipodescripcion_equipo1` FOREIGN KEY (`equipo_id`) REFERENCES `equipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_equipodescripcion_idiomas1` FOREIGN KEY (`idiomas_id`) REFERENCES `idiomas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `equipodescripcion` */

/*Table structure for table `estadocuota` */

DROP TABLE IF EXISTS `estadocuota`;

CREATE TABLE `estadocuota` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(60) COLLATE utf8_unicode_ci NOT NULL,
  `active` INT(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `estadocuota` */

INSERT  INTO `estadocuota`(id,nombre,active) VALUES (1,'PENDIENTE',1),(2,'CANCELADA',1);

/*Table structure for table `estadoequipo` */

DROP TABLE IF EXISTS `estadoequipo`;

CREATE TABLE `estadoequipo` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` INT(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `estadoequipo` */

INSERT  INTO `estadoequipo`(id,nombre,active) VALUES (1,'Usado',1),(2,'Nuevo',1);

/*Table structure for table `estadooperacion` */

DROP TABLE IF EXISTS `estadooperacion`;

CREATE TABLE `estadooperacion` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` INT(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `estadooperacion` */

INSERT  INTO `estadooperacion`(id,nombre,active) VALUES (1,'Pendiente Pago',1),(2,'Vendidos',1),(3,'Reservado',1),(4,'Pendiente Entrega',1),(5,'Entregado',1);

/*Table structure for table `fabricantes` */

DROP TABLE IF EXISTS `fabricantes`;

CREATE TABLE `fabricantes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(250) COLLATE utf8_unicode_ci NOT NULL,
  `active` INT(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `fabricantes` */

INSERT  INTO `fabricantes`(id,nombre,active) VALUES (2,'Samsung',1),(4,'fabricante',0);

/*Table structure for table `formaenvio` */

DROP TABLE IF EXISTS `formaenvio`;

CREATE TABLE `formaenvio` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `formaenvio` */

/*Table structure for table `formaenvio_has_equipo` */

DROP TABLE IF EXISTS `formaenvio_has_equipo`;

CREATE TABLE `formaenvio_has_equipo` (
  `formaenvio_id` INT(11) NOT NULL,
  `equipo_id` INT(11) NOT NULL,
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_formaenvio_has_equipo_equipo1` (`equipo_id`),
  KEY `fk_formaenvio_has_equipo_formaenvio1` (`formaenvio_id`),
  CONSTRAINT `fk_formaenvio_has_equipo_equipo1` FOREIGN KEY (`equipo_id`) REFERENCES `equipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_formaenvio_has_equipo_formaenvio1` FOREIGN KEY (`formaenvio_id`) REFERENCES `formaenvio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `formaenvio_has_equipo` */

/*Table structure for table `formapago` */

DROP TABLE IF EXISTS `formapago`;

CREATE TABLE `formapago` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(150) COLLATE utf8_unicode_ci NOT NULL,
  `active` INT(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `formapago` */

INSERT  INTO `formapago`(id,nombre,active) VALUES (1,'Efectivo',1),(2,'Credito',1);

/*Table structure for table `idiomas` */

DROP TABLE IF EXISTS `idiomas`;

CREATE TABLE `idiomas` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `prefijo` VARCHAR(10) COLLATE utf8_unicode_ci NOT NULL,
  `active` INT(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `idiomas` */

INSERT  INTO `idiomas`(id,nombre,prefijo,active) VALUES (1,'English','en',1),(2,'Polaco','pl',1),(3,'EspaÃ±ol','es',1),(4,'Aleman','de',1);

/*Table structure for table `imagen` */

DROP TABLE IF EXISTS `imagen`;

CREATE TABLE `imagen` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `equipo_id` INT(11) NOT NULL,
  `nombre` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumb` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagen` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published` INT(11) DEFAULT '0',
  `descripcion` TEXT COLLATE utf8_unicode_ci,
  `order` INT(11) DEFAULT '1',
  `active` INT(11) DEFAULT '1',
  `avatar` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imgequipo` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_imagenes_equipo1` (`equipo_id`),
  CONSTRAINT `fk_imagenes_equipo1` FOREIGN KEY (`equipo_id`) REFERENCES `equipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `imagen` */

INSERT  INTO `imagen`(id,equipo_id,nombre,thumb,imagen,published,descripcion,imagen.order,active,avatar,imgequipo) VALUES (1,13,'imagen1',NULL,'1254.png',1,NULL,1,1,NULL,NULL),(2,13,'imagen2',NULL,'1234.png',1,NULL,1,1,NULL,NULL),(4,14,'testttttttttttttt',NULL,'292a_t.jpg',0,'testttttttttttttttttttt',1,1,NULL,NULL),(5,14,'terts',NULL,'292c.jpg',0,'tsafaaaaa',1,1,NULL,NULL),(6,14,'sdafsdds',NULL,'device-00.png',0,'fsdfsdfds',1,0,NULL,NULL),(7,14,'esta es  su Imagen',NULL,'plano de red.jpg',0,'<p>\r\n	agregando una Imagen</p>\r\n',1,1,NULL,NULL),(8,14,'esto es una imagen',NULL,'plano de red.jpg',0,'dasdasda',1,1,NULL,NULL),(9,14,'esto es una imagen',NULL,NULL,0,'dasdasda',1,1,NULL,NULL),(10,6,'una nueva imagen',NULL,'plano de red.jpg',0,'',1,1,NULL,NULL),(11,6,'aca va la imagen',NULL,'plano de red.jpg',0,'',1,1,NULL,NULL),(12,13,'nueva imagen','nueva-imagen.jpg','nueva-imagen.jpg',0,'',3,1,'nueva-imagen.jpg','nueva-imagen.jpg'),(13,14,'imagen','imagen.jpg','imagen.jpg',0,'',7,1,'imagen.jpg','imagen.jpg'),(14,19,'imagen','imagen-1.jpg','imagen-1.jpg',0,'subiendo archivos',1,1,'imagen-1.jpg','imagen-1.jpg'),(15,21,'Investigation Unit Ear Nose Throat / ORL','investigation-unit-ear-nose-throat-orl.jpg','investigation-unit-ear-nose-throat-orl.jpg',0,'imagen de informacion',1,1,'investigation-unit-ear-nose-throat-orl.jpg','investigation-unit-ear-nose-throat-orl.jpg'),(16,18,'nueva imagen','nueva-imagen.jpg','nueva-imagen.jpg',0,'',1,1,'nueva-imagen.jpg','nueva-imagen.jpg'),(17,22,'imagen 1','imagen-1.jpg','imagen-1.jpg',0,'',1,1,'imagen-1.jpg','imagen-1.jpg');

/*Table structure for table `jos_home` */

DROP TABLE IF EXISTS `jos_home`;

CREATE TABLE `jos_home` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `lang` VARCHAR(10) COLLATE utf8_unicode_ci NOT NULL,
  `country` VARCHAR(2) COLLATE utf8_unicode_ci NOT NULL,
  `type` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL,
  `body` TEXT COLLATE utf8_unicode_ci NOT NULL,
  `active` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MYISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `jos_home` */

INSERT  INTO `jos_home`(id,lang,country,TYPE,body,active) VALUES (1,'es','cl','home','<div><img src=\"images/stories/frontpage-chile.jpg\" border=\"0\" alt=\"MedTechTrade\" style=\"border: 0pt none;\" /></div>\r\n<div class=\"contentheading\" style=\"margin-bottom:10px;\"><br />Bienvenido a MedTechTrade Chile</div>\r\n<p>MedTechTrade es su especialista suizo en la compra y venta a nivel mundial de equipamiento médico usado y reacondicionado.</p>\r\n<p> </p>\r\n<h3>Equipos medicos suizos a su alcanze</h3>\r\n<p><strong><img src=\"images/stories/flag_CH.png\" border=\"0\" alt=\"flag ch\" style=\"border: 0pt none;\" /> MedTechTrade</strong> es una empresa suiza, dedicada a la venta de de equipamento medico reacondicionado de marcas líderes en el mercado mundial.</p>\r\n<p><strong><img src=\"images/stories/flag_CH.png\" border=\"0\" alt=\"flag ch\" style=\"border: 0pt none;\" /> </strong><strong>MedTechTrade</strong> contribuye con equipamiento médico proveniente de Suiza, Alemania y Austria que responde a las exigencias de nuestros clientes, aplicando especial cuidado para que distintos parámetros como calidad, confiabilidad, precisión, flexibilidad y ahorro en costos de mantención, se cumplan.</p>\r\n<p> </p>\r\n<h2><strong>Nuestras top-3 ofertas destacadas: </strong></h2>\r\n<p> </p>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td><img src=\"media/pics/287a.jpg\" border=\"0\" alt=\"Microscopio Zeiss\" width=\"76\" height=\"100\" />..</td>\r\n<td><strong>Fabricante</strong>: Zeiss<br /><strong>Modelo</strong>: Standard 14<br /><strong>Descripción</strong>: Microscopio Zeiss<br /><a href=\"es/microscopios/156/microscop/287\">Link...</a></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td>.</td>\r\n</tr>\r\n<tr>\r\n<td><img src=\"media/pics/179a.jpg\" border=\"0\" alt=\"Monitor de pacientes\" width=\"77\" height=\"100\" />..</td>\r\n<td><strong>Fabricante</strong>: Marquette<br /><strong>Modelo</strong>: Solar 8000/Tram250sl <br /><strong>Descripción</strong>: Equipo de monitoreo de pacientes<br /><a href=\"es/equipos-de-monitoreo/159/critical-care-monitoring/179\">Link...</a></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td>.</td>\r\n</tr>\r\n<tr>\r\n<td><img src=\"media/pics/228c.jpg\" border=\"0\" alt=\"Set cirugia de oido\" width=\"53\" height=\"100\" />..</td>\r\n<td><strong>Fabricante</strong>: Karl Storz<br /><strong>Descripción</strong>: Instrumetal Quirúrgico para cirugía de oido<br /><a href=\"es/instrumental-quirurgico/158/ear-surgery-set/228\">Link...</a></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td><br /><strong> </strong></td>\r\n</tr>\r\n</tbody>\r\n</table>',1),(2,'es','cl','about','<p><span class=\"contentheading\">Sobre nosotros</span></p>\r\n<p>MedTechTrade es una empresa internacional con origen en Suiza y se dedica a la compra y venta de equipos medicos nuevos y reacondicionados.</p>\r\n<p>Nosotros nos especializamos en encontrar los equipos médicos que usted necesitan de alta calida a precios accesibles. Nuestro objetivo es facilitar la acquisión de equipos medicos a travéz de nuestra plataforma. Ofrecemos una plataforma de compra y venta única, donde se puede comparar entre los modelos y fabricantes más reconocidos para ahorar tiempo y dinero.</p>\r\n<p>Para asegurarnos de la satisfacción de nuestros clientes, nosotros seguimos el estándar más alto de calidad y empleamos solo profesionales altamente calificados y entrenados para reacondicionar cada equipo de acuerdo con las especificaciones originales del fabricante.<br /><strong><br />Todos nuestros equipos son acompañados de una garantía.</strong></p>\r\n<p><strong><br /></strong></p>\r\n<h3>Nuestro Equipo</h3>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 260px; padding-top: 10px;\">\r\n<p><img src=\"images/stories/cl-roland.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; border: 0pt none; margin-right:5px;\" /><strong>Roland Suttner</strong><br />Gerente General<br />Chile<br /><br /></p>\r\n</td>\r\n<td style=\"width: 260px; padding-top: 10px;\"><img src=\"images/stories/cl-darwin.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; border: 0pt none; margin-right:5px;\" /><strong>Darwin Lagos Veloso</strong><br />Ventas y Servicio Técnico<br />Chile<br /></td>\r\n</tr>\r\n<tr>\r\n<td style=\"padding-top: 10px;\"><img src=\"images/stories/cl-rosanna.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; border: 0pt none; margin-right:5px;\" /><strong>Rossana Díaz</strong><br />Logistica y Administración<br />Chile<br /></td>\r\n<td style=\"padding-top: 10px;\">\r\n<p> </p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p> </p>\r\n<hr />\r\n<p> </p>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 260px; padding-top: 10px;\"><img src=\"images/stories/ch-thomas.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; margin-right: 5px; border: 0pt none;\" /><strong>Thomas Zaugg</strong><br />Gerente General<br />Suiza<br /><br /></td>\r\n<td style=\"width: 260px; padding-top: 10px;\"><img src=\"images/stories/ch-erika.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; margin-right: 5px; border: 0pt none;\" /><strong>Erika Günther</strong><br />Logistica y Administración<br />Suiza<br /></td>\r\n</tr>\r\n<tr>\r\n<td style=\"padding-top: 10px;\"><img src=\"images/stories/ch-margareta.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; margin-right: 5px; border: 0pt none;\" /><strong>Margareta Deszynski</strong><br />Ventas y Marketing<br />Suiza<br /></td>\r\n<td style=\"padding-top: 10px;\">\r\n<p> </p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>',1),(3,'es','pe','home','<div><img src=\"images/stories/frontpage-peru.png\" border=\"0\" alt=\"MedTechTrade\" style=\"border: 0pt none;\" /></div>\r\n<div class=\"contentheading\" style=\"margin-bottom:10px;\"><br />Bienvenido a MedTechTrade Perú</div>\r\n<p>MedTechTrade es su especialista suizo en la compra y venta a nivel mundial de equipamiento médico usado y reacondicionado.</p>\r\n<p> </p>\r\n<h3>Equipos medicos suizos a su alcanze</h3>\r\n<p><strong><img src=\"images/stories/flag_CH.png\" border=\"0\" alt=\"flag ch\" style=\"border: 0pt none;\" /> MedTechTrade</strong> es una empresa suiza, dedicada a la venta de de equipamento medico reacondicionado de marcas líderes en el mercado mundial.</p>\r\n<p><strong><img src=\"images/stories/flag_CH.png\" border=\"0\" alt=\"flag ch\" style=\"border: 0pt none;\" /> </strong><strong>MedTechTrade</strong> contribuye con equipamiento médico proveniente de Suiza, Alemania y Austria que responde a las exigencias de nuestros clientes, aplicando especial cuidado para que distintos parámetros como calidad, confiabilidad, precisión, flexibilidad y ahorro en costos de mantención, se cumplan.</p>\r\n<p> </p>\r\n<h2><strong>Nuestras top-3 ofertas destacadas: </strong></h2>\r\n<p> </p>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td><img src=\"media/pics/287a.jpg\" border=\"0\" alt=\"Microscopio Zeiss\" width=\"76\" height=\"100\" />..</td>\r\n<td><strong>Fabricante</strong>: Zeiss<br /><strong>Modelo</strong>: Standard 14<br /><strong>Descripción</strong>: Microscopio Zeiss<br /><a href=\"es/microscopios/156/microscop/287\">Link...</a></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td>.</td>\r\n</tr>\r\n<tr>\r\n<td><img src=\"media/pics/179a.jpg\" border=\"0\" alt=\"Monitor de pacientes\" width=\"77\" height=\"100\" />..</td>\r\n<td><strong>Fabricante</strong>: Marquette<br /><strong>Modelo</strong>: Solar 8000/Tram250sl <br /><strong>Descripción</strong>: Equipo de monitoreo de pacientes<br /><a href=\"es/equipos-de-monitoreo/159/critical-care-monitoring/179\">Link...</a></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td>.</td>\r\n</tr>\r\n<tr>\r\n<td><img src=\"media/pics/228c.jpg\" border=\"0\" alt=\"Set cirugia de oido\" width=\"53\" height=\"100\" />..</td>\r\n<td><strong>Fabricante</strong>: Karl Storz<br /><strong>Descripción</strong>: Instrumetal Quirúrgico para cirugía de oido<br /><a href=\"es/instrumental-quirurgico/158/ear-surgery-set/228\">Link...</a></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td><br /><strong> </strong></td>\r\n</tr>\r\n</tbody>\r\n</table>',1),(4,'es','pe','about','<p><span class=\"contentheading\">Sobre nosotros</span></p>\r\n<p>MedTechTrade es una empresa internacional con origen en Suiza y se dedica a la compra y venta de equipos medicos nuevos y reacondicionados.</p>\r\n<p>Nosotros nos especializamos en encontrar los equipos médicos que usted necesitan de alta calida a precios accesibles. Nuestro objetivo es facilitar la acquisión de equipos medicos a travéz de nuestra plataforma. Ofrecemos una plataforma de compra y venta única, donde se puede comparar entre los modelos y fabricantes más reconocidos para ahorar tiempo y dinero.</p>\r\n<p>Para asegurarnos de la satisfacción de nuestros clientes, nosotros seguimos el estándar más alto de calidad y empleamos solo profesionales altamente calificados y entrenados para reacondicionar cada equipo de acuerdo con las especificaciones originales del fabricante.<br /><strong><br />Todos nuestros equipos son acompañados de una garantía.</strong></p>\r\n<p><strong><br /></strong></p>\r\n<h3>Nuestro Equipo</h3>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 260px; padding-top: 10px;\">\r\n<p><img src=\"images/stories/cl-roland.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; border: 0pt none; margin-right:5px;\" /><strong>Roland Suttner</strong><br />Gerente General<br />Chile<br /><br /></p>\r\n</td>\r\n<td style=\"width: 260px; padding-top: 10px;\"><img src=\"images/stories/cl-darwin.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; border: 0pt none; margin-right:5px;\" /><strong>Darwin Lagos Veloso</strong><br />Ventas y Servicio Técnico<br />Chile<br /></td>\r\n</tr>\r\n<tr>\r\n<td style=\"padding-top: 10px;\"><img src=\"images/stories/cl-rosanna.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; border: 0pt none; margin-right:5px;\" /><strong>Rossana Díaz</strong><br />Logistica y Administración<br />Chile<br /></td>\r\n<td style=\"padding-top: 10px;\">\r\n<p> </p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p> </p>\r\n<hr />\r\n<p> </p>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 260px; padding-top: 10px;\"><img src=\"images/stories/ch-thomas.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; margin-right: 5px; border: 0pt none;\" /><strong>Thomas Zaugg</strong><br />Gerente General<br />Suiza<br /><br /></td>\r\n<td style=\"width: 260px; padding-top: 10px;\"><img src=\"images/stories/ch-erika.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; margin-right: 5px; border: 0pt none;\" /><strong>Erika Günther</strong><br />Logistica y Administración<br />Suiza<br /></td>\r\n</tr>\r\n<tr>\r\n<td style=\"padding-top: 10px;\"><img src=\"images/stories/ch-margareta.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; margin-right: 5px; border: 0pt none;\" /><strong>Margareta Deszynski</strong><br />Ventas y Marketing<br />Suiza<br /></td>\r\n<td style=\"padding-top: 10px;\">\r\n<p> </p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>',1),(5,'de','ch','home','<div><img src=\"images/stories/frontpage-image.jpg\" border=\"0\" alt=\"MedTechTrade\" /></div>\r\n<div class=\"contentheading\" style=\"margin-bottom:10px;\"><br />Willkommen</div>\r\n<p><strong>MedTechTrade</strong> ist eine weltweite Handelsorganisation von medizinischen Geräten für Firmen und Organisationen. Unser Ziel ist es den Handel von medizinischen Geräten zu ermöglichen und zu beschleunigen.</p>\r\n<p><strong>Bitte wählen Sie Ihr Interessengebiet:</strong></p>\r\n<p><a href=\"index.php?option=com_content&amp;view=article&amp;id=68&amp;Itemid=56&amp;lang=de\"><img src=\"images/stories/bot1.png\" border=\"0\" alt=\"Medizintechnik verkaufen\" width=\"251\" height=\"102\" /></a><a href=\"index.php?option=com_adsmanager&amp;page=front&amp;Itemid=170&amp;lang=de\"><img src=\"images/stories/bot2.png\" border=\"0\" alt=\"Medizintechnik verkaufen\" width=\"251\" height=\"102\" style=\"border: 0pt none; margin-left:18px;\" /></a></p>\r\n<p> </p>\r\n<p> </p>\r\n<hr />\r\n<p> </p>\r\n<h2><strong>Die aktuellen 3 Top-Angebote:</strong></h2>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td><img src=\"media/pics/235a.jpg\" border=\"0\" alt=\"Freezer\" width=\"73\" height=\"100\" /></td>\r\n<td><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Hersteller: </span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Thermo Scientific<strong><br /> </strong></span><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Modell: </span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">ULT2586-10-V</span><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"><br /> </span></strong><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Beschreibung:</span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"> Ultra-Low Temperature Upright Freezers, Volumen 691l, Temperature range: from -50°C until -86°C<br /><a href=\"en/coolers-and-freezers/164/ultra-low-temperature-upright-freezers/236\">Link...</a></span></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td>.</td>\r\n</tr>\r\n<tr>\r\n<td><img src=\"media/pics/179a.jpg\" border=\"0\" alt=\"Solar 8000\" width=\"77\" height=\"100\" /></td>\r\n<td><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Hersteller: Marquette</span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"><strong><br /> </strong></span><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Modell: </span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Solar 8000/Tram 250sl</span><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"><br /> </span></strong><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Beschreibung:</span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"> Patient Monitors<br /><a href=\"en/monitoring-devices/159/critical-care-monitoring/179\">Link...</a></span></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td>.</td>\r\n</tr>\r\n<tr>\r\n<td><img src=\"media/pics/229a.jpg\" border=\"0\" alt=\"Stapedektomie Set\" width=\"75\" height=\"100\" style=\"border: 0px none currentColor;\" /></td>\r\n<td><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Hersteller: </span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Karl Storz</span><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"><br /> </span></strong><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Beschreibung:</span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"> Chirurgie Stapedektomie Set</span> <br /><a href=\"en/chirurgical-devices/158/surgery-stapedectomie-set-karl-storz/229\"><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Link...</span></a></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"><br /></span></strong></td>\r\n</tr>\r\n</tbody>\r\n</table>',1),(6,'de','ch','about','<p><span class=\"contentheading\">Über uns</span></p>\r\n<p>MedTechTrade ist eine Schweizer Firma, welche erneuerte Geräte aus zweiter Hand von führenden Medizintechnikherstellern weltweit anbietet.</p>\r\n<p>Wir liefern medizinische Geräte aus der Schweiz, Deutschland, Österreich und anderen europäischen Ländern ganz auf die Bedürfnisse unserer Kunden zugeschnitten.</p>\r\n<p>Wir legen besonderen Wert auf Qualität, Zuverlässigkeit, Flexibilität und können Ihnen Kosteneinsparung bei der Anschaffung von Geräten sowie beim technischen Service bieten.</p>\r\n<p>Unsere Kunden sind Krankenhäuser, Labore und medizinische Forschungseinrichtungen. Wir sind direkt für Sie da in der Schweiz, Polen, Peru und Chile.</p>\r\n<p><strong>Bei Interesse kontaktieren Sie uns noch heut.</strong></p>\r\n<p><strong><br /></strong></p>\r\n<h3>Unser Team</h3>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 260px; padding-top: 10px;\"><img src=\"images/stories/ch-thomas.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; margin-right: 5px; border: 0pt none;\" /><strong>Thomas Zaugg</strong><br />General Manager<br />Schweiz<br /><br /></td>\r\n<td style=\"width: 260px; padding-top: 10px;\"><img src=\"images/stories/ch-erika.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; margin-right: 5px; border: 0pt none;\" /><strong>Erika Günther</strong><br />Backoffice &amp; Customer Support<br />Schweiz<br /></td>\r\n</tr>\r\n<tr>\r\n<td style=\"padding-top: 10px;\"><img src=\"images/stories/ch-margareta.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; margin-right: 5px; border: 0pt none;\" /><strong>Margareta Deszynski</strong><br /><span lang=\"EN-GB\">Sales Support</span><br />Schweiz<br /></td>\r\n<td style=\"padding-top: 10px;\">\r\n<p> </p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p> </p>\r\n<hr />\r\n<p> </p>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 260px; padding-top: 10px;\">\r\n<p><img src=\"images/stories/cl-roland.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; border: 0pt none; margin-right:5px;\" /><strong>Roland Suttner</strong><br />General Manager<br />Chile<br /><br /></p>\r\n</td>\r\n<td style=\"width: 260px; padding-top: 10px;\"><img src=\"images/stories/cl-darwin.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; border: 0pt none; margin-right:5px;\" /><strong>Darwin Lagos Veloso</strong><br />Sales Support und technischer Service<br />Chile<br /></td>\r\n</tr>\r\n<tr>\r\n<td style=\"padding-top: 10px;\"><img src=\"images/stories/cl-rosanna.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; border: 0pt none; margin-right:5px;\" /><strong>Rossana Díaz</strong><br />Logistik und Administration<br />Chile<br /></td>\r\n<td style=\"padding-top: 10px;\">\r\n<p> </p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>',1),(7,'en','pl','home','<div><img src=\"images/stories/frontpage-poland.png\" border=\"0\" alt=\"MedTechTrade\" style=\"border: 0pt none;\" /></div>\r\n<div class=\"contentheading\" style=\"margin-bottom:10px;\"><br />Welcome</div>\r\n<p><strong>MedTechTrade</strong> is a global trade association for technology manufacturers, research institutions, allied professional services and development organizations with shared interests in collaborative developing technologies, products and services for the global medical products marketplace.</p>\r\n<p>Our goal is to enable and accelerate trading of medical devices for companies and institutional organisation.</p>\r\n<p> </p>\r\n<h2>Top 3 offers:</h2>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td><img src=\"media/pics/235a.jpg\" border=\"0\" alt=\"Freezer\" width=\"73\" height=\"100\" />..</td>\r\n<td><strong>Manufacturer</strong>: Thermo Scientific<br /><strong>Product name</strong>: Ultra-Low Temperature Upright Freezers<br /><strong>Model</strong>: ULT2586-10-V<br /><a href=\"en/coolers-and-freezers/164/ultra-low-temperature-upright-freezers/236\"><strong>Link...</strong></a></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td>.</td>\r\n</tr>\r\n<tr>\r\n<td><img src=\"media/pics/179a.jpg\" border=\"0\" alt=\"Patient Monitor\" width=\"77\" height=\"100\" />..</td>\r\n<td><strong>Manufacturer</strong>: GE Healthcare<br /><strong>Product name:</strong> Critical Care Monitoring<br /><strong>Model</strong>: Solar 8000/Tram 250sl<br /><a href=\"en/monitoring-devices/159/critical-care-monitoring/179\"><strong>Link...</strong></a></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td>.</td>\r\n</tr>\r\n<tr>\r\n<td><img src=\"media/pics/229a.jpg\" border=\"0\" alt=\"Stapedectomie set\" width=\"75\" height=\"100\" style=\"border: 0px none currentColor;\" />..</td>\r\n<td><strong>Manufacturer</strong>: Karl Storz<br /><strong>Product name</strong>: Surgery Stapedectomie set Karl Storz<br /><a href=\"en/chirurgical-devices/158/surgery-stapedectomie-set-karl-storz/229\"><strong>Link...</strong></a></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td><br /><strong> </strong></td>\r\n</tr>\r\n</tbody>\r\n</table>',1),(8,'en','pl','about','<p><span class=\"contentheading\">About Us</span></p>\r\n<p>MedTechTrade is a Swiss Company dedicated to the sale of pre-owned and refurbished medical devices of leading brands in the international market.</p>\r\n<p>MedTechTrade is providing medical devices coming from Switzerland, Germany, Austria and other European countries and is responding to the needs of our clients. We do put special emphasis on our main values which are quality, reliability, precision, flexibility and last but not least cost savings at purchase as well as at maintenance.</p>\r\n<p>MedTechTrade clients are hospitals, laboratories, and medical investigation companies and it has representations in Switzerland, Poland, Peru and Chile.</p>\r\n<p><strong>Feel free to get in contact with us now.</strong></p>\r\n<p><strong><br /></strong></p>\r\n<h3>Our Team</h3>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 260px; padding-top: 10px;\"><img src=\"images/stories/ch-thomas.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; margin-right: 5px; border: 0pt none;\" /><strong>Thomas Zaugg</strong><br />General Manager<br />Switzerland<br /><br /></td>\r\n<td style=\"width: 260px; padding-top: 10px;\"><img src=\"images/stories/ch-erika.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; margin-right: 5px; border: 0pt none;\" /><strong>Erika Günther</strong><br />Backoffice &amp; Customer Support<br />Switzerland<br /></td>\r\n</tr>\r\n<tr>\r\n<td style=\"padding-top: 10px;\"><img src=\"images/stories/ch-margareta.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; margin-right: 5px; border: 0pt none;\" /><strong>Margareta Deszynski</strong><br /><span lang=\"EN-GB\">Sales Support</span><br />Switzerland<br /></td>\r\n<td style=\"padding-top: 10px;\">\r\n<p> </p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p> </p>\r\n<hr />\r\n<p> </p>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 260px; padding-top: 10px;\">\r\n<p><img src=\"images/stories/cl-roland.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; border: 0pt none; margin-right:5px;\" /><strong>Roland Suttner</strong><br />General Manager<br />Chile<br /><br /></p>\r\n</td>\r\n<td style=\"width: 260px; padding-top: 10px;\"><img src=\"images/stories/cl-darwin.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; border: 0pt none; margin-right:5px;\" /><strong>Darwin Lagos Veloso</strong><br />Sales Support and technical Service<br />Chile<br /></td>\r\n</tr>\r\n<tr>\r\n<td style=\"padding-top: 10px;\"><img src=\"images/stories/cl-rosanna.jpg\" border=\"0\" width=\"90\" height=\"110\" style=\"float: left; border: 0pt none; margin-right:5px;\" /><strong>Rossana Díaz</strong><br />Logistics and Administrations<br />Chile<br /></td>\r\n<td style=\"padding-top: 10px;\">\r\n<p> </p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>',1),(9,'en','us','home','<div><img src=\"images/stories/frontpage-image.jpg\" border=\"0\" alt=\"MedTechTrade\" /></div>\r\n<div class=\"contentheading\" style=\"margin-bottom:10px;\"><br />Welcome</div>\r\n<p><strong>MedTechTrade</strong> is a global trade association for technology manufacturers, research institutions, allied professional services and development organizations with shared interests in collaborative developing technologies, products and services for the global medical products marketplace.</p>\r\n\r\n<p> </p>\r\n<hr />\r\n<p> </p>\r\n<h2><strong>Top 3 offers::</strong></h2>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td><img src=\"media/pics/235a.jpg\" border=\"0\" alt=\"Freezer\" width=\"73\" height=\"100\" /></td>\r\n<td><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Hersteller: </span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Thermo Scientific<strong><br /> </strong></span><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Modell: </span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">ULT2586-10-V</span><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"><br /> </span></strong><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Beschreibung:</span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"> Ultra-Low Temperature Upright Freezers, Volumen 691l, Temperature range: from -50°C until -86°C<br /><a href=\"en/coolers-and-freezers/164/ultra-low-temperature-upright-freezers/236\">Link...</a></span></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td>.</td>\r\n</tr>\r\n<tr>\r\n<td><img src=\"media/pics/179a.jpg\" border=\"0\" alt=\"Solar 8000\" width=\"77\" height=\"100\" /></td>\r\n<td><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Hersteller: Marquette</span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"><strong><br /> </strong></span><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Modell: </span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Solar 8000/Tram 250sl</span><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"><br /> </span></strong><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Beschreibung:</span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"> Patient Monitors<br /><a href=\"en/monitoring-devices/159/critical-care-monitoring/179\">Link...</a></span></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td>.</td>\r\n</tr>\r\n<tr>\r\n<td><img src=\"media/pics/229a.jpg\" border=\"0\" alt=\"Stapedektomie Set\" width=\"75\" height=\"100\" style=\"border: 0px none currentColor;\" /></td>\r\n<td><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Hersteller: </span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Karl Storz</span><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"><br /> </span></strong><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Beschreibung:</span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"> Chirurgie Stapedektomie Set</span> <br /><a href=\"en/chirurgical-devices/158/surgery-stapedectomie-set-karl-storz/229\"><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Link...</span></a></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"><br /></span></strong></td>\r\n</tr>\r\n</tbody>\r\n</table>',0),(10,'de','us','home','<div><img src=\"images/stories/frontpage-image.jpg\" border=\"0\" alt=\"MedTechTrade\" /></div>\r\n<div class=\"contentheading\" style=\"margin-bottom:10px;\"><br />Willkommen</div>\r\n<p><strong>MedTechTrade</strong> ist eine weltweite Handelsorganisation von medizinischen Geräten für Firmen und Organisationen. Unser Ziel ist es den Handel von medizinischen Geräten zu ermöglichen und zu beschleunigen.</p>\r\n<p><strong>Bitte wählen Sie Ihr Interessengebiet:</strong></p>\r\n<p><a href=\"index.php?option=com_content&amp;view=article&amp;id=68&amp;Itemid=56&amp;lang=de\"><img src=\"images/stories/bot1.png\" border=\"0\" alt=\"Medizintechnik verkaufen\" width=\"251\" height=\"102\" /></a><a href=\"index.php?option=com_adsmanager&amp;page=front&amp;Itemid=170&amp;lang=de\"><img src=\"images/stories/bot2.png\" border=\"0\" alt=\"Medizintechnik verkaufen\" width=\"251\" height=\"102\" style=\"border: 0pt none; margin-left:18px;\" /></a></p>\r\n<p> </p>\r\n<p> </p>\r\n<hr />\r\n<p> </p>\r\n<h2><strong>Die aktuellen 3 Top-Angebote:</strong></h2>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td><img src=\"media/pics/235a.jpg\" border=\"0\" alt=\"Freezer\" width=\"73\" height=\"100\" /></td>\r\n<td><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Hersteller: </span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Thermo Scientific<strong><br /> </strong></span><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Modell: </span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">ULT2586-10-V</span><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"><br /> </span></strong><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Beschreibung:</span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"> Ultra-Low Temperature Upright Freezers, Volumen 691l, Temperature range: from -50°C until -86°C<br /><a href=\"en/coolers-and-freezers/164/ultra-low-temperature-upright-freezers/236\">Link...</a></span></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td>.</td>\r\n</tr>\r\n<tr>\r\n<td><img src=\"media/pics/179a.jpg\" border=\"0\" alt=\"Solar 8000\" width=\"77\" height=\"100\" /></td>\r\n<td><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Hersteller: Marquette</span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"><strong><br /> </strong></span><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Modell: </span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Solar 8000/Tram 250sl</span><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"><br /> </span></strong><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Beschreibung:</span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"> Patient Monitors<br /><a href=\"en/monitoring-devices/159/critical-care-monitoring/179\">Link...</a></span></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td>.</td>\r\n</tr>\r\n<tr>\r\n<td><img src=\"media/pics/229a.jpg\" border=\"0\" alt=\"Stapedektomie Set\" width=\"75\" height=\"100\" style=\"border: 0px none currentColor;\" /></td>\r\n<td><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Hersteller: </span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Karl Storz</span><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"><br /> </span></strong><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Beschreibung:</span></strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"> Chirurgie Stapedektomie Set</span> <br /><a href=\"en/chirurgical-devices/158/surgery-stapedectomie-set-karl-storz/229\"><span style=\"font-size: 10pt; line-height: 115%; font-family: \">Link...</span></a></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td><strong><span style=\"font-size: 10pt; line-height: 115%; font-family: \"><br /></span></strong></td>\r\n</tr>\r\n</tbody>\r\n</table>',0),(11,'es','us','home','<div><img src=\"images/stories/frontpage-image.jpg\" border=\"0\" alt=\"MedTechTrade\" style=\"border: 0pt none;\" /></div>\r\n<div class=\"contentheading\" style=\"margin-bottom:10px;\"><br />Bienvenido a MedTechTrade</div>\r\n<p>MedTechTrade es su especialista suizo en la compra y venta a nivel mundial de equipamiento médico usado y reacondicionado.</p>\r\n<p> </p>\r\n<h3>Equipos medicos suizos a su alcanze</h3>\r\n<p><strong><img src=\"images/stories/flag_CH.png\" border=\"0\" alt=\"flag ch\" style=\"border: 0pt none;\" /> MedTechTrade</strong> es una empresa suiza, dedicada a la venta de de equipamento medico reacondicionado de marcas líderes en el mercado mundial.</p>\r\n<p><strong><img src=\"images/stories/flag_CH.png\" border=\"0\" alt=\"flag ch\" style=\"border: 0pt none;\" /> </strong><strong>MedTechTrade</strong> contribuye con equipamiento médico proveniente de Suiza, Alemania y Austria que responde a las exigencias de nuestros clientes, aplicando especial cuidado para que distintos parámetros como calidad, confiabilidad, precisión, flexibilidad y ahorro en costos de mantención, se cumplan.</p>\r\n<p> </p>\r\n<h2><strong>Nuestras top-3 ofertas destacadas: </strong></h2>\r\n<p> </p>\r\n<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td><img src=\"media/pics/287a.jpg\" border=\"0\" alt=\"Microscopio Zeiss\" width=\"76\" height=\"100\" />..</td>\r\n<td><strong>Fabricante</strong>: Zeiss<br /><strong>Modelo</strong>: Standard 14<br /><strong>Descripción</strong>: Microscopio Zeiss<br /><a href=\"es/microscopios/156/microscop/287\">Link...</a></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td>.</td>\r\n</tr>\r\n<tr>\r\n<td><img src=\"media/pics/179a.jpg\" border=\"0\" alt=\"Monitor de pacientes\" width=\"77\" height=\"100\" />..</td>\r\n<td><strong>Fabricante</strong>: Marquette<br /><strong>Modelo</strong>: Solar 8000/Tram250sl <br /><strong>Descripción</strong>: Equipo de monitoreo de pacientes<br /><a href=\"es/equipos-de-monitoreo/159/critical-care-monitoring/179\">Link...</a></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td>.</td>\r\n</tr>\r\n<tr>\r\n<td><img src=\"media/pics/228c.jpg\" border=\"0\" alt=\"Set cirugia de oido\" width=\"53\" height=\"100\" />..</td>\r\n<td><strong>Fabricante</strong>: Karl Storz<br /><strong>Descripción</strong>: Instrumetal Quirúrgico para cirugía de oido<br /><a href=\"es/instrumental-quirurgico/158/ear-surgery-set/228\">Link...</a></td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td><br /><strong> </strong></td>\r\n</tr>\r\n</tbody>\r\n</table>',0);

/*Table structure for table `moneda` */

DROP TABLE IF EXISTS `moneda`;

CREATE TABLE `moneda` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `simbolo` VARCHAR(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prefijo` VARCHAR(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` INT(11) NOT NULL DEFAULT '1',
  `cambio` DECIMAL(8,3) DEFAULT NULL,
  `fechacambio` DATE DEFAULT NULL,
  `codepais` VARCHAR(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `moneda` */

INSERT  INTO `moneda`(id,nombre,simbolo,prefijo,active,cambio,fechacambio,codepais) VALUES (1,'Swiss Francs','Fr',NULL,1,NULL,NULL,'CH'),(2,'US Dollars','$',NULL,1,NULL,NULL,'US'),(3,'Peruvian Nuevo Sol','S/.',NULL,1,NULL,NULL,'PE'),(4,'Chilean Peso','$',NULL,1,NULL,NULL,'CL'),(5,'Euros','â‚¬',NULL,1,NULL,NULL,'DE'),(6,'Polish new Zloty','zÅ‚',NULL,1,NULL,NULL,'PL');

/*Table structure for table `operacion` */

DROP TABLE IF EXISTS `operacion`;

CREATE TABLE `operacion` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `estadooperacion_id` INT(11) NOT NULL,
  `fecha` DATETIME NOT NULL,
  `usuario_id` INT(11) NOT NULL,
  `fechainicio` DATETIME NOT NULL,
  `fechapago` DATETIME DEFAULT NULL,
  `total` FLOAT(8,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_operacion_estadooperacion1` (`estadooperacion_id`),
  KEY `fk_operacion_usuario1` (`usuario_id`),
  CONSTRAINT `fk_operacion_estadooperacion1` FOREIGN KEY (`estadooperacion_id`) REFERENCES `estadooperacion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_operacion_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `operacion` */

INSERT  INTO `operacion`(id,estadooperacion_id,fecha,usuario_id,fechainicio,fechapago,total) VALUES (1,2,'2011-09-22 11:28:37',6,'2011-09-22 11:28:37','2011-09-22 11:28:37',NULL),(2,2,'2011-09-24 11:15:54',6,'2011-09-24 11:15:54','2011-09-24 11:15:54',NULL),(3,2,'2011-09-24 11:22:58',5,'2011-09-24 11:22:58','2011-09-24 11:22:58',NULL),(6,2,'0000-00-00 00:00:00',6,'0000-00-00 00:00:00',NULL,NULL),(7,2,'2011-10-20 00:00:00',6,'2011-10-20 00:00:00',NULL,NULL),(8,2,'2011-10-20 00:00:00',6,'2011-10-20 00:00:00',NULL,NULL),(9,2,'2011-10-20 00:00:00',6,'2011-10-20 00:00:00',NULL,NULL),(10,2,'2011-10-20 00:00:00',6,'2011-10-20 00:00:00',NULL,NULL),(11,2,'2011-10-20 00:00:00',6,'2011-10-20 00:00:00',NULL,NULL),(12,2,'2011-10-20 00:00:00',6,'2011-10-20 00:00:00',NULL,NULL),(13,2,'2011-10-20 00:00:00',6,'2011-10-20 00:00:00',NULL,NULL),(14,2,'2011-10-20 00:00:00',6,'2011-10-20 00:00:00',NULL,NULL),(15,2,'2011-10-20 00:00:00',6,'2011-10-20 00:00:00',NULL,NULL),(16,2,'2011-10-20 00:00:00',6,'2011-10-20 00:00:00',NULL,NULL),(17,2,'2011-10-20 00:00:00',6,'2011-10-20 00:00:00',NULL,NULL),(18,2,'2011-10-20 00:00:00',6,'2011-10-20 00:00:00',NULL,NULL),(19,2,'2011-10-20 00:00:00',6,'2011-10-20 00:00:00',NULL,NULL),(20,2,'2011-10-20 00:00:00',6,'2011-10-20 00:00:00',NULL,NULL),(21,2,'2011-10-20 00:00:00',6,'2011-10-20 00:00:00',NULL,NULL),(22,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(23,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(24,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(25,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(26,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(27,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(28,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(29,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(30,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(31,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(32,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(33,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(34,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(35,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(36,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(37,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(38,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(39,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(40,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(41,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(42,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(43,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(44,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(45,2,'2011-10-21 00:00:00',6,'2011-10-21 00:00:00',NULL,NULL),(46,2,'2011-10-25 15:22:37',6,'2011-10-25 15:22:37',NULL,5.00),(47,2,'2011-10-25 15:25:52',6,'2011-10-25 15:25:52',NULL,120.00),(48,2,'2011-10-26 15:44:22',3,'2011-10-26 15:44:22',NULL,58.00),(49,2,'2011-10-26 15:58:37',3,'2011-10-26 15:58:37',NULL,14.00);

/*Table structure for table `operacion_envios` */

DROP TABLE IF EXISTS `operacion_envios`;

CREATE TABLE `operacion_envios` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `operacion_id` INT(11) NOT NULL,
  `descripcion` VARCHAR(120) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` DATETIME NOT NULL,
  `active` INT(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_operacion1` (`operacion_id`),
  CONSTRAINT `fk_operacion1` FOREIGN KEY (`operacion_id`) REFERENCES `operacion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `operacion_envios` */

/*Table structure for table `operacion_has_equipo` */

DROP TABLE IF EXISTS `operacion_has_equipo`;

CREATE TABLE `operacion_has_equipo` (
  `operacion_id` INT(11) NOT NULL,
  `equipo_id` INT(11) NOT NULL,
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `precio` FLOAT DEFAULT NULL,
  `cantidad` INT(11) NOT NULL,
  `equipo_has_formapago_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_operacion_has_equipo_equipo1` (`equipo_id`),
  KEY `fk_operacion_has_equipo_operacion1` (`operacion_id`),
  KEY `fk_operacion_has_equipo_equipo_has_formapago1` (`equipo_has_formapago_id`),
  CONSTRAINT `fk_operacion_has_equipo_equipo1` FOREIGN KEY (`equipo_id`) REFERENCES `equipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_operacion_has_equipo_equipo_has_formapago1` FOREIGN KEY (`equipo_has_formapago_id`) REFERENCES `equipo_has_formapago` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_operacion_has_equipo_operacion1` FOREIGN KEY (`operacion_id`) REFERENCES `operacion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `operacion_has_equipo` */

INSERT  INTO `operacion_has_equipo`(operacion_id,equipo_id,id,precio,cantidad,equipo_has_formapago_id) VALUES (1,13,4,158,2,1),(2,12,5,258,1,1),(3,13,6,158,1,1),(24,3,8,123,0,2),(24,10,9,9,0,1),(24,13,10,58,0,2),(24,11,11,5,0,2),(25,3,12,123,0,2),(25,10,13,9,0,1),(25,13,14,58,0,2),(25,11,15,5,0,2),(26,3,16,123,0,2),(26,10,17,9,0,1),(26,13,18,58,0,2),(26,11,19,5,0,2),(29,3,20,123,0,2),(33,3,21,123,0,2),(36,11,22,5,0,2),(36,10,23,9,0,2),(39,3,24,123,0,2),(46,11,25,5,0,2),(47,6,26,120,0,2),(48,13,27,58,0,2),(49,12,28,14,0,2);

/*Table structure for table `page` */

DROP TABLE IF EXISTS `page`;

CREATE TABLE `page` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(200) CHARACTER SET latin1 NOT NULL,
  `fecha` DATETIME DEFAULT NULL,
  `active` INT(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `page` */

INSERT  INTO `page`(id,nombre,fecha,active) VALUES (1,'Welcome',NULL,1),(2,'Services',NULL,1),(3,'Sell n Trade',NULL,1),(4,'About Us',NULL,1);

/*Table structure for table `pagina` */

DROP TABLE IF EXISTS `pagina`;

CREATE TABLE `pagina` (
  `idpagina` INT(11) NOT NULL AUTO_INCREMENT,
  `titulo` TEXT COLLATE utf8_unicode_ci NOT NULL,
  `body` TEXT COLLATE utf8_unicode_ci NOT NULL,
  `page_id` INT(11) NOT NULL,
  `active` INT(11) DEFAULT '1',
  `idiomas_id` INT(11) NOT NULL,
  `paises_id` INT(11) NOT NULL,
  PRIMARY KEY (`idpagina`),
  KEY `fk_pagina_idiomas1` (`idiomas_id`),
  KEY `fk_pagina_paises1` (`paises_id`),
  KEY `FK_pagina1` (`page_id`),
  CONSTRAINT `FK_pagina1` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pagina_idiomas1` FOREIGN KEY (`idiomas_id`) REFERENCES `idiomas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pagina_paises1` FOREIGN KEY (`paises_id`) REFERENCES `paises` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `pagina` */

INSERT  INTO `pagina`(idpagina,titulo,body,page_id,active,idiomas_id,paises_id) VALUES (2,'welcome','aca ira el texto',2,1,1,14),(3,'bienvenido','dfasdfsdf',2,1,2,14),(4,'Welcome MedTechTrade is a global trade association for technology manufacturers, research institutions, allied professional services and development organizations with shared interests in collaborative developing technologies, products and services for the global medical products marketplace. Our goal is to enable and accelerate trading of medical devices for companies and institutional organisation.','<p>\r\n	&nbsp;</p>\r\n<div style=\"color: rgb(51, 51, 51); font-family: Tahoma, Helvetica, Arial, sans-serif; line-height: 15px; \">\r\n	<img alt=\"MedTechTrade\" border=\"0\" mce_src=\"images/stories/frontpage-image.jpg\" src=\"http://www.medtechtrade.com/images/stories/frontpage-image.jpg\" style=\"border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; border-image: initial; \" /></div>\r\n<div class=\"contentheading\" mce_style=\"margin-bottom:10px;\" style=\"color: rgb(51, 51, 51); font-family: Tahoma, Helvetica, Arial, sans-serif; line-height: 15px; margin-bottom: 10px; \">\r\n	<br />\r\n	Welcome</div>\r\n<p style=\"color: rgb(51, 51, 51); font-family: Tahoma, Helvetica, Arial, sans-serif; line-height: 15px; \">\r\n	<span class=\"Apple-style-span\" mce_name=\"strong\" mce_style=\"font-weight: bold;\" style=\"font-weight: bold; \">MedTechTrade</span>&nbsp;is a global trade association for technology manufacturers, research institutions, allied professional services and development organizations with shared interests in collaborative developing technologies, products and services for the global medical products marketplace.</p>\r\n<p style=\"color: rgb(51, 51, 51); font-family: Tahoma, Helvetica, Arial, sans-serif; line-height: 15px; \">\r\n	Our goal is to enable and accelerate trading of medical devices for companies and institutional organisation.</p>\r\n',1,1,1,14),(5,'Welcome MedTechTrade is a global trade association for technology manufacturers, research institutions, allied professional services and development organizations with shared interests in collaborative developing technologies, products and services for the global medical products marketplace. Our goal is to enable and accelerate trading of medical devices for companies and institutional organisation.','<p>\r\n	&nbsp;</p>\r\n<div>\r\n	<div style=\"color: rgb(51, 51, 51); font-family: Tahoma, Helvetica, Arial, sans-serif; line-height: 15px; \">\r\n		<br class=\"Apple-interchange-newline\" />\r\n		<img alt=\"MedTechTrade\" border=\"0\" mce_src=\"images/stories/frontpage-image.jpg\" src=\"http://www.medtechtrade.com/images/stories/frontpage-image.jpg\" style=\"border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; border-image: initial; \" /></div>\r\n	<div class=\"contentheading\" mce_style=\"margin-bottom:10px;\" style=\"color: rgb(51, 51, 51); font-family: Tahoma, Helvetica, Arial, sans-serif; line-height: 15px; margin-bottom: 10px; \">\r\n		<br />\r\n		Welcome</div>\r\n	<p style=\"color: rgb(51, 51, 51); font-family: Tahoma, Helvetica, Arial, sans-serif; line-height: 15px; \">\r\n		<span class=\"Apple-style-span\" mce_name=\"strong\" mce_style=\"font-weight: bold;\" style=\"font-weight: bold; \">MedTechTrade</span>&nbsp;is a global trade association for technology manufacturers, research institutions, allied professional services and development organizations with shared interests in collaborative developing technologies, products and services for the global medical products marketplace.</p>\r\n	<p style=\"color: rgb(51, 51, 51); font-family: Tahoma, Helvetica, Arial, sans-serif; line-height: 15px; \">\r\n		Our goal is to enable and accelerate trading of medical devices for companies and institutional organisation.</p>\r\n</div>',1,1,2,14),(6,'Bienvenido MedTechTrade es una asociaciÃ³n regional de comercio sin fines lucrativos compuesta de fabricantes de tecnologÃ­a del norte de New York, instituciones de investigaciÃ³n, servicios profesionales aliados y organizaciones de desarrollo econÃ³mico con intereses compartidos en el desarrollo colaborativo de tecnologÃ­as, productos y servicios para el mercado global de productos mÃ©dicos. Nuestro objetivo es acelerar la realizaciÃ³n de compaÃ±Ã­as individuales y objetivos institucionales mientras forjamos empleos y habilidades en la regiÃ³n. Para saber mÃ¡s sobre la organizaciÃ³n, o para saber como ser una organizaciÃ³n miembro, simplemente siga los vÃ­nculos en este sitio web.','<p>\r\n	<img alt=\"MedTechTrade\" border=\"0\" mce_src=\"images/stories/frontpage-image.jpg\" src=\"http://www.medtechtrade.com/images/stories/frontpage-image.jpg\" style=\"color: rgb(51, 51, 51); font-family: Tahoma, Helvetica, Arial, sans-serif; line-height: 15px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; border-image: initial; \" /></p>\r\n<p>\r\n	Bienvenido</p>\r\n<p>\r\n	MedTechTrade es una asociaci&oacute;n de comercio a nivel mundial de equipamiento m&eacute;dico para empresas y organizaciones.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nuestro objetivo es facilitar y agilizar el comercio de equipamiento medico.</p>\r\n',1,1,3,14),(7,'MedTechTrade ist eine weltweite Handelsorganisation fÃ¼r Technologiehersteller, Forschungseinrichtungen, allierten professionellen Dienstleistern und wirtschaftliche Entwicklungsorganisationen mit dem Interesse an gemeinschaftlich entwickelten Technologien, Produkten und Dienstleistungen fÃ¼r den globalen Markt medizinischer Produkte. Unser Ziel ist es den Handel von medizinischen GerÃ¤ten fÃ¼r Firmen und institutionelle Organisation zu ermÃ¶glichen und zu beschleunigen.','<p>\r\n	<img alt=\"MedTechTrade\" border=\"0\" mce_src=\"images/stories/frontpage-image.jpg\" src=\"http://www.medtechtrade.com/images/stories/frontpage-image.jpg\" style=\"color: rgb(51, 51, 51); font-family: Tahoma, Helvetica, Arial, sans-serif; line-height: 15px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; border-image: initial; \" /></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Willkommen</p>\r\n<p>\r\n	MedTechTrade ist eine weltweite Handelsorganisation von medizinischen Ger&auml;ten f&uuml;r Firmen und Organisationen.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Unser Ziel ist es den Handel von medizinischen Ger&auml;ten zu erm&ouml;glichen und zu beschleunigen.</p>\r\n',1,1,4,14),(8,'dsfsd','dsfsdf',4,1,1,14),(9,'sdfsdf','sdfsdf',4,1,2,14),(10,'sdfsffff','dsfsa',4,1,3,14),(11,'sdfsd','sdsdfsd',4,1,4,14),(12,'dasdsad','asdasdsad',2,1,3,14),(13,'asdsada','sadasdasdsad',2,1,4,14),(14,'welcome','fdsfsdfs',1,1,4,24),(15,'about us','dsfsdfsdf',4,1,4,24),(16,'welcome','<div>\r\n	<img alt=\"MedTechTrade\" border=\"0\" src=\"images/stories/frontpage-peru.png\" style=\"border: 0pt none;\" /></div>\r\n<div class=\"contentheading\" style=\"margin-bottom:10px;\">\r\n	<br />\r\n	Bienvenido a MedTechTrade Per&uacute;</div>\r\n<p>\r\n	MedTechTrade es su especialista suizo en la compra y venta a nivel mundial de equipamiento m&eacute;dico usado y reacondicionado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h3>\r\n	Equipos medicos suizos a su alcanze</h3>\r\n<p>\r\n	<strong><img alt=\"flag ch\" border=\"0\" src=\"images/stories/flag_CH.png\" style=\"border: 0pt none;\" /> MedTechTrade</strong> es una empresa suiza, dedicada a la venta de de equipamento medico reacondicionado de marcas l&iacute;deres en el mercado mundial.</p>\r\n<p>\r\n	<strong><img alt=\"flag ch\" border=\"0\" src=\"images/stories/flag_CH.png\" style=\"border: 0pt none;\" /> </strong><strong>MedTechTrade</strong> contribuye con equipamiento m&eacute;dico proveniente de Suiza, Alemania y Austria que responde a las exigencias de nuestros clientes, aplicando especial cuidado para que distintos par&aacute;metros como calidad, confiabilidad, precisi&oacute;n, flexibilidad y ahorro en costos de mantenci&oacute;n, se cumplan.</p>',1,1,3,1),(17,'about us','<p>\r\n	<span class=\"contentheading\">Sobre nosotros</span></p>\r\n<p>\r\n	MedTechTrade es una empresa internacional con origen en Suiza y se dedica a la compra y venta de equipos medicos nuevos y reacondicionados.</p>\r\n<p>\r\n	Nosotros nos especializamos en encontrar los equipos m&eacute;dicos que usted necesitan de alta calida a precios accesibles. Nuestro objetivo es facilitar la acquisi&oacute;n de equipos medicos a trav&eacute;z de nuestra plataforma. Ofrecemos una plataforma de compra y venta &uacute;nica, donde se puede comparar entre los modelos y fabricantes m&aacute;s reconocidos para ahorar tiempo y dinero.</p>\r\n<p>\r\n	Para asegurarnos de la satisfacci&oacute;n de nuestros clientes, nosotros seguimos el est&aacute;ndar m&aacute;s alto de calidad y empleamos solo profesionales altamente calificados y entrenados para reacondicionar cada equipo de acuerdo con las especificaciones originales del fabricante.<br />\r\n	<strong><br />\r\n	Todos nuestros equipos son acompa&ntilde;ados de una garant&iacute;a.</strong></p>\r\n<p>\r\n	<strong><br />\r\n	</strong></p>\r\n<h3>\r\n	Nuestro Equipo</h3>\r\n<table border=\"0\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width: 260px; padding-top: 10px;\">\r\n				<p>\r\n					<img border=\"0\" height=\"110\" src=\"images/stories/cl-roland.jpg\" style=\"float: left; border: 0pt none; margin-right:5px;\" width=\"90\" /><strong>Roland Suttner</strong><br />\r\n					Gerente General<br />\r\n					Chile<br />\r\n					&nbsp;</p>\r\n			</td>\r\n			<td style=\"width: 260px; padding-top: 10px;\">\r\n				<img border=\"0\" height=\"110\" src=\"images/stories/cl-darwin.jpg\" style=\"float: left; border: 0pt none; margin-right:5px;\" width=\"90\" /><strong>Darwin Lagos Veloso</strong><br />\r\n				Ventas y Servicio T&eacute;cnico<br />\r\n				Chile</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"padding-top: 10px;\">\r\n				<img border=\"0\" height=\"110\" src=\"images/stories/cl-rosanna.jpg\" style=\"float: left; border: 0pt none; margin-right:5px;\" width=\"90\" /><strong>Rossana D&iacute;az</strong><br />\r\n				Logistica y Administraci&oacute;n<br />\r\n				Chile</td>\r\n			<td style=\"padding-top: 10px;\">\r\n				<p>\r\n					&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n<hr />\r\n<p>\r\n	&nbsp;</p>\r\n<table border=\"0\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width: 260px; padding-top: 10px;\">\r\n				<img border=\"0\" height=\"110\" src=\"images/stories/ch-thomas.jpg\" style=\"float: left; margin-right: 5px; border: 0pt none;\" width=\"90\" /><strong>Thomas Zaugg</strong><br />\r\n				Gerente General<br />\r\n				Suiza<br />\r\n				&nbsp;</td>\r\n			<td style=\"width: 260px; padding-top: 10px;\">\r\n				<img border=\"0\" height=\"110\" src=\"images/stories/ch-erika.jpg\" style=\"float: left; margin-right: 5px; border: 0pt none;\" width=\"90\" /><strong>Erika G&uuml;nther</strong><br />\r\n				Logistica y Administraci&oacute;n<br />\r\n				Suiza</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"padding-top: 10px;\">\r\n				<img border=\"0\" height=\"110\" src=\"images/stories/ch-margareta.jpg\" style=\"float: left; margin-right: 5px; border: 0pt none;\" width=\"90\" /><strong>Margareta Deszynski</strong><br />\r\n				Ventas y Marketing<br />\r\n				Suiza</td>\r\n			<td style=\"padding-top: 10px;\">\r\n				<p>\r\n					&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',4,1,3,1),(18,'welcome','<div>\r\n	<img alt=\"MedTechTrade\" border=\"0\" src=\"images/stories/frontpage-chile.jpg\" style=\"border: 0pt none;\" /></div>\r\n<div class=\"contentheading\" style=\"margin-bottom:10px;\">\r\n	<br />\r\n	Bienvenido a MedTechTrade Chile</div>\r\n<p>\r\n	MedTechTrade es su especialista suizo en la compra y venta a nivel mundial de equipamiento m&eacute;dico usado y reacondicionado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h3>\r\n	Equipos medicos suizos a su alcanze</h3>\r\n<p>\r\n	<strong><img alt=\"flag ch\" border=\"0\" src=\"images/stories/flag_CH.png\" style=\"border: 0pt none;\" /> MedTechTrade</strong> es una empresa suiza, dedicada a la venta de de equipamento medico reacondicionado de marcas l&iacute;deres en el mercado mundial.</p>\r\n<p>\r\n	<strong><img alt=\"flag ch\" border=\"0\" src=\"images/stories/flag_CH.png\" style=\"border: 0pt none;\" /> </strong><strong>MedTechTrade</strong> contribuye con equipamiento m&eacute;dico proveniente de Suiza, Alemania y Austria que responde a las exigencias de nuestros clientes, aplicando especial cuidado para que distintos par&aacute;metros como calidad, confiabilidad, precisi&oacute;n, flexibilidad y ahorro en costos de mantenci&oacute;n, se cumplan.</p>',1,1,3,161),(19,'about us','<p>\r\n	<span class=\"contentheading\">Sobre nosotros</span></p>\r\n<p>\r\n	MedTechTrade es una empresa internacional con origen en Suiza y se dedica a la compra y venta de equipos medicos nuevos y reacondicionados.</p>\r\n<p>\r\n	Nosotros nos especializamos en encontrar los equipos m&eacute;dicos que usted necesitan de alta calida a precios accesibles. Nuestro objetivo es facilitar la acquisi&oacute;n de equipos medicos a trav&eacute;z de nuestra plataforma. Ofrecemos una plataforma de compra y venta &uacute;nica, donde se puede comparar entre los modelos y fabricantes m&aacute;s reconocidos para ahorar tiempo y dinero.</p>\r\n<p>\r\n	Para asegurarnos de la satisfacci&oacute;n de nuestros clientes, nosotros seguimos el est&aacute;ndar m&aacute;s alto de calidad y empleamos solo profesionales altamente calificados y entrenados para reacondicionar cada equipo de acuerdo con las especificaciones originales del fabricante.<br />\r\n	<strong><br />\r\n	Todos nuestros equipos son acompa&ntilde;ados de una garant&iacute;a.</strong></p>\r\n<p>\r\n	<strong><br />\r\n	</strong></p>\r\n<h3>\r\n	Nuestro Equipo</h3>\r\n<table border=\"0\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width: 260px; padding-top: 10px;\">\r\n				<p>\r\n					<img border=\"0\" height=\"110\" src=\"images/stories/cl-roland.jpg\" style=\"float: left; border: 0pt none; margin-right:5px;\" width=\"90\" /><strong>Roland Suttner</strong><br />\r\n					Gerente General<br />\r\n					Chile<br />\r\n					&nbsp;</p>\r\n			</td>\r\n			<td style=\"width: 260px; padding-top: 10px;\">\r\n				<img border=\"0\" height=\"110\" src=\"images/stories/cl-darwin.jpg\" style=\"float: left; border: 0pt none; margin-right:5px;\" width=\"90\" /><strong>Darwin Lagos Veloso</strong><br />\r\n				Ventas y Servicio T&eacute;cnico<br />\r\n				Chile</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"padding-top: 10px;\">\r\n				<img border=\"0\" height=\"110\" src=\"images/stories/cl-rosanna.jpg\" style=\"float: left; border: 0pt none; margin-right:5px;\" width=\"90\" /><strong>Rossana D&iacute;az</strong><br />\r\n				Logistica y Administraci&oacute;n<br />\r\n				Chile</td>\r\n			<td style=\"padding-top: 10px;\">\r\n				<p>\r\n					&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n<hr />\r\n<p>\r\n	&nbsp;</p>\r\n<table border=\"0\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width: 260px; padding-top: 10px;\">\r\n				<img border=\"0\" height=\"110\" src=\"images/stories/ch-thomas.jpg\" style=\"float: left; margin-right: 5px; border: 0pt none;\" width=\"90\" /><strong>Thomas Zaugg</strong><br />\r\n				Gerente General<br />\r\n				Suiza<br />\r\n				&nbsp;</td>\r\n			<td style=\"width: 260px; padding-top: 10px;\">\r\n				<img border=\"0\" height=\"110\" src=\"images/stories/ch-erika.jpg\" style=\"float: left; margin-right: 5px; border: 0pt none;\" width=\"90\" /><strong>Erika G&uuml;nther</strong><br />\r\n				Logistica y Administraci&oacute;n<br />\r\n				Suiza</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"padding-top: 10px;\">\r\n				<img border=\"0\" height=\"110\" src=\"images/stories/ch-margareta.jpg\" style=\"float: left; margin-right: 5px; border: 0pt none;\" width=\"90\" /><strong>Margareta Deszynski</strong><br />\r\n				Ventas y Marketing<br />\r\n				Suiza</td>\r\n			<td style=\"padding-top: 10px;\">\r\n				<p>\r\n					&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',4,1,3,161),(20,'welcome','<div>\r\n	<img alt=\"MedTechTrade\" border=\"0\" src=\"images/stories/frontpage-image.jpg\" /></div>\r\n<div class=\"contentheading\" style=\"margin-bottom:10px;\">\r\n	<br />\r\n	Willkommen</div>\r\n<p>\r\n	<strong>MedTechTrade</strong> ist eine weltweite Handelsorganisation von medizinischen Ger&auml;ten f&uuml;r Firmen und Organisationen. Unser Ziel ist es den Handel von medizinischen Ger&auml;ten zu erm&ouml;glichen und zu beschleunigen.</p>\r\n<p>\r\n	<strong>Bitte w&auml;hlen Sie Ihr Interessengebiet:</strong></p>\r\n<p>\r\n	<a href=\"index.php?option=com_content&amp;view=article&amp;id=68&amp;Itemid=56&amp;lang=de\"><img alt=\"Medizintechnik verkaufen\" border=\"0\" height=\"102\" src=\"images/stories/bot1.png\" width=\"251\" /></a><a href=\"index.php?option=com_adsmanager&amp;page=front&amp;Itemid=170&amp;lang=de\"><img alt=\"Medizintechnik verkaufen\" border=\"0\" height=\"102\" src=\"images/stories/bot2.png\" style=\"border: 0pt none; margin-left:18px;\" width=\"251\" /></a></p>',1,1,4,99),(21,'about us','<p>\r\n	<span class=\"contentheading\">&Uuml;ber uns</span></p>\r\n<p>\r\n	MedTechTrade ist eine Schweizer Firma, welche erneuerte Ger&auml;te aus zweiter Hand von f&uuml;hrenden Medizintechnikherstellern weltweit anbietet.</p>\r\n<p>\r\n	Wir liefern medizinische Ger&auml;te aus der Schweiz, Deutschland, &Ouml;sterreich und anderen europ&auml;ischen L&auml;ndern ganz auf die Bed&uuml;rfnisse unserer Kunden zugeschnitten.</p>\r\n<p>\r\n	Wir legen besonderen Wert auf Qualit&auml;t, Zuverl&auml;ssigkeit, Flexibilit&auml;t und k&ouml;nnen Ihnen Kosteneinsparung bei der Anschaffung von Ger&auml;ten sowie beim technischen Service bieten.</p>\r\n<p>\r\n	Unsere Kunden sind Krankenh&auml;user, Labore und medizinische Forschungseinrichtungen. Wir sind direkt f&uuml;r Sie da in der Schweiz, Polen, Peru und Chile.</p>\r\n<p>\r\n	<strong>Bei Interesse kontaktieren Sie uns noch heut.</strong></p>\r\n<p>\r\n	<strong><br />\r\n	</strong></p>\r\n<h3>\r\n	Unser Team</h3>\r\n<table border=\"0\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width: 260px; padding-top: 10px;\">\r\n				<img border=\"0\" height=\"110\" src=\"images/stories/ch-thomas.jpg\" style=\"float: left; margin-right: 5px; border: 0pt none;\" width=\"90\" /><strong>Thomas Zaugg</strong><br />\r\n				General Manager<br />\r\n				Schweiz<br />\r\n				&nbsp;</td>\r\n			<td style=\"width: 260px; padding-top: 10px;\">\r\n				<img border=\"0\" height=\"110\" src=\"images/stories/ch-erika.jpg\" style=\"float: left; margin-right: 5px; border: 0pt none;\" width=\"90\" /><strong>Erika G&uuml;nther</strong><br />\r\n				Backoffice &amp; Customer Support<br />\r\n				Schweiz</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"padding-top: 10px;\">\r\n				<img border=\"0\" height=\"110\" src=\"images/stories/ch-margareta.jpg\" style=\"float: left; margin-right: 5px; border: 0pt none;\" width=\"90\" /><strong>Margareta Deszynski</strong><br />\r\n				<span lang=\"EN-GB\">Sales Support</span><br />\r\n				Schweiz</td>\r\n			<td style=\"padding-top: 10px;\">\r\n				<p>\r\n					&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n<hr />\r\n<p>\r\n	&nbsp;</p>\r\n<table border=\"0\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width: 260px; padding-top: 10px;\">\r\n				<p>\r\n					<img border=\"0\" height=\"110\" src=\"images/stories/cl-roland.jpg\" style=\"float: left; border: 0pt none; margin-right:5px;\" width=\"90\" /><strong>Roland Suttner</strong><br />\r\n					General Manager<br />\r\n					Chile<br />\r\n					&nbsp;</p>\r\n			</td>\r\n			<td style=\"width: 260px; padding-top: 10px;\">\r\n				<img border=\"0\" height=\"110\" src=\"images/stories/cl-darwin.jpg\" style=\"float: left; border: 0pt none; margin-right:5px;\" width=\"90\" /><strong>Darwin Lagos Veloso</strong><br />\r\n				Sales Support und technischer Service<br />\r\n				Chile</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"padding-top: 10px;\">\r\n				<img border=\"0\" height=\"110\" src=\"images/stories/cl-rosanna.jpg\" style=\"float: left; border: 0pt none; margin-right:5px;\" width=\"90\" /><strong>Rossana D&iacute;az</strong><br />\r\n				Logistik und Administration<br />\r\n				Chile</td>\r\n			<td style=\"padding-top: 10px;\">\r\n				<p>\r\n					&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',4,1,4,99);

/*Table structure for table `paises` */

DROP TABLE IF EXISTS `paises`;

CREATE TABLE `paises` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(150) COLLATE utf8_unicode_ci NOT NULL,
  `code` VARCHAR(10) COLLATE utf8_unicode_ci NOT NULL,
  `active` INT(11) NOT NULL DEFAULT '1',
  `integrate` INT(11) DEFAULT NULL,
  `idioma` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=501 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `paises` */

INSERT  INTO `paises`(id,nombre,CODE,active,integrate,idioma) VALUES (1,'Peru','PE',1,1,3),(2,'FRANCE','FR',1,NULL,NULL),(3,'ITALY','IT',1,NULL,NULL),(4,'UNITED ARAB EMIRATES','AE',1,NULL,24),(5,'SWEDEN','SE',1,NULL,4),(6,'KAZAKHSTAN','KZ',1,NULL,NULL),(7,'PORTUGAL','PT',1,NULL,NULL),(8,'GREECE','GR',1,NULL,NULL),(9,'SAUDI ARABIA','SA',1,NULL,NULL),(10,'RUSSIAN FEDERATION','RU',1,NULL,NULL),(11,'UNITED KINGDOM','GB',1,NULL,NULL),(12,'DENMARK','DK',1,NULL,NULL),(13,'HONG KONG','HK',1,NULL,NULL),(14,'UNITED STATES','US',1,NULL,NULL),(15,'INDIA','IN',1,NULL,NULL),(16,'BRAZIL','BR',1,NULL,NULL),(17,'CANADA','CA',1,NULL,NULL),(18,'PUERTO RICO','PR',1,NULL,NULL),(19,'NETHERLANDS','NL',1,NULL,NULL),(20,'ARGENTINA','AR',1,NULL,NULL),(21,'BAHAMAS','BS',1,NULL,NULL),(22,'DOMINICA','DM',1,NULL,NULL),(23,'BELGIUM','BE',1,NULL,NULL),(24,'GERMANY','DE',1,1,4),(25,'IRELAND','IE',1,NULL,NULL),(26,'SOUTH AFRICA','ZA',1,NULL,NULL),(27,'MAURITIUS','MU',1,NULL,NULL),(28,'EGYPT','EG',1,NULL,NULL),(29,'MOROCCO','MA',1,NULL,NULL),(30,'NIGERIA','NG',1,NULL,NULL),(31,'GABON','GA',1,NULL,NULL),(32,'NAMIBIA','NA',1,NULL,NULL),(33,'RWANDA','RW',1,NULL,NULL),(34,'MADAGASCAR','MG',1,NULL,NULL),(35,'MAURITANIA','MR',1,NULL,NULL),(36,'TANZANIA, UNITED REPUBLIC OF','TZ',1,NULL,NULL),(37,'COTE D\'IVOIRE','CI',1,NULL,NULL),(38,'GHANA','GH',1,NULL,NULL),(39,'ZIMBABWE','ZW',1,NULL,NULL),(40,'BENIN','BJ',1,NULL,NULL),(41,'CONGO','CG',1,NULL,NULL),(42,'CAMEROON','CM',1,NULL,NULL),(43,'CONGO, THE DEMOCRATIC REPUBLIC OF THE','CD',1,NULL,NULL),(44,'MALAWI','MW',1,NULL,NULL),(45,'UGANDA','UG',1,NULL,NULL),(46,'MOZAMBIQUE','MZ',1,NULL,NULL),(47,'NIGER','NE',1,NULL,NULL),(48,'KENYA','KE',1,NULL,NULL),(49,'BOTSWANA','BW',1,NULL,NULL),(50,'ANGOLA','AO',1,NULL,NULL),(51,'LIBERIA','LR',1,NULL,NULL),(52,'ZAMBIA','ZM',1,NULL,NULL),(53,'SEYCHELLES','SC',1,NULL,NULL),(54,'LESOTHO','LS',1,NULL,NULL),(55,'SIERRA LEONE','SL',1,NULL,NULL),(56,'ALGERIA','DZ',1,NULL,NULL),(57,'CAYMAN ISLANDS','KY',1,NULL,NULL),(58,'CUBA','CU',1,NULL,NULL),(59,'SENEGAL','SN',1,NULL,NULL),(60,'COMOROS','KM',1,NULL,NULL),(61,'BRITISH INDIAN OCEAN TERRITORY','IO',1,NULL,NULL),(62,'ICELAND','IS',1,NULL,NULL),(63,'SUDAN','SD',1,NULL,NULL),(64,'MALI','ML',1,NULL,NULL),(65,'BURKINA FASO','BF',1,NULL,NULL),(66,'SWAZILAND','SZ',1,NULL,NULL),(67,'TOGO','TG',1,NULL,NULL),(68,'LIBYAN ARAB JAMAHIRIYA','LY',1,NULL,NULL),(69,'REUNION','RE',1,NULL,NULL),(70,'CAPE VERDE','CV',1,NULL,NULL),(71,'EQUATORIAL GUINEA','GQ',1,NULL,NULL),(72,'CENTRAL AFRICAN REPUBLIC','CF',1,NULL,NULL),(73,'GAMBIA','GM',1,NULL,NULL),(74,'TUNISIA','TN',1,NULL,NULL),(75,'JAPAN','JP',1,NULL,NULL),(76,'AUSTRALIA','AU',1,NULL,NULL),(77,'TURKEY','TR',1,NULL,NULL),(78,'SPAIN','ES',1,NULL,NULL),(79,'POLAND','PL',1,1,2),(80,'LATVIA','LV',1,NULL,NULL),(81,'UKRAINE','UA',1,NULL,NULL),(82,'IRAN, ISLAMIC REPUBLIC OF','IR',1,NULL,NULL),(83,'BELARUS','BY',1,NULL,NULL),(84,'CZECH REPUBLIC','CZ',1,NULL,NULL),(85,'PALESTINIAN TERRITORY, OCCUPIED','PS',1,NULL,NULL),(86,'SERBIA','RS',1,NULL,NULL),(87,'BULGARIA','BG',1,NULL,NULL),(88,'OMAN','OM',1,NULL,NULL),(89,'GEORGIA','GE',1,NULL,NULL),(90,'ROMANIA','RO',1,NULL,NULL),(91,'NORWAY','NO',1,NULL,NULL),(92,'ARMENIA','AM',1,NULL,NULL),(93,'ALBANIA','AL',1,NULL,NULL),(94,'CYPRUS','CY',1,NULL,NULL),(95,'ISRAEL','IL',1,NULL,NULL),(96,'HUNGARY','HU',1,NULL,NULL),(97,'SLOVENIA','SI',1,NULL,NULL),(98,'AUSTRIA','AT',1,NULL,NULL),(99,'SWITZERLAND','CH',1,1,4),(100,'THAILAND','TH',1,NULL,NULL),(101,'CHINA','CN',1,NULL,NULL),(102,'MALAYSIA','MY',1,NULL,NULL),(103,'PAKISTAN','PK',1,NULL,NULL),(104,'NEW ZEALAND','NZ',1,NULL,NULL),(105,'KOREA, REPUBLIC OF','KR',1,NULL,NULL),(106,'SINGAPORE','SG',1,NULL,NULL),(107,'BANGLADESH','BD',1,NULL,NULL),(108,'INDONESIA','ID',1,NULL,NULL),(109,'PHILIPPINES','PH',1,NULL,NULL),(110,'TAIWAN, PROVINCE OF CHINA','TW',1,NULL,NULL),(111,'AFGHANISTAN','AF',1,NULL,NULL),(112,'VIET NAM','VN',1,NULL,NULL),(113,'PANAMA','PA',1,NULL,NULL),(114,'BRUNEI DARUSSALAM','BN',1,NULL,NULL),(115,'MONTENEGRO','ME',1,NULL,NULL),(116,'FINLAND','FI',1,NULL,NULL),(117,'IRAQ','IQ',1,NULL,NULL),(118,'TAJIKISTAN','TJ',1,NULL,NULL),(119,'LEBANON','LB',1,NULL,NULL),(120,'MOLDOVA, REPUBLIC OF','MD',1,NULL,NULL),(121,'ESTONIA','EE',1,NULL,NULL),(122,'SLOVAKIA','SK',1,NULL,NULL),(123,'BOSNIA AND HERZEGOVINA','BA',1,NULL,NULL),(124,'KUWAIT','KW',1,NULL,NULL),(125,'LITHUANIA','LT',1,NULL,NULL),(126,'LUXEMBOURG','LU',1,NULL,NULL),(127,'BELIZE','BZ',1,NULL,NULL),(128,'ANTIGUA AND BARBUDA','AG',1,NULL,NULL),(129,'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','MK',1,NULL,NULL),(130,'SAN MARINO','SM',1,NULL,NULL),(131,'SERBIA','CS',1,NULL,NULL),(132,'YUGOSLAVIA','YU',1,NULL,NULL),(133,'HONG KONG','CN',1,NULL,NULL),(134,'VIRGIN ISLANDS, BRITISH','VG',1,NULL,NULL),(135,'MALTA','MT',1,NULL,NULL),(136,'FALKLAND ISLANDS (MALVINAS)','FK',1,NULL,NULL),(137,'BAHRAIN','BH',1,NULL,NULL),(138,'UZBEKISTAN','UZ',1,NULL,NULL),(139,'AZERBAIJAN','AZ',1,NULL,NULL),(140,'MONACO','MC',1,NULL,NULL),(141,'HAITI','HT',1,NULL,NULL),(142,'BOLIVIA','BO',1,NULL,NULL),(143,'GUAM','GU',1,NULL,NULL),(144,'JAMAICA','JM',1,NULL,NULL),(145,'MEXICO','MX',1,NULL,NULL),(146,'ECUADOR','EC',1,NULL,NULL),(147,'BERMUDA','BM',1,NULL,NULL),(148,'PERU','PE',1,NULL,NULL),(149,'COLOMBIA','CO',1,NULL,NULL),(150,'YEMEN','YE',1,NULL,NULL),(151,'SYRIAN ARAB REPUBLIC','SY',1,NULL,NULL),(152,'HONDURAS','HN',1,NULL,NULL),(153,'NICARAGUA','NI',1,NULL,NULL),(154,'DOMINICAN REPUBLIC','DO',1,NULL,NULL),(155,'GUATEMALA','GT',1,NULL,NULL),(156,'VENEZUELA','VE',1,NULL,NULL),(157,'URUGUAY','UY',1,NULL,NULL),(158,'JORDAN','JO',1,NULL,NULL),(159,'COSTA RICA','CR',1,NULL,NULL),(160,'EL SALVADOR','SV',1,NULL,NULL),(161,'CHILE','CL',1,1,3),(162,'VIRGIN ISLANDS, U.S.','VI',1,NULL,NULL),(163,'GIBRALTAR','GI',1,NULL,NULL),(164,'PARAGUAY','PY',1,NULL,NULL),(165,'COOK ISLANDS','CK',1,NULL,NULL),(166,'SAINT KITTS AND NEVIS','KN',1,NULL,NULL),(167,'FIJI','FJ',1,NULL,NULL),(168,'SAMOA','WS',1,NULL,NULL),(169,'MARSHALL ISLANDS','MH',1,NULL,NULL),(170,'NORTHERN MARIANA ISLANDS','MP',1,NULL,NULL),(171,'PALAU','PW',1,NULL,NULL),(172,'ANGUILLA','AI',1,NULL,NULL),(173,'BARBADOS','BB',1,NULL,NULL),(174,'GRENADA','GD',1,NULL,NULL),(175,'SAINT VINCENT AND THE GRENADINES','VC',1,NULL,NULL),(176,'CROATIA','HR',1,NULL,NULL),(177,'NETHERLANDS ANTILLES','AN',1,NULL,NULL),(178,'GUYANA','GY',1,NULL,NULL),(179,'TRINIDAD AND TOBAGO','TT',1,NULL,NULL),(180,'ARUBA','AW',1,NULL,NULL),(181,'SRI LANKA','LK',1,NULL,NULL),(182,'MALDIVES','MV',1,NULL,NULL),(183,'QATAR','QA',1,NULL,NULL),(184,'NEPAL','NP',1,NULL,NULL),(185,'MONTSERRAT','MS',1,NULL,NULL),(186,'NULL','NULL',1,NULL,NULL),(187,'KYRGYZSTAN','KG',1,NULL,NULL),(188,'BURUNDI','BI',1,NULL,NULL),(189,'UNITED KINGDOM','JE',1,NULL,NULL),(190,'TURKMENISTAN','TM',1,NULL,NULL),(191,'ISLE OF MAN','IM',1,NULL,NULL),(192,'GUINEA','GN',1,NULL,NULL),(193,'MYANMAR','MM',1,NULL,NULL),(194,'PAPUA NEW GUINEA','PG',1,NULL,NULL),(195,'BHUTAN','BT',1,NULL,NULL),(196,'CHAD','TD',1,NULL,NULL),(197,'ALAND ISLANDS','AX',1,NULL,NULL),(198,'LIECHTENSTEIN','LI',1,NULL,NULL),(199,'FAROE ISLANDS','FO',1,NULL,NULL),(200,'SOMALIA','SO',1,NULL,NULL),(201,'ETHIOPIA','ET',1,NULL,NULL),(202,'LAO PEOPLE\'S DEMOCRATIC REPUBLIC','LA',1,NULL,NULL),(203,'MARTINIQUE','MQ',1,NULL,NULL),(204,'GUADELOUPE','GP',1,NULL,NULL),(205,'HOLY SEE (VATICAN CITY STATE)','VA',1,NULL,NULL),(206,'ANDORRA','AD',1,NULL,NULL),(207,'ANTARCTICA','AQ',1,NULL,NULL),(208,'MACAO','MO',1,NULL,NULL),(209,'GREENLAND','GL',1,NULL,NULL),(210,'SURINAME','SR',1,NULL,NULL),(211,'DJIBOUTI','DJ',1,NULL,NULL),(212,'GUINEA-BISSAU','GW',1,NULL,NULL),(213,'ERITREA','ER',1,NULL,NULL),(214,'CAMBODIA','KH',1,NULL,NULL),(215,'MONGOLIA','MN',1,NULL,NULL),(216,'VANUATU','VU',1,NULL,NULL),(217,'NEW CALEDONIA','NC',1,NULL,NULL),(218,'FRENCH POLYNESIA','PF',1,NULL,NULL),(219,'MICRONESIA, FEDERATED STATES OF','FM',1,NULL,NULL),(220,'SAO TOME AND PRINCIPE','ST',1,NULL,NULL),(221,'AMERICAN SAMOA','AS',1,NULL,NULL),(222,'FRENCH SOUTHERN TERRITORIES','TF',1,NULL,NULL),(223,'FRENCH GUIANA','GF',1,NULL,NULL),(224,'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','GS',1,NULL,NULL),(225,'SOLOMON ISLANDS','SB',1,NULL,NULL),(226,'TUVALU','TV',1,NULL,NULL),(227,'KIRIBATI','KI',1,NULL,NULL),(228,'TONGA','TO',1,NULL,NULL),(229,'NIUE','NU',1,NULL,NULL),(230,'TOKELAU','TK',1,NULL,NULL),(231,'NORFOLK ISLAND','NF',1,NULL,NULL),(232,'NAURU','NR',1,NULL,NULL),(233,'TURKS AND CAICOS ISLANDS','TC',1,NULL,NULL),(234,'SAINT LUCIA','LC',1,NULL,NULL),(235,'MAYOTTE','YT',1,NULL,NULL),(257,'FRANCE','FR',1,NULL,NULL),(258,'ITALY','IT',1,NULL,NULL),(259,'UNITED ARAB EMIRATES','AE',1,NULL,NULL),(260,'SWEDEN','SE',1,NULL,NULL),(261,'KAZAKHSTAN','KZ',1,NULL,NULL),(262,'PORTUGAL','PT',1,NULL,NULL),(263,'GREECE','GR',1,NULL,NULL),(264,'SAUDI ARABIA','SA',1,NULL,NULL),(265,'RUSSIAN FEDERATION','RU',1,NULL,NULL),(266,'UNITED KINGDOM','GB',1,NULL,NULL),(267,'DENMARK','DK',1,NULL,NULL),(268,'HONG KONG','HK',1,NULL,NULL),(269,'UNITED STATES','US',1,NULL,NULL),(270,'INDIA','IN',1,NULL,NULL),(271,'BRAZIL','BR',1,NULL,NULL),(272,'CANADA','CA',1,NULL,NULL),(273,'PUERTO RICO','PR',1,NULL,NULL),(274,'NETHERLANDS','NL',1,NULL,NULL),(275,'ARGENTINA','AR',1,NULL,NULL),(276,'BAHAMAS','BS',1,NULL,NULL),(277,'DOMINICA','DM',1,NULL,NULL),(278,'BELGIUM','BE',1,NULL,NULL),(279,'GERMANY','DE',1,NULL,NULL),(280,'IRELAND','IE',1,NULL,NULL),(281,'SOUTH AFRICA','ZA',1,NULL,NULL),(282,'MAURITIUS','MU',1,NULL,NULL),(283,'EGYPT','EG',1,NULL,NULL),(284,'MOROCCO','MA',1,NULL,NULL),(285,'NIGERIA','NG',1,NULL,NULL),(286,'GABON','GA',1,NULL,NULL),(287,'NAMIBIA','NA',1,NULL,NULL),(288,'RWANDA','RW',1,NULL,NULL),(289,'MADAGASCAR','MG',1,NULL,NULL),(290,'MAURITANIA','MR',1,NULL,NULL),(291,'TANZANIA, UNITED REPUBLIC OF','TZ',1,NULL,NULL),(292,'COTE D\'IVOIRE','CI',1,NULL,NULL),(293,'GHANA','GH',1,NULL,NULL),(294,'ZIMBABWE','ZW',1,NULL,NULL),(295,'BENIN','BJ',1,NULL,NULL),(296,'CONGO','CG',1,NULL,NULL),(297,'CAMEROON','CM',1,NULL,NULL),(298,'CONGO, THE DEMOCRATIC REPUBLIC OF THE','CD',1,NULL,NULL),(299,'MALAWI','MW',1,NULL,NULL),(300,'UGANDA','UG',1,NULL,NULL),(301,'MOZAMBIQUE','MZ',1,NULL,NULL),(302,'NIGER','NE',1,NULL,NULL),(303,'KENYA','KE',1,NULL,NULL),(304,'BOTSWANA','BW',1,NULL,NULL),(305,'ANGOLA','AO',1,NULL,NULL),(306,'LIBERIA','LR',1,NULL,NULL),(307,'ZAMBIA','ZM',1,NULL,NULL),(308,'SEYCHELLES','SC',1,NULL,NULL),(309,'LESOTHO','LS',1,NULL,NULL),(310,'SIERRA LEONE','SL',1,NULL,NULL),(311,'ALGERIA','DZ',1,NULL,NULL),(312,'CAYMAN ISLANDS','KY',1,NULL,NULL),(313,'CUBA','CU',1,NULL,NULL),(314,'SENEGAL','SN',1,NULL,NULL),(315,'COMOROS','KM',1,NULL,NULL),(316,'BRITISH INDIAN OCEAN TERRITORY','IO',1,NULL,NULL),(317,'ICELAND','IS',1,NULL,NULL),(318,'SUDAN','SD',1,NULL,NULL),(319,'MALI','ML',1,NULL,NULL),(320,'BURKINA FASO','BF',1,NULL,NULL),(321,'SWAZILAND','SZ',1,NULL,NULL),(322,'TOGO','TG',1,NULL,NULL),(323,'LIBYAN ARAB JAMAHIRIYA','LY',1,NULL,NULL),(324,'REUNION','RE',1,NULL,NULL),(325,'CAPE VERDE','CV',1,NULL,NULL),(326,'EQUATORIAL GUINEA','GQ',1,NULL,NULL),(327,'CENTRAL AFRICAN REPUBLIC','CF',1,NULL,NULL),(328,'GAMBIA','GM',1,NULL,NULL),(329,'TUNISIA','TN',1,NULL,NULL),(330,'JAPAN','JP',1,NULL,NULL),(331,'AUSTRALIA','AU',1,NULL,NULL),(332,'TURKEY','TR',1,NULL,NULL),(333,'SPAIN','ES',1,NULL,NULL),(334,'POLAND','PL',1,NULL,NULL),(335,'LATVIA','LV',1,NULL,NULL),(336,'UKRAINE','UA',1,NULL,NULL),(337,'IRAN, ISLAMIC REPUBLIC OF','IR',1,NULL,NULL),(338,'BELARUS','BY',1,NULL,NULL),(339,'CZECH REPUBLIC','CZ',1,NULL,NULL),(340,'PALESTINIAN TERRITORY, OCCUPIED','PS',1,NULL,NULL),(341,'SERBIA','RS',1,NULL,NULL),(342,'BULGARIA','BG',1,NULL,NULL),(343,'OMAN','OM',1,NULL,NULL),(344,'GEORGIA','GE',1,NULL,NULL),(345,'ROMANIA','RO',1,NULL,NULL),(346,'NORWAY','NO',1,NULL,NULL),(347,'ARMENIA','AM',1,NULL,NULL),(348,'ALBANIA','AL',1,NULL,NULL),(349,'CYPRUS','CY',1,NULL,NULL),(350,'ISRAEL','IL',1,NULL,NULL),(351,'HUNGARY','HU',1,NULL,NULL),(352,'SLOVENIA','SI',1,NULL,NULL),(353,'AUSTRIA','AT',1,NULL,NULL),(354,'SWITZERLAND','CH',1,NULL,NULL),(355,'THAILAND','TH',1,NULL,NULL),(356,'CHINA','CN',1,NULL,NULL),(357,'MALAYSIA','MY',1,NULL,NULL),(358,'PAKISTAN','PK',1,NULL,NULL),(359,'NEW ZEALAND','NZ',1,NULL,NULL),(360,'KOREA, REPUBLIC OF','KR',1,NULL,NULL),(361,'SINGAPORE','SG',1,NULL,NULL),(362,'BANGLADESH','BD',1,NULL,NULL),(363,'INDONESIA','ID',1,NULL,NULL),(364,'PHILIPPINES','PH',1,NULL,NULL),(365,'TAIWAN, PROVINCE OF CHINA','TW',1,NULL,NULL),(366,'AFGHANISTAN','AF',1,NULL,NULL),(367,'VIET NAM','VN',1,NULL,NULL),(368,'PANAMA','PA',1,NULL,NULL),(369,'BRUNEI DARUSSALAM','BN',1,NULL,NULL),(370,'MONTENEGRO','ME',1,NULL,NULL),(371,'FINLAND','FI',1,NULL,NULL),(372,'IRAQ','IQ',1,NULL,NULL),(373,'TAJIKISTAN','TJ',1,NULL,NULL),(374,'LEBANON','LB',1,NULL,NULL),(375,'MOLDOVA, REPUBLIC OF','MD',1,NULL,NULL),(376,'ESTONIA','EE',1,NULL,NULL),(377,'SLOVAKIA','SK',1,NULL,NULL),(378,'BOSNIA AND HERZEGOVINA','BA',1,NULL,NULL),(379,'KUWAIT','KW',1,NULL,NULL),(380,'LITHUANIA','LT',1,NULL,NULL),(381,'LUXEMBOURG','LU',1,NULL,NULL),(382,'BELIZE','BZ',1,NULL,NULL),(383,'ANTIGUA AND BARBUDA','AG',1,NULL,NULL),(384,'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','MK',1,NULL,NULL),(385,'SAN MARINO','SM',1,NULL,NULL),(386,'SERBIA','CS',1,NULL,NULL),(387,'YUGOSLAVIA','YU',1,NULL,NULL),(388,'HONG KONG','CN',1,NULL,NULL),(389,'VIRGIN ISLANDS, BRITISH','VG',1,NULL,NULL),(390,'MALTA','MT',1,NULL,NULL),(391,'FALKLAND ISLANDS (MALVINAS)','FK',1,NULL,NULL),(392,'BAHRAIN','BH',1,NULL,NULL),(393,'UZBEKISTAN','UZ',1,NULL,NULL),(394,'AZERBAIJAN','AZ',1,NULL,NULL),(395,'MONACO','MC',1,NULL,NULL),(396,'HAITI','HT',1,NULL,NULL),(397,'BOLIVIA','BO',1,NULL,NULL),(398,'GUAM','GU',1,NULL,NULL),(399,'JAMAICA','JM',1,NULL,NULL),(400,'MEXICO','MX',1,NULL,NULL),(401,'ECUADOR','EC',1,NULL,NULL),(402,'BERMUDA','BM',1,NULL,NULL),(403,'COLOMBIA','CO',1,NULL,NULL),(404,'YEMEN','YE',1,NULL,NULL),(405,'SYRIAN ARAB REPUBLIC','SY',1,NULL,NULL),(406,'HONDURAS','HN',1,NULL,NULL),(407,'NICARAGUA','NI',1,NULL,NULL),(408,'DOMINICAN REPUBLIC','DO',1,NULL,NULL),(409,'GUATEMALA','GT',1,NULL,NULL),(410,'VENEZUELA','VE',1,NULL,NULL),(411,'URUGUAY','UY',1,NULL,NULL),(412,'JORDAN','JO',1,NULL,NULL),(413,'COSTA RICA','CR',1,NULL,NULL),(414,'EL SALVADOR','SV',1,NULL,NULL),(415,'CHILE','CL',1,NULL,NULL),(416,'VIRGIN ISLANDS, U.S.','VI',1,NULL,NULL),(417,'GIBRALTAR','GI',1,NULL,NULL),(418,'PARAGUAY','PY',1,NULL,NULL),(419,'COOK ISLANDS','CK',1,NULL,NULL),(420,'SAINT KITTS AND NEVIS','KN',1,NULL,NULL),(421,'FIJI','FJ',1,NULL,NULL),(422,'SAMOA','WS',1,NULL,NULL),(423,'MARSHALL ISLANDS','MH',1,NULL,NULL),(424,'NORTHERN MARIANA ISLANDS','MP',1,NULL,NULL),(425,'PALAU','PW',1,NULL,NULL),(426,'ANGUILLA','AI',1,NULL,NULL),(427,'BARBADOS','BB',1,NULL,NULL),(428,'GRENADA','GD',1,NULL,NULL),(429,'SAINT VINCENT AND THE GRENADINES','VC',1,NULL,NULL),(430,'CROATIA','HR',1,NULL,NULL),(431,'NETHERLANDS ANTILLES','AN',1,NULL,NULL),(432,'GUYANA','GY',1,NULL,NULL),(433,'TRINIDAD AND TOBAGO','TT',1,NULL,NULL),(434,'ARUBA','AW',1,NULL,NULL),(435,'SRI LANKA','LK',1,NULL,NULL),(436,'MALDIVES','MV',1,NULL,NULL),(437,'QATAR','QA',1,NULL,NULL),(438,'NEPAL','NP',1,NULL,NULL),(439,'MONTSERRAT','MS',1,NULL,NULL),(440,'NULL','NULL',1,NULL,NULL),(441,'KYRGYZSTAN','KG',1,NULL,NULL),(442,'BURUNDI','BI',1,NULL,NULL),(443,'UNITED KINGDOM','JE',1,NULL,NULL),(444,'TURKMENISTAN','TM',1,NULL,NULL),(445,'ISLE OF MAN','IM',1,NULL,NULL),(446,'GUINEA','GN',1,NULL,NULL),(447,'MYANMAR','MM',1,NULL,NULL),(448,'PAPUA NEW GUINEA','PG',1,NULL,NULL),(449,'BHUTAN','BT',1,NULL,NULL),(450,'CHAD','TD',1,NULL,NULL),(451,'ALAND ISLANDS','AX',1,NULL,NULL),(452,'LIECHTENSTEIN','LI',1,NULL,NULL),(453,'FAROE ISLANDS','FO',1,NULL,NULL),(454,'SOMALIA','SO',1,NULL,NULL),(455,'ETHIOPIA','ET',1,NULL,NULL),(456,'LAO PEOPLE\'S DEMOCRATIC REPUBLIC','LA',1,NULL,NULL),(457,'MARTINIQUE','MQ',1,NULL,NULL),(458,'GUADELOUPE','GP',1,NULL,NULL),(459,'HOLY SEE (VATICAN CITY STATE)','VA',1,NULL,NULL),(460,'ANDORRA','AD',1,NULL,NULL),(461,'ANTARCTICA','AQ',1,NULL,NULL),(462,'MACAO','MO',1,NULL,NULL),(463,'GREENLAND','GL',1,NULL,NULL),(464,'SURINAME','SR',1,NULL,NULL),(465,'DJIBOUTI','DJ',1,NULL,NULL),(466,'GUINEA-BISSAU','GW',1,NULL,NULL),(467,'ERITREA','ER',1,NULL,NULL),(468,'CAMBODIA','KH',1,NULL,NULL),(469,'MONGOLIA','MN',1,NULL,NULL),(470,'VANUATU','VU',1,NULL,NULL),(471,'NEW CALEDONIA','NC',1,NULL,NULL),(472,'FRENCH POLYNESIA','PF',1,NULL,NULL),(473,'MICRONESIA, FEDERATED STATES OF','FM',1,NULL,NULL),(474,'SAO TOME AND PRINCIPE','ST',1,NULL,NULL),(475,'AMERICAN SAMOA','AS',1,NULL,NULL),(476,'FRENCH SOUTHERN TERRITORIES','TF',1,NULL,NULL),(477,'FRENCH GUIANA','GF',1,NULL,NULL),(478,'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','GS',1,NULL,NULL),(479,'SOLOMON ISLANDS','SB',1,NULL,NULL),(480,'TUVALU','TV',1,NULL,NULL),(481,'KIRIBATI','KI',1,NULL,NULL),(482,'TONGA','TO',1,NULL,NULL),(483,'NIUE','NU',1,NULL,NULL),(484,'TOKELAU','TK',1,NULL,NULL),(485,'NORFOLK ISLAND','NF',1,NULL,NULL),(486,'NAURU','NR',1,NULL,NULL),(487,'TURKS AND CAICOS ISLANDS','TC',1,NULL,NULL),(488,'SAINT LUCIA','LC',1,NULL,NULL),(489,'MAYOTTE','YT',1,NULL,NULL),(500,'Others','OT',1,NULL,NULL);

/*Table structure for table `pregunta` */

DROP TABLE IF EXISTS `pregunta`;

CREATE TABLE `pregunta` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `categoriapregunta_id` INT(11) NOT NULL DEFAULT '1',
  `equipo_id` INT(11) NOT NULL,
  `usuario_id` INT(11) NOT NULL,
  `asunto` VARCHAR(120) NOT NULL,
  `formulacion` VARCHAR(255) NOT NULL,
  `fechaFormulacion` DATETIME NOT NULL,
  `fechaRespuesta` DATETIME DEFAULT NULL,
  `respuesta` VARCHAR(255) DEFAULT NULL,
  `copiaEmail` INT(11) DEFAULT '0',
  `active` INT(11) NOT NULL DEFAULT '1',
  `estado` INT(11) DEFAULT '0' COMMENT '0 : no resuelto\n1 : resuelto',
  PRIMARY KEY (`id`),
  KEY `fk_preguntas_categoriapregunta1` (`categoriapregunta_id`),
  KEY `fk_pregunta_usuario1` (`usuario_id`),
  KEY `fk_pregunta_equipo1` (`equipo_id`)
) ENGINE=INNODB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `pregunta` */

INSERT  INTO `pregunta`(id,categoriapregunta_id,equipo_id,usuario_id,asunto,formulacion,fechaFormulacion,fechaRespuesta,respuesta,copiaEmail,active,estado) VALUES (1,1,4,6,'testin','testing\r\nacerca del producto\r\n4\r\nsaludos','2011-09-26 13:08:38','2011-10-19 17:10:42','respuesta',1,1,1),(2,1,4,6,'testing 1','teststttttttttttttttttttt','0000-00-00 00:00:00',NULL,NULL,0,1,0),(3,1,4,6,'testing 1','teststttttttttttttttttttt','2011-09-26 00:00:00',NULL,NULL,0,1,0),(4,1,3,6,'acerca del equipo','una pregunta acerca de este producto','0000-00-00 00:00:00',NULL,NULL,1,1,0),(5,1,22,3,'deseo comprar este equipo y no puedo volver','deseo comprar este equipo como lo puedo realizar','2011-10-27 22:10:41',NULL,NULL,1,1,0);

/*Table structure for table `publicacionequipo` */

DROP TABLE IF EXISTS `publicacionequipo`;

CREATE TABLE `publicacionequipo` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` INT(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `publicacionequipo` */

INSERT  INTO `publicacionequipo`(id,nombre,active) VALUES (1,'Pendiente',1),(2,'Activada',1),(3,'Eliminada',1),(4,'Vendido',1);

/*Table structure for table `reserva` */

DROP TABLE IF EXISTS `reserva`;

CREATE TABLE `reserva` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `equipo_id` INT(11) NOT NULL,
  `usuario_id` INT(11) NOT NULL,
  `fechagrabacion` DATETIME DEFAULT NULL,
  `order` INT(11) DEFAULT NULL,
  `active` INT(11) DEFAULT '1',
  `tipo_reserva_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_favorito__equipo1` (`equipo_id`),
  KEY `fk_favorito__usuario1` (`usuario_id`),
  KEY `fk_reserva_tipo_reserva1` (`tipo_reserva_id`),
  CONSTRAINT `fk_favorito__equipo1` FOREIGN KEY (`equipo_id`) REFERENCES `equipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_favorito__usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reserva_tipo_reserva1` FOREIGN KEY (`tipo_reserva_id`) REFERENCES `tipo_reserva` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `reserva` */

INSERT  INTO `reserva`(id,equipo_id,usuario_id,fechagrabacion,reserva.order,active,tipo_reserva_id) VALUES (5,13,6,'2011-09-28 16:56:43',NULL,1,2),(6,4,7,'2011-09-28 12:34:08',NULL,1,2),(7,4,3,'2011-09-28 21:33:46',NULL,1,2),(8,4,19,'2011-10-05 17:57:56',NULL,1,2),(9,14,19,'2011-10-05 18:33:02',NULL,1,2),(10,4,19,'2011-10-05 18:38:23',NULL,1,1),(11,3,20,'2011-10-05 21:08:47',NULL,1,2),(12,3,20,'2011-10-05 21:10:09',NULL,1,1),(13,5,6,'2011-10-08 00:43:44',NULL,1,2),(14,3,6,'2011-10-13 14:57:29',NULL,1,1),(15,13,6,'2011-10-13 15:45:12',NULL,1,1),(16,4,6,'2011-10-13 16:35:28',NULL,1,1),(17,3,6,'2011-10-24 16:55:49',NULL,1,2),(18,13,3,'2011-10-25 22:54:44',NULL,1,2),(19,13,3,'2011-10-25 22:54:49',NULL,1,1),(20,22,3,'2011-10-27 22:49:20',NULL,1,1);

/*Table structure for table `tipo_reserva` */

DROP TABLE IF EXISTS `tipo_reserva`;

CREATE TABLE `tipo_reserva` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `active` INT(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Categoria de equipo usuario:\n- Reserva\n- Add Favorito';

/*Data for the table `tipo_reserva` */

INSERT  INTO `tipo_reserva`(id,nombre,active) VALUES (1,'Reserva',1),(2,'Favorito',1);

/*Table structure for table `tipousuario` */

DROP TABLE IF EXISTS `tipousuario`;

CREATE TABLE `tipousuario` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` INT(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tipousuario` */

INSERT  INTO `tipousuario`(id,nombre,active) VALUES (1,'Manager',1),(2,'Registered',1),(3,'User',1);

/*Table structure for table `traducciones` */

DROP TABLE IF EXISTS `traducciones`;

CREATE TABLE `traducciones` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `idiomas_id` INT(11) NOT NULL,
  `nombretabla` VARCHAR(250) COLLATE utf8_unicode_ci NOT NULL,
  `nombrecampo` VARCHAR(150) COLLATE utf8_unicode_ci NOT NULL,
  `texto` TEXT COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_traducciones_idiomas1` (`idiomas_id`),
  CONSTRAINT `fk_traducciones_idiomas1` FOREIGN KEY (`idiomas_id`) REFERENCES `idiomas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `traducciones` */

/*Table structure for table `usuario` */

DROP TABLE IF EXISTS `usuario`;

CREATE TABLE `usuario` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellido` VARCHAR(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` VARCHAR(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login` VARCHAR(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clave` VARCHAR(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipousuario_id` INT(11) NOT NULL,
  `sendemail` INT(11) DEFAULT NULL,
  `fecharegistro` DATETIME NOT NULL,
  `ultimavisita` DATETIME NOT NULL,
  `activacion` VARCHAR(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` INT(11) DEFAULT '0',
  `direccion` VARCHAR(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codpostal` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `ciudad` VARCHAR(150) COLLATE utf8_unicode_ci NOT NULL,
  `institucion` VARCHAR(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paises_id` INT(11) NOT NULL,
  `direccion1` VARCHAR(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion2` VARCHAR(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tratamiento` INT(11) DEFAULT NULL COMMENT '0 Sr, 1 Sra/Srta',
  `telefono` VARCHAR(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` VARCHAR(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechamodificacion` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_usuario_tipousuario1` (`tipousuario_id`),
  KEY `fk_usuario_paises1` (`paises_id`),
  CONSTRAINT `fk_usuario_paises1` FOREIGN KEY (`paises_id`) REFERENCES `paises` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_tipousuario1` FOREIGN KEY (`tipousuario_id`) REFERENCES `tipousuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `usuario` */

INSERT  INTO `usuario`(id,nombre,apellido,email,login,clave,tipousuario_id,sendemail,fecharegistro,ultimavisita,activacion,active,direccion,codpostal,ciudad,institucion,paises_id,direccion1,direccion2,tratamiento,telefono,fax,fechamodificacion) VALUES (3,'Luis','mayta mamani','slovacus1@gmail.com1','admin','sha1$87711$488758631b7e41e578f9b4b95d96cecfa288f696',1,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,1,'mi casa','1414','Lima','master',1,NULL,NULL,NULL,NULL,NULL,NULL),(4,'Luis alberto','mayta mamani','slovacus2@gmail.com1','kusanagi','sha1$21ee2$9be1a234e6a93bcb218c56189d70b6a0be2d7d8b',3,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,1,'mi casa','1414','Lima','dsdfsd',1,NULL,NULL,NULL,NULL,NULL,NULL),(5,'matias','franci','mfranci@gmail.com','mfranci','sha1$9d02a$de595b6b2d2bd7d9f3a8ece6f9813e7553fc8aa6',3,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,1,'mi casa','1414','lima','dsfsdfs',1,NULL,NULL,NULL,NULL,NULL,NULL),(6,'Luis alberto','mayta mamani','slovacus3@gmail.com','usuario','sha1$6319f$7dda231dcf59e9fba26dc7d23a1513b61084595b',3,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,1,'mi casa','1414','Lima','Stocker Group',1,NULL,NULL,1,'','',NULL),(7,'teresa','chunga ','tj.chunga@gmail.com','tchunga','sha1$2599c$917f9e211f5f06b9f52739bb7891de9274c6a02e',3,NULL,'2011-09-23 15:24:11','2011-09-23 15:24:11',NULL,1,'-','-','Lima','st',1,NULL,NULL,NULL,NULL,NULL,NULL),(11,'teresa','chunga','tere_chunga@hotmail.com','tjche','sha1$2599c$917f9e211f5f06b9f52739bb7891de9274c6a02e',2,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,1,'pando','lima 16','lima','sx',1,NULL,NULL,NULL,NULL,NULL,NULL),(14,'Renzo','Tejada','rtejada@gmail.com','rtejada','sha1$b80c0$a4155141411a5265da33f905a48c27569f89f0a6',3,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,1,'direccion','1414','lima','mi casa',1,NULL,NULL,NULL,NULL,NULL,NULL),(19,'Luis alberto','alberto','kofyagami2k@hotmail.com1','kofyagami2k1','sha1$621b7$7e9ca75e8cd06d262f473264238b14baba5f08bf',3,NULL,'2011-10-05 05:13:04','2011-10-05 05:13:04',NULL,1,'mi casa','1414','Lima','mi casa',1,NULL,NULL,NULL,NULL,NULL,NULL),(20,'Luis alberto','alberto','kofyagami2k@hotmail.com3','kofyagami2k3','sha1$30bb1$d615515802984b75b574663ae0b1823a2035cf15',3,NULL,'2011-10-05 08:58:35','2011-10-05 08:58:35',NULL,1,'mi casa','1414','Lima','weqwe',1,NULL,NULL,NULL,NULL,NULL,NULL),(21,'Luis alberto','alberto','kofyagami2k@hotmail.com','kofyagami','sha1$c93f0$1401c459269052c1eaf2a85ef380ac604be465f2',3,NULL,'2011-10-07 11:20:04','2011-10-07 11:20:04','sha1$d7193$59e9d102a575f3c57615b005cbf128c6b353ea05',1,'mi casa','1414','Lima','fkjsdflksdf',1,NULL,NULL,NULL,NULL,NULL,NULL),(22,'nombre','apellido','slovacus12@gmail.com','login','sha1$fb448$cdaf47e38b5d4149393481673296fb0a52c6f529',3,NULL,'2011-10-19 08:00:06','2011-10-19 08:00:06','sha1$7ec6d$5dd881ad8c366e052221be65bbbb604b04598946',0,'mi casa','1414','Lima','dsfsd',1,NULL,NULL,1,'1452','132156',NULL),(23,'luis alberto','mayta mamani','slovacus@gmail.com','kusanagui','sha1$6ba45$750b6a8e6dfe6e3e61aa6cdea09d632a968030c4',2,NULL,'2011-10-25 09:56:09','2011-10-25 09:56:09','sha1$c236a$053d8cf40783fb470b69a4e5069c0622e9e3aed7',1,'mi casa','1414','lima','lima',1,NULL,NULL,NULL,NULL,NULL,'2011-10-25 21:57:15');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
